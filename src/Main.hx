package;

import js.Lib;
import js.Browser;
import storage.LocalForage;

/**
 * Root class of the game. Entry point for the code.
 * @author Funtacles
 */
class Main 
{
	static function main() 
	{
		LocalForage.config({ driver: LocalForage.INDEXEDDB, name: "CoC", storeName: "HTMLCoC" });
		
		UI.init();
		
		Game.start();
	}
	
}