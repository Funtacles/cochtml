package ui;

/**
 * ...
 * @author Funtacles
 */
class SelectListOption 
{
	public var text:String;
	public var value:String;
	
	
	public function new(text:String, value:String) 
	{
		this.text = text;
		this.value = value;
	}
	
}