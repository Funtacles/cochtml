package;
import haxe.macro.Expr;

/**
 * ...
 * @author Funtacles
 */
class StringUtil 
{
	/**
	 * Capitalized the first letter in the string.
	 * @param	s	The string to capitalize.
	 * @return	The passed in string but with the first letter capitalized, or the original string if it contains no letters.
	 */
	public static function capitalize(s:String):String {
		for (i in 0...s.length){
			var l = s.charAt(i);
			if (isLetter(l)) {
				var lhs = s.substring(0, i);
				var rhs = s.substring(i + 1);
				var l = l.toUpperCase();
				return lhs + l + rhs;
			}
		}
		
		return s;
	}
	
	public static function isCapital(letter:String):Bool {
		var l = letter.charCodeAt(0);
		return l >= 65 && l <= 90;
	}
	
	/**
	 * Splits a string on multiple delimiters.
	 * @param	delims		The set of delimiters to split the string on.
	 * @param 	keepDelims	Keep the delimiters in the list
	 * @return	An array containing the parts of the string, broken up by the delimiters.
	 */
	public static function splitOn(delims:Array<String>, keepDelims:Bool = false):Array<String> {
		var parts = new Array<String>();
		
		for (delim in delims){
			var newParts = new Array<String>();
			for (part in parts){
				newParts.concat(part.split(delim));
			}
			parts = newParts;
		}
		
		return parts;
	}
	
	private static var smallNumbers:Array<String> = 
		["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
		"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
	public static function numberToWord(value:Int, capitalize:Bool = false):String {
		var text = "";
		
		if (value < 20)
			text = smallNumbers[value];
		
		return capitalize ? StringUtil.capitalize(text) : text;
	}
	
	public static function isLetter(s:String):Bool{
		var l = s.charCodeAt(0);
		return l >= 65 && l <= 90 || l >= 97 && l <= 122;
	}
	
	public static function randomWord(args:Array<String>):String {
		return args[Std.random(args.length)];
	}
	
	public static function isInt(s:String):Bool {
		if (isNullOrEmpty(s))
			return false;
		
		return ~/^([+-]?[1-9]\d*|0)$/.match(s);
	}

	public static function isFloat(s:String):Bool {
		if (isNullOrEmpty(s))
			return false;
		
		return ~/^([+-]?[0-9]\d*[.]?\d*|0)$/.match(s);
	}
	
	public static function isDiceRoll(s:String):Bool {
		if (isNullOrEmpty(s))
			return false;
		
		return ~/^([1-9]\d*[d]\d*)\s*([+-]\s*\d+)?$/.match(s);
	}
	
	public static function isNullOrEmpty(s:String):Bool {
		var undefined:Bool = untyped __js__('"undefined" === typeof s');
		return undefined || s == null || s.length == 0;
	}
}