package system;
import data.Character;
import data.Player;

/**
 * @author Funtacles
 */
typedef ParserContext =
{
	var player:Player;
	@:optional var other:Character;
}