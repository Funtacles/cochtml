package system;
import data.Player;
import data.body.enums.*;
import enums.Stat;

/**
 * ...
 * @author Funtacles
 */
class PlayerAppearance 
{
	public static function getAppearance(player:Player):String {
		var text:String = "";
		
		var race:String = "human";
		race = player.getRace();
		//Discuss race
		text += "<h2><u>Appearance</u></h2>";
		if (race != player.startingRace)	
			text += "You began your journey as a " + player.startingRace + ", but gave that up as you explored the dangers of this realm.";
			
		text += "You are a [tallness] tall [genderword] [race], with [bodytype].";
			
		//text += "  <b>You are currently " + (player.armorDescript() != "gear" ? "wearing your " + player.armorDescript() : "naked") + "" + " and using your " + player.weaponName + " as a weapon.</b>");
		//if (player.featheryHairPinEquipped()) {
			//// This may be relocated into a method later. Probably something, like player.headAccessory()
			//// Note, that earrings count as piercings, meaning, that head accessories and earrings are to be handled seperately
			//var hairPinText:String = "";
			//hairPinText += "  <b>You have a hair-pin with a single red feather plume";
			//if (player.hairLength > 0)
				//hairPinText += " in your " + player.hairDescript() + ".</b>";
			//else
				//hairPinText += " on your head.</b>";
			//text += hairPinText);
		//}
		//if (player.jewelryName != "nothing") 
			//text += "  <b>Girding one of your fingers is " + player.jewelryName + ".</b>");
			
		text += faceDetails(player);
		
		text += "  It has " + player.faceDesc() + ".";
			
		text += eyeDetails(player);
			
		text += hairAndEarDetails(player);
			
		//Beards!
		if (player.beardLength > 0) {
			text += "  You have a " + Appearance.beardDescription(player) + " ";
			if (player.beardStyle != BeardType.Goatee) {
				text += "covering your " + StringUtil.randomWord(["jaw, chin and cheeks"]);
			}
			else {
				text += "protruding from your chin";
			}
			text += ".";
		}
		
		//Tongue
		switch(player.tongueType){
			case TongueType.Snake:
				text += "  A snake-like tongue occasionally flits between your lips, tasting the air.";
			case TongueType.Demonic: 
				text += "  A slowly undulating tongue occasionally slips from between your lips.  It hangs nearly two feet long when you let the whole thing slide out, though you can retract it to appear normal.";
			case TongueType.Draconic: 
				text += "  Your mouth contains a thick, fleshy tongue that, if you so desire, can telescope to a distance of about four feet.  It has sufficient manual dexterity that you can use it almost like a third arm.";
			case TongueType.Echidna:
				text += "  A thin echidna tongue, at least a foot long, occasionally flits out from between your lips.";
			case TongueType.Lizard:
				text += "  Your mouth contains a thick, fleshy lizard tongue, bringing to mind the tongue of large predatory reptiles."
						  +" It can reach up to one foot, its forked tips tasting the air as they flick at the end of each movement.";
			default:
		}
			
		text += hornsDetails(player);
			
		text += "<br><br>You have a humanoid shape with the usual torso, arms, hands, and fingers.";
			
		text += wingAndArmDetails(player);
			
		text += hipsAndAssDescription(player);
			
		text += tailDetails(player);
			
		//LOWERBODY SPECIAL
		switch(player.lowerBody.type)
		{
			case LowerBodyType.Human: 
				text += "  [Legcount] normal human legs grow down from your waist, ending in normal human feet.";
			case LowerBodyType.Ferret:
				text += "  [Legcount] furry, digitigrade legs form below your [hips].  The fur is thinner on the feet, and your toes are tipped with claws.";
			case LowerBodyType.Hoofed: 
				text += "  Your [legcount] legs are muscled and jointed oddly, covered in fur, and end in a bestial hooves.";
			case LowerBodyType.Wolf: 
				text += "  You have [legcount] digitigrade legs that end in wolf paws.";
			case LowerBodyType.Dog: 
				text += "  [Legcount] digitigrade legs grow downwards from your waist, ending in dog-like hind-paws.";
			case LowerBodyType.Naga:
					text += "  Below your waist, in place of where your legs would be, your body transitions into a long snake like tail."
						  +" Your snake-like lower body is covered by " + player.lowerBody.skin.tone + " color scales,"
						  +" with " + player.lowerBody.nagaUnderBodyColor() + " color ventral scales along your underside.";
			case LowerBodyType.DemonHighHeels:
				text += "  Your [legcount] perfect lissome legs end in mostly human feet, apart from the horn protruding straight down from the heel that forces you to walk with a sexy, swaying gait.";
			case LowerBodyType.DemonClaws:
				text += "  Your [legcount] lithe legs are capped with flexible clawed feet.  Sharp black nails grow where once you had toe-nails, giving you fantastic grip.";
			case LowerBodyType.Bee:
				text += "  Your [legcount] legs are covered in a shimmering insectile carapace up to mid-thigh, looking more like a set of 'fuck-me-boots' than exoskeleton.  A bit of downy yellow and black fur fuzzes your upper thighs, just like a bee.";
			case LowerBodyType.Goo:
				text += "  In place of legs you have a shifting amorphous blob.  Thankfully it's quite easy to propel yourself around on.  The lowest portions of your [armor] float around inside you, bringing you no discomfort.";
			case LowerBodyType.Cat:
				text += "  [Legcount] digitigrade legs grow downwards from your waist, ending in soft, padded cat-paws.";
			case LowerBodyType.Lizard: 
				text += "  [Legcount] digitigrade legs grow down from your [hips], ending in clawed feet.  There are three long toes on the front, and a small hind-claw on the back.";
			case LowerBodyType.Salamander: 
				text += "  [Legcount] digitigrade legs covered in thick, leathery red scales up to the mid-thigh grow down from your [hips], ending in clawed feet.  There are three long toes on the front, and a small hind-claw on the back.";
			case LowerBodyType.Bunny: 
				text += "  Your [legcount] legs thicken below the waist as they turn into soft-furred rabbit-like legs.  You even have large bunny feet that make hopping around a little easier than walking.";
			case LowerBodyType.Harpy: 
				text += "  Your [legcount] legs are covered with [furcolor] plumage.  Thankfully the thick, powerful thighs are perfect for launching you into the air, and your feet remain mostly human, even if they are two-toed and tipped with talons.";
			case LowerBodyType.Kangaroo: 
				text += "  Your [legcount] furry legs have short thighs and long calves, with even longer feet ending in prominently-nailed toes.";
			case LowerBodyType.Spider:
				text += "  Your [legcount] legs are covered in a reflective black, insectile carapace up to your mid-thigh, looking more like a set of 'fuck-me-boots' than exoskeleton.";
			case LowerBodyType.Fox:
				text += "  Your [legcount] legs are crooked into high knees with hocks and long feet, like those of a fox; cute bulbous toes decorate the ends.";
			case LowerBodyType.Dragon:
				text += "  [Legcount] human-like legs grow down from your [hips], sheathed in scales and ending in clawed feet.  There are three long toes on the front, and a small hind-claw on the back.";
			case LowerBodyType.Racoon:
				text += "  Your [legcount] legs, though covered in fur, are humanlike.  Long feet on the ends bear equally long toes, and the pads on the bottoms are quite sensitive to the touch.";
			case LowerBodyType.ClovenHoofed:
				text += "  [Legcount] digitigrade legs form below your [hips], ending in cloven hooves.";
			case LowerBodyType.Imp: 
				text += " [Legcount] digitigrade legs form below your [hips], ending in clawed feet. Three extend out the front, and one smaller one is in the back to keep your balance.";
			case LowerBodyType.Cockatrice:
				text += " [Legcount] digitigrade legs grow down from your [hips], ending in clawed feet."
						  +" There are three long toes on the front, and a small hind-claw on the back."
						  +" A layer of " + (player.hasCockatriceSkin() ? player.skin.furColor : player.hair.color) + " feathers covers your legs from the"
						  +" hip to the knee, ending in a puffy cuff.";
			default:
		}
			
			
			//if (player.findPerk(PerkLib.Incorporeality) >= 0)
				//text += "  Of course, your " + player.legs() + " are partially transparent due to their ghostly nature."); // isn't goo transparent anyway?
			//
			//text += "\n");
			//if (player.hasStatusEffect(StatusEffects.GooStuffed))
			//
			//{
				//text += "\n<b>Your gravid-looking belly is absolutely stuffed full of goo. There's no way you can get pregnant like this, but at the same time, you look like some fat-bellied breeder.</b>\n");
			//}
			////Pregnancy Shiiiiiitz
			//if ((player.buttPregnancyType == PregnancyStore.PREGNANCY_FROG_GIRL) || (player.buttPregnancyType == PregnancyStore.PREGNANCY_SATYR) || player.isPregnant()) 
			//{
				//if (player.pregnancyType == PregnancyStore.PREGNANCY_OVIELIXIR_EGGS) 
				//{
					//text += "<b>");
					////Compute size
					//temp = player.statusEffectv3(StatusEffects.Eggs) + player.statusEffectv2(StatusEffects.Eggs) * 10;
					//if (player.pregnancyIncubation <= 50 && player.pregnancyIncubation > 20) 
					//{
						//text += "Your swollen pregnant belly is as large as a ");
						//if (temp < 10) 
							//text += "basketball.");
						//if (temp >= 10 && temp < 20) 
							//text += "watermelon.");
						//if (temp >= 20) 
							//text += "beach ball.");
					//}
					//if (player.pregnancyIncubation <= 20) 
					//{
						//text += "Your swollen pregnant belly is as large as a ");
						//if (temp < 10) 
							//text += "watermelon.");
						//if (temp >= 10 && temp < 20) 
							//text += "beach ball.");
						//if (temp >= 20) 
							//text += "large medicine ball.");
					//}
					//text += "</b>");
					//temp = 0;
				//}
				////Satur preggos - only shows if bigger than regular pregnancy or not pregnancy
				//else if (player.buttPregnancyType == PregnancyStore.PREGNANCY_SATYR && player.buttPregnancyIncubation > player.pregnancyIncubation) 
				//{
					//if (player.buttPregnancyIncubation < 125 && player.buttPregnancyIncubation >= 75) 
					//{
						//text += "<b>You've got the beginnings of a small pot-belly.</b>");
					//}
					//else if (player.buttPregnancyIncubation >= 50) 
					//{
						//text += "<b>The unmistakable bulge of pregnancy is visible in your tummy, yet it feels odd inside you - wrong somehow.</b>");	
					//}
					//else if (player.buttPregnancyIncubation >= 30) 
					//{
						//text += "<b>Your stomach is painfully distended by your pregnancy, making it difficult to walk normally.</b>");
					//}
					//else 
					//{ //Surely Benoit and Cotton deserve their place in this list
						//if (player.pregnancyType == PregnancyStore.PREGNANCY_IZMA || player.pregnancyType == PregnancyStore.PREGNANCY_MOUSE || player.pregnancyType == PregnancyStore.PREGNANCY_AMILY || player.pregnancyType == PregnancyStore.PREGNANCY_JOJO && (flags[kFLAGS.JOJO_STATUS] <= 0 || flags[kFLAGS.JOJO_BIMBO_STATE] >= 3) || player.pregnancyType == PregnancyStore.PREGNANCY_EMBER || player.pregnancyType == PregnancyStore.PREGNANCY_BENOIT || player.pregnancyType == PregnancyStore.PREGNANCY_COTTON || player.pregnancyType == PregnancyStore.PREGNANCY_URTA || player.pregnancyType == PregnancyStore.PREGNANCY_BEHEMOTH) 
							//text += "\n<b>Your belly protrudes unnaturally far forward, bulging with the spawn of one of this land's natives.</b>");
						//else if (player.pregnancyType != PregnancyStore.PREGNANCY_MARBLE) 
							//text += "\n<b>Your belly protrudes unnaturally far forward, bulging with the unclean spawn of some monster or beast.</b>");
						//else text += "\n<b>Your belly protrudes unnaturally far forward, bulging outwards with Marble's precious child.</b>");
					//}
				//}
				////URTA PREG
				//else if (player.pregnancyType == PregnancyStore.PREGNANCY_URTA) 
				//{
					//if (player.pregnancyIncubation <= 432 && player.pregnancyIncubation > 360)
					//{
						//text += "<b>Your belly is larger than it used to be.</b>\n");
					//}
					//if (player.pregnancyIncubation <= 360 && player.pregnancyIncubation > 288) 
					//{
						//text += "<b>Your belly is more noticeably distended.   You're pretty sure it's Urta's.</b>");
					//}
					//if (player.pregnancyIncubation <= 288 && player.pregnancyIncubation > 216) 
					//{
						//text += "<b>The unmistakable bulge of pregnancy is visible in your tummy, and the baby within is kicking nowadays.</b>");	
					//}
					//if (player.pregnancyIncubation <= 216 && player.pregnancyIncubation > 144) 
					//{
						//text += "<b>Your belly is large and very obviously pregnant to anyone who looks at you.  It's gotten heavy enough to be a pain to carry around all the time.</b>");		
					//}
					//if (player.pregnancyIncubation <= 144 && player.pregnancyIncubation > 72) 
					//{
						//text += "<b>It would be impossible to conceal your growing pregnancy from anyone who glanced your way.  It's large and round, frequently moving.</b>");
					//}
					//if (player.pregnancyIncubation <= 72 && player.pregnancyIncubation > 48) 
					//{
						//text += "<b>Your stomach is painfully distended by your pregnancy, making it difficult to walk normally.</b>");
					//}
					//if (player.pregnancyIncubation <= 48) 
					//{
						//text += "\n<b>Your belly protrudes unnaturally far forward, bulging with the spawn of one of this land's natives.</b>");
					//}
				//}
				//else if (player.buttPregnancyType == PregnancyStore.PREGNANCY_FROG_GIRL) 
				//{
					//if (player.buttPregnancyIncubation >= 8) 
						//text += "<b>Your stomach is so full of frog eggs that you look about to birth at any moment, your belly wobbling and shaking with every step you take, packed with frog ovum.</b>");
					//else text += "<b>You're stuffed so full with eggs that your belly looks obscenely distended, huge and weighted with the gargantuan eggs crowding your gut. They make your gait a waddle and your gravid tummy wobble obscenely.</b>");
				//}
				//else if (player.pregnancyType == PregnancyStore.PREGNANCY_FAERIE) { //Belly size remains constant throughout the pregnancy
					//text += "<b>Your belly remains swollen like a watermelon. ");
					//if (player.pregnancyIncubation <= 100)
						//text += "It's full of liquid, though unlike a normal pregnancy the passenger you’re carrying is tiny.</b>");
					//else if (player.pregnancyIncubation <= 140)
						//text += "It feels like it’s full of thick syrup or jelly.</b>");
					//else text += "It still feels like there’s a solid ball inside your womb.</b>");    
				//}
				//else 
				//{
					//if (player.pregnancyIncubation <= 336 && player.pregnancyIncubation > 280)
					//{
						//text += "<b>Your belly is larger than it used to be.</b>");
					//}
					//if (player.pregnancyIncubation <= 280 && player.pregnancyIncubation > 216) 
					//{
						//text += "<b>Your belly is more noticeably distended.   You are probably pregnant.</b>");
					//}
					//if (player.pregnancyIncubation <= 216 && player.pregnancyIncubation > 180) 
					//{
						//text += "<b>The unmistakable bulge of pregnancy is visible in your tummy.</b>");	
					//}
					//if (player.pregnancyIncubation <= 180 && player.pregnancyIncubation > 120) 
					//{
						//text += "<b>Your belly is very obviously pregnant to anyone who looks at you.</b>");		
					//}
					//if (player.pregnancyIncubation <= 120 && player.pregnancyIncubation > 72) 
					//{
						//text += "<b>It would be impossible to conceal your growing pregnancy from anyone who glanced your way.</b>");
					//}
					//if (player.pregnancyIncubation <= 72 && player.pregnancyIncubation > 48) 
					//{
						//text += "<b>Your stomach is painfully distended by your pregnancy, making it difficult to walk normally.</b>");
					//}
					//if (player.pregnancyIncubation <= 48) 
					//{ //Surely Benoit and Cotton deserve their place in this list
						//if (player.pregnancyType == PregnancyStore.PREGNANCY_IZMA || player.pregnancyType == PregnancyStore.PREGNANCY_MOUSE || player.pregnancyType == PregnancyStore.PREGNANCY_AMILY || (player.pregnancyType == PregnancyStore.PREGNANCY_JOJO && flags[kFLAGS.JOJO_STATUS] <= 0) || player.pregnancyType == PregnancyStore.PREGNANCY_EMBER || player.pregnancyType == PregnancyStore.PREGNANCY_BENOIT || player.pregnancyType == PregnancyStore.PREGNANCY_COTTON || player.pregnancyType == PregnancyStore.PREGNANCY_URTA || player.pregnancyType == PregnancyStore.PREGNANCY_MINERVA || player.pregnancyType == PregnancyStore.PREGNANCY_BEHEMOTH) 
							//text += "\n<b>Your belly protrudes unnaturally far forward, bulging with the spawn of one of this land's natives.</b>");
						//else if (player.pregnancyType != PregnancyStore.PREGNANCY_MARBLE) 
							//text += "\n<b>Your belly protrudes unnaturally far forward, bulging with the unclean spawn of some monster or beast.</b>");
						//else text += "\n<b>Your belly protrudes unnaturally far forward, bulging outwards with Marble's precious child.</b>");
					//}
				//}
				//text += "\n");
			//}
			//if (player.hasStatusEffect(StatusEffects.ParasiteEel)){
				//if (player.statusEffectv1(StatusEffects.ParasiteEel) == 1){
					//text += "A small bulge appears on your belly sometimes, signalling the presence of the eel parasite.");
				//}
				//if (player.statusEffectv1(StatusEffects.ParasiteEel) == 2){
					//text += "Bulges appears on your belly sometimes, signalling the presence of the eel parasites.");
				//}
				//if (player.statusEffectv1(StatusEffects.ParasiteEel) > 2){
					//text += "Several bulges appear on your belly, sliding across your abdomen, signalling you're infested with eel parasites.");
				//}
				//text += " Every few moments, one of the creatures slides out of your [vagina], twists aimlessly, and hides itself again.\n");
			//}
			text += "<br><br>";
			if (player.gillType == GillType.Anemone) 
				text += "A pair of feathery gills are growing out just below your neck, spreading out horizontally and draping down your chest.  They allow you to stay in the water for quite a long time.  ";
			
			text += breastDetails(player);
			
			//Crotchial stuff - mention snake
			if (player.lowerBody.type == LowerBodyType.Naga && player.getGender() != Gender.Neuter) 
			{
				text += "<br><br>Your sex";
				if (player.getGender() == Gender.Herm || player.cocks.length > 1) 
					text += "es are ";
				else text += " is ";
					text += "concealed within a cavity in your tail when not in use, though when the need arises, you can part your concealing slit and reveal your true self.";
			}

			text += penisDetails(player);
			
			text += ballsDetails(player);
			
			text += vaginaDetails(player);
			
			//Genderless
			if (player.cocks.length == 0 && player.vaginas.length == 0) 
				text += "<br><br>You have a curious lack of any sexual endowments.<br><br>";
			
			
			//BUNGHOLIO
			if (player.ass != null) 
			{
				text += "<br><br>You have one [asshole], placed between your butt-cheeks where it belongs.";
			}
			
			////Piercings!
			//if (player.eyebrowPierced > 0) 
				//text += "\nA solitary " + player.eyebrowPShort + " adorns your eyebrow, looking very stylish.");
			//if (player.earsPierced > 0) 
				//text += "\nYour ears are pierced with " + player.earsPShort + ".");
			//if (player.nosePierced > 0) 
				//text += "\nA " + player.nosePShort + " dangles from your nose.");
			//if (player.lipPierced > 0) 
				//text += "\nShining on your lip, a " + player.lipPShort + " is plainly visible.");
			//if (player.tonguePierced > 0) 
				//text += "\nThough not visible, you can plainly feel your " + player.tonguePShort + " secured in your tongue.");
			//if (player.nipplesPierced == 3) 
				//text += "\nYour " + player.nippleDescript(0) + "s ache and tingle with every step, as your heavy " + player.nipplesPShort + " swings back and forth.");
			//else if (player.nipplesPierced > 0) 
				//text += "\nYour " + player.nippleDescript(0) + "s are pierced with " + player.nipplesPShort + ".");
			//if (player.cocks.length > 0) 
			//{
				//if (player.cocks[0].pierced > 0) 
				//{
					//text += "\nLooking positively perverse, a " + player.cocks[0].pShortDesc + " adorns your " + player.cockDescript(0) + ".");
				//}
			//}
			//if (flags[kFLAGS.CERAPH_BELLYBUTTON_PIERCING] == 1) 
				//text += "\nA magical, ruby-studded bar pierces your belly button, allowing you to summon Ceraph on a whim.");
			//if (player.hasVagina()) 
			//{
				//if (player.vaginas[0].labiaPierced > 0) 
					//text += "\nYour " + player.vaginaDescript(0) + " glitters with the " + player.vaginas[0].labiaPShort + " hanging from your lips.");
				//if (player.vaginas[0].clitPierced > 0) 
					//text += "<br>Impossible to ignore, your " + player.clitDescript() + " glitters with its " + player.vaginas[0].clitPShort + ".");
			//}
			
			//MONEY!
			if (player.gems == 0) 
				text += "<br><br><b>Your money-purse is devoid of any currency.</b>";
			else if (player.gems == 1) 
				text += "<br><br><b>You have " + player.gems + " shining gem, collected in your travels.</b>";
			else if (player.gems > 1) 
				text += "<br><br><b>You have " + Util.numberWithCommas(player.gems) + " shining gems, collected in your travels.</b>";
			else {
				text += "<br><br><b>Something is wrong with your gems!</b>";
			}
			
		return text;
	}

	private static function faceDetails(player:Player):String {
		var text = "";
		if ([FaceType.Human, FaceType.SharkTeeth, FaceType.Bunny, FaceType.SpiderFangs, FaceType.FerretMask].indexOf(player.faceType) != -1) 
		{
			if (player.skin.type == SkinType.Normal || player.skin.type == SkinType.Goo) 
				text += "  Your face is human in shape and structure, with [skin].";
			if (player.skin.isFurry()) 
				text += "  Under your [skinFurScales] you have a human-shaped head with [skin].";
			if (player.skin.isScaly()) 
				text += "  Your face is fairly human in shape, but is covered in [skin].";
			if (player.faceType == FaceType.SharkTeeth) 
				text += "  A set of razor-sharp, retractable shark-teeth fill your mouth and gives your visage a slightly angular appearance.";
			else if (player.faceType == FaceType.Bunny) 
				text += "  The constant twitches of your nose and the length of your incisors gives your visage a hint of bunny-like cuteness.";
			else if (player.faceType == FaceType.SpiderFangs) 
				text += "  A set of retractable, needle-like fangs sit in place of your canines and are ready to dispense their venom.";
			else if (player.faceType == FaceType.FerretMask)
				text += "  The [skinFurScales] around your eyes is significantly darker than the rest of your face, giving you a cute little ferret mask.";
		}
		else if (player.faceType == FaceType.Ferret)
		{
			if (player.skin.type == SkinType.Normal) text += "  Your face is an adorable cross between human and ferret features, complete with a wet nose and whiskers.  The only oddity is your lack of fur, leaving only [skin] visible on your ferret-like face.";
			else text += "  Your face is coated in [furcolor] fur with [skin] underneath, an adorable cross between human and ferret features.  It is complete with a wet nose and whiskers.";
		}
		else if (player.faceType == FaceType.RacoonMask) 
		{
			if (player.skin.type == SkinType.Normal || player.skin.type == SkinType.Goo) 
			{
				text += "  Your face is human in shape and structure, with [skin]";
				if ((player.skin.tone == "ebony" || player.skin.tone == "black")) 
					text += ", though with your dusky hue, the black raccoon mask you sport isn't properly visible.";
				else text += ", though it is decorated with a sly-looking raccoon mask over your eyes.";
			}
			else 
			{
				if (((player.skin.furColor == "black" || player.skin.furColor == "midnight") && (player.skin.isFurry() || player.skin.isScaly()))) 
					text += "  Under your [skincovering] hides a black raccoon mask, barely visible due to your inky hue, and";
				else text += "  Your [skincovering] are decorated with a sly-looking raccoon mask, and under them";
				text += " you have a human-shaped head with [skin].";
			}
		} 
		else if (player.faceType == FaceType.Racoon) 
		{
			text += "  You have a triangular raccoon face, replete with sensitive whiskers and a little black nose; a mask shades the space around your eyes, set apart from your " + player.skin.coveringDesc() + " by a band of white.";
			if (player.skin.type == SkinType.Normal) 
				text += "  It looks a bit strange with only the skin and no fur.";
			else if (player.skin.isScaly()) 
				text += "  The presence of said scales gives your visage an eerie look, more reptile than mammal.";
		}
		else if (player.faceType == FaceType.Fox) 
		{
			text += "  You have a tapered, shrewd-looking vulpine face with a speckling of downward-curved whiskers just behind the nose.";
			if (player.skin.type == SkinType.Normal) 
				text += "  Oddly enough, there's no fur on your animalistic muzzle, just [skin]."; 
			else if (player.skin.isFurry())
					text += "  A coat of [skin] decorates your muzzle.";
			else if (player.skin.isScaly()) 
				text += "  Strangely, [skin] adorn every inch of your animalistic visage.";
		}
		else if (player.faceType == FaceType.BuckTeeth) 
		{
			text += "  Your face is generally human in shape and structure, with [skin] and mousey buckteeth.";
		}
		else if (player.faceType == FaceType.Mouse) 
		{
			text += "  You have a snubby, tapered mouse's face, with whiskers, a little pink nose, and [skin].  Two large incisors complete it.";
		}
		else if (player.faceType == FaceType.SnakeFangs) 
		{
			if (player.skin.type == SkinType.Normal || player.skin.type == SkinType.Goo) 
				text += "  You have a fairly normal face, with [skin].  The only oddity is your pair of dripping fangs which often hang over your lower lip.";
			else if(player.skin.isFurry())
				text += "  Under your [skincovering] you have a human-shaped head with [skin].  In addition, a pair of fangs hang over your lower lip, dripping with venom.";
			else
				text += "  Your face is fairly human in shape, but is covered in [skin].  In addition, a pair of fangs hang over your lower lip, dripping with venom.";
		}
		else if (player.faceType == FaceType.Horse) 
		{
			if (player.skin.type == SkinType.Normal || player.skin.type == SkinType.Goo) 
				text += "  Your face is equine in shape and structure.  The odd visage is hairless and covered with [skin].";
			if (player.skin.isFurry()) 
				text += "  Your face is almost entirely equine in appearance, even having [skincovering].  Underneath the fur, you believe you have [skin].";
			if (player.skin.isScaly()) 
				text += "  You have the face and head structure of a horse, overlaid with glittering [skin].";
		}
		//wolf-face
		else if (player.faceType == FaceType.Wolf) 
		{
			text += "  You have an angular wolf's face complete with a muzzle and black nose";
			if (!player.skin.isFurry())
				text += ", though devoid of any fur.";
			else
				text += ".";
		}
		else if (player.faceType == FaceType.Dog) 
		{
			if (player.skin.type == SkinType.Normal || player.skin.type == SkinType.Goo) 
				text += "  You have a dog-like face, complete with a wet nose.  The odd visage is hairless and covered with [skincovering].";
			if (player.skin.isFurry()) 
				text += "  You have a dog's face, complete with wet nose and panting tongue.  You've got [skincovering], hiding your [skin] underneath your furry visage.";
			if (player.skin.isScaly()) 
				text += "  You have the facial structure of a dog, wet nose and all, but overlaid with glittering [skinconvering].";
		}
			////cat-face
			//if (player.faceType == FACE_CAT) 
			//{
				//if (player.hasPlainSkin() || player.hasGooSkin()) 
					//text += "  You have a cat-like face, complete with a cute, moist nose and whiskers.  The " + player.skinDescript() + " that is revealed by your lack of fur looks quite unusual on so feline a face.");
				//if (player.hasFur())
					//if (player.hasDifferentUnderBody())
						//text += "  You have a cat-like face, complete with moist nose and whiskers.  You have [skinFurScales] on your upper jaw and head, while your lower jaw is decorated by [underBody.skinFurScales], hiding your [skin.noadj] underneath.");
					//else
						//text += "  You have a cat-like face, complete with moist nose and whiskers.  Your " + player.skinDesc + " is " + player.furColor + ", hiding your [skin.noadj] underneath.");
				//if (player.hasScales()) 
					//text += "  Your facial structure blends humanoid features with those of a cat.  A moist nose and whiskers are included, but overlaid with glittering " + player.skinFurScales() + ".");
				//if (player.eyeType != EYES_BLACK_EYES_SAND_TRAP)
				//{
					//text += "  Of course, no feline face would be complete without vertically slit eyes");
					//text += !player.hasReptileEyes() ? "." : ", although they come with a second set of eyelids, which is somewhat unusual for a cats face.");
				//}
			//}
			////Minotaaaauuuur-face
			//if (player.faceType == FACE_COW_MINOTAUR) 
			//{
				//if (player.hasPlainSkin() || player.hasGooSkin()) 
					//text += "  You have a face resembling that of a minotaur, with cow-like features, particularly a squared off wet nose.  Despite your lack of fur elsewhere, your visage does have a short layer of " + player.furColor + " fuzz.");
				//if (player.hasFur()) 
					//text += "  You have a face resembling that of a minotaur, with cow-like features, particularly a squared off wet nose.  Your " + player.skinFurScales() + " thickens noticeably on your head, looking shaggy and more than a little monstrous once laid over your visage.");
				//if (player.hasScales()) 
					//text += "  Your face resembles a minotaur's, though strangely it is covered in shimmering scales, right up to the flat cow-like nose that protrudes from your face.");
			//}
			////Lizard-face
			//if (player.faceType == FACE_LIZARD) 
			//{
				//if (player.hasPlainSkin() || player.hasGooSkin()) 
					//text += "  You have a face resembling that of a lizard, and with your toothy maw, you have quite a fearsome visage.  The reptilian visage does look a little odd with just " + player.skinDescript() + ".");
				//if (player.hasFur()) 
					//text += "  You have a face resembling that of a lizard.  Between the toothy maw, pointed snout, and the layer of " + player.skinFurScales() + " covering your face, you have quite the fearsome visage.");
				//if (player.hasScales()) {
					//text += "  Your face is that of a lizard, complete with a toothy maw and pointed snout.");
					//if (!player.hasReptileUnderBody())
						//text += "  Reflective [skinFurScales] complete the look, making you look quite fearsome.");
					//else
						//text += "  Reflective [skinFurScales] on your upper jaw and head and [underBody.skinFurScales] on your lower jaw complete the look, making you look quite fearsome.");
				//}
			//}
			//if (player.faceType == FACE_DRAGON) 
			//{
				//text += "  Your face is a narrow, reptilian muzzle.  It looks like a predatory lizard's, at first glance, but with an unusual array of spikes along the under-jaw.  It gives you a regal but fierce visage.  Opening your mouth reveals several rows of dagger-like sharp teeth.  The fearsome visage is decorated by [skinFurScales]");
				//text += player.hasReptileUnderBody() ? " on your upper jaw and head and [underBody.skinFurScales] on your lower jaw." : ".");
			//}
			//if (player.faceType == FACE_KANGAROO) 
			//{
				//text += "  Your face is ");
				//if (player.hasPlainSkin()) 
					//text += "bald");
				//else text += "covered with " + player.skinFurScales());
				//text += " and shaped like that of a kangaroo, somewhat rabbit-like except for the extreme length of your odd visage.");
			//}
			////<mod>
			//if (player.faceType == FACE_PIG)
			//{
				//text += "  Your face is like that of a pig, with " + player.skinTone + " skin, complete with a snout that is always wiggling.");
			//}
			//if (player.faceType == FACE_BOAR)
			//{
				//text += "  Your face is like that of a boar, ");
				//if (player.hasFur()) 
					//text += "with " + player.skinTone + " skin underneath your " + player.furColor + " fur"); 
				//text += ", complete with tusks and a snout that is always wiggling.");
			//}
			//if (player.faceType == FACE_RHINO)
			//{
				//text += "  Your face is like that of a rhino");
				//if (player.hasPlainSkin())
					//text += ", with " + player.skinDescript() + ", complete with a long muzzle and a horn on your nose.");
				//else
					//text += " with a long muzzle and a horn on your nose.  Oddly, your face is also covered in " + player.skinFurScales() + ".");
			//}
			//if (player.faceType == FACE_ECHIDNA)
			//{
				//text += "  Your odd visage consists of a long, thin echidna snout.");
				//if (player.hasPlainSkin())
					//text += "  The " + player.skinDescript() + " that is revealed by your lack of fur looks quite unusual.");
				//else if (player.hasFur())
					//text += "  It's covered in " + player.skinFurScales() + ".");
				//else if (player.hasScales())
					//text += "  It's covered in " + player.skinFurScales() + ", making your face even more unusual.");
			//}
			//if (player.faceType == FACE_DEER)
			//{
				//text += "  Your face is like that of a deer, with a nose at the end of your muzzle.");
				//if (player.hasPlainSkin())
					//text += "  The " + player.skinDescript() + " that is revealed by your lack of fur looks quite unusual.");
				//else if (player.hasFur())
					//if (player.hasDifferentUnderBody())
						//text += "  It's covered in [skinFurScales] on your upper jaw and head and [underBody.skinFurScales]"
						          //+" on your lower jaw that covers your [skin.noadj] underneath.");
					//else
						//text += "  It's covered in [skinFurScales] that covers your [skin.noadj] underneath.");
				//else if (player.hasScales())
					//text += "  It's covered in " + player.skinFurScales() + ", making your face looks more unusual.");
			//}
			//if (player.faceType == FACE_COCKATRICE)
			//{
				//if (player.underBody.skin.type == SKIN_TYPE_FEATHERED)
					//text += "  You have a cockatrice’s face, complete with " + player.furColor + " feathered skin and a muzzle like beak.");
				//else
					//text += "  You have a cockatrice’s face, complete with [skinFurScales] and a muzzle like beak.");
			//}
		
		return text;
	}
	
	private static function eyeDetails(player:Player):String{
		var text = "";
		
		if (player.eyeType == EyeType.Spider) 
			text += " Your eyes are normal, save for their black irises, making them ominous and hypnotizing.";
		else if (player.eyeType == EyeType.SandtrapBlack) 
			text += "  Your eyes are solid spheres of inky, alien darkness.";
		else if (player.eyeType == EyeType.Wolf) 
			text += "  Your amber eyes are circled by darkness to help keep the sun from obscuring your view and have a second eyelid to keep them wet. You're rather near-sighted, but your peripherals are great!";
		else if (player.eyeType == EyeType.Cockatrice)
			text += "  You have electric blue eyes spiderwebbed with lightning like streaks that signal their power and slit reptilian pupils."
					  +" When excited your pupils dilate into wide circles.";
		else if (player.faceType != FaceType.Cat && player.hasReptileEyes())
		{
			text += "  Your eyes are";
			switch (player.eyeType)
			{
				case EyeType.Dragon: text += " prideful, fierce dragon eyes with vertically slitted pupils and burning orange irises. They glitter even in the darkness and they";
				case EyeType.Lizard: text += " those of a lizard with vertically slitted pupils and green-yellowish irises. They";
				case EyeType.Basilisk: text += " basilisk eyes, grey reptilian pools with vertically slitted pupils. They";
				default:
			}
			text += " come with the typical second set of eyelids, allowing you to blink twice as much as others.";
			if (player.eyeType == EyeType.Basilisk)
				text += " Others seem compelled to look into them.";
		}
		
		if (player.eyeCount > 2) 
			text += " In addition to your primary two eyes, you have " + player.eyeCount + " eyes positioned on your forehead.";
			
		return text;
	}
	
	private static function hairAndEarDetails(player:Player):String {
		var text = "";
		
		if (player.hair.length == 0) 
		{
			if (player.skin.type == SkinType.Fur) 
				text += "  You have no hair, only a thin layer of fur atop of your head.  ";
			else if (player.skin.type == SkinType.Wool) 
				text += "  You have no hair, only a thin layer of wool atop of your head.  ";
			
			switch(player.earType){
				case EarType.Horse: 
					text += "  A pair of horse-like ears rise up from the top of your head.";
				case EarType.Sheep: 
					text += "  Two tear drop shaped ears peek out from the sides of your head, their fluffy texture and lazy positioning giving you a cute and sleepy air.";
				case EarType.Ferret: 
					text += "  A pair of small, rounded ferret ears sit on top of your head.";
				case EarType.Dog: 
					text += "  A pair of dog ears protrude from your skull, flopping down adorably.";
				case EarType.Cow:
					text += "  A pair of round, floppy cow ears protrude from the sides of your skull.";
				case EarType.Elfin:
					text += "  A pair of large pointy ears stick out from your skull.";
				case EarType.Cat:
					text += "  A pair of cute, fuzzy cat ears have sprouted from the top of your head.";
				case EarType.Pig:
					text += "  A pair of pointy, floppy pig ears have sprouted from the top of your head.";
				case EarType.Lizard:
					text += "  A pair of rounded protrusions with small holes on the sides of your head serve as your ears.";
				case EarType.Bunny:
					text += "  A pair of floppy rabbit ears stick up from the top of your head, flopping around as you walk.";
				case EarType.Fox:
					text += "  A pair of large, adept fox ears sit high on your head, always listening.";
				case EarType.Dragon:
					text += "  A pair of rounded protrusions with small holes on the sides of your head serve as your ears.  Bony fins sprout behind them.";
				case EarType.Racoon:
					text += "  A pair of vaguely egg-shaped, furry raccoon ears adorns your head.";
				case EarType.Mouse:
					text += "  A pair of large, dish-shaped mouse ears tops your head.";
				case EarType.Rhino:
					text += "  A pair of open tubular rhino ears protrude from your head.";
				case EarType.Echidna:
					text += "  A pair of small rounded openings appear on your head that are your ears.";
				case EarType.Deer:
					text += "  A pair of deer-like ears rise up from the top of your head.";
				case EarType.Wolf:
					text += "  A pair of wolf ears stick out from your head, attuned to every sound around you.";
				default:
			}
			
			if (player.antennae == AntennaeType.Bee) 
				text += "  Floppy antennae also appear on your skull, bouncing and swaying in the breeze.";
			else if (player.antennae == AntennaeType.Cockatrice)
				text += "  Two long antennae like feathers sit on your hairline, curling over the shape of your head.";
		}
		else 
		{
			switch(player.earType){
				case EarType.Human:
					text += "  Your " + Appearance.hairDescription(player) + " looks good on you, accentuating your features well.";
				case EarType.Horse: 
					text += "  The " + Appearance.hairDescription(player) + " on your head parts around a pair of very horse-like ears that grow up from your head.";
				case EarType.Sheep: 
					text += "  Two tear drop shaped ears part your [hair] and peek out from the sides of your head, their fluffy texture and lazy positioning giving you a cute and sleepy air.";
				case EarType.Ferret: 
					text += "  A pair of small, rounded ferret ears burst through the top of your " + Appearance.hairDescription(player) + ".";
				case EarType.Dog: 
					text += "  The " + Appearance.hairDescription(player) + " on your head is overlapped by a pair of pointed dog ears.";
				case EarType.Cow:
					text += "  The " + Appearance.hairDescription(player) + " on your head is parted by a pair of rounded cow ears that stick out sideways.";
				case EarType.Elfin:
					text += "  The " + Appearance.hairDescription(player) + " on your head is parted by a pair of cute pointed ears, bigger than your old human ones.";
				case EarType.Cat:
					text += "  The " + Appearance.hairDescription(player) + " on your head is parted by a pair of cute, fuzzy cat ears, sprouting from atop your head and pivoting towards any sudden noises.";
				case EarType.Pig:
					text += "  The " + Appearance.hairDescription(player) + " on your head is parted by a pair of pointy, floppy pig ears. They often flick about when you’re not thinking about it.";
				case EarType.Lizard:
					text += "  The " + Appearance.hairDescription(player) + " atop your head makes it nigh-impossible to notice the two small rounded openings that are your ears.";
				case EarType.Bunny:
					text += "  A pair of floppy rabbit ears stick up out of your " + Appearance.hairDescription(player) + ", bouncing around as you walk.";
				case EarType.Fox:
					text += "  The " + Appearance.hairDescription(player) + " atop your head is parted by a pair of large, adept fox ears that always seem to be listening.";
				case EarType.Dragon:
					text += "  The " + Appearance.hairDescription(player) + " atop your head is parted by a pair of rounded protrusions with small holes on the sides of your head serve as your ears.  Bony fins sprout behind them.";
				case EarType.Racoon:
					text += "  The " + Appearance.hairDescription(player) + " on your head parts around a pair of egg-shaped, furry raccoon ears.";
				case EarType.Mouse:
					text += "  The " + Appearance.hairDescription(player) + " atop your head is funneled between and around a pair of large, dish-shaped mouse ears that stick up prominently.";
				case EarType.Rhino:
					text += "  The " + Appearance.hairDescription(player) + " on your head is parted by a pair of tubular rhino ears.";
				case EarType.Echidna:
					text += "  Your " + Appearance.hairDescription(player) + " makes it near-impossible to see the small, rounded openings that are your ears.";
				case EarType.Deer:
					text += "  The " + Appearance.hairDescription(player) + " on your head parts around a pair of deer-like ears that grow up from your head.";
				case EarType.Wolf:
					text += "  A pair of wolf ears stick out from your head, parting your [hair] and remaining alert to your surroundings.";
				case EarType.Kangaroo:
					text += "  The " + Appearance.hairDescription(player) + " atop your head is parted by a pair of long, furred kangaroo ears that stick out at an angle.";
				default:
			}
			
			if (player.gillType == GillType.Fish) 
			{
				text += "  A set of fish like gills reside on your neck, several small slits that can close flat against your skin."
						   +" They allow you to stay in the water for quite a long time.";
			}
			// GILLS_ANEMONE are handled below
			if (player.antennae == AntennaeType.Bee) 
			{
				if (player.earType == EarType.Bunny) 
					text += "  Limp antennae also grow from just behind your hairline, waving and swaying in the breeze with your ears.";
				else text += "  Floppy antennae also grow from just behind your hairline, bouncing and swaying in the breeze.";
			}
			else if (player.antennae == AntennaeType.Cockatrice)
			{
				text += "  Two long antennae like feathers sit on your hairline, curling over the shape of your head.";
			}
		}
		
		if (player.earType == EarType.Cockatrice) {
			text += "  From the sides of your head protrude a quartet of feathers, the longest being vertical while the 3 shorter ones come"
					  +" out at a 1 o'clock, 2 o'clock and 3 o'clock angle. Behind them hides the avian hole that is your ear.";
		}
		
		return text;
	}
	
	private static function hornsDetails(player:Player):String {
		var text = "";
		
		switch(player.hornType)
		{
			case HornType.Imp:
				text += " A set of pointed imp horns rest atop your head.";
			case HornType.Demon:
				if (player.horns == 2) 
					text += "  A small pair of pointed horns has broken through the [skincovering] on your forehead, proclaiming some demonic taint to any who see them.";
				if (player.horns == 4) 
					text += "  A quartet of prominent horns has broken through your [skincovering].  The back pair are longer, and curve back along your head.  The front pair protrude forward demonically.";
				if (player.horns == 6) 
					text += "  Six horns have sprouted through your [skincovering], the back two pairs curve backwards over your head and down towards your neck, while the front two horns stand almost "+Measurement.numInchesOrCentimetres(8)+" long upwards and a little forward.";
				if (player.horns >= 8) 
					text += "  A large number of thick demonic horns sprout through your [skincovering], each pair sprouting behind the ones before.  The front jut forwards nearly " + Measurement.numInchesOrCentimetres(10) + " while the rest curve back over your head, some of the points ending just below your ears.  You estimate you have a total of " + StringUtil.numberToWord(player.horns) + " horns.";	
			case HornType.Bovine:
				if (player.horns < 3) 
					text += "  Two tiny horn-like nubs protrude from your forehead, resembling the horns of the young livestock kept by your village.";
				if (player.horns >= 3 && player.horns < 6) 
					text += "  Two moderately sized horns grow from your forehead, similar in size to those on a young bovine.";
				if (player.horns >= 6 && player.horns < 12) 
					text += "  Two large horns sprout from your forehead, curving forwards like those of a bull.";
				if (player.horns >= 12 && player.horns < 20) 
					text += "  Two very large and dangerous looking horns sprout from your head, curving forward and over a foot long.  They have dangerous looking points.";
				if (player.horns >= 20) 
					text += "  Two huge horns erupt from your forehead, curving outward at first, then forwards.  The weight of them is heavy, and they end in dangerous looking points.";
			case HornType.Draconic2x:
				text += "  A pair of " + Measurement.numInchesOrCentimetres(player.horns) + " horns grow from the sides of your head, sweeping backwards and adding to your imposing visage.";
			case HornType.Draconic4x:
				text += "  Two pairs of horns, roughly a foot long, sprout from the sides of your head.  They sweep back and give you a fearsome look, almost like the dragons from your village's legends.";
			case HornType.Deer:
				text += "  Two antlers, forking into " + StringUtil.numberToWord(player.horns) + " points, have sprouted from the top of your head, forming a spiky, regal crown of bone.";
			case HornType.Sheep:
				if (player.horns == 1) 
					text += "  A pair of small sheep horns sit atop your head. They curl out and upwards in a slight crescent shape.";
				else
					text += "  A pair of large sheep horns sit atop your head. They curl out and upwards in a crescent shape.";
			case HornType.Ram:
				if (player.horns == 1) 
					text += "  A set of " + StringUtil.numberToWord(player.horns) + " inch ram horns sit atop your head, curling around in a tight spiral at the side of your head before coming to an upwards hook around your ears.";
				else
					text += "  A set of large " + StringUtil.numberToWord(player.horns) + " inch ram horns sit atop your head, curling around in a tight spiral at the side of your head before coming to an upwards hook around your ears.";
			case HornType.Goat:
				if (player.horns == 1) 
					text += "  A pair of stubby goat horns sprout from the sides of your head.";
				else
					text += "  A pair of tall-standing goat horns sprout from the sides of your head.  They are curved and patterned with ridges.";
			case HornType.Rhino:
				if (player.horns >= 2) {
					if (player.faceType == FaceType.Rhino)
						text += "  A second horn sprouts from your forehead just above the horn on your nose.";
					else
						text += "  A single horn sprouts from your forehead.  It is conical and resembles a rhino's horn.";
					text += "  You estimate it to be about "+Measurement.numInchesOrCentimetres(7)+" long.";
				}
				else {
					text += "  A single horn sprouts from your forehead.  It is conical and resembles a rhino's horn.  You estimate it to be about "+Measurement.numInchesOrCentimetres(6)+" long.";
				}
			case HornType.Unicorn:
				text += "  A single sharp nub of a horn sprouts from the center of your forehead.";
				if (player.horns < 12)
					text += "  You estimate it to be about "+Measurement.numInchesOrCentimetres(6)+" long.";
				else
					text += "  It has developed its own cute little spiral. You estimate it to be about " + Measurement.numInchesOrCentimetres(12) + " long, " + Measurement.numInchesOrCentimetres(2) + " thick and very sturdy. A very useful natural weapon.";
				default:
		}
		return text;
	}
	
	private static function breastDetails(player:Player):String {
		var text = "";
		if (player.breastRows.length == 1) 
		{
			var manlyChest:Bool = player.breastRows[0].breastRating == BreastSize.Flat && player.getGender() == Gender.Male;
			
			if (manlyChest)
				text += "You have a flat, manly chest with " + StringUtil.numberToWord(player.breastRows[0].nipplesPerBreast * 2);
			else {
				text += "You have [breastcount|0] [breasts|0], each supporting ";
				text +=  player.breastRows[0].nipplesPerBreast == 1 ? "a" : StringUtil.numberToWord(player.breastRows[0].nipplesPerBreast); //Number of nipples.
			}
			
			text += " " + Measurement.shortSuffix(player.nippleLength, 2) + " ";		  // Length of nipples
			text += Appearance.nippleDescription(player, 0) + (player.breastRows[0].nipplesPerBreast == 1 && !manlyChest ? "." : "s."); //Nipple description and plural
			
			if (player.breastRows[0].milkFullness > 75) 
				text += "  Your [breasts|0] are painful and sensitive from being so stuffed with milk.  You should release the pressure soon.";
			if (player.breastRows[0].breastRating.getIndex() >= 1) 
				text += "  You could easily fill a [breastcup|0] bra.";
		}
		//many rows
		else 
		{
			text += "You have [breastrows] rows of breasts, the topmost pair starting at your chest.<br>";
			for (i in 0...player.breastRows.length) 
			{
				if (i == 0) 
					text += "--Your uppermost rack houses ";
				if (i == 1) 
					text += "<br>--The second row holds ";
				if (i == 2) 
					text += "<br>--Your third row of breasts contains ";
				if (i == 3) 
					text += "<br>--Your fourth set of tits cradles ";
				if (i == 4) 
					text += "<br>--Your fifth and final mammary grouping swells with ";
				text += StringUtil.numberToWord(player.breastRows[i].breasts) + ' [breasts|$i] with ';
				text += StringUtil.numberToWord(player.breastRows[i].nipplesPerBreast); //Number of nipples per breast
				text += " " + Measurement.shortSuffix(player.nippleLength) + " ";		// Length of nipples
				text += Appearance.nippleDescription(player, i) + (player.breastRows[i].nipplesPerBreast == 1 ? " each." : "s each."); //Description and Plural
				if (player.breastRows[i].breastRating != BreastSize.Flat) 
					text += '  They could easily fill a [breastcup|$i] bra.';
				if (player.breastRows[i].milkFullness > 75) 
					text += '  Your [breasts|$i] are painful and sensitive from being so stuffed with milk.  You should release the pressure soon.';
			}
		}	
		
		return text;
	}
	
	private static function wingAndArmDetails(player:Player):String {
		var text = "";
		
		if(player.wings != null)
			switch(player.wings.type)
			{
				case WingType.SmallBee:
					text += "  A pair of tiny-yet-beautiful bee-wings sprout from your back, too small to allow you to fly.";
				case WingType.LargeBee:
					text += "  A pair of large bee-wings sprout from your back, reflecting the light through their clear membranes beautifully.  They flap quickly, allowing you to easily hover in place or fly.";
				case WingType.Imp:
					text += " A pair of imp wings sprout from your back, flapping cutely but otherwise being of little use.";
				case WingType.LargeImp:
					text += " A pair of large imp wings fold behind your shoulders. With a muscle-twitch, you can extend them, and use them to soar gracefully through the air.";
				case WingType.TinyBatlike:
					text += "  A pair of tiny bat-like demon-wings sprout from your back, flapping cutely, but otherwise being of little use.";
				case WingType.LargeBatlike:
					text += "  A pair of large bat-like demon-wings fold behind your shoulders.  With a muscle-twitch, you can extend them, and use them to soar gracefully through the air.";
				case WingType.SharkFin:
					text += "  A large shark-like fin has sprouted between your shoulder blades.  With it you have far more control over swimming underwater.";
				case WingType.LargeFeathered:
					text += "  A pair of large, feathery wings sprout from your back.  Though you usually keep the " + player.wings.color + "-colored wings folded close, they can unfurl to allow you to soar as gracefully as a harpy.";
				case WingType.DraconicSmall:
					text += "  Small, vestigial wings sprout from your shoulders.  They might look like bat's wings, but the membranes are covered in fine, delicate scales.";
				case WingType.DraconicLarge:
					text += "  Magnificent wings sprout from your shoulders.  When unfurled they stretch further than your arm span, and a single beat of them is all you need to set out toward the sky.  They look a bit like bat's wings, but the membranes are covered in fine, delicate scales and a wicked talon juts from the end of each bone.";
				case WingType.GiantDragonfly:	
					text += "  Giant dragonfly wings hang from your shoulders.  At a whim, you could twist them into a whirring rhythm fast enough to lift you off the ground and allow you to fly.";
				default:
			}
			
			switch(player.armType)
			{
				case ArmType.Human:
					text += "  Your arms are entirely human in form, with the same [skin] as the rest of your body. ";
				case ArmType.Harpy:
					text += "  Feathers hang off your arms from shoulder to wrist, giving them a slightly wing-like look.";
				case ArmType.Wolf:
					text += "  Your arms are shaped like a wolf's, overly muscular at your shoulders and biceps before quickly slimming down. They're covered in [furcolor] fur and end in paws with just enough flexibility to be used as hands. They're rather difficult to move in directions besides back and forth.";	
				case ArmType.Spider:
					text += "  Shining black exoskeleton covers your arms from the biceps down, resembling a pair of long black gloves from a distance.";
				case ArmType.Salamander:
					text += "  Shining thick, leathery red scales cover your arms from the biceps down and your fingernails are now short, fiery-red curved claws.";
				case ArmType.Predator:
					text += "  Your arms are covered by [skinFurScales] and your fingernails are now [claws].";
				case ArmType.Cockatrice:
					text += "  Your arms are covered in " + (player.hasCockatriceSkin() ? player.skin.furColor : player.hair.color) + " feathers from the"
							  +" shoulder down to the elbow where they stop in a fluffy cuff. A handful of long feathers grow from your elbow in the form"
							  +" of vestigial wings, and while they may not let you fly, they certainly help you jump. Your lower arm is coated in"
							  +" leathery [skintone] scales and your fingertips terminate in deadly looking avian talons.";
			}
		
		return text;
	}
	
	private static function hipsAndAssDescription(player:Player):String {
		var text = "";
		
		if (player.hasCockatriceSkin()) {
			text += "  You’ve got a thick layer of [furcolor] feathers covering your body, while [skincovering] coat you from"
					  +" chest to groin. Around your neck is a ruff of [lowerskincovering] which tends to puff out with your emotions.";
		} else if (player.lowerBody.skin != null) {
			text += "  While most of your body is covered by [skincovering] you have [lowerskincovering] covering your belly.";
		}
		
		if (player.isTaur()) 
		{
			if (player.lowerBody.type == LowerBodyType.Hoofed) 
				text += "  From the waist down you have the body of a horse, with all " + StringUtil.numberToWord(player.lowerBody.legCount)+ " legs capped by hooves.";
			else if (player.lowerBody.type == LowerBodyType.Pony) 
				text += "  From the waist down you have an incredibly cute and cartoonish parody of a horse's body, with all " + StringUtil.numberToWord(player.lowerBody.legCount)+ " legs ending in flat, rounded feet.";
			else
				text += "  Where your legs would normally start you have grown the body of a feral animal, with all " + StringUtil.numberToWord(player.lowerBody.legCount)+ " legs.";
		}
		if (player.lowerBody.type == LowerBodyType.Drider) 
			text += "  Where your legs would normally start you have grown the body of a spider, with " + StringUtil.numberToWord(player.lowerBody.legCount) + " spindly legs that sprout from its sides.";
		//Hip info only displays if you aren't a centaur. 
		if (!player.isTaur()) 
		{
			text += "  You have " + Appearance.hipDescription(player);
			
			if (player.thickness > 70) 
			{
				if (player.hipRating < 6) 
				{
					if (player.tone < 65) 
						text += " buried under a noticeable muffin-top, and";
					else text += " that blend into your pillar-like waist, and";
				}
				if (player.hipRating >= 6 && player.hipRating < 10) 
					text += " that blend into the rest of your thick form, and";
				if (player.hipRating >= 10 && player.hipRating < 15) 
					text += " that would be much more noticeable if you weren't so wide-bodied, and";
				if (player.hipRating >= 15 && player.hipRating < 20) 
					text += " that sway and emphasize your thick, curvy shape, and";
				if (player.hipRating >= 20) 
					text += " that sway hypnotically on your extra-curvy frame, and";
			}
			else if (player.thickness < 30) 
			{
				if (player.hipRating < 6) 
					text += " that match your trim, lithe body, and";
				if (player.hipRating >= 6 && player.hipRating < 10) 
					text += " that sway to and fro, emphasized by your trim body, and";
				if (player.hipRating >= 10 && player.hipRating < 15) 
					text += " that swell out under your trim waistline, and";
				if (player.hipRating >= 15 && player.hipRating < 20) 
					text += ", emphasized by your narrow waist, and";
				if (player.hipRating >= 20) 
					text += " that swell disproportionately wide on your lithe frame, and";
			}
			else 
			{
				if (player.hipRating < 6) 
					text += ", and";
				if (player.femininity > 50) 
				{
					if (player.hipRating >= 6 && player.hipRating < 10) 
						text += " that draw the attention of those around you, and";
					if (player.hipRating >= 10 && player.hipRating < 15) 
						text += " that make you walk with a sexy, swinging gait, and";
					if (player.hipRating >= 15 && player.hipRating < 20) 
						text += " that make it look like you've birthed many children, and";
					if (player.hipRating >= 20) 
						text += " that make you look more like an animal waiting to be bred than any kind of human, and";
				}
				else 
				{
					if (player.hipRating >= 6 && player.hipRating < 10) 
						text += " that give you a graceful stride, and";
					if (player.hipRating >= 10 && player.hipRating < 15) 
						text += " that add a little feminine swing to your gait, and";
					if (player.hipRating >= 15 && player.hipRating < 20) 
						text += " that force you to sway and wiggle as you move, and";
					if (player.hipRating >= 20) 
					{
						text += " that give your ";
						if (player.balls > 0) 
							text += "balls plenty of room to breathe";
						//else if (player.hasCock()) 
							//text += Appearance.multiCockDescript(player) + " plenty of room to swing";
						else if (player.hasVagina()) 
							text += Appearance.vaginaDescript(player) + " a nice, wide berth";
						else text += "vacant groin plenty of room";
						text += ", and";
					}
				}
			}
		}
		
		//ASS
		if (player.isTaur()) 
		{
			if (player.tone < 65) 
			{
				text += "  Your " + Appearance.buttDescription(player);
				if (player.buttRating < 4) 
					text += " is lean, from what you can see of it.";
				if (player.buttRating >= 4 && player.buttRating < 6) 
					text += " looks fairly average.";
				if (player.buttRating >= 6 && player.buttRating <10) 
					text += " is fairly plump and healthy.";
				if (player.buttRating >= 10 && player.buttRating < 15) 
					text += " jiggles a bit as you trot around.";
				if (player.buttRating >= 15 && player.buttRating < 20) 
					text += " jiggles and wobbles as you trot about.";
				if (player.buttRating >= 20) 
					text += " is obscenely large, bordering freakish, even for a horse.";
			}
			else 
			{
				text += "  Your " + Appearance.buttDescription(player);
				if (player.buttRating < 4) 
					text += " is barely noticeable, showing off the muscles of your haunches.";
				if (player.buttRating >= 4 && player.buttRating < 6) 
					text += " matches your toned equine frame quite well.";
				if (player.buttRating >= 6 && player.buttRating <10) 
					text += " gives hints of just how much muscle you could put into a kick.";
				if (player.buttRating >= 10 && player.buttRating < 15) 
					text += " surges with muscle whenever you trot about.";
				if (player.buttRating >= 15 && player.buttRating < 20) 
					text += " flexes its considerable mass as you move.";
				if (player.buttRating >= 20) 
					text += " is stacked with layers of muscle, huge even for a horse.";
			}
		}
		else 
		{
			if (player.tone < 60) 
			{
				text += " your " + Appearance.buttDescription(player);
				if (player.buttRating < 4) 
					text += " looks great under your gear.";
				if (player.buttRating >= 4 && player.buttRating < 6) 
					text += " has the barest amount of sexy jiggle.";
				if (player.buttRating >= 6 && player.buttRating <10) 
					text += " fills out your clothing nicely.";
				if (player.buttRating >= 10 && player.buttRating < 15) 
					text += " wobbles enticingly with every step.";
				if (player.buttRating >= 15 && player.buttRating < 20) 
					text += " wobbles like a bowl full of jello as you walk.";
				if (player.buttRating >= 20) 
					text += " is obscenely large, bordering freakish, and makes it difficult to run.";
			}
			else 
			{
				text += " your " + Appearance.buttDescription(player);
				if (player.buttRating < 4) 
					text += " molds closely against your form.";
				if (player.buttRating >= 4 && player.buttRating < 6) 
					text += " contracts with every motion, displaying the detailed curves of its lean musculature.";
				if (player.buttRating >= 6 && player.buttRating <10) 
					text += " fills out your clothing nicely.";
				if (player.buttRating >= 10 && player.buttRating < 15) 
					text += " stretches your gear, flexing it with each step.";
				if (player.buttRating >= 15 && player.buttRating < 20) 
					text += " threatens to bust out from under your kit each time you clench it.";
				if (player.buttRating >= 20) 
					text += " is marvelously large, but completely stacked with muscle.";
			}
		}
		
		return text;
	}
	
	private static function tailDetails(player:Player):String {
		var text = "";
		
		if (player.tail == null)
			return "";
		
		switch(player.tail.type)
		{
			case TailType.Horse:
				text += "  A long [haircolor] horsetail hangs from your [butt], smooth and shiny.";
			case TailType.Ferret:
				text += "  A long ferret tail sprouts from above your [butt].  It is thin, tapered, and covered in shaggy [furcolor] fur.";
			case TailType.Sheep:
				text += "  A fluffy sheep tail hangs down from your [butt]. It occasionally twitches and shakes, its puffy fluff begging to be touched.";
			case TailType.Dog:
				text += "  A fuzzy [furcolor] dogtail sprouts just above your [butt], wagging to and fro whenever you are happy.";
			case TailType.Demonic:
				text += "  A narrow tail ending in a spaded tip curls down from your [butt], wrapping around your [leg] sensually at every opportunity.";
			case TailType.Cow:
				text += "  A long cowtail with a puffy tip swishes back and forth as if swatting at flies.";
			case TailType.SpiderAbdomen:
				text += "  A large, spherical spider-abdomen has grown out from your backside, covered in shiny black chitin.  Though it's heavy and bobs with every motion, it doesn't seem to slow you down.";
				if (player.tail.tailVenom > 50 && player.tail.tailVenom < 80) 
					text += "  Your bulging arachnid posterior feels fairly full of webbing.";
				if (player.tail.tailVenom >= 80 && player.tail.tailVenom < 100) 
					text += "  Your arachnid rear bulges and feels very full of webbing.";
				if (player.tail.tailVenom == 100) 
					text += "  Your swollen spider-butt is distended with the sheer amount of webbing it's holding.";
			case TailType.BeeAbdomen:
				text += "  A large insectile bee-abdomen dangles from just above your backside, bobbing with its own weight as you shift.  It is covered in hard chitin with black and yellow stripes, and tipped with a dagger-like stinger.";
				if (player.tail.tailVenom > 50 && player.tail.tailVenom < 80) 
					text += "  A single drop of poison hangs from your exposed stinger.";
				if (player.tail.tailVenom >= 80 && player.tail.tailVenom < 100) 
					text += "  Poisonous bee venom coats your stinger completely.";
				if (player.tail.tailVenom == 100) 
					text += "  Venom drips from your poisoned stinger regularly.";
			case TailType.Shark:
				text += "  A long shark-tail trails down from your backside, swaying to and fro while giving you a dangerous air.";
			case TailType.Cat:
				text += "  A soft [furcolor] cat-tail sprouts just above your [butt], curling and twisting with every step to maintain perfect balance.";
			case TailType.Lizard:
				text += "  A tapered tail hangs down from just above your [ass].  It sways back and forth, assisting you with keeping your balance.";
			case TailType.Salamander:
				text += "  A tapered, covered in red scales tail hangs down from just above your [ass].  It sways back and forth, assisting you with keeping your balance. When you are in battle or when you want could set ablaze whole tail in red-hot fire.";
			case TailType.Rabbit:
				text += "  A short, soft bunny tail sprouts just above your [ass], twitching constantly whenever you don't think about it.";
			case TailType.Harpy:
				text += "  A tail of feathers fans out from just above your [ass], twitching instinctively to help guide you if you were to take flight.";
			case TailType.Kangaroo:
				text += "  A conical, ";
				if (player.skin.type == SkinType.Goo) 
					text += "gooey, [skintone]";
				else 
					text += "furry, [furcolor]";
				text += ", tail extends from your [ass], bouncing up and down as you move and helping to counterbalance you.";
			case TailType.Fox:
				if (player.tail.tailCount <= 1) 
					text += "  A swishing [hairOrFurColors] fox's brush extends from your [ass], curling around your body - the soft fur feels lovely.";
				else 
					text += "  [Tailcount] swishing [hairOrFurColors] fox's tails extend from your [ass], curling around your body - the soft fur feels lovely.";
			case TailType.Draconic:
				text += "  A thin, scaly, prehensile reptilian tail, almost as long as you are tall, swings behind you like a living bullwhip.  Its tip menaces with spikes of bone, meant to deliver painful blows.";
			case TailType.Racoon:
				text += "  A black-and-[hairOrFurColors]-ringed raccoon tail waves behind you.";
			case TailType.Mouse:
				text += "  A naked, [skintone] mouse tail pokes from your butt, dragging on the ground and twitching occasionally.";
			case TailType.Behemoth:
				text += "  A long seemingly-tapering tail pokes from your butt, ending in spikes just like behemoth's.";
			case TailType.Pig:
				text += "  A short, curly pig tail sprouts from just above your [butt].";
			case TailType.Scorpion:
				text += "  A chitinous scorpion tail sprouts from just above your [butt], ready to dispense venom.";
			case TailType.Goat:
				text += "  A very short, stubby goat tail sprouts from just above your [butt].";
			case TailType.Rhino:
				text += "  A ropey rhino tail sprouts from just above your [butt], swishing from time to time.";
			case TailType.Echidna:
				text += "  A stumpy echidna tail forms just about your [ass].";
			case TailType.Deer:
				text += "  A very short, stubby deer tail sprouts from just above your [butt].";
			case TailType.Wolf:
				text += "  A thick-furred wolf tail hangs above your [ass].";
			case TailType.Imp:
				text += " A thin imp tail almost as long as you are tall hangs from above your [butt], dotted at the end with a small puff of hair.";
			case TailType.Cockatrice:
				text += " A thick, scaly, prehensile reptilian tail hangs from your [butt], about half as long as you are tall."
					  +" The first inch or so is feathered, terminating in a 'v'shape and giving way to your [skintone] scales.";
			default:
		}

		return text;
	}
	
	public static function penisDetails(player:Player):String {
		var text = "";
		
		if (player.hasCock()) {
			var rando:Int = Std.random(100);
			
			text += "<br>";
			
			// Is taur and has multiple cocks?
			if (player.isTaur() && player.cocks.length == 1)
				text += "<br>Your equipment has shifted to lie between your hind legs, like a feral animal.";
			else if (player.isTaur())
				text += "<br>Between your hind legs, you have grown [multicock]!<br>";
			else if (player.cocks.length == 1)
				text += "<br>";
			else
				text += "<br>Where a penis would normally be located, you have instead grown [multicock]!<br>";
				
			for (cock_index in 0...player.cocks.length) {
				rando++;
				
				// How to start the sentence?
				if  (player.cocks.length == 1)  text += "Your ";
				else if (cock_index == 0)      text += "--Your first ";
				else if (rando % 5 == 0)       text += "--The next ";
				else if (rando % 5 == 1)       text += "--The " + StringUtil.numberToWord(cock_index+1) + " of your ";
				else if (rando % 5 == 2)       text += "--One of your ";
				else if (rando % 5 == 3)       text += "--The " + StringUtil.numberToWord(cock_index+1) + " ";
				else if (rando % 5 == 4)       text += "--Another of your ";
				
				// How large?
				text += Appearance.cockDescript(player, player.cocks[cock_index]) + ((rando % 5) % 3 == 0 || cock_index == 0 ? "":"s") +  " is " + Measurement.inchesOrCentimetres(player.cocks[cock_index].length) + " long and ";
				text += Measurement.inchesOrCentimetres(player.cocks[cock_index].thickness);
				if      (rando % 3 == 0)  text += " wide.";
				else if (rando % 3 == 1)  text += " thick.";
				else if (rando % 3 == 2)  text += " in diameter.";
				
				switch(player.cocks[cock_index].type)
				{
					case PenisType.Horse:
						text += "  It's mottled black and brown in a very animalistic pattern.  The 'head' of its shaft flares proudly, just like a horse's.";
					case PenisType.Dog:
						text += "  It is shiny, pointed, and covered in veins, just like a large dog's cock.";
					case PenisType.Wolf: 
						text += "  It is shiny red, pointed, and covered in veins, just like a large wolf's cock.";
					case PenisType.Fox:
						text += "  It is shiny, pointed, and covered in veins, just like a large fox's cock.";
					case PenisType.Demon: 
						text += "  The crown is ringed with a circle of rubbery protrusions that grow larger as you get more aroused.  The entire thing is shiny and covered with tiny, sensitive nodules that leave no doubt about its demonic origins.";
					case PenisType.Tentacle:
						text += "  The entirety of its green surface is covered in perspiring beads of slick moisture.  It frequently shifts and moves of its own volition, the slightly oversized and mushroom-like head shifting in coloration to purplish-red whenever you become aroused.";
					case PenisType.Cat:
						text += "  It ends in a single point, much like a spike, and is covered in small, fleshy barbs. The barbs are larger at the base and shrink in size as they get closer to the tip.  Each of the spines is soft and flexible, and shouldn't be painful for any of your partners.";
					case PenisType.Lizard:
						text += "  It's a deep, iridescent purple in color.  Unlike a human penis, the shaft is not smooth, and is instead patterned with multiple bulbous bumps.";
					case PenisType.Anemone:
						text += "  The crown is surrounded by tiny tentacles with a venomous, aphrodisiac payload.  At its base a number of similar, longer tentacles have formed, guaranteeing that pleasure will be forced upon your partners.";
					case PenisType.Kangaroo:
						text += "  It usually lies coiled inside a sheath, but undulates gently and tapers to a point when erect, somewhat like a taproot.";
					case PenisType.Dragon:
						text += "  With its tapered tip, there are few holes you wouldn't be able to get into.  It has a strange, knot-like bulb at its base, but doesn't usually flare during arousal as a dog's knot would.";
					case PenisType.Bee:
						text += "  It's a long, smooth black shaft that's rigid to the touch.  Its base is ringed with a layer of " + Measurement.shortSuffix(4) + " long soft bee hair.  The tip has a much finer layer of short yellow hairs.  The tip is very sensitive, and it hurts constantly if you don’t have bee honey on it.";
					case PenisType.Pig:
						text += "  It's bright pinkish red, ending in a prominent corkscrew shape at the tip.";
					case PenisType.Avian:
						text += "  It's a red, tapered cock that ends in a tip.  It rests nicely in a sheath.";
					case PenisType.Rhino:
						text += "  It's a smooth, tough pink colored and takes on a long and narrow shape with an oval shaped bulge along the center.";
					case PenisType.Echidna:
						text += "  It is quite a sight to behold, coming well-equipped with four heads.";
					default:
				}
				
				// Knot?
				if (player.cocks[cock_index].knotMultiplier > 1) {
					if (player.cocks[cock_index].knotMultiplier >= 1.8) 
						text += '  The obscenely swollen lump of flesh near the base of your [cock|$cock_index] looks almost comically mismatched for your cock.';
					else if (player.cocks[cock_index].knotMultiplier >= 1.4) 
						text += '  A large bulge of flesh nestles just above the bottom of your [cock|$cock_index], to ensure it stays where it belongs during mating.';
					else // knotMultiplier < 1.4
						text += '  A small knot of thicker flesh is near the base of your [cock|$cock_index], ready to expand to help you lodge it inside a female.';
					text += "  The knot is " + Measurement.inchesOrCentimetres(player.cocks[cock_index].thickness * player.cocks[cock_index].knotMultiplier) + " thick when at full size.";
				}

				// Chastity?
				//if (flags[kFLAGS.CERAPHCHASITY] > 0){
					//text += " Tightly wrapped in a golden metallic case, no matter how hard you try, you can't seem to be able take it off without some help.";
				//}
			}

			//Worm flavor
			//if (player.hasStatusEffect(StatusEffects.Infested))
				//text += "Every now and again slimy worms coated in spunk slip partway out of your " + player.multiCockDescriptLight() + ", tasting the air like tongues of snakes.<br>";
			//
			//if (player.hasStatusEffect(StatusEffects.ParasiteSlug) || player.findPerk(PerkLib.ParasiteMusk) >= 0){
				//text += "Precum oozes constantly from your " + player.multiCockDescriptLight() + ". Occasionaly, your body seizes and clenches, launching thick dollops of pre everywhere.<br>";
			//}

		}
		
		return text;
	}
	
	public static function ballsDetails(player:Player):String{
		var text = "";
					//Of Balls and Sacks!
		if (player.balls > 0) 
		{
			/*if (player.hasStatusEffect(StatusEffects.Uniball))
			{
				if (player.hasGooSkin()) 
					text += "Your [sack] clings tightly to your groin, dripping and holding " + player.ballsDescript() + " snugly against you.";
				else
					text += "Your [sack] clings tightly to your groin, holding " + player.ballsDescript() + " snugly against you.";
			}
			else */if (player.cocks.length == 0) 
			{
				if (player.skin.type == SkinType.Normal) 
					text += " A [sack] with [ballsdetails] swings heavily under where a penis would normally grow.";
				if (player.skin.isFurry()) 
					text += " A fuzzy [sack] filled with [ballsdetails] swings low under where a penis would normally grow.";
				if (player.skin.isScaly()) 
					text += " A scaley [sack] hugs your [ballsdetails] tightly against your body.";
				if (player.skin.type == SkinType.Goo) 
					text += " An oozing, semi-solid sack with [ballsdetails] swings heavily under where a penis would normally grow.";
			}
			else 
			{
				if (player.skin.type == SkinType.Normal) 
					text += " A [sack] with [ballsdetails] swings heavily beneath your [multicock].";
				if (player.skin.isFurry()) 
					text += " A fuzzy [sack] filled with [ballsdetails] swings low under your [multicock].";
				if (player.skin.isScaly()) 
					text += " A scaley [sack] hugs your [ballsdetails] tightly against your body.";
				if (player.skin.type == SkinType.Goo) 
					text += " An oozing, semi-solid sack with [ballsdetails] swings heavily beneath your [multicock].";
			}
			text += "  You estimate each of them to be about [ballsize] across.";
		}
			
		return text;
	}
	
	private static function vaginaDetails(player:Player):String{
		var text = "";
		
		if (player.vaginas.length > 0) 
		{
			if (player.getGender() == Gender.Female && player.isTaur()) 
				text += "<br><br>Your womanly parts have shifted to lie between your hind legs, in a rather feral fashion.";
			text += "<br><br>";
			if (player.vaginas.length == 1) 
				text += "You have a [vagina], with a " + Measurement.inchesOrCentimetres(player.vaginas[0].clitLength) + " clit";
			if (player.vaginas[0].virgin) 
				text += " and an intact hymen"; // Wait, won't this fuck up, with multiple vaginas?
			text += ".  ";
			//if (player.hasStatusEffect(StatusEffects.ParasiteEel)){
				//if (player.statusEffectv1(StatusEffects.ParasiteEel) == 1) text += "Instead of the usual girl-cum, a few strings of a thick, viscous, slightly opaque fluid oozes from ";
				//if (player.statusEffectv1(StatusEffects.ParasiteEel) == 2) text += "Instead of the usual girl-cum, several strings of a thick, viscous, slightly opaque fluid oozes from ";
				//if (player.statusEffectv1(StatusEffects.ParasiteEel) > 2) text += "Instead of the usual girl-cum, obscene amounts of a thick, viscous slime seeps from ";
				//if (player.vaginas[0].vaginalLooseness< VAGINA_LOOSENESS_LOOSE) 
					//text += "your " + player.vaginaDescript(0) + ", ";
				//if ([VaginaLooseness.Loose, VaginaLooseness.Gaping].indexOf(player.vaginas[0].vaginalLooseness) != -1) 
					//text += "your " + player.vaginaDescript(0) + ", its lips slightly parted, ";
				//if (player.vaginas[0].vaginalLooseness>= VAGINA_LOOSENESS_GAPING_WIDE) 
					//text += "the massive hole that is your " + player.vaginaDescript(0) + ", ";
				//text += player.clothedOrNakedLower("covering and drenching any underwear you put on immediately", "leaving constant trails of slime wherever you go.");
			//} else {
				if (player.stats[Stat.Libido] < 50 && player.lust < 50){
					switch(player.vaginas[0].vaginalWetness)
					{
						case VaginaWetness.Dry, VaginaWetness.Normal:
							text += "A mild wetness dampens the entrance of ";
						case VaginaWetness.Wet, VaginaWetness.Slick:
							text += "Moisture gleams in ";
						case VaginaWetness.Drooling, VaginaWetness.Slavering:
							text += "Occasional beads of lubricant drip from ";
						default:
					}
					
					switch(player.vaginas[0].vaginalLooseness)
					{
						case VaginaLooseness.Tight, VaginaLooseness.Normal:
							text += "your [vagina]. ";
						case VaginaLooseness.Loose, VaginaLooseness.Gaping:
							text += "your [vagina], its lips slightly parted. ";
						case VaginaLooseness.GapingWide, VaginaLooseness.ClownCar:
							text += "the massive hole that is your cunt.  ";
					}
				}
				if ((player.stats[Stat.Libido] >= 50 || player.lust >=50) && (player.stats[Stat.Libido] < 80 && player.lust < 80)) //kinda horny
				{
					switch(player.vaginas[0].vaginalWetness)
					{
						case VaginaWetness.Dry, VaginaWetness.Normal:
							text += "Moisture gleams in ";
						case VaginaWetness.Wet, VaginaWetness.Slick:
							text += "Occasional beads of lubricant drip from ";
						case VaginaWetness.Drooling, VaginaWetness.Slavering:
							text += "Thin streams of lubricant occasionally dribble from ";
					}
					
					switch(player.vaginas[0].vaginalLooseness)
					{
						case VaginaLooseness.Tight, VaginaLooseness.Normal:
							text += "your [vagina]. ";
						case VaginaLooseness.Loose, VaginaLooseness.Gaping:
							text += "your [vagina], its lips slightly parted. ";
						case VaginaLooseness.GapingWide, VaginaLooseness.ClownCar:
							text += "the massive hole that is your cunt.  ";
					}
				}
				if ((player.stats[Stat.Libido] > 80 || player.lust > 80)) //WTF horny!
				{
					switch(player.vaginas[0].vaginalWetness)
					{
						case VaginaWetness.Dry, VaginaWetness.Normal:
							text += "Occasional beads of lubricant drip from ";
						case VaginaWetness.Wet, VaginaWetness.Slick:
							text += "Thin streams of lubricant occasionally dribble from ";
						case VaginaWetness.Drooling, VaginaWetness.Slavering:
							text += "Thick streams of lubricant drool constantly from ";
					}
					
					switch(player.vaginas[0].vaginalLooseness)
					{
						case VaginaLooseness.Tight, VaginaLooseness.Normal:
							text += "your [vagina]. ";
						case VaginaLooseness.Loose, VaginaLooseness.Gaping:
							text += "your [vagina], its lips slightly parted. ";
						case VaginaLooseness.GapingWide, VaginaLooseness.ClownCar:
							text += "the massive hole that is your cunt.  ";
					}
				}
				
			//}
			
			//if (flags[kFLAGS.CERAPHCHASITY] > 0){
				//text += " Covered by a metallic plate that leaves only enough room for fluids to go through.");
			//}
		}
		
		return text;
	}
}