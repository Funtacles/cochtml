package system;
import data.Creature;
import data.Character;
import data.body.BreastRow;
import data.body.Penis;
import data.body.enums.*;

/**
 * ...
 * @author Funtacles
 */
class Appearance 
{
	public static function hairOrFur(creature:Creature):String {
		if (creature.skin.isFurry())
			return "fur";
		else
			return "hair";
	}

	public static function hairDescription(creature:Creature):String
	{
		var description:String = "";
		
		if (creature.hair.length == 0)
			return StringUtil.randomWord(["shaved", "bald", "smooth", "hairless"]) + " head";
		
		if (creature.hair.length < 1)
			description += StringUtil.randomWord(["close-cropped, ", "trim, ", "very short, "]);
		else if (creature.hair.length < 3)
			description += "short, ";
		else if (creature.hair.length < 6)
			description += "shaggy, ";
		else if (creature.hair.length < 10)
			description += "moderately long, ";
		else if (creature.hair.length < 16)
			description += StringUtil.randomWord(["long, ", "shoulder-length, "]);
		else if (creature.hair.length < 26)
			description += StringUtil.randomWord(["very long, ", "flowing locks of "]);
		else if (creature.hair.length < 40)
			description += "ass-length, ";
		else if (creature.hair.length < creature.height)
			description += "obscenely long, ";
		else // if (i_creature.hairLength >= i_creature.tallness)
			description += StringUtil.randomWord(["floor-length, ", "floor-dragging, "]);
		//
		// COLORS
		//
		description += creature.hair.color + " ";
		
		// HAIR WORDS
		switch (creature.hair.type) {
			case HairType.BasaliskSpines:
				return description + StringUtil.randomWord(["rubbery spines", "spiny crown", "basilisk spines", "reptilian spines"]);
			case HairType.BasaliskPlume:
				return description + StringUtil.randomWord(["feathered hair", "fluffy plume", "basilisk plume","shock of feathers"]);
			case HairType.Wool:
				return description + StringUtil.randomWord(["woolen hair", "poofy hair", "soft wool", "untameable woolen hair"]);
			default:
		}
		
		//If furry and longish hair sometimes call it a mane (50%)
		
		switch(creature.hair.type){
			case HairType.Feather:
				description += "feather-";
			case HairType.Ghost:
				description += "transparent ";
			case HairType.Goo:
				description += "goo-";
			case HairType.Anemone:
				description += "tentacle-";
			case HairType.Quill:
				description += "quill-";
				default:
		}
		
		if (creature.skin.type == SkinType.Fur && creature.hair.length > 3) {
			return return description + StringUtil.randomWord(["mane", "hair"]);
		}
		
		return description + "hair";
	}
	
	public static function beardDescription(creature:Creature):String {
		var description:String = "";
		if (creature.beardLength == 0) {
			description = StringUtil.randomWord(["shaved","bald", "smooth", "hairless"]) + " chin and cheeks";
			return description;
		}
		
		if (creature.beardLength < 0.2) {
			description += StringUtil.randomWord(["close-cropped, ", "trim, ", "very short, "]);
		}
		if (creature.beardLength >= 0.2 && creature.beardLength < 0.5) description += "short, ";
		if (creature.beardLength >= 0.5 && creature.beardLength < 1.5) description += "medium, ";
		if (creature.beardLength >= 1.5 && creature.beardLength < 3) description += "moderately long, ";
		if (creature.beardLength >= 3 && creature.beardLength < 6) description += StringUtil.randomWord(["long, ", "neck-length, "]);
		if (creature.beardLength >= 6) description += StringUtil.randomWord(["very long, ", "chest-length, "]);
		
		// COLORS
		description += creature.hair.color + " ";
		//
		// BEARD WORDS
		// Follows hair type.
		if (creature.hair.type == HairType.Normal) description += "";
		else if (creature.hair.type == HairType.Ghost) description += "transparent ";
		else if (creature.hair.type == HairType.Goo) description += "gooey ";
		else if (creature.hair.type == HairType.Anemone) description += "tentacley ";
		
		if (creature.beardStyle == BeardType.Normal) description += "beard";
		else if (creature.beardStyle == BeardType.Goatee) description += "goatee";
		else if (creature.beardStyle == BeardType.CleanCut) description += "clean-cut beard";
		else if (creature.beardStyle == BeardType.MountainMan) description += "mountain-man beard";
		
		return description;
	}

	public static function breastDescript(breastRow:BreastRow):String {
		var lactation = breastRow.lactationMultiplier;
		
		if (breastRow.breastRating == BreastSize.Flat) return "flat breasts";
		var descript:String = (Std.random(2) == 0 ? breastSize(breastRow.breastRating) : ""); //Add a description of the breast size 50% of the time
		switch (Std.random(10)) {
			case 1:
				if (lactation > 2) return descript + "milk-udders";
			case 2:
				if (lactation > 1.5) descript += "milky ";
				if (breastRow.breastRating.getIndex() > 4) return descript + "tits";
			case 4:
			case 5:
			case 6:
				return descript + "tits";
			case 7:
				if (lactation >= 2.5) return descript + "udders";
				if (lactation >= 1) descript += "milk ";
				return descript + "jugs";
			case 8:
				if (breastRow.breastRating.getIndex() > 6) return descript + "love-pillows";
				return descript + "boobs";
			case 9:
				if (breastRow.breastRating.getIndex() > 6) return descript + "tits";
			default:
		}
		return descript + "breasts";
	}
	
	public static function nippleDescription(creature:Creature, row:Int):String
	{
		//DEBUG SHIT!
		if (row > (creature.breastRows.length - 1)) {
			return "<B>Error: Invalid breastRows (" + row + ") passed to nippleDescription()</b>";
		}
		if (row < 0) {
			return "<B>Error: Invalid breastRows (" + row + ") passed to nippleDescription()</b>";
		} 
		
		var haveDescription:Bool = false;
		var description:String = "";
		//Size descriptors 33% chance
		if (Std.random(3) == 0) {
			if (creature.nippleLength < .25)
				description += StringUtil.randomWord( ["tiny ","itty-bitty ","teeny-tiny ","dainty "]);
			else if (creature.nippleLength < 1)
				description += StringUtil.randomWord(["prominent ","pencil eraser-sized ","eye-catching ","pronounced ","striking "]);
			else if (creature.nippleLength < 2)
				description += StringUtil.randomWord(["forwards-jutting ","over-sized ","fleshy ","large protruding "]);
			else if (creature.nippleLength < 3.2)
				description += StringUtil.randomWord(["elongated ","massive ","awkward ","lavish ","hefty "]);
			if (creature.nippleLength >= 3.2)
				description += StringUtil.randomWord(["bulky ","ponderous ","thumb-sized ","cock-sized ","cow-like "]);
			haveDescription = true;
		}
		//Milkiness/Arousal/Wetness Descriptors 33% of the time
		if (Std.random(3) == 0 && !haveDescription) {
			//Fuckable chance first!
			if (creature.hasFuckableNipples()) {
				//Fuckable and lactating?
				if (creature.biggestLactation() > 1)
					description += StringUtil.randomWord(["milk-lubricated ","lactating ","lactating ","milk-slicked ","milky "]);
				//Just fuckable
				else 
					description += StringUtil.randomWord(["wet ","mutated ","slimy ","damp ","moist ","slippery ","oozing ","sloppy ","dewy "]);
				haveDescription = true;
			}
			//Just lactating!
			else if (creature.biggestLactation() > 0) {
				if (creature.biggestLactation() <= 1)							//Light lactation
					description += StringUtil.randomWord(["milk moistened ","slightly lactating ","milk-dampened "]);
				else if (creature.biggestLactation() <= 2) 						//Moderate lactation
					description += StringUtil.randomWord(["lactating ","milky ","milk-seeping "]);
				else															//Heavy lactation
					description += StringUtil.randomWord(["dripping ","dribbling ","milk-leaking ","drooling "]);
				haveDescription = true;
			}
		}
		//Possible arousal descriptors
		else if (Std.random(3) == 0 && !haveDescription) {
			if (creature.lust > 50 && creature.lust < 75) {
				description += StringUtil.randomWord(["erect ","perky ","erect ","firm ","tender "]);
				haveDescription = true;
			}
			if (creature.lust >= 75) {
				description += StringUtil.randomWord( ["throbbing ","trembling ","needy ","throbbing "]);
				haveDescription = true;
			}
		}
		if (!haveDescription && Std.random(2) == 0 && creature.nipplesPierced > 0 && row == 0) {
			if (creature.nipplesPierced == 5) description += "chained ";
			else description += "pierced ";
			haveDescription = true;
		}
		if (!haveDescription && creature.skin.type == SkinType.Goo) {
			description += StringUtil.randomWord(["slime-slick ","goopy ","slippery "]);
		}
		/*if (!haveDescription && creature.hasStatusEffect(StatusEffects.BlackNipples)) {
			options = ["black ",
				"ebony ",
				"sable "];
			description += StringUtil.randomWord(options);
		}*/

		var choice:Int = 0;
		choice = Std.random(5);
		if (choice == 0) description += "nipple";
		if (choice == 1) {
			if (creature.nippleLength < .5) description += "perky nipple";
			else description += "cherry-like nub";
		}
		if (choice == 2) {
			if (creature.hasFuckableNipples()) description += "fuckable nip";
			else {
				if (creature.biggestLactation() >= 1 && creature.nippleLength >= 1) description += "teat";
				else description += "nipple";
			}
		}
		if (choice == 3) {
			if (creature.hasFuckableNipples()) description += "nipple-hole";
			else {
				if (creature.biggestLactation() >= 1 && creature.nippleLength >= 1) description += "teat";
				else description += "nipple";
			}
		}
		if (choice == 4) {
			if (creature.hasFuckableNipples()) description += "nipple-cunt";
			else description += "nipple";
		}
		return description;
	}

	public static function hipDescription(character:Character):String {
		var description:String = "";
		switch(character.hipRating){
			case HipSize.Boyish:
				description = StringUtil.randomWord( ["tiny ", "narrow ", "boyish "]);
			case HipSize.Slender:
				description = StringUtil.randomWord(["slender ", "narrow ", "thin "]);
				if (character.thickness < 30 && (character.getGender() != Gender.Male || character.femininity > 55)) {
					description = StringUtil.randomWord(["slightly-flared ", "curvy "]);
				}
			case HipSize.Average:
				description = StringUtil.randomWord(["well-formed ", "pleasant "]);
				if (character.thickness < 30 && (character.getGender() != Gender.Male || character.femininity > 55)) {
					description = StringUtil.randomWord(["flared ", "curvy "]);
				}
			case HipSize.Ample:
				description = StringUtil.randomWord(["ample ", "noticeable ", "girly "]);
			case HipSize.Curvy:
				description = StringUtil.randomWord(["flared ", "curvy ", "wide "]);
			case HipSize.Fertile:
				description += StringUtil.randomWord(["fertile ", "child-bearing ", "voluptuous "]);
			case HipSize.InhumanWide:
				description += StringUtil.randomWord(["broodmother-sized ", "cow-like ","inhumanly-wide "]);
		}
		
		//Taurs
		if (character.isTaur() && Std.random(3) == 0) 
			description += "flanks";
		else if (character.lowerBody.type == LowerBodyType.Naga && Std.random(3) == 0) 
			description += "sides";
		else {
			description += StringUtil.randomWord(["hips", "thighs"]);
		}
		
		return description;
	}
		
	public static function cockDescript(creature:Creature, cock:Penis):String
	{
		var cockType:PenisType = cock.type;
		var isPierced:Bool = creature.cocks.length == 1 && cock.isPierced; //Only describe as pierced or sock covered if the creature has just one cock
		var isGooey:Bool = creature.skin.type == SkinType.Goo;
		return cockDescription(cockType, cock.length, cock.thickness, creature.lust, creature.cumQ(), isPierced, isGooey);
	}
		
	//This function takes all the variables independently so that a creature object is not required for a player.cockDescription.
	//This allows a single player.cockDescription function to produce output for both player.cockDescript and the old NPCCockDescript.
	public static function cockDescription(cockType:PenisType, length:Float, girth:Float, lust:Int = 50, cumQ:Float = 10, isPierced:Bool = false, isGooey:Bool = false): String {
		if (Std.random(2) == 0) {
			if (cockType == PenisType.Human) return cockAdjective(cockType, length, girth, lust, cumQ, isPierced, isGooey) + " " + cockNoun(cockType);
			else return cockAdjective(cockType, length, girth, lust, cumQ, isPierced, isGooey) + ", " + cockNoun(cockType);
		}
		return cockNoun(cockType);
	}
		
	public static function cockNoun(cockType:PenisType):String
	{
		switch(cockType)
		{
			case PenisType.Human: 
				return StringUtil.randomWord(["cock","cock","cock","cock","cock","prick","prick","pecker","shaft","shaft","shaft"]);
			case PenisType.Bee:
				return StringUtil.randomWord(["bee prick","bee prick","bee prick","bee prick","insectoid cock","insectoid cock","furred monster"]);
			case PenisType.Dog:
				return StringUtil.randomWord(["dog-shaped dong", "canine shaft", "pointed prick", "knotty dog-shaft", "bestial cock", "animalistic puppy-pecker", "pointed dog-dick",
					"pointed shaft","canine member","canine cock","knotted dog-cock"]);
			case PenisType.Fox:
				return StringUtil.randomWord(["fox-shaped dong","vulpine shaft","pointed prick","knotty fox-shaft","bestial cock","animalistic vixen-pricker",
					"pointed fox-dick","pointed shaft","vulpine member","vulpine cock","knotted fox-cock"]);
			case PenisType.Horse:
				return StringUtil.randomWord(["flared horse-cock","equine prick","bestial horse-shaft","flat-tipped horse-member","animalistic stallion-prick",
					"equine dong", "beast cock", "flared stallion-cock"]);
			case PenisType.Demon:
				return StringUtil.randomWord(["nub-covered demon-dick","nubby shaft","corrupted cock","perverse pecker","bumpy demon-dick","demonic cock",
					"demonic dong","cursed cock","infernal prick","unholy cock","blighted cock"]);
			case PenisType.Tentacle:
				return StringUtil.randomWord(["twisting tentacle-prick","wriggling plant-shaft","sinuous tentacle-cock","squirming cock-tendril","writhing tentacle-pecker",
					"wriggling plant-prick","penile flora","smooth shaft","undulating tentacle-dick","slithering vine-prick","vine-shaped cock"]);
			case PenisType.Cat:
				return StringUtil.randomWord(["feline dick","spined cat-cock","pink kitty-cock","spiny prick","animalistic kitty-prick","oddly-textured cat-penis",
					"feline member","spined shaft","feline shaft","barbed dick","nubby kitten-prick"]);
			case PenisType.Lizard:
				return StringUtil.randomWord(["reptilian dick","purple cock","inhuman cock","reptilian prick","purple prick","purple member","serpentine member","serpentine shaft",
					"reptilian shaft","bulbous snake-shaft","bulging snake-dick"]);
			case PenisType.Anemone:
				return StringUtil.randomWord(["anemone dick","tentacle-ringed cock","blue member","stinger-laden shaft","pulsating prick","anemone prick","stinger-coated member",
					"blue cock","tentacle-ringed dick","near-transparent shaft","squirming shaft"]);
			case PenisType.Kangaroo:
				return StringUtil.randomWord(["kangaroo-like dick","pointed cock","marsupial member","tapered shaft","curved pecker","pointed prick","squirming kangaroo-cock",
					"marsupial cock","tapered kangaroo-dick","curved kangaroo-cock","squirming shaft"]);
			case PenisType.Dragon:
				return StringUtil.randomWord(["dragon-like dick","segmented shaft","pointed prick","knotted dragon-cock","mythical mast","segmented tool","draconic dick",
				"draconic cock","tapered dick","unusual endowment","scaly shaft"]);
			case PenisType.Displacer:
				return StringUtil.randomWord(["coerl cock","tentacle-tipped phallus","starfish-tipped shaft","alien member","almost-canine dick","bizarre prick",
					"beastly cock","cthulhu-tier cock","coerl cock","animal dong","star-capped tool","knotted erection"]);
			case PenisType.Avian:
				return StringUtil.randomWord(["bird cock","bird dick","bird pecker","avian cock","avian dick","avian penis","avian prick","avian pecker","tapered cock","tapered prick"]);
			case PenisType.Pig:
				return StringUtil.randomWord(["pig cock","pig dick","pig penis","pig-like cock","pig-like dick","swine cock","swine penis","corkscrew-tipped cock","hoggish cock","pink pig-cock","pink pecker"]);
			case PenisType.Rhino:
				return StringUtil.randomWord(["oblong cock", "oblong dick","oblong prick","rhino cock","rhino dick","rhino penis","rhino pecker","rhino prick","bulged rhino cock","bulged rhino dick"]);
			case PenisType.Wolf:
				return StringUtil.randomWord(["wolf-shaped dong","canine shaft","pointed prick","knotty wolf-shaft","bestial cock","animalistic wolf-pecker","pointed wolf-dick",
					"pointed shaft","canine member","canine cock","knotted wolf-cock"]);
			case PenisType.Echidna:
				return StringUtil.randomWord(["strange echidna dick","strange echidna cock","echidna dick","echidna penis","echidna cock","exotic endowment","four-headed prick",
					"four-headed penis","four-headed cock","four-headed dick"]);
			default: 
				return StringUtil.randomWord(["cock","prick","pecker","shaft"]);
		}
	}
		
	public static function cockAdj(creature:Creature, index:Int = 0):String {
		var isPierced:Bool = (creature.cocks.length == 1) && (creature.cocks[index].isPierced); //Only describe as pierced or sock covered if the creature has just one cock
		var isGooey:Bool = creature.skin.type == SkinType.Goo;
		return Appearance.cockAdjective(creature.cocks[index].type, creature.cocks[index].length, creature.cocks[index].thickness, creature.lust, creature.cumQ(), isPierced, isGooey);
	}

	//New cock adjectives.  The old one sucked dicks
	//This function handles all cockAdjectives. Previously there were separate functions for the player, monsters and NPCs.
	public static function cockAdjective(cockType:PenisType, length:Float, girth:Float, lust:Int = 50, cumQ:Float = 10, isPierced:Bool = false, isGooey:Bool = false):String {
		var options = ["length","girth"];
		if (lust>75) options.push("lust");
		if (isPierced) options.push("pierced");
		if (isGooey) options.push("goo");
		switch(StringUtil.randomWord(options)) {
			case "lust":
				if (lust > 90) { 
					if (cumQ < 50) return StringUtil.randomWord(["throbbing", "pulsating"]); //Weak as shit cum
					if (cumQ < 200) return StringUtil.randomWord(["dribbling", "leaking", "drooling"]); //lots of cum? drippy.
					return StringUtil.randomWord(["very drippy", "pre-gushing", "cum-bubbling", "pre-slicked", "pre-drooling"]); //Tons of cum
				}
				else {//A little less lusty, but still lusty.
					if (cumQ < 50) return StringUtil.randomWord(["turgid", "blood-engorged", "rock-hard", "stiff", "eager"]); //Weak as shit cum
					if (cumQ < 200) return StringUtil.randomWord(["turgid", "blood-engorged", "rock-hard", "stiff", "eager", "fluid-beading", "slowly-oozing"]); //A little drippy
					return StringUtil.randomWord(["dribbling", "drooling", "fluid-leaking", "leaking"]); //uber drippy
				}
			case "pierced": return "pierced";
			case "goo": return StringUtil.randomWord(["goopey", "gooey", "slimy"]);
			case "girth":
				if (girth <= 0.75) return StringUtil.randomWord(["thin", "slender", "narrow"]);
				if (girth <= 1.2) return "ample";
				if (girth <= 1.4) return StringUtil.randomWord(["ample", "big"]);
				if (girth <= 2) return StringUtil.randomWord(["broad", "meaty", "girthy"]);
				if (girth <= 3.5) return StringUtil.randomWord(["fat", "distended", "wide"]);
				return StringUtil.randomWord(["inhumanly distended", "monstrously thick", "bloated"]);
			default:
				if (length < 3) return StringUtil.randomWord(["little", "toy-sized", "mini", "budding", "tiny"]);
				if (length < 5) return StringUtil.randomWord(["short", "small"]);
				if (length < 7) return StringUtil.randomWord(["fair-sized", "nice"]);
				if (length < 9) {
					if (cockType == PenisType.Horse) return StringUtil.randomWord(["sizable", "pony-sized", "colt-like"]);
					return StringUtil.randomWord(["sizable", "long", "lengthy"]);
				}
				if (length < 13) {
					if (cockType == PenisType.Dog) return StringUtil.randomWord(["huge", "foot-long", "mastiff-like"]);
					return StringUtil.randomWord(["huge", "foot-long", "cucumber-length"]);
				}
				if (length < 18) return StringUtil.randomWord(["massive", "knee-length", "forearm-length"]);
				if (length < 30) return StringUtil.randomWord(["enormous", "giant", "arm-like"]);
				if (cockType == PenisType.Tentacle && Std.random(2) == 0) return "coiled";
				return StringUtil.randomWord(["towering", "freakish", "monstrous", "massive"]);
		}
	}
		
	//Cock adjectives for single cock
	private static function cockAdjectives(cockLength:Float, cockThickness:Float, cockType:PenisType, creature:Creature):String
	{
		var description:String = "";
		var descripts: Int = 0;
		if (Std.random(4) == 0) {
			if (cockLength < 3) {
				description = StringUtil.randomWord(["tiny", "toy-sized", "little"]);
			} else if (cockLength < 5) {
				description = StringUtil.randomWord(["short", "small"]);
			} else if (cockLength < 7) {
				description = StringUtil.randomWord(["fair-sized", "nice"]);
			} else if (cockLength < 9) {
				description = StringUtil.randomWord(["long", "lengthy", "sizable"]);
			} else if (cockLength < 13) {
				description = StringUtil.randomWord(["huge", "foot-long"]);
			} else if (cockLength < 18) {
				description = StringUtil.randomWord(["massive", "forearm-length"]);
			} else if (cockLength < 30) {
				description = StringUtil.randomWord(["enormous", "monster-length"]);
			} else {
				description = StringUtil.randomWord(["towering", "freakish", "massive"]);
			}
			descripts = 1;
		} else if (Std.random(4) == 0 && descripts == 0) {
			if (cockThickness <= .75) description += "narrow";
			else if (cockThickness <= 1.1) description += "nice";
			else if (cockThickness <= 1.4) {
				description = StringUtil.randomWord(["ample", "big"]);
			} else if (cockThickness <= 2) {
				description = StringUtil.randomWord(["broad", "girthy"]);
			} else if (cockThickness <= 3.5) {
				description = StringUtil.randomWord(["fat", "distended"]);
			} else {
				description = StringUtil.randomWord(["inhumanly distended", "monstrously thick"]);
			}
			descripts = 1;
		} else if (creature.lust > 90) {
			if (creature.cumQ() > 50 && creature.cumQ() < 200 && Std.random(2) == 0) {
				description += "pre-slickened";
				descripts = 1;
			}
			if (creature.cumQ() >= 200 && Std.random(2) == 0) {
				description += "cum-drooling";
				descripts = 1;
			}
			if (descripts == 0) {
				description = StringUtil.randomWord(["throbbing", "pulsating"]);
				descripts = 1;
			}
		} else if (creature.lust > 75) {
			if (descripts == 0 && creature.cumQ() > 50 && creature.cumQ() < 200 && Std.random(2) == 0) {
				description += "pre-leaking";
				descripts = 1;
			}
			if (descripts == 0 && creature.cumQ() >= 200 && Std.random(2) == 0) {
				description += "pre-cum dripping";
				descripts = 1;
			}
			if (descripts == 0) {
				description = StringUtil.randomWord(["rock-hard", "eager"]);
				descripts = 1;
			}
		}
		//Not lusty at all, fallback adjective
		else if (creature.lust > 50) description += "hard";
		else description += "ready";
		return description;
	}

		//public static function cockMultiNoun(cockType:CockTypesEnum):String
		//{
			///*
			//if (cockType is int) {
				//trace("Someone is still calling cockNoun with an integer cock type");
				//trace("Fix this shit already, dammit!");
				//cockType = CockTypesEnum.ParseConstantByIndex(cockType);
			//}
			//*/
			//var options:Array;
			//var description:String = "";
			//if (cockType == CockTypesEnum.HUMAN) {
				//options = ["cock",
					//"cock",
					//"cock",
					//"cock",
					//"cock",
					//"prick",
					//"prick",
					//"pecker",
					//"shaft",
					//"shaft",
					//"shaft"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.BEE) {
				//options = ["bee prick",
					//"bee prick",
					//"bee prick",
					//"bee prick",
					//"insectoid cock",
					//"insectoid cock",
					//"furred monster"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.DOG) {
				//options = ["doggie dong",
					//"canine shaft",
					//"pointed prick",
					//"dog-shaft",
					//"dog-cock",
					//"puppy-pecker",
					//"dog-dick",
					//"pointed shaft",
					//"canine cock",
					//"canine cock",
					//"dog cock"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.HORSE) {
				//options = ["horsecock",
					//"equine prick",
					//"horse-shaft",
					//"horse-prick",
					//"stallion-prick",
					//"equine dong"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.DEMON) {
				//options = ["demon-dick",
					//"nubby shaft",
					//"corrupted cock",
					//"perverse pecker",
					//"bumpy demon-dick",
					//"demonic cock",
					//"demonic dong",
					//"cursed cock",
					//"infernal prick",
					//"unholy cock",
					//"blighted cock"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.TENTACLE) {
				//options = ["tentacle prick",
					//"plant-like shaft",
					//"tentacle cock",
					//"cock-tendril",
					//"tentacle pecker",
					//"plant prick",
					//"penile flora",
					//"smooth inhuman shaft",
					//"tentacle dick",
					//"vine prick",
					//"vine-like cock"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.CAT) {
				//options = ["feline dick",
					//"cat-cock",
					//"kitty-cock",
					//"spiny prick",
					//"pussy-prick",
					//"cat-penis",
					//"feline member",
					//"spined shaft",
					//"feline shaft",
					//"'barbed' dick",
					//"kitten-prick"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.LIZARD) {
				//options = ["reptile-dick",
					//"purple cock",
					//"inhuman cock",
					//"reptilian prick",
					//"purple prick",
					//"purple member",
					//"serpentine member",
					//"serpentine shaft",
					//"reptilian shaft",
					//"snake-shaft",
					//"snake dick"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.RHINO) {
				//options = ["oblong cock",
					//"rhino dick",
					//"rhino cock",
					//"bulged rhino cock",
					//"rhino penis",
					//"rhink dong",
					//"oblong penis",
					//"oblong dong",
					//"oblong dick"];
				//description += StringUtil.randomWord(options);
			//}
			//else if (cockType == CockTypesEnum.WOLF) {
				//options = ["wolf dong",
					//"canine shaft",
					//"pointed prick",
					//"wolf-shaft",
					//"wolf-cock",
					//"wolf-pecker",
					//"wolf-dick",
					//"pointed shaft",
					//"canine cock",
					//"canine cock",
					//"wolf cock"];
				//description += StringUtil.randomWord(options);
			//}
			//else {
				//description += StringUtil.randomWord("cock", "prick", "pecker", "shaft");
			//}
			//return description;
		//}
//
	/**
	 * Describe creatures balls.
	 * @param    i_forcedSize    Force a description of the size of the balls
	 * @param    plural        Show plural forms
	 * @param    i_creature        Monster, Player
	 * @param    i_withArticle    Show description with article in front
	 * @return    Full description of balls
	 */
	public static function ballsDescription(forcedSize:Bool, plural:Bool, creature:Creature, withArticle:Bool = false):String
	{
		if (creature.balls == 0) return "prostate";
		
		var haveDescription:Bool = false;
		var description:String = "";
		
		if (plural /* && !creature.hasStatusEffect(StatusEffects.Uniball)*/) {
			if (creature.balls == 1) {
				if (withArticle)
					description += StringUtil.randomWord(["a single","a solitary","a lone","an individual"]);
				else
					description += StringUtil.randomWord(["single","solitary","lone","individual"]);
			}
			else if (creature.balls == 2) {
				if (withArticle)
					description += StringUtil.randomWord(["a pair of", "two", "a duo of"]);
				else
					description += StringUtil.randomWord(["pair of","two","duo of"]);
			}
			else if (creature.balls == 3) {
				if (withArticle)
					description += StringUtil.randomWord(["three","triple", "a trio of"]);
				else
					description += StringUtil.randomWord(["three","triple", "trio of"]);
			}
			else if (creature.balls == 4) {
				if (withArticle)
					description += StringUtil.randomWord(["four","quadruple", "a quartette of"]);
				else
					description += StringUtil.randomWord(["four","quadruple", "quartette of"]);
			}
			else {
				if (withArticle)
					description += StringUtil.randomWord(["a multitude of","many","a large handful of"]);
				else
					description += StringUtil.randomWord(["multitude of","many","large handful of"]);
			}
		}
		//size!
		if (creature.ballSize > 1 && (Std.random(3) <= 1 || forcedSize)) {
			if (!StringUtil.isNullOrEmpty(description)) description += " ";
			
			if (creature.ballSize >= 18)
				description += "hideously swollen and oversized";
			else if (creature.ballSize >= 15)
				description += "beachball-sized";
			else if (creature.ballSize >= 12)
				description += "watermelon-sized";
			else if (creature.ballSize >= 9)
				description += "basketball-sized";
			else if (creature.ballSize >= 7)
				description += "soccerball-sized";
			else if (creature.ballSize >= 5)
				description += "cantaloupe-sized";
			else if (creature.ballSize >= 4)
				description += "grapefruit-sized";
			else if (creature.ballSize >= 3)
				description += "apple-sized";
			else if (creature.ballSize >= 2)
				description += "baseball-sized";
			else if (creature.ballSize > 1)
				description += "large";
			else if (creature.ballSize < 1)
				description += "small";
		}
		//UNIBALL
		//if (creature.hasStatusEffect(StatusEffects.Uniball)) {
			//if (description) description += " ";
			//options = ["tightly-compressed",
				//"snug",
				//"cute",
				//"pleasantly squeezed",
				//"compressed-together"];
			//description += StringUtil.randomWord(options);
		//}
		//Descriptive
		if (creature.hoursSinceCum >= 48 && Std.random(2) == 0 && !forcedSize) {
			if (!StringUtil.isNullOrEmpty(description)) description += " ";
			
			description += StringUtil.randomWord(["overflowing","swollen","cum-engorged"]);
		}
		//lusty
		if (creature.lust > 90 && (description == "") && Std.random(2) == 0 && !forcedSize) {
			description += StringUtil.randomWord(["eager","full","needy","desperate","throbbing","heated","trembling","quivering","quaking"]);
		}
		//Slimy skin
		if (creature.skin.type == SkinType.Goo) {
			if (!StringUtil.isNullOrEmpty(description)) description += " ";
			
			description += StringUtil.randomWord(["goopey","gooey","slimy"]);
		}
		if (!StringUtil.isNullOrEmpty(description)) description += " ";
		
		description += StringUtil.randomWord(["nut","gonad","teste","testicle","testicle","ball","ball","ball"]);
		if (plural) description += "s";

		//if (creature.hasStatusEffect(StatusEffects.Uniball) && rand(2) == 0) {
			//if (rand(3) == 0)
				//description += " merged into a cute, spherical package";
			//else if (rand(2) == 0)
				//description += " combined into a round, girlish shape";
			//else
				//description += " squeezed together into a perky, rounded form";
		//}
		return description;
	}

	//Returns random description of scrotum
	public static function sackDescript(creature:Creature):String
	{
		if (creature.balls == 0) return "prostate";
		
		return StringUtil.randomWord(["scrotum", "sack", "nutsack", "ballsack", "beanbag", "pouch"]);
	}

	public static function sheathDescript(character:Character):String
	{
		if (character.hasSheath()) return "sheath";
		else return "base";
	}

	public static function vaginaDescript(creature:Creature, vaginaIndex:Int = 0, forceDesc:Bool=false):String
	{
		var description:String = "";
		var weighting:Float = 0;
		var haveDescription:Bool = false;
		var vagina = creature.vaginas[vaginaIndex];

		//Very confusing way to display values.
		if (vagina.vaginalLooseness == VaginaLooseness.Tight) weighting = 61;
		if (vagina.vaginalLooseness == VaginaLooseness.GapingWide || vagina.vaginalLooseness == VaginaLooseness.ClownCar) weighting = 10;

		//tightness descript - 40% display rate
		if (forceDesc || Std.random(100) + weighting > 60) {
			if (vagina.vaginalLooseness == VaginaLooseness.Tight) {
				if (vagina.virgin) description += "virgin";
				else description += "tight";
			}
			if (vagina.vaginalLooseness == VaginaLooseness.Loose)
				description += "loose";
			if (vagina.vaginalLooseness == VaginaLooseness.Gaping)
				description += "very loose";
			if (vagina.vaginalLooseness == VaginaLooseness.GapingWide)
				description += "gaping";
			if (vagina.vaginalLooseness == VaginaLooseness.ClownCar)
				description += "gaping-wide";

		}
		//wetness descript - 30% display rate
		if (forceDesc || Std.random(100) + weighting > 70) {
			if (description != "") description += ", ";
			//if (creature.hasStatusEffect(StatusEffects.ParasiteEel)){
				//if (creature.statusEffectv1(StatusEffects.ParasiteEel) == 1) description += "ooze dripping";
				//else if (creature.statusEffectv1(StatusEffects.ParasiteEel) >= 2 && creature.statusEffectv1(StatusEffects.ParasiteEel) <= 4) description += "ooze drooling";
				//else description += "ooze bloated";
			//}else{
				switch(vagina.vaginalWetness){
					case VaginaWetness.Dry:
						description += "dry";
					case VaginaWetness.Normal:
						description += "moist";
					case VaginaWetness.Wet:
						description += "wet";
					case VaginaWetness.Slick:
						description += "slick";
					case VaginaWetness.Drooling:
						description += "drooling";
					case VaginaWetness.Slavering:
						description += "slavering";
				}
			//}
		}
		if (vagina.labiaPierced > 0 && (forceDesc || Std.random(3) == 0)) {
			if (description != "") description += ", ";
			description += "pierced";
		}
		if (description == "" && creature.skin.type == SkinType.Goo) {
			if (description != "")
				description += ", ";
			
			description += StringUtil.randomWord(["gooey", "slimy"]);
		}
		if (vagina.type == VaginaType.BlackSandTrap && (forceDesc || Std.random(2) == 0)) {
			if (description != "") description += ", ";
			description += StringUtil.randomWord(["black", "onyx","ebony","dusky","sable", "obsidian", "midnight-hued","jet black"]);
		}

		if (description != "")
			description += " ";
			
		description +=  StringUtil.randomWord(["vagina", "pussy", "cooter", "twat", "cunt", "snatch", "fuck-hole", "muff"]);
		
		return description;
	}
		
		//public static function clitDescription(i_creature:Creature):String
		//{
			//var description:String = "";
			//var options:Array;
			//var haveDescription:Boolean = false;
			////Length Adjective - 50% chance
			//if (rand(2) == 0) {
				////small clits!
				//if (i_creature.getClitLength() <= .5) {
					//options = ["tiny ",
						//"little ",
						//"petite ",
						//"diminutive ",
						//"miniature "];
					//description += StringUtil.randomWord(options);
				//}
				////"average".
				//if (i_creature.getClitLength() > .5 && i_creature.getClitLength() < 1.5) {
					////no size comment
				//}
				////Biggies!
				//if (i_creature.getClitLength() >= 1.5 && i_creature.getClitLength() < 4) {
					//options = ["large ",
						//"large ",
						//"substantial ",
						//"substantial ",
						//"considerable "];
					//description += StringUtil.randomWord(options);
				//}
				////'Uge
				//if (i_creature.getClitLength() >= 4) {
					//options = ["monster ",
						//"tremendous ",
						//"colossal ",
						//"enormous ",
						//"bulky "];
					//description += StringUtil.randomWord(options);
				//}
			//}
			////Descriptive descriptions - 50% chance of being called
			//if (rand(2) == 0) {
				////Doggie descriptors - 50%
				////TODO Conditionals don't make sense, need to introduce a class variable to keep of "something" or move race or Creature/Character
				//if (i_creature.hasFur() > 2 && !haveDescription && rand(2) == 0) {
					//description += "bitch-";
					//haveDescription = true;
				//}
				///*Horse descriptors - 50%
				 //if (creature.hasFur() > 2 && !descripted && rand(2) == 0) {
				 //descripted = true;
				 //descript += "mare-";
				 //}*/
				////Horny descriptors - 75% chance
				//if (i_creature.lust100 > 70 && rand(4) < 3 && !haveDescription) {
					//options = ["throbbing ",
						//"pulsating ",
						//"hard "];
					//description += StringUtil.randomWord(options);
					//haveDescription = true;
				//}
				////High libido - always use if no other descript
				//if (i_creature.lib100 > 50 && rand(2) == 0 && !haveDescription) {
					//options = ["insatiable ",
						//"greedy ",
						//"demanding ",
						//"rapacious"];
					//description += StringUtil.randomWord(options);
					//haveDescription = true;
				//}
			//}
			//if (i_creature.hasVagina()) {
				//if (!haveDescription && i_creature.vaginas[0].clitPierced > 0) {
					//description += "pierced ";
					//haveDescription = true;
				//}
			//}
			//else {
				//CoC_Settings.error("ERROR: CLITDESCRIPT WITH NO CLIT");
				//return("ERROR: CLITDESCRIPT WITH NO CLIT");
			//}
//
			////Clit nouns
			//options = ["clit",
				//"clitty",
				//"button",
				//"pleasure-buzzer",
				//"clit",
				//"clitty",
				//"button",
				//"clit",
				//"clit",
				//"button"];
			//if (kGAMECLASS.flags[kFLAGS.SFW_MODE] > 0) {
				//options = ["bump", "button"];
			//}
			//description += StringUtil.randomWord(options);
//
			//return description;
		//}
		
		/**
		 * Gives a full description of a Character's butt.
		 * Be aware that it only supports Characters, not all Creatures.
		 * @param    i_character
		 * @return    A full description of a Character's butt.
		 */
		public static function buttDescription(character:Character):String
		{
			var description:String = "";
			if (character.buttRating <= 1) {
				if (character.tone >= 60)
					description += "incredibly tight, perky ";
				else {
					description = StringUtil.randomWord(["tiny", "very small", "dainty"]);
					if (character.tone <= 30 && Std.random(3) == 0) description += " yet soft";
					description += " ";
				}
			}
			if (character.buttRating > 1 && character.buttRating < 4) {
				if (character.tone >= 65)
					description = StringUtil.randomWord(["perky, muscular ", "tight, toned ", "compact, muscular ","tight ","muscular, toned "]);
				else if (character.tone >= 30)
					description = StringUtil.randomWord(["tight ","firm ","compact ","petite "]);
				else 
					description = StringUtil.randomWord(["small, heart-shaped ","soft, compact ","soft, heart-shaped ","small, cushy ","small ","petite ","snug " ]);
			}
			if (character.buttRating >= 4 && character.buttRating < 6) {
				if (character.tone >= 65)
					description = StringUtil.randomWord(["nicely muscled ","nice, toned ","muscly ","nice toned ","toned ","fair "]);
				else if (character.tone >= 30)
					description = StringUtil.randomWord(["nice ", "fair "]);
				else 
					description = StringUtil.randomWord(["nice, cushiony ", "soft ","nicely-rounded, heart-shaped ","cushy ","soft, squeezable "]);
			}
			if (character.buttRating >= 6 && character.buttRating < 8) {
				if (character.tone >= 65)
					description = StringUtil.randomWord(["full, toned ","muscly handful of ","shapely, toned ","muscular, hand-filling ","shapely, chiseled ","full ","chiseled "]);
				else if (character.tone >= 30)
					description = StringUtil.randomWord(["handful of ","full ","shapely ","hand-filling "]);
				else {
					if (Std.random(8) == 0) return "supple, handful of ass";
					description = StringUtil.randomWord(["somewhat jiggly ","soft, hand-filling ","cushiony, full ","plush, shapely ","full ","soft, shapely ","rounded, spongy "]);
				}
			}
			if (character.buttRating >= 8 && character.buttRating < 10) {
				if (character.tone >= 65)
					description = StringUtil.randomWord(["large, muscular ","substantial, toned ","big-but-tight ","squeezable, toned ","large, brawny ","big-but-fit ","powerful, squeezable ","large "]);
				else if (character.tone >= 30)
					description = StringUtil.randomWord(["squeezable ","large ","substantial "]);
				else
					description = StringUtil.randomWord(["large, bouncy ","soft, eye-catching ","big, slappable ","soft, pinchable ","large, plush ","squeezable ","cushiony ","plush ","pleasantly plump "]);
			}
			if (character.buttRating >= 10 && character.buttRating < 13) {
				if (character.tone >= 65)
					description = StringUtil.randomWord(["thick, muscular ","big, burly ","heavy, powerful ","spacious, muscular ","toned, cloth-straining ","thick ","thick, strong "]);
				else if (character.tone >= 30)
					description = StringUtil.randomWord(["jiggling ","spacious ","heavy ","cloth-straining "]);
				else
					description = StringUtil.randomWord(["super-soft, jiggling ", "spacious, cushy ", "plush, cloth-straining ", "squeezable, over-sized ", "spacious ",
						"heavy, cushiony ","slappable, thick ","jiggling ","spacious ","soft, plump "]);
			}
			if (character.buttRating >= 13 && character.buttRating < 16) {
				//TOIGHT LIKE A TIGER
				if (character.tone >= 65) 
					description = StringUtil.randomWord( ["expansive, muscled ","voluminous, rippling ","generous, powerful ","big, burly ","well-built, voluminous ","powerful ","muscular ","powerful, expansive "]);
				else if (character.tone >= 30)
					description = StringUtil.randomWord( ["expansive ","generous ","voluminous ","wide "]);
				else 
					description = StringUtil.randomWord(["pillow-like ","generous, cushiony ","wide, plush ","soft, generous ","expansive, squeezable ","slappable ","thickly-padded ",
						"wide, jiggling ","wide ","voluminous ","soft, padded "]);
			}
			if (character.buttRating >= 16 && character.buttRating < 20) {
				if (character.tone >= 65)
					description = StringUtil.randomWord(["huge, toned ","vast, muscular ","vast, well-built ","huge, muscular ","strong, immense ","muscle-bound "]);
				else if (character.tone >= 30){
					if (Std.random(5) == 0) return "jiggling expanse of ass";
					if (Std.random(5) == 0) return "copious ass-flesh";
					description = StringUtil.randomWord(["huge ","vast ","giant "]);
				} else
					description = StringUtil.randomWord( ["vast, cushiony ", "huge, plump ","expansive, jiggling ","huge, cushiony ","huge, slappable ","seam-bursting ",
						"plush, vast ","giant, slappable ","giant ","huge ","swollen, pillow-like "]);
			}
			if (character.buttRating >= 20) {
				if (character.tone >= 65) {
					if (Std.random(7) == 0) return "colossal, muscly ass";
					description = StringUtil.randomWord(["ginormous, muscle-bound ","colossal yet toned ","strong, tremendously large ","tremendous, muscled ","ginormous, toned ","colossal, well-defined "]);
				} else if (character.tone >= 30)
					description = StringUtil.randomWord(["ginormous ","colossal ","tremendous ","gigantic "]);
				else
					description = StringUtil.randomWord(["ginormous, jiggly ","plush, ginormous ","seam-destroying ","tremendous, rounded ","bouncy, colossal ","thong-devouring ",
						"tremendous, thickly padded ","ginormous, slappable ","gigantic, rippling ","gigantic ","ginormous ","colossal ","tremendous "]);
			}
			
			description += StringUtil.randomWord(["butt", "butt", "butt", "butt", "ass", "ass", "ass", "ass", "backside", "backside", "derriere", "rump", "bottom"]);
			
			return description;
		}

		///**
		 //* Gives a short description of a creature's butt.
		 //* Different from buttDescription in that it supports all creatures, not just characters.
		 //* Warning, very judgemental.
		 //* @param    creature
		 //* @return Short description of a butt.
		 //*/
		//public static function buttDescriptionShort(i_creature:Creature):String
		//{
			//var description:String = "";
			//var options:Array;
			//if (i_creature.buttRating <= 1) {
				//options = ["insignificant ",
					//"very small "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating > 1 && i_creature.buttRating < 4) {
				//options = ["tight ",
					//"firm ",
					//"compact "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating >= 4 && i_creature.buttRating < 6) {
				//options = ["regular ",
					//"unremarkable "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating >= 6 && i_creature.buttRating < 8) {
				//if (rand(3) == 0) return "handful of ass";
				//options = ["full ",
					//"shapely "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating >= 8 && i_creature.buttRating < 10) {
				//options = ["squeezable ",
					//"large ",
					//"substantial "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating >= 10 && i_creature.buttRating < 13) {
				//options = ["jiggling ",
					//"spacious ",
					//"heavy "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating >= 13 && i_creature.buttRating < 16) {
				//if (rand(3) == 0) return "generous amount of ass";
				//options = ["expansive ",
					//"voluminous "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating >= 16 && i_creature.buttRating < 20) {
				//if (rand(3) == 2) return "jiggling expanse of ass";
				//options = ["huge ",
					//"vast "];
				//description = StringUtil.randomWord(options);
			//}
			//if (i_creature.buttRating >= 20) {
				//options = ["ginormous ",
					//"colossal ",
					//"tremendous "];
				//description = StringUtil.randomWord(options);
			//}
			//options = ["butt ",
				//"ass "];
			//description += StringUtil.randomWord(options);
			//if (rand(2) == 0) description += "cheeks";
			//return description;
		//}
		
		public static function assholeDescript(creature:Creature, forceDesc:Bool=false):String
		{
			var description:String = "";
			
			if (forceDesc || Std.random(3) <= 1)
			{
				switch(creature.ass.analWetness)
				{
					case AnalWetness.Moist:
						description += "moist ";
					case AnalWetness.Slimy:
						description += "slimy ";
					case AnalWetness.Drooling:
						description += "drooling ";
					case AnalWetness.SlimeDrooling:
						description += "slime-drooling ";
					default:
				}
			}
			
			//25% tightness description
			if (forceDesc || Std.random(4) == 0 || (creature.ass.analLooseness.getIndex() <= 1 && Std.random(4) <= 2)) 
			{
				switch(creature.ass.analLooseness)
				{
					case AnalLooseness.Virgin:
						description += "virgin ";
					case AnalLooseness.Tight:
						description += "tight ";
					case AnalLooseness.Normal:
						description += "loose ";
					case AnalLooseness.Loose:
						description += "roomy ";
					case AnalLooseness.Stretched:
						description += "stretched ";
					case AnalLooseness.Gaping:
						description += "gaping ";
				}
			}
			
			description += StringUtil.randomWord(["anus","pucker","backdoor","asshole","butthole", "starfish", "bunghole"]);
			return description;
		}
		
		//public static function wingsDescript(i_creature:Creature):String
		//{
			//return DEFAULT_WING_NAMES[i_creature.wingType] + " wings";
		//}
//
		//public static function eyesDescript(i_creature:Creature):String
		//{
			//return DEFAULT_EYES_NAMES[i_creature.eyeType] + " eyes";
		//}
//
		//public static function nagaLowerBodyColor2(i_creature:Creature):String
		//{
			//if (i_creature.underBody.skin.tone in NAGA_LOWER_BODY_COLORS)
				//return NAGA_LOWER_BODY_COLORS[i_creature.underBody.skin.tone];
//
			//return i_creature.underBody.skin.tone;
		//}
		
		public static var BREAST_CUP_NAMES:Array<String> = [
			"flat",//0
			//				1			2			3			4			5				6			7		8			9
			"A-cup", "B-cup", "C-cup", "D-cup", "DD-cup", "big DD-cup", "E-cup", "big E-cup", "EE-cup",// 1-9
			"big EE-cup", "F-cup", "big F-cup", "FF-cup", "big FF-cup", "G-cup", "big G-cup", "GG-cup", "big GG-cup", "H-cup",//10-19
			"big H-cup", "HH-cup", "big HH-cup", "HHH-cup", "I-cup", "big I-cup", "II-cup", "big II-cup", "J-cup", "big J-cup",//20-29
			"JJ-cup", "big JJ-cup", "K-cup", "big K-cup", "KK-cup", "big KK-cup", "L-cup", "big L-cup", "LL-cup", "big LL-cup",//30-39
			"M-cup", "big M-cup", "MM-cup", "big MM-cup", "MMM-cup", "large MMM-cup", "N-cup", "large N-cup", "NN-cup", "large NN-cup",//40-49
			"O-cup", "large O-cup", "OO-cup", "large OO-cup", "P-cup", "large P-cup", "PP-cup", "large PP-cup", "Q-cup", "large Q-cup",//50-59
			"QQ-cup", "large QQ-cup", "R-cup", "large R-cup", "RR-cup", "large RR-cup", "S-cup", "large S-cup", "SS-cup", "large SS-cup",//60-69
			"T-cup", "large T-cup", "TT-cup", "large TT-cup", "U-cup", "large U-cup", "UU-cup", "large UU-cup", "V-cup", "large V-cup",//70-79
			"VV-cup", "large VV-cup", "W-cup", "large W-cup", "WW-cup", "large WW-cup", "X-cup", "large X-cup", "XX-cup", "large XX-cup",//80-89
			"Y-cup", "large Y-cup", "YY-cup", "large YY-cup", "Z-cup", "large Z-cup", "ZZ-cup", "large ZZ-cup", "ZZZ-cup", "large ZZZ-cup",//90-99
			//HYPER ZONE
			"hyper A-cup", "hyper B-cup", "hyper C-cup", "hyper D-cup", "hyper DD-cup", "hyper big DD-cup", "hyper E-cup", "hyper big E-cup", "hyper EE-cup",//100-109
			"hyper big EE-cup", "hyper F-cup", "hyper big F-cup", "hyper FF-cup", "hyper big FF-cup", "hyper G-cup", "hyper big G-cup", "hyper GG-cup", "hyper big GG-cup", "hyper H-cup",//110-119
			"hyper big H-cup", "hyper HH-cup", "hyper big HH-cup", "hyper HHH-cup", "hyper I-cup", "hyper big I-cup", "hyper II-cup", "hyper big II-cup", "hyper J-cup", "hyper big J-cup",//120-129
			"hyper JJ-cup", "hyper big JJ-cup", "hyper K-cup", "hyper big K-cup", "hyper KK-cup", "hyper big KK-cup", "hyper L-cup", "hyper big L-cup", "hyper LL-cup", "hyper big LL-cup",//130-139
			"hyper M-cup", "hyper big M-cup", "hyper MM-cup", "hyper big MM-cup", "hyper MMM-cup", "hyper large MMM-cup", "hyper N-cup", "hyper large N-cup", "hyper NN-cup", "hyper large NN-cup",//140-149
			"hyper O-cup", "hyper large O-cup", "hyper OO-cup", "hyper large OO-cup", "hyper P-cup", "hyper large P-cup", "hyper PP-cup", "hyper large PP-cup", "hyper Q-cup", "hyper large Q-cup",//150-159
			"hyper QQ-cup", "hyper large QQ-cup", "hyper R-cup", "hyper large R-cup", "hyper RR-cup", "hyper large RR-cup", "hyper S-cup", "hyper large S-cup", "hyper SS-cup", "hyper large SS-cup",//160-169
			"hyper T-cup", "hyper large T-cup", "hyper TT-cup", "hyper large TT-cup", "hyper U-cup", "hyper large U-cup", "hyper UU-cup", "hyper large UU-cup", "hyper V-cup", "hyper large V-cup",//170-179
			"hyper VV-cup", "hyper large VV-cup", "hyper W-cup", "hyper large W-cup", "hyper WW-cup", "hyper large WW-cup", "hyper X-cup", "hyper large X-cup", "hyper XX-cup", "hyper large XX-cup",//180-189
			"hyper Y-cup", "hyper large Y-cup", "hyper YY-cup", "hyper large YY-cup", "hyper Z-cup", "hyper large Z-cup", "hyper ZZ-cup", "hyper large ZZ-cup", "hyper ZZZ-cup", "hyper large ZZZ-cup",//190-199
			"jacques00-cup"
		];
		
		public static function breastCup(size:BreastSize):String
		{
			return BREAST_CUP_NAMES[size.getIndex()];
		}
		
		///**
		 //* Returns breast size from cup name.
		 //* Acceptable input: "flat","A","B","C","D","DD","DD+",... "ZZZ","ZZZ+" or exact match from BREAST_CUP_NAMES array
		 //*/
		//public static function breastCupInverse(name:String, defaultValue:Number = 0):Number
		//{
			//if (name.length == 0) return defaultValue;
			//if (name == "flat") return 0;
			//var big:Boolean = name.charAt(name.length - 1) == "+";
			//if (big) name = name.substr(0, name.length - 1);
			//for (var i:int = 0; i < BREAST_CUP_NAMES.length; i++) {
				//if (name == BREAST_CUP_NAMES[i]) return i;
				//if (BREAST_CUP_NAMES[i].indexOf(name) == 0) return i + (big ? 1 : 0);
			//}
			//return defaultValue;
		//}
//
		//public static function createMapFromPairs(src:Array):Object
		//{
			//var result:Object = {};
			//for (var i:int = 0; i < src.length; i++) result[src[i][0]] = src[i][1];
			//return result;
		//}
//
		//
		//public static const DEFAULT_GENDER_NAMES:Object = createMapFromPairs(
				//[
					//[GENDER_NONE, "genderless"],
					//[GENDER_MALE, "male"],
					//[GENDER_FEMALE, "female"],
					//[GENDER_HERM, "hermaphrodite"],
				//]
		//);

		///**
		 //* Assume scale = [[0,"small"],[5,"average"],[10,"big"]]
		 //*      value < 0   ->   "less than small"
		 //*      value = 0   ->   "small"
		 //*  0 < value < 5   ->   "between small and average"
		 //*      value = 5   ->   "average"
		 //*  5 < value < 10  ->   "between average and big"
		 //*      value = 10  ->   "big"
		 //*      value > 10  ->   "more than big"
		 //*/
		//public static function describeByScale(value:Number, scale:Array, lessThan:String = "less than", moreThan:String = "more than"):String
		//{
			//if (scale.length == 0) return "indescribable";
			//if (scale.length == 1) return "about " + scale[0][1];
			//if (value < scale[0][0]) return lessThan + " " + scale[0][1];
			//if (value == scale[0][0]) return scale[0][1];
			//for (var i:int = 1; i < scale.length; i++) {
				//if (value < scale[i][0]) return "between " + scale[i - 1][1] + " and " + scale[i][1];
				//if (value == scale[i][0]) return scale[i][1];
			//}
			//return moreThan + " " + scale[scale.length - 1][1];
		//}
		
	public static function legs(creature:Creature):String
	{
		var select:Int = 0;
		if (creature.lowerBody.type == LowerBodyType.Drider)
			return "[legcount] spider legs";
		if (creature.isTaur())
			return "[legcount] legs";
			
		switch(creature.lowerBody.type)
		{
			case LowerBodyType.Naga:
				return "snake-like coils";
			case LowerBodyType.Goo:
				return "mounds of goo";
			case LowerBodyType.Pony:
				return "cute pony-legs";
			case LowerBodyType.Bunny:
				select = Std.random(5);
				if (select == 0)
					return "fuzzy, bunny legs";
				else if (select == 1)
					return "fur-covered legs";
				else if (select == 2)
					return "furry legs";
				else
					return "legs";
			case LowerBodyType.Harpy:
				select = Std.random(5);
				if (select == 0)
					return "bird-like legs";
				else if (select == 1)
					return "feathered legs";
				else
					return "legs";
			case LowerBodyType.Fox:
				select = Std.random(4);
				if (select == 0)
					return "fox-like legs";
				else if (select == 1)
					return "vulpine legs";
				else 
					return "legs";
			case LowerBodyType.Racoon:
				select = Std.random(4);
				if (select == 0)
					return "raccoon-like legs";
				else
					return "legs";
			case LowerBodyType.ClovenHoofed:
				select = Std.random(4);
				if (select == 0)
					return "pig-like legs";
				else if (select == 1)
					return "legs";
				else if (select == 2)
					return "legs";
				else
					return "swine legs";
			default:
				return "legs";
		}
	}
	
	public static function leg(creature:Creature):String
	{
		var select:Int = 0;
		switch(creature.lowerBody.type)
		{
			case LowerBodyType.Naga:
				return "snake-tail";
			case LowerBodyType.Goo:
				return "mound of goo";
			case LowerBodyType.Hoofed:
				return "equine leg";
			case LowerBodyType.Pony:
				return "cartoonish pony-leg";
			case LowerBodyType.Bunny:
				select = Std.random(5);
				if (select == 0)
					return "fuzzy, bunny leg";
				else if (select == 1)
					return "fur-covered leg";
				else if (select == 2)
					return "furry leg";
				else
					return "leg";
			case LowerBodyType.Harpy:
				select = Std.random(5);
				if (select == 0)
					return "bird-like leg";
				else if (select == 1)
					return "feathered leg";
				else
					return "leg";
			case LowerBodyType.Fox:
				select = Std.random(4);
				if (select == 0)
					return "fox-like leg";
				else if (select == 1)
					return "vulpine leg";
				else 
					return "leg";
			case LowerBodyType.Racoon:
				select = Std.random(4);
				if (select == 0)
					return "raccoon-like leg";
				else
					return "leg";
			case LowerBodyType.ClovenHoofed:
				select = Std.random(4);
				if (select == 0)
					return "pig-like leg";
				else if (select == 1)
					return "leg";
				else if (select == 2)
					return "leg";
				else
					return "swine leg";
			default:
				return "leg";
		}
	}
		
		//public static function allBreastsDescript(creature:Creature):String
		//{
			//var storage:String = "";
			//if (creature.breastRows.length == 0) return "unremarkable chest muscles ";
			//if (creature.breastRows.length == 2) {
				////if (creature.totalBreasts() == 4) storage += "quartet of ";
				//storage += "two rows of ";
			//}
			//if (creature.breastRows.length == 3) {
				//if (rand(2) == 0) storage += "three rows of ";
				//else storage += "multi-layered ";
			//}
			//if (creature.breastRows.length == 4) {
				//if (rand(2) == 0) storage += "four rows of ";
				//else storage += "four-tiered ";
			//}
			//if (creature.breastRows.length == 5) {
				//if (rand(2) == 0) storage += "five rows of ";
				//else storage += "five-tiered ";
			//}
			//storage += biggestBreastSizeDescript(creature);
			//return storage;
//
		//}
		//
		//public static function tailDescript(i_creature:Creature):String
		//{
			//if (i_creature.tailType == TAIL_TYPE_NONE)
			//{
				//trace("WARNING: Creature has no tails to describe.");
				//return "<b>!Creature has no tails to describe!</b>";
			//}
			//
			//var descript:String = "";
			//
			//if (i_creature.tailType == TAIL_TYPE_FOX && i_creature.tailVenom >= 1)
			//{
				//// Kitsune tails, we're using tailVenom to track tail count
				//if (i_creature.tailVenom > 1)
				//{
					//if (i_creature.tailVenom == 2) descript += "pair ";
					//else if (i_creature.tailVenom == 3) descript += "trio ";
					//else if (i_creature.tailVenom == 4) descript += "quartet ";
					//else if (i_creature.tailVenom == 5) descript += "quintet ";
					//else if (i_creature.tailVenom > 5) descript += "bundle ";
					//
					//descript += "of kitsune tails";
				//}
				//else descript += "kitsune tail";
			//}
			//else
			//{
				//descript += DEFAULT_TAIL_NAMES[i_creature.tailType];
				//descript += " tail";
			//}
			//
			//return descript;
		//}
		//
		//public static function oneTailDescript(i_creature:Creature):String
		//{
			//if (i_creature.tailType == TAIL_TYPE_NONE)
			//{
				//trace("WARNING: Creature has no tails to describe.");
				//return "<b>!Creature has no tails to describe!</b>";
			//}
			//
			//var descript:String = "";
			//
			//if (i_creature.tailType == TAIL_TYPE_FOX && i_creature.tailVenom >= 1)
			//{
				//if (i_creature.tailVenom == 1)
				//{
					//descript += "your kitsune tail";
				//}
				//else
				//{
					//descript += "one of your kitsune tails";
				//}
			//}
			//else
			//{
				//descript += "your " + DEFAULT_TAIL_NAMES[i_creature.tailType] + " tail";
			//}
			//
			//return descript;
		//}
//
		//public static function biggestBreastSizeDescript(creature:Creature):String
		//{
			//var temp14:int = Math.random() * 3;
			//var descript:String = "";
			//var temp142:int = creature.biggestTitRow();
			////ERROR PREVENTION
			//if (creature.breastRows.length - 1 < temp142) {
				//CoC_Settings.error("");
				//return "<b>ERROR, biggestBreastSizeDescript() working with invalid breastRow</b>";
			//}
			//else if (temp142 < 0) {
				//CoC_Settings.error("");
				//return "ERROR SHIT SON!  BIGGESTBREASTSIZEDESCRIPT PASSED NEGATIVE!";
			//}
			//if (creature.breastRows[temp142].breastRating < 1) return "flat breasts";
			////50% of the time size-descript them
			//if (rand(2) == 0) descript += breastSize(creature.breastRows[temp142].breastRating);
			////Nouns!
			//temp14 = rand(10);
			//if (temp14 == 0) descript += "breasts";
			//if (temp14 == 1) {
				//if (creature.breastRows[temp142].lactationMultiplier > 2) descript += "milk-udders";
				//else descript += "breasts";
			//}
			//if (temp14 == 2) {
				//if (creature.breastRows[temp142].lactationMultiplier > 1.5) descript += "milky ";
				//if (creature.breastRows[temp142].breastRating > 4) descript += "tits";
				//else descript += "breasts";
			//}
			//if (temp14 == 3) {
				////if (creature.breastRows[temp142].breastRating > 6) descript += "rack";
				//descript += "breasts";
			//}
			//if (temp14 == 4) descript += "tits";
			//if (temp14 == 5) descript += "tits";
			//if (temp14 == 6) descript += "tits";
			//if (temp14 == 7) {
				//if (creature.breastRows[temp142].lactationMultiplier >= 1 && creature.breastRows[temp142].lactationMultiplier < 2.5) descript += "milk jugs";
				//if (creature.breastRows[temp142].lactationMultiplier >= 2.5) descript += "udders";
				//if (creature.breastRows[temp142].lactationMultiplier < 1) descript += "jugs";
			//}
			//if (temp14 == 8) {
				//if (creature.breastRows[temp142].breastRating > 6) descript += "love-pillows";
				//else descript += "boobs";
			//}
			//if (temp14 == 9) {
				//if (creature.breastRows[temp142].breastRating > 6) descript += "tits";
				//else descript += "breasts";
			//}
			//return descript;
		//}
//
	public static function breastSize(size:BreastSize):String
	{
		var val = size.getIndex();
		var descript:String = "";
		//Catch all for dudes.
		if (val < 1) return "manly ";
		//Small - A->B
		if (val <= 2) {
			descript += StringUtil.randomWord(["palmable ", "tight ", "perky ", "baseball-sized "]);
		}
		//C-D
		else if (val <= 4) {
			descript += StringUtil.randomWord(["nice ", "hand-filling ", "well-rounded ", "supple ", "softball-sized "]);
		}
		//DD->big EE
		else if (val < 11) {
			descript += StringUtil.randomWord(["big ", "large ", "pillowy ", "jiggly ", "volleyball-sized "]);
		}
		//F->big FF
		else if (val < 15) {
			descript += StringUtil.randomWord(["soccerball-sized ", "hand-overflowing ", "generous ", "jiggling "]);
		}
		//G -> HHH
		else if (val < 24) {
			descript += StringUtil.randomWord(["basketball-sized ", "whorish ", "cushiony ", "wobbling "]);
		}
		//I -> KK
		else if (val < 35) {
			descript += StringUtil.randomWord(["massive motherly ", "luscious ", "smothering ", "prodigious "]);
		}
		//K- > MMM+
		else if (val < 100) {
			descript += StringUtil.randomWord(["mountainous ", "monumental ", "back-breaking ", "exercise-ball-sized ", "immense "]);
		}
		//Hyper sizes
		else {
			descript += StringUtil.randomWord(["ludicrously-sized ", "hideously large ", "absurdly large ", "back-breaking ", "colossal ", "immense "]);
		}
		return descript;
	}
///* Moved to Creature.as
		//public static function chestDesc(creature:Creature):String
		//{
			//if (creature.biggestTitSize() < 1) return "chest";
			//else return biggestBreastSizeDescript(creature);
		//}
//*/
//
		//public static function assholeOrPussy(creature:Creature):String
		//{
			//if (creature.hasVagina()) return vaginaDescript(creature, 0);
			//return assholeDescript(creature);
		//}
//
//
	public static function multiCockDescriptLight(creature:Creature):String
	{
		if (creature.cocks.length < 1)
			return "<B>Error: multiCockDescriptLight() called with no penises present.</B>";
		
		//Get cock counts
		var descript:String = "";
		var cockCounts = new Map<PenisType, Int>();
		var numCockTypes = 0;
		
		var descripted:Bool = false;
		//If one, return normal cock descript
		if (creature.cocks.length == 1) return Appearance.cockDescript(creature, creature.cocks[0]);
		
		//Count cocks 
		for (i in 0...creature.cocks.length)
		{
			if (!cockCounts.exists(creature.cocks[i].type)){
				cockCounts.set(creature.cocks[i].type, 0);
				numCockTypes++;
			}
			
			cockCounts[creature.cocks[i].type]++;
		}
		
		if (creature.cocks.length == 2) {
			//For cocks that are the same
			if (numCockTypes == 1) {
				descript += StringUtil.randomWord(["pair of ", "two ", "brace of ", "matching ", "twin "]);
				descript += cockAdj(creature, 0);
				descript += ", " + cockNoun(creature.cocks[0].type) + "s";
			}
			//Nonidentical
			else {
				descript += StringUtil.randomWord(["pair of ", "two ", "brace of "]);
				descript += cockAdj(creature) + ", ";
				descript += StringUtil.randomWord(["mutated cocks", "mutated dicks", "mixed cocks", "mismatched dicks"]);
			}
		}
		if (creature.cocks.length == 3) {
			//For samecocks
			if (numCockTypes == 1) {
				descript += StringUtil.randomWord(["three ", "group of ", "<i>ménage à trois</i> of ", "triad of ", "triumvirate of "]);
				descript += cockAdj(creature, 0);
				descript += ", " + cockNoun(creature.cocks[0].type) + "s";
			}
			else {
				descript += StringUtil.randomWord(["three ", "group of "]);
				descript += cockAdj(creature, 0) + ", ";
				descript += StringUtil.randomWord(["mutated cocks", "mutated dicks", "mixed cocks", "mismatched dicks"]);
			}
		}
		//Large numbers of cocks!
		if (creature.cocks.length > 3) {
			descript += StringUtil.randomWord(["bundle of ", "obscene group of ", "cluster of ", "wriggling bunch of "]);
			//Cock adjectives and nouns
			descripted = false;
			//Same
			if (numCockTypes == 1) {
				descript += cockAdj(creature) + ", ";
				descript += cockNoun(creature.cocks[0].type) + "s";
				descripted = true;
			}
			//If mixed
			if (!descripted) {
				descript += cockAdj(creature) + ", ";
				descript += StringUtil.randomWord(["mutated cocks", "mutated dicks", "mixed cocks", "mismatched dicks"]);
			}
		}
		return descript;
	}

	public static function multiCockDescript(creature:Creature):String
	{
		if (creature.cocks.length < 1) {
			return "<B>Error: multiCockDescript() called with no penises present.</B>";
		}
		
		//Get cock counts
		var descript:String = "";
		var cockCounts = new Map<PenisType, Int>();
		var numCockTypes = 0;
		var averageLength:Float = 0, averageThickness:Float = 0;
		
		var descripted:Bool = false;
		//If one, return normal cock descript
		if (creature.cocks.length == 1) return Appearance.cockDescript(creature, creature.cocks[0]);
		
		//Count cocks 
		for (i in 0...creature.cocks.length)
		{
			if (!cockCounts.exists(creature.cocks[i].type)){
				cockCounts.set(creature.cocks[i].type, 0);
				numCockTypes++;
			}
			
			cockCounts[creature.cocks[i].type]++;
			
			averageLength += creature.cocks[i].length;
			averageThickness += creature.cocks[i].thickness;
		}
		
		//Crunch averages
		averageLength /= creature.cocks.length;
		averageThickness /= creature.cocks.length;
		
		//Quantity descriptors
		if (creature.cocks.length == 1) {
			return cockDescript(creature, creature.cocks[0]);
		}
		if (creature.cocks.length == 2) {
			//For cocks that are the same
			if (numCockTypes == 1) {
				descript += StringUtil.randomWord(["a pair of ", "two ", "a brace of ", "matching ", "twin "]);
				descript += cockAdjectives(averageLength, averageThickness, creature.cocks[0].type, creature);
				descript += ", " + cockNoun(creature.cocks[0].type) + "s";
			} else {
				descript += StringUtil.randomWord(["a pair of ", "two ", "a brace of "]);
				descript += cockAdjectives(averageLength, averageThickness, creature.cocks[0].type, creature) + ", ";
				descript += StringUtil.randomWord(["mutated cocks", "mutated dicks", "mixed cocks", "mismatched dicks"]);
			}
		}
		if (creature.cocks.length == 3) {
			//For samecocks
			if (numCockTypes == 1) {
				descript += StringUtil.randomWord(["three ", "a group of ", "a <i>ménage à trois</i> of ", "a triad of ", "a triumvirate of "]);
				descript += cockAdjectives(averageLength, averageThickness, creature.cocks[0].type, creature);
				descript += ", " + cockNoun(creature.cocks[0].type) + "s";
			} else {
				descript += StringUtil.randomWord(["three ", "a group of "]);
				descript += cockAdjectives(averageLength, averageThickness, creature.cocks[0].type, creature);
				descript += StringUtil.randomWord([", mutated cocks", ", mutated dicks", ", mixed cocks", ", mismatched dicks"]);
			}
		}
		//Large numbers of cocks!
		if (creature.cocks.length > 3) {
			descript += StringUtil.randomWord(["a bundle of ", "an obscene group of ", "a cluster of ", "a wriggling group of "]);
			//Cock adjectives and nouns
			//If same types...
			if (numCockTypes == 1) {
				descript += cockAdjectives(averageLength, averageThickness, creature.cocks[0].type, creature) + ", ";
				descript += cockNoun(creature.cocks[0].type) + "s";
			} else {
				descript += cockAdjectives(averageLength, averageThickness, creature.cocks[0].type, creature) + ", ";
				descript += StringUtil.randomWord(["mutated cocks", "mutated dicks", "mixed cocks", "mismatched dicks"]);
			}
		}
		return descript;
	}
}