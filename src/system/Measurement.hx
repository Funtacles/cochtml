package system;

/**
 * ...
 * @author Funtacles
 */
class Measurement 
{
	public static function inchesToInchFeet(inches:Float):String {
		return Math.floor(inches / 12) + " foot " + inches % 12 + " inch";
	}
	
	//public static function footInchOrMetres(inches:Float, precision:Int = 2):String
	//{
		//if (flags[kFLAGS.USE_METRICS])
			//return (Math.round(inches * 2.54) / Math.pow(10, precision)).toFixed(precision) + " metres";
		//
		//return Math.floor(inches / 12) + " foot " + inches % 12 + " inch";
	//}
//
	public static function numInchesOrCentimetres(inches:Float):String
	{
		var metric:Bool = false; //flags[kFLAGS.USE_METRICS]
		
		if (inches < 1) return inchesOrCentimetres(inches);
		
		var value:Int = Math.round(inches);
		
		if (metric) {
			value = Math.round(inches * 2.54);
			return StringUtil.numberToWord(value) + (value == 1 ? " centimetre" : " centimetres");
		}
		
		if (inches % 12 == 0)
			return (inches == 12 ? "a foot" : StringUtil.numberToWord(Math.round(inches / 12)) + " feet");
		
		return  StringUtil.numberToWord(value) + (value == 1 ? " inch" : " inches");
	}
	
	public static function inchesOrCentimetres(inches:Float, precision:Int = 1):String
	{
		var metric:Bool = false; //flags[kFLAGS.USE_METRICS]
		
		if(metric)
			inches = inchToCm(inches);
		
		var value:Float = Math.round(inches * Math.pow(10, precision)) / Math.pow(10, precision);
			
		var text:String = value + ( metric ? " centimetre" : " inch");
		
		if (value == 1) return text;
		
		return text + (metric ? "s" : "es");
	}
	
	public static function shortSuffix(inches:Float, precision:Int = 1):String
	{
		var metric:Bool = false; //flags[kFLAGS.USE_METRICS]
		
		if(metric)
			inches = inchToCm(inches);
		
		var value:Float = Math.round(inches * Math.pow(10, precision)) / Math.pow(10, precision);
		return value + (false ? "-cm" : "-inch");
	}

	public static function inchToCm(inches:Float):Float
	{
		return inches * 2.54;
	}
	
}