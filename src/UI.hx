package;

import data.Player;
import enums.Stat;
import js.Browser;
import js.html.Element;
import js.html.HTMLDocument;

/**
 * ...
 * @author Funtacles
 */
class UI 
{
    public static inline var MAIN_BUTTON_COUNT:Int = 15;
    public static inline var SYSTEM_BUTTON_COUNT:Int = 6;
	
	private static var _mainButtons:Array<ButtonData>;
	
	private static var _statValues:Map<Stat, Int>;
	private static var _statMaxes:Map<Stat, Int>;
	
	public static function init():Void {
		_mainButtons = new Array<ButtonData>();
		for (i in 0...MAIN_BUTTON_COUNT) {
			var element:Element = Browser.document.getElementById('button${i}');
			_mainButtons.push({enabled:true, control:element, callback: null});
		}
		
		_statValues = new Map<Stat, Int>();
		_statMaxes = new Map<Stat, Int>();
	}

    public static function setName(name: String): Void {
        var nameField = Browser.document.getElementById('charName');
        nameField.innerText = name;
    }

	public static function initSidebar(player:Player):Void {
		levelArrowVisible(false);
		xpArrowVisible(false);
		
		setName(player.shortName);
		
		setStat(Stat.Strength, player.stats[Stat.Strength]);
		setStat(Stat.Toughness, player.stats[Stat.Toughness]);
		setStat(Stat.Speed, player.stats[Stat.Speed]);
		setStat(Stat.Intelligence, player.stats[Stat.Intelligence]);
		setStat(Stat.Libido, player.stats[Stat.Libido]);
		setStat(Stat.Sensitivity, player.stats[Stat.Sensitivity]);
		setStat(Stat.Corruption, player.stats[Stat.Corruption]);
		
		setStat(Stat.HP, player.hp, player.maxHP());
		setStat(Stat.Lust, player.lust);
		setStat(Stat.Fatigue, player.fatigue);
		setStat(Stat.Fullness, player.hunger);
		
		setLevel(player.level);
		
		setStat(Stat.XP, player.xp, player.requiredXP());
		
		setMoney(player.gems);
	}
	
	public static function sidebarVisible(visible: Bool): Void {
        var sidebar = Browser.document.getElementById("stats");
        if (visible) {
            sidebar.style.visibility = null;
        } else {
            sidebar.style.visibility = "hidden";
        }
    }
	
    public static function setStat(stat:Stat, value:Int, maxvalue: Null<Int> = null): Void {
        if (Util.isNullOrUndefined(maxvalue))
            maxvalue = 100;
		
		_statValues[stat] = value;
		_statMaxes[stat] = maxvalue;
		
		updateStatBar(stat);
    }
	
	private static function updateStatBar(stat:Stat) {
		var fieldName: String;
		var showMax: Bool = false;
		
		switch (stat) {
            case Stat.Strength:
                fieldName = "strBar";
            case Stat.Toughness:
                fieldName = "touBar";
            case Stat.Speed:
                fieldName = "speBar";
            case Stat.Intelligence:
                fieldName = "intBar";
            case Stat.Libido:
                fieldName = "libBar";
            case Stat.Sensitivity:
                fieldName = "senBar";
            case Stat.Corruption:
                fieldName = "corBar";
            case Stat.HP:
                fieldName = "hpBar";
                showMax = true;
            case Stat.Lust:
                fieldName = "lustBar";
                showMax = true;
            case Stat.Fatigue:
                fieldName = "fatigueBar";
                showMax = true;
            case Stat.Fullness:
                fieldName = "hungerBar";
                showMax = true;
            case Stat.XP:
                fieldName = "xpBar";
                showMax = true;
        }
		
		var document:HTMLDocument = Browser.document;
		
        var statField:Element = document.getElementById(fieldName);
		
        var statbar:Element = statField.getElementsByClassName("bar-fill")[0];
        statbar.setAttribute("style", 'width: ${_statValues[stat] / _statMaxes[stat] * 100}%;');
		
        var statnum:Element = statField.getElementsByClassName("statnum")[0];
        statnum.innerHTML = showMax ? '${_statValues[stat]}/${_statMaxes[stat]}' : '${_statValues[stat]}';
	}
	
	public static function changeStat(stat:Stat, amount:Int):Void {
		_statValues[stat] += amount;
		
		updateStatBar(stat);
		
		var fieldName: String;
		switch (stat) {
            case Stat.Strength:
                fieldName = "strArrow";
            case Stat.Toughness:
                fieldName = "touArrow";
            case Stat.Speed:
                fieldName = "speArrow";
            case Stat.Intelligence:
                fieldName = "intArrow";
            case Stat.Libido:
                fieldName = "libArrow";
            case Stat.Sensitivity:
                fieldName = "senArrow";
            case Stat.Corruption:
                fieldName = "corArrow";
            case Stat.HP:
                fieldName = "hpArrow";
            case Stat.Lust:
                fieldName = "lustArrow";
            case Stat.Fatigue:
                fieldName = "fatigueArrow";
            case Stat.Fullness:
                fieldName = "hungerArrow";
            case Stat.XP:
                fieldName = "xpArrow";
        }
		
		var statArrow:Element = Browser.document.getElementById(fieldName);
		
		if (amount > 0)
			statArrow.className = "statup";
		else if (amount < 0)
			statArrow.className = "statdown";
		
	}
	
	public static function hideStatArrows():Void {
		var arrows = ["strArrow", "touArrow", "speArrow", "intArrow", "libArrow", "senArrow", "corArrow",
			"hpArrow", "lustArrow", "fatigueArrow", "hungerArrow", "xpArrow"];
		
		for (arrow in arrows){
			var statArrow = Browser.document.getElementById(arrow);
			statArrow.className = "";
		}
	}
	
	public static function levelArrowVisible(visible:Bool):Void {
		var levelArrow = Browser.document.getElementById('levelArrow');
		if (visible)
			levelArrow.style.visibility = null;
        else
			levelArrow.style.visibility = "hidden";
	}
	
	public static function xpArrowVisible(visible:Bool):Void {
		var xpArrow = Browser.document.getElementById('xpArrow');
		if (visible)
			xpArrow.style.visibility = null;
        else
			xpArrow.style.visibility = "hidden";
	}

    public static function setLevel(value: Int):Void {
        var levelField = Browser.document.getElementById('levelNum');
        levelField.innerText = '${value}';
    }

    public static function setMoney(value: Int): Void {
        var gemField = Browser.document.getElementById('gemNum');
        gemField.innerText = Util.numberWithCommas(value);
    }

    public static function setDay(value: Int): Void {
        var dayField = Browser.document.getElementById('daynumber');
        dayField.innerText = '${value}';
    }

    public static function setTime(hour: Int, minutes:Int): Void {
        var timeField = Browser.document.getElementById('daytime');
        var ampm = hour >= 12 ? "PM" : "AM";
        if (hour > 12) {
            hour -= 12;
        } else if (hour < 1) {
            hour += 12;
        }
        hour %= 13;
        timeField.innerText = '${hour}:${Util.pad(minutes)} ${ampm}';
    }

    public static function setMainText(text:String): Void {
        var textField = Browser.document.getElementById('maintext');
        textField.innerHTML = text;
    }

    public static function appendToMainText(text: String): Void {
        var textField = Browser.document.getElementById('maintext');
        textField.innerHTML += text;
    }

    public static function hideMainButtons() {
        for (i in 0...MAIN_BUTTON_COUNT) {
            var button = Browser.document.getElementById('button${i}');
            button.style.visibility = "hidden";
        }
    }

    public static function setMainButton(number: Int, label: String, callback: Void -> Void = null, toolTip: String = null): Void {
        var button = _mainButtons[number].control;
        button.innerText = label;
		
        if (toolTip != null)
            button.setAttribute("title", toolTip);
        else
            button.removeAttribute("title");
		
        button.style.visibility = null;
		
		_mainButtons[number].callback = callback;
		
        if (callback == null)
            disableMainButton(number);
        else
            enableMainButton(number);
    }
	
	public static function disableMainButton(number:Int): Void {
		var button = _mainButtons[number].control;
		button.onclick = null;
		button.classList.add("disabled");
	}
	
	public static function enableMainButton(number:Int) : Void {
		var button = _mainButtons[number].control;
		button.onclick = _mainButtons[number].callback;
		button.classList.remove("disabled");
	}

    public static function hideSystemButtons() {
        for (i in 0...SYSTEM_BUTTON_COUNT) {
            var button = Browser.document.getElementById('sysbutton${i}');
            button.style.visibility = "hidden";
        }
    }

    public static function setSystemButton(number: Int, label: String, callback: Void -> Void): Void {
        var button = Browser.document.getElementById('sysbutton${number}');
        button.innerText = label;
        button.onclick = callback;
        button.style.visibility = null;
    }

	public static function clearMainText(): Void {
        setMainText("");
    }

    public static function writeLine(text: String): Void {
        write(text + "<br>");
    }

    public static function write(text: String): Void {
        appendToMainText(text);
    }
	
	public static function showAchievement(title:String, content:String):Void {
		Browser.document.getElementById("acheader").innerText = title;
		Browser.document.getElementById("accontent").innerText = content;
		Browser.document.getElementById("achievement").className = "show";
		untyped window.setTimeout(hideAchievement, 5000);
	}
	
}

typedef ButtonData = {
	var enabled:Bool;
	var control:Element;
	var callback:Void -> Void;
}