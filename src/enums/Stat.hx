package enums;

/**
 * @author Funtacles
 */
enum Stat 
{
	Strength;
    Toughness;
    Speed;
    Intelligence;
    Libido;
    Sensitivity;
    Corruption;
    HP;
    Lust;
    Fatigue;
    Fullness;
    XP;
}