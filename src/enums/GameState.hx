package enums;

/**
 * @author Funtacles
 */
enum GameState 
{
    StartMenu;
    Navigation;
    Event;
    Combat;
    NewGame;
}