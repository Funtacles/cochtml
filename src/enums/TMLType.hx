package enums;

/**
 * @author Funtacles
 */
enum TMLType 
{
	None;
	Text;
	Integer;
	Number;
	Dice;
}