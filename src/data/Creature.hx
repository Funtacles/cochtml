package data;
import data.body.enums.*;
import data.body.*;
import data.game.statuseffect.IStatusEffect;
import data.game.perk.PerkStore;
import enums.Stat;
import js.html.Element;
import storage.tml.TMLElement;
import data.game.flag.*;
import system.Appearance;

/**
 * ...
 * @author Funtacles
 */
class Creature
{
	public var shortName:String;
	public var article:String;
	
	@:isVar public var pronoun(get, set):String;
	function get_pronoun():String 
	{
		return pronoun;
	}
	function set_pronoun(value:String):String 
	{
		return pronoun = value;
	}
	
	public var stats:Map<Stat, Int>;
	
	
	//Combat Stats
	public var hp:Int = 0;
	public var lust:Int = 0;
	public var fatigue:Int;
	
	//Level Stats
	public var xp:Int;
	public var level:Int;
	public var gems:Int;
	public var additionalXP:Int;

	public function getGender():Gender
	{
		if (hasCock() && hasVagina()) {
			return Herm;
		}
		if (hasCock()) {
			return Male;
		}
		if (hasVagina()) {
			return Female;
		}
		return Neuter;
	}

	public var height:Int;
	public var tone:Int;
	
	public var hair:Hair;

	public var beardStyle:BeardType;
	public var beardLength:Float;
	
	public var skin:Skin;
	
	public var faceType:FaceType;

	public var clawTone:String = "";
	public var clawType:ClawType = Normal;
	
	public var earType:EarType = Human;
	public var earValue:Int = 0;
	
	public var hornType:HornType;
	public var horns:Int = 0;

	public var wings:Wings;

	public var tail:Tail;
	
	public var hipRating:Int;
	
	public var buttRating:Int;
	
	public var nipplesPierced:Int = 0;
	public var nipplesPShort:String = "";
	public var nipplesPLong:String = "";
	public var lipPierced:Int = 0;
	public var lipPShort:String = "";
	public var lipPLong:String = "";
	public var tonguePierced:Int = 0;
	public var tonguePShort:String = "";
	public var tonguePLong:String = "";
	public var eyebrowPierced:Int = 0;
	public var eyebrowPShort:String = "";
	public var eyebrowPLong:String = "";
	public var earsPierced:Int = 0;
	public var earsPShort:String = "";
	public var earsPLong:String = "";
	public var nosePierced:Int = 0;
	public var nosePShort:String = "";
	public var nosePLong:String = "";

	public var antennae:AntennaeType;

	public var eyeType:EyeType;
	public var eyeCount:Int;

	public var tongueType:TongueType;

	public var armType:ArmType;

	public var gillType:GillType;
	public function hasGills():Bool { return gillType != GillType.None; }

	public var cocks:Array<Penis>;
	//balls
	public var balls:Int = 0;
	public var cumMultiplier:Float = 1;
	public var ballSize:Float = 0;
	
	public var hoursSinceCum:Int = 0;
	
	public var vaginas:Array<Vagina>;
	//Fertility is a % out of 100. 
	public var fertility:Int;
	public var nippleLength:Float;
	public var breastRows:Array<BreastRow>;
	public var ass:Ass;
	public var lowerBody:LowerBody;
	
	private var _statusEffects:Array<IStatusEffect>;

	//Constructor
	public function new()
	{
		cocks = new Array<Penis>();
		vaginas = new Array<Vagina>();
		breastRows = new Array<BreastRow>();
		_statusEffects = new Array<IStatusEffect>();
		
		hornType = HornType.None;
		nippleLength = 1;
		
		hair = new Hair();
		
		beardStyle = BeardType.Normal;
		beardLength = 0;
		
		stats = new Map<Stat, Int>();
		
		for (stat in Stat.createAll())
		{
			stats.set(stat, 0);
		}
		
		hp = 1;
		lust = 0;
		fatigue = 0;
		
		level = 1;
		xp = 0;
		gems = 0;
	}
	
	public function buildFromTemplate(tml:TMLElement):Void {
		if (tml.hasChild("Height"))
			height = tml.getFirstChild("Height").asInt();
		
		if (tml.hasChild("Tone"))
			tone = tml.getFirstChild("Tone").asInt();
		
		if (tml.hasChild("Hair"))
			hair.setFromTml(tml.getFirstChild("Hair"));
		
		if (tml.hasChild("Tongue"))
			tongueType = TongueType.createByName(tml.getFirstChild("Tongue").asString());
		
		if (tml.hasChild("Face"))
			faceType = FaceType.createByName(tml.getFirstChild("Face").asString());
			
		if (tml.hasChild("Eyes"))
			eyeType = EyeType.createByName(tml.getFirstChild("Eyes").asString());
		
		if (tml.hasChild("Arms"))
			armType = ArmType.createByName(tml.getFirstChild("Arms").asString());
		
		if (tml.hasChild("BreastRow"))
		{
			var rows = tml.getChildrenWithName("BreastRow");
			for (row in rows) {
				breastRows.push(new BreastRow(row));
			}
		}
		
		if (tml.hasChild("NippleLength"))
			nippleLength = tml.getFirstChild("NippleLength").asFloat();
			
		if (tml.hasChild("HipSize"))
			hipRating = tml.getFirstChild("HipSize").asInt();
			
		if (tml.hasChild("ButtSize"))
			buttRating = tml.getFirstChild("ButtSize").asInt();
			
		if (tml.hasChild("Ass"))
			ass = Ass.fromTML(tml.getFirstChild("Ass"));
		
		if (tml.hasChild("Skin"))
			skin = Skin.fromTML(tml.getFirstChild("Skin"));
		
		if (tml.hasChild("LowerBody"))
			lowerBody = new LowerBody(tml.getFirstChild("LowerBody"));
			
		if (tml.hasChild("Penis"))
		{
			var vaginas = tml.getChildrenWithName("Penis");
			for (penis in vaginas) {
				cocks.push(new Penis(penis));
			}
		}
		
		if (tml.hasChild("Balls"))
		{
			var b = tml.getFirstChild("Balls");
			balls = b.asInt();
			ballSize = b.getFirstChild("Size").asInt(); 
		}
		
		if (tml.hasChild("Vagina"))
		{
			var vags = tml.getChildrenWithName("Vagina");
			for (vag in vags) {
				vaginas.push(Vagina.fromTML(vag));
			}
		}
		
		if (tml.hasChild("Fertility"))
			fertility = tml.getFirstChild("Fertility").asInt();
	}
	
	/**
	 * Changes the value of a stat by the amount specified, but caps the value between 0 and the max for that stat.
	 * @param	stat	The stat to change
	 * @param	amount	The amount to change the stat by. Can be negative.
	 * @return	The amount the stat was changed by after caps are applied.
	 */
	public function changeStat(stat:Stat, amount:Int):Int {
		var max = statMax(stat);
		
		var origVal = stats[stat];
		
		stats[stat] += amount;
		
		if (stats[stat] > max)
			stats[stat] = max;
		else if (stats[stat] < 0)
			stats[stat] = 0;
		
		return stats[stat] - origVal;
	}
	
	/**
	 * Gets the maximum value a stat can have.
	 * @param	stat	The stat to get the max value of.
	 * @return	The maximum possible value of the returned stat.
	 */
	public function statMax(stat:Stat):Int {
		switch (stat) {
            case Stat.HP:
                return maxHP();
            case Stat.Lust:
                return maxLust();
            case Stat.Fatigue:
                return 100;
            case Stat.Fullness:
                return 100;
			default:
				return 100;
        }
	}
	
	public function maxHP():Int
	{
		return 100;
	}

	public function orgasm(type:OrgasmType):Void
	{
		switch (type) {
			case  OrgasmType.Vaginal: 	Flags.getCounter(CounterFlag.TIMES_ORGASM_VAGINAL).value++;
			case  OrgasmType.Anal:    	Flags.getCounter(CounterFlag.TIMES_ORGASM_ANAL).value++;
			case  OrgasmType.Cock:    	Flags.getCounter(CounterFlag.TIMES_ORGASM_COCK).value++;
			case  OrgasmType.Lips:    	Flags.getCounter(CounterFlag.TIMES_ORGASM_LIPS).value++;
			case  OrgasmType.Tits:    	Flags.getCounter(CounterFlag.TIMES_ORGASM_TITS).value++;
			case  OrgasmType.Nipples: 	Flags.getCounter(CounterFlag.TIMES_ORGASM_NIPPLES).value++;
			case  OrgasmType.Ovi:		Flags.getCounter(CounterFlag.TIMES_ORGASM_OVI).value++;
			default:
				if (!hasVagina() && !hasCock()) {
					orgasm(OrgasmType.Anal);
					return;
				}

				if (hasVagina() && hasCock()) {
					orgasm(Std.random(2) == 0 ? OrgasmType.Vaginal : OrgasmType.Cock);
					return;
				}

				orgasm(hasVagina() ? OrgasmType.Vaginal : OrgasmType.Cock);
				return;
		}

		Game.setStat(Stat.Lust, 0);
		hoursSinceCum = 0;
	}

	/**
	 * Adds a new status effect or replaces an existing effect if the status effect doesn't stack.
	 * @param	effect
	 */
	public function addStatusEffect(effect:IStatusEffect):Void
	{
		if (hasStatusEffect(effect.id) && !effect.stackable)
			removeStatusEffect(effect.id);
			
		_statusEffects.push(effect);
	}
	
	/**
	 * Removes a status effect
	 * @param	id	The id of the status effect to remove.
	 */
	public function removeStatusEffect(id:String):Void {
		for (i in 0..._statusEffects.length) {
			if (_statusEffects[i].id == id)
				_statusEffects.remove(_statusEffects[i]);
		}
	}
	
	public function hasStatusEffect(id:String):Bool {
		for (i in 0..._statusEffects.length) {
			if (_statusEffects[i].id == id)
				return true;
		}
		
		return false;
	}
//
	//public function statusEffect(idx:Int):IStatusEffect
	//{
		//return statusEffects[idx];
	//}
	//
	//public function clearStatuses():Void
	//{
		//while (_statusEffects.length > 0)
		//{
			//_statusEffects.pop();
		//}
	//}		

	
	public function biggestTitSize():BreastSize
	{
		if (breastRows.length == 0)
			return BreastSize.Flat;
			
		var counter:Int = breastRows.length;
		var index:Int = 0;
		while (counter > 0)
		{
			counter--;
			if (breastRows[index].breastRating.getIndex() < breastRows[counter].breastRating.getIndex())
				index = counter;
		}
		return breastRows[index].breastRating;
	}
	
	public function biggestCock():Penis
	{
		if (cocks.length == 0)
			return null;
		
		var biggest:Float = 0;
		var index:Int = 0;
		
		for (i in 0...cocks.length)
		{
			if (cocks[i].area > biggest) {
				biggest = cocks[i].area;
				index = i;
			}
		}
		
		return cocks[index];
	}
	
	public function longestCock():Penis
	{
		if (cocks.length == 0)
			return null;
		
		var longest:Float = 0;
		var index:Int = 0;
		
		for (i in 0...cocks.length)
		{
			if (cocks[i].length > longest) {
				longest = cocks[i].length;
				index = i;
			}
		}
		
		return cocks[index];
	}
	
	public function getCocksOfType(type:PenisType):Array<Penis>{
		var list = new Array<Penis>();
		
		for (cock in cocks) {
			if (cock.type == type)
				list.push(cock);
		}
		
		return list;
	}
	
	public function longestCockOfType(type:PenisType):Penis
	{
		var filtered = getCocksOfType(type);
		
		if (filtered.length == 0)
			return null;
		
		filtered.sort(function(a:Penis, b:Penis):Int {return Math.round(a.length - b.length); });
		
		return filtered[0];
	}
	
	public function thickestCockOfType(type:PenisType):Penis
	{
		var filtered = getCocksOfType(type);
		
		if (filtered.length == 0)
			return null;
		
		filtered.sort(function(a:Penis, b:Penis):Int {return Math.round(a.thickness - b.thickness); });
		
		return filtered[0];
	}
	
	public function biggestCockOfType(type:PenisType):Penis
	{
		var filtered = getCocksOfType(type);
		
		if (filtered.length == 0)
			return null;
			
		filtered.sort(function(a:Penis, b:Penis):Int {return Math.round(a.area - b.area); });
		
		return filtered[0];
	}
	
	public function totalCockThickness():Float
	{
		var thick:Float = 0;
		for (cock in cocks)
			thick += cock.thickness;
		return thick;
	}
	
	public function thickestCock():Penis
	{
		if (cocks.length == 0)
			return null;
		
		var thickest:Float = 0;
		var index:Int = 0;
		
		for (i in 0...cocks.length)
		{
			if (cocks[i].length > thickest) {
				thickest = cocks[i].thickness;
				index = i;
			}
		}
		
		return cocks[index];
	}
	
	public function thinnestCock():Penis
	{
		if (cocks.length == 0)
			return null;
		
		var thinnest:Float = 999999;
		var index:Int = 0;
		
		for (i in 0...cocks.length)
		{
			if (cocks[i].length < thinnest) {
				thinnest = cocks[i].thickness;
				index = i;
			}
		}
		
		return cocks[index];
	}
	
	public function smallestCock():Penis
	{
		if (cocks.length == 0)
			return null;
		
		var smallest:Float = 999999;
		var index:Int = 0;
		
		for (i in 0...cocks.length)
		{
			if (cocks[i].area < smallest) {
				smallest = cocks[i].area;
				index = i;
			}
		}
		
		return cocks[index];
	}
	
	public function shortestCock():Penis
	{
		if (cocks.length == 0)
			return null;
		var counter:Int = cocks.length;
		var index:Int = 0;
		while (counter > 0)
		{
			counter--;
			if (cocks[index].length > cocks[counter].length)
				index = counter;
		}
		return cocks[index];
	}
	
	public function cockThatFits(i_fits:Float = 0, type:String = "area"):Int
	{
		if (cocks.length <= 0)
			return -1;
		var cockIdxPtr:Int = cocks.length;
		//Current largest fitter
		var cockIndex:Int = -1;
		while (cockIdxPtr > 0)
		{
			cockIdxPtr--;
			if (type == "area")
			{
				if (cocks[cockIdxPtr].area <= i_fits)
				{
					//If one already fits
					if (cockIndex >= 0)
					{
						//See if the newcomer beats the saved small guy
						if (cocks[cockIdxPtr].area > cocks[cockIndex].area)
							cockIndex = cockIdxPtr;
					}
					//Store the index of fitting dick
					else
						cockIndex = cockIdxPtr;
				}
			}
			else if (type == "length")
			{
				if (cocks[cockIdxPtr].length <= i_fits)
				{
					//If one already fits
					if (cockIndex >= 0)
					{
						//See if the newcomer beats the saved small guy
						if (cocks[cockIdxPtr].length > cocks[cockIndex].length)
							cockIndex = cockIdxPtr;
					}
					//Store the index of fitting dick
					else
						cockIndex = cockIdxPtr;
				}
			}
		}
		return cockIndex;
	}
	
	///**
	 //* Get the vaginal capacity bonus based on body type, perks and the bonus capacity status.
	 //* 
	 //* @return the vaginal capacity bonus for this creature
	 //*/
	//private function vaginalCapacityBonus():Float {
		//var bonus:Float = 0;
		//
		//if (!hasVagina()) {
			//return 0;
		//}
//
		//if (isTaur()){
			//bonus += 50;
		//}else if (lowerBody == LOWER_BODY_TYPE_NAGA){
			//bonus += 20;
		//}
//
		//if (hasPerk(PerkLib.WetPussy))
			//bonus += 20;
		//if (hasPerk(PerkLib.HistorySlut) || hasPerk(PerkLib.HistorySlut2))
			//bonus += 20;
		//if (hasPerk(PerkLib.OneTrackMind))
			//bonus += 10;
		//if (hasPerk(PerkLib.Cornucopia))
			//bonus += 30;
		//if (hasPerk(PerkLib.FerasBoonWideOpen))
			//bonus += 25;
		//if (hasPerk(PerkLib.FerasBoonMilkingTwat))
			//bonus += 40;
		//if (hasStatusEffect(StatusEffects.ParasiteEel)) bonus = 10 * statusEffectv1(StatusEffects.ParasiteEel);
		//bonus += statusEffectv1(StatusEffects.BonusVCapacity);	
			//
		//return bonus;
	//}
		
	//public function analCapacity():Float
	//{
		//var bonus:Float = 0;
		////Centaurs = +30 capacity
		//if (isTaur())
			//bonus = 30;
		//if (hasPerk(PerkStore.HistorySlut))
			//bonus += 20;
		//if (hasPerk(PerkStore.Cornucopia))
			//bonus += 30;
		//if (hasPerk(PerkStore.OneTrackMind))
			//bonus += 10;
		//if (ass.analWetness != AnalWetness.Dry)
			//bonus += 15;
		//return ((bonus + statusEffectv1(StatusEffects.BonusACapacity) + 6 * ass.analLooseness * ass.analLooseness) * (1 + ass.analWetness / 10));
	//}
	
	public function hasFuckableNipples():Bool
	{
		for (breastRow in breastRows)
			if (breastRow.fuckable)
				return true;
		
		return false;
	}
	
	//public function hasBreasts():Bool
	//{
		//if (breastRows.length > 0)
		//{
			//if (biggestTitSize() >= 1)
				//return true;
		//}
		//return false;
	//}
	//
	//public function hasNipples():Bool
	//{
		//var counter:Number = breastRows.length;
		//while (counter > 0)
		//{
			//counter--;
			//if (breastRows[counter].nipplesPerBreast > 0)
				//return true;
		//}
		//return false;
	//}
	//
	//public function lactationSpeed():Float
	//{
		////Lactation * breastSize x 10 (milkPerBreast) determines scene
		//return biggestLactation() * biggestTitSize() * 10;
	//}
	
	public function biggestLactation():Float
	{
		if (breastRows.length == 0)
			return 0;
			
		var biggest:Float = 0;
		for (i in 0...breastRows.length){
			if (breastRows[i].lactationMultiplier > biggest)
				biggest = breastRows[i].lactationMultiplier;
		}
		return biggest;
	}
	
	//public function milked():Void
	//{
		//if (hasStatusEffect(StatusEffects.LactationReduction))
			//changeStatusValue(StatusEffects.LactationReduction, 1, 0);
		//if (hasStatusEffect(StatusEffects.LactationReduc0))
			//removeStatusEffect(StatusEffects.LactationReduc0);
		//if (hasStatusEffect(StatusEffects.LactationReduc1))
			//removeStatusEffect(StatusEffects.LactationReduc1);
		//if (hasStatusEffect(StatusEffects.LactationReduc2))
			//removeStatusEffect(StatusEffects.LactationReduc2);
		//if (hasStatusEffect(StatusEffects.LactationReduc3))
			//removeStatusEffect(StatusEffects.LactationReduc3);
		//if (hasPerk(PerkLib.Feeder))
		//{
			////You've now been milked, reset the timer for that
			//addStatusValue(StatusEffects.Feeder,1,1);
			//changeStatusValue(StatusEffects.Feeder, 2, 0);
		//}
	//}
	//public function boostLactation():Float
	//{
		//if (breastRows.length == 0)
			//return 0;
		//var counter:Int = breastRows.length;
		//var index:Int = 0;
		//var changes:Number = 0;
		//var temp2:Number = 0;
		////Prevent lactation decrease if lactating.
		//if (todo >= 0)
		//{
			//if (hasStatusEffect(StatusEffects.LactationReduction))
				//changeStatusValue(StatusEffects.LactationReduction, 1, 0);
			//if (hasStatusEffect(StatusEffects.LactationReduc0))
				//removeStatusEffect(StatusEffects.LactationReduc0);
			//if (hasStatusEffect(StatusEffects.LactationReduc1))
				//removeStatusEffect(StatusEffects.LactationReduc1);
			//if (hasStatusEffect(StatusEffects.LactationReduc2))
				//removeStatusEffect(StatusEffects.LactationReduc2);
			//if (hasStatusEffect(StatusEffects.LactationReduc3))
				//removeStatusEffect(StatusEffects.LactationReduc3);
		//}
		//if (todo > 0)
		//{
			//while (todo > 0)
			//{
				//counter = breastRows.length;
				//todo -= .1;
				//while (counter > 0)
				//{
					//counter--;
					//if (breastRows[index].lactationMultiplier > breastRows[counter].lactationMultiplier)
						//index = counter;
				//}
				//temp2 = .1;
				//if (breastRows[index].lactationMultiplier > 1.5)
					//temp2 /= 2;
				//if (breastRows[index].lactationMultiplier > 2.5)
					//temp2 /= 2;
				//if (breastRows[index].lactationMultiplier > 3)
					//temp2 /= 2;
				//changes += temp2;
				//breastRows[index].lactationMultiplier += temp2;
			//}
		//}
		//else
		//{
			//while (todo < 0)
			//{
				//counter = breastRows.length;
				//index = 0;
				//if (todo > -.1)
				//{
					//while (counter > 0)
					//{
						//counter--;
						//if (breastRows[index].lactationMultiplier < breastRows[counter].lactationMultiplier)
							//index = counter;
					//}
					////trace(biggestLactation());
					//breastRows[index].lactationMultiplier += todo;
					//if (breastRows[index].lactationMultiplier < 0)
						//breastRows[index].lactationMultiplier = 0;
					//todo = 0;
				//}
				//else
				//{
					//todo += .1;
					//while (counter > 0)
					//{
						//counter--;
						//if (breastRows[index].lactationMultiplier < breastRows[counter].lactationMultiplier)
							//index = counter;
					//}
					//temp2 = todo;
					//changes += temp2;
					//breastRows[index].lactationMultiplier += temp2;
					//if (breastRows[index].lactationMultiplier < 0)
						//breastRows[index].lactationMultiplier = 0;
				//}
			//}
		//}
		//return changes;
	//}
	//
	//public function averageLactation():Float
	//{
		//if (breastRows.length == 0)
			//return 0;
		//var counter:Int = breastRows.length;
		//var index:Int = 0;
		//while (counter > 0)
		//{
			//counter--;
			//index += breastRows[counter].lactationMultiplier;
		//}
		//return Math.floor(index / breastRows.length);
	//}
	//
	////Calculate bonus virility rating!
	////anywhere from 5% to 100% of normal cum effectiveness thru herbs!
	//public function virilityQ():Float
	//{
		//if (!hasCock())
			//return 0;
		//var percent:Number = 0.01;
		//if (cumQ() >= 250)
			//percent += 0.01;
		//if (cumQ() >= 800)
			//percent += 0.01;
		//if (cumQ() >= 1600)
			//percent += 0.02;
		//if (hasPerk(PerkLib.BroBody))
			//percent += 0.05;
		//if (hasPerk(PerkLib.MaraesGiftStud))
			//percent += 0.15;
		//if (hasPerk(PerkLib.FerasBoonAlpha))
			//percent += 0.10;
		//if (perkv1(PerkLib.ElvenBounty) > 0)
			//percent += 0.05;
		//if (hasPerk(PerkLib.FertilityPlus))
			//percent += 0.03;
		//if (hasPerk(PerkLib.FertilityMinus) && lib100 < 25) //Reduces virility by 3%.
			//percent -= 0.03;
		//if (hasPerk(PerkLib.PiercedFertite))
			//percent += 0.03;
		//if (hasPerk(PerkLib.OneTrackMind))
			//percent += 0.03;
		//if (hasPerk(PerkLib.MagicalVirility))
			//percent += 0.05 + (perkv1(PerkLib.MagicalVirility) * 0.01);
		////Messy Orgasms?
		//if (hasPerk(PerkLib.MessyOrgasms))
			//percent += 0.03;
		////Satyr Sexuality
		//if (hasPerk(PerkLib.SatyrSexuality))
			//percent += 0.10;
		////Fertite ring bonus!
		//if (jewelryEffectId == JewelryLib.MODIFIER_FERTILITY)
			//percent += (jewelryEffectMagnitude / 100);
		//if (hasPerk(PerkLib.AscensionVirility))
			//percent += perkv1(PerkLib.AscensionVirility) * 0.05;				
		//if (percent > 1)
			//percent = 1;
		//if (percent < 0)
			//percent = 0;
//
		//return percent;
	//}
	//
	//Calculate cum return
	public function cumQ():Float
	{
		if (!hasCock())
			return 0;
		var quantity:Float = 0;
		//Base value is ballsize*ballQ*cumefficiency by a factor of 2.
		//Other things that affect it: 
		//lust - 50% = normal output.  0 = half output. 100 = +50% output.
		//trace("CUM ESTIMATE: " + Int(1.25*2*cumMultiplier*2*(lust + 50)/10 * (hoursSinceCum+10)/24)/10 + "(no balls), " + Int(ballSize*balls*cumMultiplier*2*(lust + 50)/10 * (hoursSinceCum+10)/24)/10 + "(withballs)");
		var lustCoefficient:Float = (lust + 50) / 10;
		//If realistic mode is enabled, limits cum to capacity.
		//if (flags[kFLAGS.HUNGER_ENABLED] >= 1)
		//{
			//lustCoefficient = (lust + 50) / 5;
			//if (hasPerk(PerkLib.PilgrimsBounty)) lustCoefficient = 30;
			//var percent:Number;
			//percent = lustCoefficient + (hoursSinceCum + 10);
			//if (percent > 100)
				//percent = 100;
			//if (quantity > cumCapacity()) 
				//quantity = cumCapacity();
			//return (percent / 100) * cumCapacity();
		//}
		//Pilgrim's bounty maxes lust coefficient
		//if (hasPerk(PerkLib.PilgrimsBounty))
			//lustCoefficient = 150 / 10;
		/*if ((balls == 0 || hasStatusEffect(StatusEffects.Uniball)) && findPerk(PerkLib.PotentProstate) >= 0) //If you have the Potent Prostate perk and your balls are tiny/nonexistant, then your prostate picks up the slack.
			quantity = Int(4 * 2 * cumMultiplier * 2 * lustCoefficient * (hoursSinceCum + 10) / 24) / 10;
		else */if (balls == 0)
			quantity = Math.round(1.25 * 2 * cumMultiplier * 2 * lustCoefficient * (hoursSinceCum + 10) / 24) / 10;
		else
			quantity = Math.round(ballSize * balls * cumMultiplier * 2 * lustCoefficient * (hoursSinceCum + 10) / 24) / 10;
		//if (hasPerk(PerkLib.BroBody))
			//quantity *= 1.3;
		//if (hasPerk(PerkLib.FertilityPlus))
			//quantity *= 1.5;
		//if (hasPerk(PerkLib.FertilityMinus) && lib100 < 25)
			//quantity *= 0.7;
		//if (hasPerk(PerkLib.MessyOrgasms))
			//quantity *= 1.5;
		//if (hasPerk(PerkLib.OneTrackMind))
			//quantity *= 1.1;
		//if (findPerk(PerkLib.ParasiteMusk) >= 0)
			//quantity *= 1.2;
		//if (hasPerk(PerkLib.MaraesGiftStud))
			//quantity += 350;
		//if (hasPerk(PerkLib.FerasBoonAlpha))
			//quantity += 200;
		//if (hasPerk(PerkLib.MagicalVirility))
			//quantity += 200 + (perkv1(PerkLib.MagicalVirility) * 100);
		//if (hasPerk(PerkLib.FerasBoonSeeder))
			//quantity += 1000;
		//if (hasPerk("Elven Bounty") >= 0) quantity += 250;;
			//quantity += perkv1(PerkLib.ElvenBounty);
		//if (hasPerk(PerkLib.BroBody))
			//quantity += 200;
		//if (hasPerk(PerkLib.SatyrSexuality))
			//quantity += 50;
		//quantity += statusEffectv1(StatusEffects.Rut);
		//quantity *= (1 + (2 * perkv1(PerkLib.PiercedFertite)) / 100);
		//if (jewelryEffectId == JewelryLib.MODIFIER_FERTILITY)
			//quantity *= (1 + (jewelryEffectMagnitude / 100));
		//trace("Final Cum Volume: " + Int(quantity) + "mLs.");
		//if (quantity < 0) trace("SOMETHING HORRIBLY WRONG WITH CUM CALCULATIONS");
		if (quantity < 2)
			quantity = 2;
		if (quantity > 10000000)
			quantity = 10000000;
		return quantity;
	}
	//
	////Limits how much cum you can produce. Can be altered with perks, ball size, and multiplier. Only applies to realistic mode.
	//public function cumCapacity():Float
	//{
		//if (!hasCock()) return 0;
		//var cumCap:Float = 0;
		////Alter capacity by balls.
		//if (balls > 0) cumCap += Math.pow(((4 / 3) * Math.PI * (ballSize / 2)), 3) * balls;// * cumMultiplier
		//else cumCap +=  Math.pow(((4 / 3) * Math.PI * 1), 3) * 2;// * cumMultiplier
		////Alter capacity by perks.
		//if (hasPerk(PerkLib.BroBody)) cumCap *= 1.3;
		//if (findPerk(PerkLib.ParasiteMusk) >= 0) cumCap *= 1.2;
		//if (hasPerk(PerkLib.FertilityPlus)) cumCap *= 1.5;
		//if (hasPerk(PerkLib.FertilityMinus) && lib100 < 25) cumCap *= 0.7;
		//if (hasPerk(PerkLib.MessyOrgasms)) cumCap *= 1.5;
		//if (hasPerk(PerkLib.OneTrackMind)) cumCap *= 1.1;
		//if (hasPerk(PerkLib.MaraesGiftStud)) cumCap += 350;
		//if (hasPerk(PerkLib.FerasBoonAlpha)) cumCap += 200;
		//if (hasPerk(PerkLib.MagicalVirility)) cumCap += 200;
		//if (hasPerk(PerkLib.FerasBoonSeeder)) cumCap += 1000;
		//cumCap += perkv1(PerkLib.ElvenBounty);
		//if (hasPerk(PerkLib.BroBody)) cumCap += 200;
		//cumCap += statusEffectv1(StatusEffects.Rut);
		//cumCap *= (1 + (2 * perkv1(PerkLib.PiercedFertite)) / 100);
		////Alter capacity by accessories.
		//if (jewelryEffectId == JewelryLib.MODIFIER_FERTILITY) cumCap *= (1 + (jewelryEffectMagnitude / 100));
			//
		//cumCap *= cumMultiplier;
		//cumCap = Math.round(cumCap);
		//if (cumCap > Int.MAX_VALUE) 
			//cumCap = Int.MAX_VALUE;
		//return cumCap;
	//}
	//
	//public function countCocksOfType(type:PenisType):Int {
		//if (cocks.length == 0) return 0;
		//var counter:Int = 0;
		//for (x in 0...cocks.length) {
			//if (cocks[x].type == type) counter++;
		//}
		//return counter;
	//}
	//public function findFirstCockType(ctype:PenisType):Int
	//{
		//for (i in 0... cocks.length) {
			//if (cocks[index].type == ctype)
				//return index;
		//}
		//
		//return -1;
	//}
	//
	////Change first normal cock to horsecock!
	////Return number of affected cock, otherwise -1
	//public function addHorseCock():Int
	//{
		//var counter:Int = cocks.length;
		//while (counter > 0)
		//{
			//counter--;
			////Human - > horse
			//if (cocks[counter].cockType == CockTypesEnum.HUMAN)
			//{
				//cocks[counter].cockType = CockTypesEnum.HORSE;
				//return counter;
			//}
			////Dog - > horse
			//if (cocks[counter].cockType == CockTypesEnum.DOG)
			//{
				//cocks[counter].cockType = CockTypesEnum.HORSE;
				//return counter;
			//}
			////Wolf - > horse
			//if (cocks[counter].cockType == CockTypesEnum.WOLF)
			//{
				//cocks[counter].cockType = CockTypesEnum.HORSE;
				//return counter;
			//}
			////Tentacle - > horse
			//if (cocks[counter].cockType == CockTypesEnum.TENTACLE)
			//{
				//cocks[counter].cockType = CockTypesEnum.HORSE;
				//return counter;
			//}
			////Demon -> horse
			//if (cocks[counter].cockType == CockTypesEnum.DEMON)
			//{
				//cocks[counter].cockType = CockTypesEnum.HORSE;
				//return counter;
			//}
			////Catch-all
			//if (cocks[counter].cockType.Index > 4)
			//{
				//cocks[counter].cockType = CockTypesEnum.HORSE;
				//return counter;
			//}
		//}
		//return -1;
	//}
	
	public function hasCock():Bool
	{
		return cocks.length >= 1;
	}
	
	//public function hasSockRoom():Bool
	//{
		//var index:Int = cocks.length;
		//while (index > 0)
		//{
			//index--;
			//if (cocks[index].sock == "")
				//return true;
		//}
		//return false;
	//}
	//
	//public function hasSock(arg:String = ""):Bool
	//{
		//var index:Int = cocks.length;
		//
		//while (index > 0)
		//{
			//index--;
			//if (cocks[index].sock != "")
			//{
			//if (arg == "" || cocks[index].sock == arg)
				//return true;
			//}
		//}
		//return false;
	//}
	//public function countCockSocks(type:String):Int
	//{
		//var count:Int = 0;
		//
		//for (i in 0...cocks.length) {
			//if (cocks[i].sock == type) {
				//count++;
			//}
		//}
		//return count;
	//}
	//
	//public function canAutoFellate():Bool
	//{
		//if (!hasCock())
			//return false;
		//return (cocks[0].cockLength >= 20);
	//}
//
	//public function copySkinToUnderBody():Void
	//{
		//lowerBody.skin.setFromTML(skin.getTML());
	//}
//
	//public static var canFlyWings:Array<WingType> = [
		//WingType.LargeBee,
		//WingType.LargeBatlike,
		//WingType.LargeFeathered,
		//WingType.DraconicLarge,
		//WingType.GiantDragonfly,
		//WingType.LargeImp,
		//WingType.Harpy
	//];
//
	////PC can fly?
	//public function canFly():Bool
	//{
		////web also makes false!
		//if (hasStatusEffect(StatusEffects.Web))
			//return false;
		//return canFlyWings.indexOf(wingType) != -1;
//
	//}
	
	public function hasMuzzle():Bool
	{
		return [FaceType.Horse,
			FaceType.Dog,
			FaceType.Cat,
			FaceType.Lizard,
			FaceType.Kangaroo,
			FaceType.Fox,
			FaceType.Dragon,
			FaceType.Rhino,
			FaceType.Echidna,
			FaceType.Deer,
			FaceType.Wolf].indexOf(faceType) != -1;
	}
	
	//public function canUseStare():Bool
	//{
		//return [EYES_BASILISK, EYES_COCKATRICE].indexOf(eyeType) != -1;
	//}
//
	//public function isHoofed():Bool
	//{
		//return [
			//LOWER_BODY_TYPE_HOOFED,
			//LOWER_BODY_TYPE_CLOVEN_HOOFED,
		//].indexOf(lowerBody) != -1;
	//}
//
	//public function isCentaur():Bool
	//{
		//return isTaur() && isHoofed();
	//}
//
	//public function isBimbo():Bool
	//{
		//if (hasPerk(PerkLib.BimboBody)) return true;
		//if (hasPerk(PerkLib.BimboBrains)) return true;
		//if (hasPerk(PerkLib.FutaForm)) return true;
		//if (hasPerk(PerkLib.FutaFaculties)) return true;
//
		//return false;
	//}

	public function hasVagina():Bool
	{
		return vaginas.length > 0;
	}
	
	//public function hasVirginVagina():Bool
	//{
		//if (vaginas.length > 0)
			//return vaginas[0].virgin;
		//return false;
	//}
//
	////GENDER IDENTITIES
	//public function genderText(male:String = "man", female:String = "woman", futa:String = "herm", eunuch:String = "eunuch"):String
	//{
		//if (vaginas.length > 0) {
			//if (cocks.length > 0) return futa;
			//return female;
		//}
		//else if (cocks.length > 0) {
			//return male;
		//}
		//return eunuch;
	//}
//
	//public function manWoman(caps:Bool = false):String
	//{
		////Dicks?
		//if (totalCocks() > 0)
		//{
			//if (hasVagina())
			//{
				//if (caps)
					//return "Futa";
				//else
					//return "futa";
			//}
			//else
			//{
				//if (caps)
					//return "Man";
				//else
					//return "man";
			//}
		//}
		//else
		//{
			//if (hasVagina())
			//{
				//if (caps)
					//return "Woman";
				//else
					//return "woman";
			//}
			//else
			//{
				//if (caps)
					//return "Eunuch";
				//else
					//return "eunuch";
			//}
		//}
	//}
	//
	//public function mfn(male:String, female:String, neuter:String):String
	//{
		//if (gender == 0)
			//return neuter;
		//else
			//return mf(male, female);
	//}
	
	public var femininity:Int = 50;
	
	public function maleOrFemale(male:String, female:String):String
	{
		if (hasCock() && hasVagina()) // herm
			return (biggestTitSize().getIndex() >= 2 || femininity >= 65) ? female : male;
		
		if (hasCock()) // male
			return (biggestTitSize().getIndex() >= 1 && femininity > 55 || femininity >= 75) ? female : male;
		
		if (hasVagina()) // pure female
			return (biggestTitSize().getIndex() > 1 || femininity >= 35) ? female : male;
		
		// genderless
		return (biggestTitSize().getIndex() >= 2 || femininity >= 65) ? female : male;
	}
	
	public function genderWord(caps:Bool = false):String
	{
		switch (getGender()) {
			case Gender.Neuter:   	return caps ? maleOrFemale("Genderless", "Fem-genderless") : maleOrFemale("genderless", "fem-genderless");
			case Gender.Male:   	return caps ? maleOrFemale("Male", "Dickgirl")             : maleOrFemale("male", "dickgirl");
			case Gender.Female: 	return caps ? maleOrFemale("Cuntboy", "Female")            : maleOrFemale("cuntboy", "female");
			case Gender.Herm:   	return caps ? maleOrFemale("Maleherm", "Hermaphrodite")    : maleOrFemale("maleherm", "hermaphrodite");
			default: return "<b>Gender error!</b>";

		}
	}
	
	///**
	 //* Checks if the creature is technically male: has cock but not vagina.
	 //*/
	//public function isMale():Bool
	//{
		//return gender == GENDER_MALE;
	//}
	//
	///**
	 //* Checks if the creature is technically female: has vagina but not cock.
	 //*/
	//public function isFemale():Bool
	//{
		//return gender == GENDER_FEMALE;
	//}
	//
	///**
	 //* Checks if the creature is technically herm: has both cock and vagina.
	 //*/
	//public function isHerm():Bool
	//{
		//return gender == GENDER_HERM;
	//}
	//
	///**
	 //* Checks if the creature is technically genderless: has neither cock nor vagina.
	 //*/
	//public function isGenderless():Bool
	//{
		//return gender == GENDER_NONE;
	//}
//
	///**
	 //* Checks if the creature is technically male or herm: has at least a cock.
	 //*/
	//public function isMaleOrHerm():Bool
	//{
		//return (gender & GENDER_MALE) != 0;
	//}
//
	///**
	 //* Checks if the creature is technically female or herm: has at least a vagina.
	 //*/
	//public function isFemaleOrHerm():Bool
	//{
		//return (gender & GENDER_FEMALE) != 0;
	//}
	//
	//public function addBodyPart(tml:TMLElement):Void {
		//
	//}
	//
	//public function removeBodyPart(tml:Element):Void {
		//
	//}
	//
	//public function buttChangeNoDisplay(cArea:Float):Bool {
		//var stretched:Bool = false;
		////cArea > capacity = autostreeeeetch half the time.
		//if (cArea >= analCapacity() && rand(2) == 0) {
			//ass.analLooseness++;
			//stretched = true;
			////Reset butt stretchin recovery time
			//if (hasStatusEffect(StatusEffects.ButtStretched)) changeStatusValue(StatusEffects.ButtStretched,1,0);
		//}
		////If within top 10% of capacity, 25% stretch
		//if (cArea < analCapacity() && cArea >= .9*analCapacity() && rand(4) == 0) {
			//ass.analLooseness++;
			//stretched = true;
		//}
		////if within 75th to 90th percentile, 10% stretch
		//if (cArea < .9 * analCapacity() && cArea >= .75 * analCapacity() && rand(10) == 0) {
			//ass.analLooseness++;
			//stretched = true;
		//}
		////Anti-virgin
		//if (ass.analLooseness == 0) {
			//ass.analLooseness++;
			//stretched = true;
		//}
		//if (ass.analLooseness > 5) ass.analLooseness = 5;
		////Delay un-stretching
		//if (cArea >= .5 * analCapacity()) {
			////Butt Stretched used to determine how long since last enlargement
			//if (!hasStatusEffect(StatusEffects.ButtStretched)) createStatusEffect(StatusEffects.ButtStretched,0,0,0,0);
			////Reset the timer on it to 0 when restretched.
			//else changeStatusValue(StatusEffects.ButtStretched,1,0);
		//}
		//if (stretched) {
			//trace("BUTT STRETCHED TO " + (ass.analLooseness) + ".");
		//}
		//return stretched;
	//}
//
//
	//public function cuntChangeNoDisplay(cArea : Float) : Bool {
		//if (vaginas.length == 0) return false;
		//var stretched : Bool = vaginas[0].stretch(cArea, hasPerk(PerkLib.FerasBoonMilkingTwat),vaginalCapacityBonus());
		//
		//// Delay stretch recovery
		//if (cArea >= .5 * vaginalCapacity()) {
			//vaginas[0].resetRecoveryProgress();
		//}
		//
		//return stretched;
	//}
	//
	//public function inHeat():Bool {
		//return hasStatusEffect(StatusEffects.Heat);
	//}
	//
	//public function inRut():Bool {
		//return hasStatusEffect(StatusEffects.Rut);
	//}
//
	//public function bonusFertility():Float
	//{
		//var counter:Number = 0;
		//if (inHeat())
			//counter += statusEffectv1(StatusEffects.Heat);
		//if (hasPerk(PerkLib.FertilityPlus))
			//counter += 15;
		//if (hasPerk(PerkLib.FertilityMinus) && lib100 < 25)
			//counter -= 15;
		//if (hasPerk(PerkLib.MaraesGiftFertility))
			//counter += 50;
		//if (hasPerk(PerkLib.FerasBoonBreedingBitch))
			//counter += 30;
		//if (hasPerk(PerkLib.MagicalFertility))
			//counter += 10 + (perkv1(PerkLib.MagicalFertility) * 5);
		//counter += perkv2(PerkLib.ElvenBounty);
		//counter += perkv1(PerkLib.PiercedFertite);
		//if (jewelryEffectId == JewelryLib.MODIFIER_FERTILITY)
			//counter += jewelryEffectMagnitude;
		//counter += perkv1(PerkLib.AscensionFertility) * 5;
		//return counter;
	//}
//
	//public function totalFertility():Float
	//{
		//return (bonusFertility() + fertility);
	//}
//
	//public function hasBeak():Bool
	//{
		//return [FACE_BEAK, FACE_COCKATRICE].indexOf(faceType) != -1;
	//}
//

	//public function isFurryOrScaley():Bool
	//{
		//return isFurry() || hasScales();
	//}
	
	public function hairOrFurColors():String
	{
		if (!skin.isFluffy())
			return hair.color;

		if (!lowerBody.skin.isFluffy() || ["no", skin.furColor].indexOf(lowerBody.skin.furColor) != -1)
			return skin.furColor;

		// Uses formatStringArray in case we add more skin layers
		// If more layers are added, we'd probably need some remove duplicates function
		return [skin.furColor,lowerBody.skin.furColor].join(", ");
	}
	
	public function isTaur():Bool
	{
		return lowerBody.legCount > 2 && lowerBody.type != LowerBodyType.Drider;
	}
	
	public function hasSpiderEyes():Bool
	{
		return eyeType == EyeType.Spider && eyeCount == 4;
	}
	
	//// <mod name="Predator arms" author="Stadler76">
	//public function claws():String
	//{
		//var toneText:String = clawTone == "" ? " " : (", " + clawTone + " ");
//
		//switch (clawType) {
			//case CLAW_TYPE_NORMAL: return "fingernails";
			//case CLAW_TYPE_LIZARD: return "short curved" + toneText + "claws";
			//case CLAW_TYPE_DRAGON: return "powerful, thick curved" + toneText + "claws";
			//// Since mander and cockatrice arms are hardcoded and the others are NYI, we're done here for now
		//}
		//return "fingernails";
	//}
	//// </mod>
//
	
//
	//public function feet():String
	//{
		//var select:Int = 0;
		////lowerBody:
		////0 - normal
		//if (lowerBody == 0)
			//return "feet";
		////1 - hooves
		//if (lowerBody == 1)
			//return "hooves";
		////2 - paws
		//if (lowerBody == 2)
			//return "paws";
		////3 - snakelike body
		//if (lowerBody == 3)
			//return "coils";
		////4 - centaur!
		//if (lowerBody == 4)
			//return "hooves";
		////5 - demonic heels
		//if (lowerBody == 5)
			//return "demonic high-heels";
		////6 - demonic claws
		//if (lowerBody == 6)
			//return "demonic foot-claws";
		////8 - goo shit
		//if (lowerBody == 8)
			//return "slimey cillia";
		//if (lowerBody == 11)
			//return "flat pony-feet";
		////BUNNAH
		//if (lowerBody == 12) {
			//select = rand(5);
			//if (select == 0)
				//return "large bunny feet";
			//else if (select == 1)
				//return "rabbit feet";
			//else if (select == 2)
				//return "large feet";
			//else
				//return "feet";
		//}
		//if (lowerBody == 13) {
			//select = Math.floor(Math.random() * (5));
			//if (select == 0)
				//return "taloned feet";
			//else
				//return "feet";
		//}
		//if (lowerBody == 14)
			//return "foot-paws";
		//if (lowerBody == 17) {
			//select = rand(4);
			//if (select == 0)
				//return "paws";
			//else if (select == 1)
				//return "soft, padded paws";
			//else if (select == 2)
				//return "fox-like feet";
			//else
				//return "paws";
		//}
		//if (lowerBody == 19) {
			//select = Math.floor(Math.random() * (3));
			//if (select == 0)
				//return "raccoon-like feet";
			//else if (select == 1)
				//return "long-toed paws";
			//else if (select == 2)
				//return "feet";
			//else
				//return "paws";
		//}
		//return "feet";
	//}
//
	//public function foot():String
	//{
		//var select:Int = 0;
		////lowerBody:
		////0 - normal
		//if (lowerBody == 0)
			//return "foot";
		////1 - hooves
		//if (lowerBody == 1)
			//return "hoof";
		////2 - paws
		//if (lowerBody == 2)
			//return "paw";
		////3 - snakelike body
		//if (lowerBody == 3)
			//return "coiled tail";
		////4 - centaur!
		//if (lowerBody == 4)
			//return "hoof";
		////8 - goo shit
		//if (lowerBody == 8)
			//return "slimey undercarriage";
		////PONY
		//if (lowerBody == 11)
			//return "flat pony-foot";
		////BUNNAH
		//if (lowerBody == 12) {
			//select = Math.random() * (5);
			//if (select == 0)
				//return "large bunny foot";
			//else if (select == 1)
				//return "rabbit foot";
			//else if (select == 2)
				//return "large foot";
			//else
				//return "foot";
		//}
		//if (lowerBody == 13) {
			//select = Math.floor(Math.random() * (5));
			//if (select == 0)
				//return "taloned foot";
			//else
				//return "foot";
		//}
		//if (lowerBody == 17) {
			//select = Math.floor(Math.random() * (4));
			//if (select == 0)
				//return "paw";
			//else if (select == 1)
				//return "soft, padded paw";
			//else if (select == 2)
				//return "fox-like foot";
			//else
				//return "paw";
		//}
		//if (lowerBody == 14)
			//return "foot-paw";
		//if (lowerBody == 19) {
			//select = Math.floor(Math.random() * (3));
			//if (select == 0)
				//return "raccoon-like foot";
			//else if (select == 1)
				//return "long-toed paw";
			//else if (select == 2)
				//return "foot";
			//else
				//return "paw";
		//}
		//return "foot";
	//}
	
	//public function eggs():Int
	//{
		//if (!hasPerk(PerkStore.SpiderOvipositor) && !hasPerk(PerkStore.BeeOvipositor))
			//return -1;
		//else if (hasPerk(PerkLib.SpiderOvipositor))
			//return perkv1(PerkLib.SpiderOvipositor);
		//else
			//return perkv1(PerkLib.BeeOvipositor);
	//}
	
	//public function addEggs(arg:Int = 0):Int
	//{
		//if (!hasPerk(PerkLib.SpiderOvipositor) && !hasPerk(PerkLib.BeeOvipositor))
			//return -1;
		//else {
			//if (hasPerk(PerkLib.SpiderOvipositor)) {
				//addPerkValue(PerkLib.SpiderOvipositor, 1, arg);
				//if (eggs() > 50)
					//setPerkValue(PerkLib.SpiderOvipositor, 1, 50);
				//return perkv1(PerkLib.SpiderOvipositor);
			//}
			//else {
				//addPerkValue(PerkLib.BeeOvipositor, 1, arg);
				//if (eggs() > 50)
					//setPerkValue(PerkLib.BeeOvipositor, 1, 50);
				//return perkv1(PerkLib.BeeOvipositor);
			//}
		//}
	//}
//
	//public function dumpEggs():Void
	//{
		//if (!hasPerk(PerkLib.SpiderOvipositor) && !hasPerk(PerkLib.BeeOvipositor))
			//return;
		//setEggs(0);
		////Sets fertile eggs = regular eggs (which are 0)
		//fertilizeEggs();
	//}
//
	//public function setEggs(arg:Int = 0):Int
	//{
		//if (!hasPerk(PerkLib.SpiderOvipositor) && !hasPerk(PerkLib.BeeOvipositor))
			//return -1;
		//else {
			//if (hasPerk(PerkLib.SpiderOvipositor)) {
				//setPerkValue(PerkLib.SpiderOvipositor, 1, arg);
				//if (eggs() > 50)
					//setPerkValue(PerkLib.SpiderOvipositor, 1, 50);
				//return perkv1(PerkLib.SpiderOvipositor);
			//}
			//else {
				//setPerkValue(PerkLib.BeeOvipositor, 1, arg);
				//if (eggs() > 50)
					//setPerkValue(PerkLib.BeeOvipositor, 1, 50);
				//return perkv1(PerkLib.BeeOvipositor);
			//}
		//}
	//}
//
	//public function fertilizedEggs():Int
	//{
		//if (!hasPerk(PerkLib.SpiderOvipositor) && !hasPerk(PerkLib.BeeOvipositor))
			//return -1;
		//else if (hasPerk(PerkLib.SpiderOvipositor))
			//return perkv2(PerkLib.SpiderOvipositor);
		//else
			//return perkv2(PerkLib.BeeOvipositor);
	//}
//
	//public function fertilizeEggs():Int
	//{
		//if (!hasPerk(PerkLib.SpiderOvipositor) && !hasPerk(PerkLib.BeeOvipositor))
			//return -1;
		//else if (hasPerk(PerkLib.SpiderOvipositor))
			//setPerkValue(PerkLib.SpiderOvipositor, 2, eggs());
		//else
			//setPerkValue(PerkLib.BeeOvipositor, 2, eggs());
		//return fertilizedEggs();
	//}
	
	//public function totalBreasts():Int
	//{
		//var counter:Int = breastRows.length;
		//var total:Int = 0;
		//while (counter > 0) {
			//counter--;
			//total += breastRows[counter].breasts;
		//}
		//return total;
	//}
//
	public function totalNipples():Int
	{
		var total:Int = 0;
		for (i in 0...breastRows.length)
		{
			total += breastRows[i].nipplesPerBreast *  breastRows[i].breasts;
		}
		
		return total;
	}

	//public function smallestTitSize():BreastSize
	//{
		//if (breastRows.length == 0)
			//return -1;
		//var counter:Int = breastRows.length;
		//var index:Int = 0;
		//while (counter > 0) {
			//counter--;
			//if (breastRows[index].breastRating > breastRows[counter].breastRating)
				//index = counter;
		//}
		//return breastRows[index].breastRating;
	//}
//
	//public function smallestTitRow():Int
	//{
		//if (breastRows.length == 0)
			//return -1;
		//var counter:Int = breastRows.length;
		//var index:Int = 0;
		//while (counter > 0) {
			//counter--;
			//if (breastRows[index].breastRating > breastRows[counter].breastRating)
				//index = counter;
		//}
		//return index;
	//}
//
	//public function biggestTitRow():Int
	//{
		//var counter:Int = breastRows.length;
		//var index:Int = 0;
		//while (counter > 0) {
			//counter--;
			//if (breastRows[index].breastRating < breastRows[counter].breastRating)
				//index = counter;
		//}
		//return index;
	//}
//
	//public function averageBreastSize():Int
	//{
		//var counter:Int = breastRows.length;
		//var average:Number = 0;
		//while (counter > 0) {
			//counter--;
			//average += breastRows[counter].breastRating;
		//}
		//if (breastRows.length == 0)
			//return 0;
		//return (average / breastRows.length);
	//}
//
	//public function averageCockThickness():Float
	//{
		//var counter:Float = cocks.length;
		//var average:Float = 0;
		//while (counter > 0) {
			//counter--;
			//average += cocks[counter].cockThickness;
		//}
		//if (cocks.length == 0)
			//return 0;
		//return (average / cocks.length);
	//}
//
	//public function averageNippleLength():Float
	//{
		//var counter:Float = breastRows.length;
		//var average:Int = 0;
		//while (counter > 0) {
			//counter--;
			//average += (breastRows[counter].breastRating / 10 + .2);
		//}
		//return (average / breastRows.length);
	//}
//
	//public function averageVaginalLooseness():Float
	//{
		//var counter:Float = vaginas.length;
		//var average:Float = 0;
		////If the player has no vaginas
		//if (vaginas.length == 0)
			//return 2;
		//while (counter > 0) {
			//counter--;
			//average += vaginas[counter].vaginalLooseness;
		//}
		//return (average / vaginas.length);
	//}
//
	//public function averageVaginalWetness():Float
	//{
		////If the player has no vaginas
		//if (vaginas.length == 0)
			//return 2;
		//var counter:Float = vaginas.length;
		//var average:Float = 0;
		//while (counter > 0) {
			//counter--;
			//average += vaginas[counter].vaginalWetness;
		//}
		//return (average / vaginas.length);
	//}
//
	//public function averageCockLength():Float
	//{
		//var counter:Float = cocks.length;
		//var average:Float = 0;
		//while (counter > 0) {
			//counter--;
			//average += cocks[counter].cockLength;
		//}
		//if (cocks.length == 0)
			//return 0;
		//return (average / cocks.length);
	//}
//
	//public function canTitFuck():Bool
	//{
		//if (breastRows.length == 0) return false;
		//
		//var counter:Float = breastRows.length;
		//var index:Int = 0;
		//while (counter > 0) {
			//counter--;
			//if (breastRows[index].breasts < breastRows[counter].breasts && breastRows[counter].breastRating > 3)
				//index = counter;
		//}
		//return breastRows[index].breasts >= 2
			   //&& breastRows[index].breastRating > 3;
//
	//}
//
	//public function mostBreastsPerRow():Int
	//{
		//if (breastRows.length == 0) return 2;
		//
		//var counter:Int = breastRows.length;
		//var index:Int = 0;
		//while (counter > 0) {
			//counter--;
			//if (breastRows[index].breasts < breastRows[counter].breasts)
				//index = counter;
		//}
		//return breastRows[index].breasts;
	//}
//
	//public function averageNipplesPerBreast():Float
	//{
		//var counter:Int = breastRows.length;
		//var breasts:Int = 0;
		//var nipples:Int = 0;
		//while (counter > 0) {
			//counter--;
			//breasts += breastRows[counter].breasts;
			//nipples += breastRows[counter].nipplesPerBreast * breastRows[counter].breasts;
		//}
		//if (breasts == 0)
			//return 0;
		//return Math.floor(nipples / breasts);
	//}
//
	//public function allBreastsDescript():String
	//{
		//return Appearance.allBreastsDescript(this);
	//}
//
	//Simplified these cock descriptors and brought them into the creature class
	public function sMultiCockDesc():String {
		return (cocks.length > 1 ? "one of your " : "your ") + cockMultiLDescriptionShort();
	}
	
	public function oMultiCockDesc():String {
		return (cocks.length > 1 ? "each of your " : "your ") + cockMultiLDescriptionShort();
	}
	
	private function cockMultiLDescriptionShort():String {
		if (cocks.length < 1) {
			return "<b>ERROR: NO WANGS DETECTED for cockMultiLightDesc()</b>";
		}
		if (cocks.length == 1) { //For a songle cock return the default description
			return Appearance.cockDescript(this, cocks[0]);
		}
		
		if (getCocksOfType(cocks[0].type).length == cocks.length) 
			return Appearance.cockNoun(cocks[0].type) + "s";
			
		return Appearance.cockNoun(PenisType.Human) + "s";
	}
	
	public function hasSheath():Bool {
		if (cocks.length == 0) return false;
		for (i in 0...cocks.length) {
			switch (cocks[i].type) {
				case PenisType.Cat:
				case PenisType.Displacer:
				case PenisType.Dog:
				case PenisType.Wolf:
				case PenisType.Fox:
				case PenisType.Horse:
				case PenisType.Kangaroo:
				case PenisType.Avian:
				case PenisType.Echidna:
					return true; //If there's even one cock of any of these types then return true
				default:
			}
		}
		return false;
	}

	//public function cockClit(number:Int = 0):String {
		//if (hasCock() && number >= 0 && number < cockTotal())
			//return cockDescript(number);
		//else
			//return clitDescript();
	//}
	//public function chestDesc():String
	//{
		//if (biggestTitSize() < 1) return "chest";
		//return Appearance.biggestBreastSizeDescript(this);
////			return Appearance.chestDesc(this);
	//}
//
	//public function allChestDesc():String {
		//if (biggestTitSize() < 1) return "chest";
		//return allBreastsDescript();
	//}
	//
	//public function biggestBreastSizeDescript():String {
		//return Appearance.biggestBreastSizeDescript(this);
	//}
	//
	//public function clitDescript():String {
		//return Appearance.clitDescription(this);
	//}
//
	//public function cockHead(cockNum:Int = 0):String {
		//if (cockNum < 0 || cockNum > cocks.length - 1) {
			//CoC_Settings.error("");
			//return "ERROR";
		//}
		//switch (cocks[cockNum].cockType) {
			//case CockTypesEnum.CAT:
				//if (rand(2) == 0) return "point";
				//return "narrow tip";
			//case CockTypesEnum.DEMON:
				//if (rand(2) == 0) return "tainted crown";
				//return "nub-ringed tip";
			//case CockTypesEnum.DISPLACER:
				//switch (rand(5)) {
					//case  0: return "star tip";
					//case  1: return "blooming cock-head";
					//case  2: return "open crown";
					//case  3: return "alien tip";
					//default: return "bizarre head";
				//}
			//case CockTypesEnum.DOG:
			//case CockTypesEnum.WOLF:
			//case CockTypesEnum.FOX:
				//if (rand(2) == 0) return "pointed tip";
				//return "narrow tip";
			//case CockTypesEnum.HORSE:
				//if (rand(2) == 0) return "flare";
				//return "flat tip";
			//case CockTypesEnum.KANGAROO:
				//if (rand(2) == 0) return "tip";
				//return "point";
			//case CockTypesEnum.LIZARD:
				//if (rand(2) == 0) return "crown";
				//return "head";
			//case CockTypesEnum.TENTACLE:
				//if (rand(2) == 0) return "mushroom-like tip";
				//return "wide plant-like crown";
			//case CockTypesEnum.PIG:
				//if (rand(2) == 0) return "corkscrew tip";
				//return "corkscrew head";
			//case CockTypesEnum.RHINO:
				//if (rand(2) == 0) return "flared head";
				//return "rhinoceros dickhead";
			//case CockTypesEnum.ECHIDNA:
				//if (rand(2) == 0) return "quad heads";
				//return "echidna quad heads";
			//default:
		//}
		//if (rand(2) == 0) return "crown";
		//if (rand(2) == 0) return "head";
		//return "cock-head";
	//}
//
	////Short cock description. Describes length or girth. Supports multiple cocks.
	//public function cockDescriptShort(i_cockIndex:Int = 0):String
	//{
		//// catch calls where we're outside of combat, and eCockDescript could be called.
		//if (cocks.length == 0)
			//return "<B>ERROR. INVALID CREATURE SPECIFIED to cockDescriptShort</B>";
//
		//var description:String = "";
		////var descripted:Bool = false;
		////Discuss length one in 3 times
		//if (rand(3) == 0) {
			//if (cocks[i_cockIndex].cockLength >= 30)
				//description = "towering ";
			//else if (cocks[i_cockIndex].cockLength >= 18)
				//description = "enormous ";
			//else if (cocks[i_cockIndex].cockLength >= 13)
				//description = "massive ";
			//else if (cocks[i_cockIndex].cockLength >= 10)
				//description = "huge ";
			//else if (cocks[i_cockIndex].cockLength >= 7)
				//description = "long ";
			//else if (cocks[i_cockIndex].cockLength >= 5)
				//description = "average ";
			//else
				//description = "short ";
			////descripted = true;
		//}
		//else if (rand(2) == 0) { //Discuss girth one in 2 times if not already talked about length.
			////narrow, thin, ample, broad, distended, voluminous
			//if (cocks[i_cockIndex].cockThickness <= .75) description = "narrow ";
			//if (cocks[i_cockIndex].cockThickness > 1 && cocks[i_cockIndex].cockThickness <= 1.4) description = "ample ";
			//if (cocks[i_cockIndex].cockThickness > 1.4 && cocks[i_cockIndex].cockThickness <= 2) description = "broad ";
			//if (cocks[i_cockIndex].cockThickness > 2 && cocks[i_cockIndex].cockThickness <= 3.5) description = "fat ";
			//if (cocks[i_cockIndex].cockThickness > 3.5) description = "distended ";
			////descripted = true;
		//}
////Seems to work better without this comma:			if (descripted && cocks[i_cockIndex].cockType != CockTypesEnum.HUMAN) description += ", ";
		//description += Appearance.cockNoun(cocks[i_cockIndex].cockType);
//
		//return description;
	//}
	
	public function hasCockatriceSkin():Bool
	{
		return skin.type == SkinType.LizardScales && lowerBody.type == LowerBodyType.Cockatrice;
	}
	
	///**
	 //* Echidna 1 ft long (i'd consider it barely qualifying), demonic 2 ft long, draconic 4 ft long
	 //*/
	//public function hasLongTongue():Bool {
		//return tongueType == TONGUE_DEMONIC || tongueType == TONGUE_DRACONIC || tongueType == TONGUE_ECHIDNA;
	//}
	//
	//public function damageToughnessModifier(displayMode:Bool = false):Float {
		////Return 0 if Grimdark
		//if (flags[kFLAGS.GRIMDARK_MODE] > 0) return 0;
		////Calculate
		//var temp:Float = 0;
		//if (tou < 25) temp = (tou * 0.4);
		//else if (tou < 50) temp = 10 + ((tou-25) * 0.3);
		//else if (tou < 75) temp = 17.5 + ((tou-50) * 0.2);
		//else if (tou < 100) temp = 22.5 + ((tou-75) * 0.1);
		//else temp = 25;
		////displayMode is for stats screen.
		//if (displayMode) return temp;
		//else return rand(temp);
	//}
	//
	//public function damagePercent(displayMode:Bool = false, applyModifiers:Bool = false):Float {
		//var mult:Number = 100;
		//var armorMod:Number = armorDef;
		////--BASE--
		////Toughness modifier.
		//if (!displayMode) {
			//mult -= damageToughnessModifier();
			//if (mult < 75) mult = 75;
		//}
		////Modify armor rating based on weapons.
		//if (applyModifiers) {
			//armorMod *= game.player.weapon.armorMod;
			//if (game.player.findPerk(PerkLib.LungingAttacks) >= 0 && !game.combat.isWieldingRangedWeapon()) armorMod *= 0.75;
			//if (armorMod < 0) armorMod = 0;
		//}
		//mult -= armorMod;
		//
		////--PERKS--
		////Take damage you masochist!
		//if (hasPerk(PerkLib.Masochist) && lib >= 60) {
			//mult *= 0.8;
			//if (short == game.player.short && !displayMode) game.dynStats("lus", 2);
		//}
		//if (hasPerk(PerkLib.ImmovableObject) && tou >= 75) {
			//mult *= 0.9;
		//}
		//
		////--STATUS AFFECTS--
		////Black cat beer = 25% reduction!
		//if (statusEffectv1(StatusEffects.BlackCatBeer) > 0)
			//mult *= 0.75;
		//// Uma's Massage bonuses
		//var effect:StatusEffectClass = statusEffectByType(StatusEffects.UmasMassage);
		//if (effect != null && effect.value1 == UmasShop.MASSAGE_RELAXATION) {
			//mult *= effect.value2;
		//}
		////Vigorous. Adversities can foster hope, and resilience.
		//if (statusEffectv1(StatusEffects.Resolve) == 1) mult *= statusEffectv2(StatusEffects.Resolve);
		////Round things off.
		//mult = Math.round(mult);
		////Caps damage reduction at 80%.
		//if (mult < 20) mult = 20;
		//return mult;
	//}
	//
	//public function lustPercent():Float {
		//var lust:Float = 100;
		//var minLustCap:Float = 25;
		//if (flags[kFLAGS.NEW_GAME_PLUS_LEVEL] > 0 && flags[kFLAGS.NEW_GAME_PLUS_LEVEL] < 3) minLustCap -= flags[kFLAGS.NEW_GAME_PLUS_LEVEL] * 5;
		//else if (flags[kFLAGS.NEW_GAME_PLUS_LEVEL] >= 3) minLustCap -= 15;
		////2.5% lust resistance per level - max 75.
		//if (level < 100) {
			//if (level <= 11) lust -= (level - 1) * 3;
			//else if (level > 11 && level <= 21) lust -= (30 + (level - 11) * 2);
			//else if (level > 21 && level <= 31) lust -= (50 + (level - 21) * 1);
			//else if (level > 31) lust -= (60 + (level - 31) * 0.2);
		//}
		//else lust = 25;
		//
		////++++++++++++++++++++++++++++++++++++++++++++++++++
		////ADDITIVE REDUCTIONS
		////THESE ARE FLAT BONUSES WITH LITTLE TO NO DOWNSIDE
		////TOTAL IS LIMITED TO 75%!
		////++++++++++++++++++++++++++++++++++++++++++++++++++
		////Corrupted Libido reduces lust gain by 10%!
		//if (hasPerk(PerkLib.CorruptedLibido)) lust -= 10;
		////Acclimation reduces by 15%
		//if (hasPerk(PerkLib.Acclimation)) lust -= 15;
		////Purity blessing reduces lust gain
		//if (hasPerk(PerkLib.PurityBlessing)) lust -= 5;
		////Resistance = 10%
		//if (hasPerk(PerkLib.Resistance)) lust -= 10;
		//if (hasPerk(PerkLib.ChiReflowLust)) lust -= UmasShop.NEEDLEWORK_LUST_LUST_RESIST;
		//
		//if (lust < minLustCap) lust = minLustCap;
		//if (statusEffectv1(StatusEffects.BlackCatBeer) > 0) {
			//if (lust >= 80) lust = 100;
			//else lust += 20;
		//}
		//lust += Math.round(perkv1(PerkLib.PentUp)/2);
		////Children have both additive and multiplicative lust resistance
		//if (isChild()) lust -= 10;
		////Teenagers have an additive penalty and reduction of total resistance
		//if (isTeen()) lust += 15;
		////++++++++++++++++++++++++++++++++++++++++++++++++++
		////MULTIPLICATIVE REDUCTIONS
		////THESE PERKS ALSO RAISE MINIMUM LUST OR HAVE OTHER
		////DRAWBACKS TO JUSTIFY IT.
		////++++++++++++++++++++++++++++++++++++++++++++++++++
		////Children have both additive and multiplicative lust resistance
		//if (isChild()) lust *= 0.55;
		////The elderly gain lust more slowly
		//if (isElder()) lust *= 0.9;
		////Bimbo body slows lust gains!
		//if ((hasStatusEffect(StatusEffects.BimboChampagne) || hasPerk(PerkLib.BimboBody)) && lust > 0) lust *= .75;
		//if (hasPerk(PerkLib.BroBody) && lust > 0) lust *= .75;
		//if (hasPerk(PerkLib.FutaForm) && lust > 0) lust *= .75;
		////Omnibus' Gift reduces lust gain by 15%
		//if (hasPerk(PerkLib.OmnibusGift)) lust *= .85;
		////Luststick reduces lust gain by 10% to match increased min lust
		//if (hasPerk(PerkLib.LuststickAdapted)) lust *= 0.9;
		//if (hasStatusEffect(StatusEffects.Berzerking)) lust *= .6;
		//if (hasPerk(PerkLib.PureAndLoving)) lust *= 0.95;
		////Berserking removes half!
		//if (hasStatusEffect(StatusEffects.Lustzerking)) lust += ((100 - lust) / 2);
		////Items
		//if (jewelryEffectId == JewelryLib.PURITY) lust *= 1 - (jewelryEffectMagnitude / 100);
		//if (armorName == game.armors.DBARMOR.name) lust *= 0.9;
		//if (weaponName == game.weapons.HNTCANE.name) lust *= 0.75;
		//// Lust mods from Uma's content -- Given the short duration and the gem cost, I think them being multiplicative is justified.
		//// Changing them to an additive bonus should be pretty simple (check the static values in UmasShop.as)
		//var effect:StatusEffectClass = statusEffectByType(StatusEffects.UmasMassage);
		//if (effect != null) {
			//if (effect.value1 == UmasShop.MASSAGE_RELIEF || effect.value1 == UmasShop.MASSAGE_LUST) {
				//lust *= effect.value2;
			//}
		//}
		////Stalwart - Many fall in the face of chaos; but not this one, not today.
		//if (statusEffectv1(StatusEffects.Resolve) == 7) lust *= statusEffectv2(StatusEffects.Resolve);
		////Teenagers have an additive penalty and reduction of total resistance
		//if (isTeen() && lust < 100) lust += ((100 - lust) * 0.3);
		//lust = Math.round(lust);
		//return lust;
	//}
	//
	///**
	//* Look into perks and special effects and @return summery extra chance to avoid attack granted by them.
	//* 
	//* Is overriden in Player to work with Unhindered.
	//*/
	//public function getEvasionChance():Float
	//{
		//var chance:Float = 0;
		//if (hasPerk(PerkLib.Evade)) chance += 10;
		//if (hasPerk(PerkLib.Flexibility)) chance += 6;
		//if (hasPerk(PerkLib.Misdirection) && armorName == "red, high-society bodysuit") chance += 10;
		////Masochistic. Those who covet injury find it in no short supply.
		//if (statusEffectv1(StatusEffects.Resolve) == 2) chance -= statusEffectv3(StatusEffects.Resolve);
		//return chance;
	//}
   //
	//public var EVASION_SPEED:String = "Speed"; // enum maybe?
	//public var EVASION_EVADE:String = "Evade";
	//public var EVASION_FLEXIBILITY:String = "Flexibility";
	//public var EVASION_MISDIRECTION:String = "Misdirection";
	//public var EVASION_UNHINDERED:String = "Unhindered";
	//private var evasionRoll:Float = 0;
   //
	///**
	//* Try to avoid and @return a reason if successfull or null if failed to evade.
	//* 
	//* If attacker is null then you can specify attack speed for enviromental and non-combat cases. If no speed and attacker specified and then only perks would be accounted.
	//* 
	//* This does NOT account blind!
	//* 
	//* Is overriden in Player to work with Unhindered.
	//*/
	//public function getEvasionReason(useMonster:Bool = true, attackSpeed:Int = Int.MIN_VALUE):String
	//{
		//// speed
		//if (useMonster && game.monster != null && attackSpeed == Int.MIN_VALUE) attackSpeed = game.monster.spe;
		//if (attackSpeed != Int.MIN_VALUE && spe - attackSpeed > 0 && Int(Math.random() * (((spe - attackSpeed) / 4) + 80)) > 80) return "Speed";
		////note, Player.speedDodge is still used, since this function can't return how close it was
//
		//evasionRoll = rand(100);
		//// perks
		//if (hasPerk(PerkLib.Evade) && ((evasionRoll = evasionRoll - 10) < 0)) return "Evade";
		//if (hasPerk(PerkLib.Flexibility) && ((evasionRoll = evasionRoll - 6) < 0)) return "Flexibility";
		//if (hasPerk(PerkLib.Misdirection) && armorName == "red, high-society bodysuit" && ((evasionRoll = evasionRoll - 10) < 0)) return "Misdirection";
		//return null;
	//}
   //
	//public function getEvasionRoll(useMonster:Bool = true, attackSpeed:Int = Int.MIN_VALUE):Bool
	//{
		//return getEvasionReason(useMonster, attackSpeed) != null;
	//}
	
	public function maxFatigue():Int
	{
		var max:Int = 100;
		//if (hasPerk(PerkStore.ImprovedEndurance) >= 0) max += 20;
		//if (hasPerk(PerkStore.AscensionEndurance) >= 0) max += perkv1(PerkLib.AscensionEndurance) * 5;
		if (max > 999) max = 999;
		return max;
	}

	public function maxLust():Int
	{
		var max:Int = 100;
		//if (this.demonScore() >= 4) max += 20;
		//if (hasPerk(PerkStore.ImprovedSelfControl)) max += 20;
		//if (hasPerk(PerkStore.BroBody) || hasPerk(PerkLib.BimboBody)|| findPerk(PerkLib.FutaForm) >= 0) max += 20;
		//if (hasPerk(PerkStore.OmnibusGift)) max += 15;
		//if (hasPerk(PerkStore.AscensionDesires)) max += perkv1(PerkLib.AscensionDesires) * 5;
		if (max > 999) max = 999;
		return max;
	}
	
	/**
	 *Get the remaining fatigue of the Creature.
	 *@return maximum amount of fatigue that still can be used
	 */
	public function fatigueLeft():Int
	{
		return maxFatigue() - fatigue;
	}

}
