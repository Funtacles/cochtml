package data.game.flag;

/**
 * @author Funtacles
 */
interface IFlag
{
	var id:String;
	
	function isGlobal():Bool;
}