package data.game.flag;

/**
 * ...
 * @author Funtacles
 */
class Flags 
{
	private static var _flags:Map<String, IFlag>;
	
	public static function init():Void 
	{
		_flags = new Map<String, IFlag>();
		
		registerFlag(new TriggerFlag(TriggerFlag.ISABELLA_FOLLOWER_ACCEPTED));
		
		registerFlag(new TriggerFlag(TriggerFlag.CERAPHCHASITY));
	}
	
	public static function registerFlag(f:IFlag):Void {
		if (_flags == null)
			throw "Flag system has not been initialized. Call init before trying to register flags.";
		
		if (_flags.exists(f.id))
			throw "Flag already exists in Flag System: " + f.id;
		
		_flags.set(f.id, f);
	}
	
	public static function getCounter(id:String):CounterFlag {
		if (!_flags.exists(id))
			throw "Flag has not been registered in Flag System: " + id;
		
		if (!Std.is(_flags.get(id), CounterFlag))
			throw '"$id" is not a counter flag';
		
		return cast(_flags.get(id), CounterFlag);
	}
	
	public static function getAchievement(id:String):AchievementFlag{
		if (!_flags.exists(id))
			throw "Flag has not been registered in Flag System: " + id;
		
		if (!Std.is(_flags.get(id), AchievementFlag))
			throw '"$id" is not an achievement flag';
		
		return cast(_flags.get(id), AchievementFlag);
	}
	
	public static function getTrigger(id:String):TriggerFlag {
		if (!_flags.exists(id))
			throw "Flag has not been registered in Flag System: " + id;
		
		if (!Std.is(_flags.get(id), TriggerFlag))
			throw '"$id" is not a trigger flag';
		
		return cast(_flags.get(id), TriggerFlag);
	}
	
	public static function loadFlags():Void {
		
	}
	
	public static function saveFlags():Void {
		
	}
	
}