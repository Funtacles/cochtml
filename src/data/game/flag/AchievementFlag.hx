package data.game.flag;

/**
 * ...
 * @author Funtacles
 */
class AchievementFlag implements IFlag 
{
	public var id:String;
	public var earned:Bool;
	
	public function new(id:String) 
	{
		this.id = id;
	}
	
	public function isGlobal():Bool {
		return true;
	}
}