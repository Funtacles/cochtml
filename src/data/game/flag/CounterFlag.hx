package data.game.flag;

/**
 * ...
 * @author Funtacles
 */
class CounterFlag implements IFlag
{
	public var id:String;
	public var value:Int;
	private var _isGlobal:Bool;
	
	public function new(id:String, isGlobal:Bool) 
	{
		this.id = id;
		this._isGlobal = isGlobal;
		value = 0;
	}
	
	public function isGlobal():Bool {
		return _isGlobal;
	}
	
	
	public static inline var MARBLE_KIDS:String                                                 = "marblekidscount";
	public static inline var MARBLE_NURSERY_CONSTRUCTION:String                                 = "marblenurseryprogress"; 		// Marble's Bitch-Bin (0 = not start, less than 100 = in progress.  100 = Finished
	
	public static inline var TIMES_FUCKED_URTA:String                                           = "urtatimesfucked"; 			// URTA - times fucked
	public static inline var URTA_COMFORTABLE_WITH_OWN_BODY:String                              = "urtabodycomfort"; 			// URTA - horsecock comfort level (-1 = hates self, no luvs)
	public static inline var URTA_TIME_SINCE_LAST_CAME:String                                   = "urtatimesincelastcame"; 		// URTA - hours until can be horny again
	public static inline var URTA_PC_AFFECTION_COUNTER:String                                   = "urtaaffection"; 				// Urta love counter
	public static inline var URTA_PC_LOVE_COUNTER:String                                        = "urtalove"; 					// Urta Love Level
	public static inline var URTA_ANGRY_AT_PC_COUNTDOWN:String                                  = "urtaangryatpc"; 				// Urta pissed countdown
	public static inline var URTA_DRINK_FREQUENCY:String                                        = "urtadrinkfreq"; 				// Urta drink toggle = 0 is same, 1 is more, -1 is less.
	
	public static inline var AMILY_AFFECTION:String                                             = "amilyaffectionlevel"; 		// (< 15 = low.  In between = medium. 40+= high affect)
	public static inline var AMILY_BIRTH_TOTAL:String                                           = "amilybirthtotal"; 			// Amily Birth Total.
	public static inline var AMILY_FOLLOWER:String                                              = "amilyfollower"; 				// Amily Follower. 0=Not Follower, 1=Follower 2=Corrupted Follower
	public static inline var AMILY_FUCK_COUNTER:String                                          = "amilyfuckcounter"; 			// Amily Fuck Counter.
	public static inline var AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO:String                = "amilyblockcountdown"; // AMILY: Jojo fixing countdown - no encounters with amily till it reaches 0.

	public static inline var PLAYER_RESISTED_AKBAL:String                                       = "playerresistsakbal"; 		// Akbal resisted?  - 1 means the PC has resisted at least once
	public static inline var AKBAL_SUBMISSION_COUNTER:String                                    = "akbalsubmissions"; 			// Akbal submission counter
	public static inline var AKBAL_SUBMISSION_STATE:String                                      = "akbalsubmitstate"; 			// Akbal submission state. -1=Lost to him, 1=Beaten him, 2=Akbal is your bitch

	public static inline var MINOTAUR_CUM_ADDICTION_TRACKER:String                              = "minotaurcumaddiction"; 		// Minotaur cum Addiction Tracker
	public static inline var TIME_SINCE_LAST_CONSUMED_MINOTAUR_CUM:String                       = "minotaurtimesincelastcum"; 	// Time Since Last Minocum fix
	public static inline var MINOTAUR_CUM_ADDICTION_STATE:String                                = "minotaurcumaddictionstate"; 	// Current Addict State (0 = normal, 1= addicted, 1 = needy, 2= withdrawal)
	public static inline var MINOTAUR_CUM_REALLY_ADDICTED_STATE:String                          = "minotaurcumreallyaddicted"; 	// lust status affect timer
	
	public static inline var PC_FETISH:String                                                   = "pcfetishlevel"; 				// Fetish Level 1 - exhibitionism, 2 - bondage, 3 - pacifism
	public static inline var TIMES_AUTOFELLATIO_DUE_TO_CAT_FLEXABILITY:String                   = "catautofellate"; 			// CAT: Autofellated
	public static inline var TIMES_AUTOFELLATIOED_EXGARTUAN:String                              = "exgartuanautofelate"; 		// Times Autofellated Exgartuan
	public static inline var TIMES_MASTURBATED:String                                           = "times_masturbated";
	public static inline var TIMES_ORGASM_VAGINAL:String                                        = "times_orgasm_vaginal";
	public static inline var TIMES_ORGASM_ANAL:String                                           = "times_orgasm_anal";
	public static inline var TIMES_ORGASM_COCK:String                                           = "times_orgasm_cock";
	public static inline var TIMES_ORGASM_TITS:String                                           = "times_orgasm_tits";
	public static inline var TIMES_ORGASM_NIPPLES:String                                        = "times_orgasm_nipples";
	public static inline var TIMES_ORGASM_LIPS:String                                           = "times_orgasm_lips";
	public static inline var TIMES_ORGASM_OVI:String											= "times_orgasm_ovi";
	
	public static inline var INCREASED_HAIR_GROWTH_TIME_REMAINING:String                        = "pchairgrowthtimeleft"; 		// Increased Hair-growth Days Left
	public static inline var INCREASED_HAIR_GROWTH_SERUM_TIMES_APPLIED:String                   = "pchairgrowthtimesapplied"; 	//Increased Hair-growth stack
	
	public static inline var NUMBER_OF_TIMES_MET_SCYLLA:String                                  = "scyllamettimes"; 			// Met Scylla:   -1 = decline.  0 = not met.  1 = sucked once, 2 = sucked twice, etc

	public static inline var TIMES_ENCOUNTED_TAMANIS_DAUGHTERS:String                           = "timesmettamanidaughters"; 	// Times encountered Tamani's Daughters
	public static inline var TAMANI_TIMES_HYPNOTISED:String                                     = "timestamanihypno"; 			// Tamani Hypno Level" Description="increases by 1 for each hypno event.  1-3 slight lust raises, 4-9 medium lust raises, 10-19 super high lust raises, 20+ high chance of autorape with special scene.
	public static inline var TAMANI_DAUGHTER_PREGGO_COUNTDOWN:String                            = "tamanidaughterprego"; 		// Daughter Pregnancy Counter" Description="they will not return until this countdown timer is 0.  Same length as Tamani's incubation &#x2014; approx 1 week.
	public static inline var TAMANI_DAUGHTER_CHAIR_COUNTER:String                               = "tamanidaughterchair"; 		// Times In Goblin Daughter 'CHAIR'
	public static inline var TIMES_FUCKED_TAMANIS_DAUGHTERS:String                              = "tamanidaughtersfucked"; 		// Time slept with Daughters
	
	public static inline var CERULEAN_POTION_BAD_END_FUTA_COUNTER:String                        = "ceruleanpotionbadend"; 		// Cerulean Potion Bad-End Futa Count
	public static inline var CERULEAN_SUCCUBUS_HERM_COUNTER:String                              = "ceruleanpotionherm"; // Cerulean Succubus: Herm times used
	
	public static inline var EDRYN_NUMBER_OF_KIDS:String                                        = "edrynkidcount"; 				// EDRYN: Kids
	public static inline var EDRYN_GIFT_COUNTER:String                                          = "edryngiftcount"; 			// EDRYN: GOOD HAND OUTS
	
	public static inline var JOJO_RATHAZUL_INTERACTION_COUNTER:String                           = "rathazulandjojoattanagra"; 	// RATHAZUL: NON CORRUPT JOJO STUFF
	public static inline var RATHAZUL_CAMP_INTERACTION_COUNTDOWN:String                         = "rathazulcampinteraction"; 	// Rathazul inter-follower countdown timer
	public static inline var JOJO_LAST_MEDITATION:String                                        = "jojolastmeditation"; 		// Last time Jojo meditation used

	public static inline var MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT:String                          = "marbleandamilyfreakout"; 	// Which came first?  Marble(1) or Amily(2) - Freakout at Camp if both Marble and Amily present.    1=true, 2=No freakout

	public static inline var MET_SOPHIE_COUNTER:String                                          = "metsophie"; 					// Met Sophie times
	public static inline var FUCKED_SOPHIE_COUNTER:String                                       = "fuckedsophie"; 				// Times Had Sex With Sophie
	public static inline var BREASTFEAD_SOPHIE_COUNTER:String                                   = "breastfedsophie"; 			// Times Breastfed Sophie
	public static inline var SOPHIE_EGGS_LAID:String                                            = "sophieeggslaid"; 			// Sophie eggs laid
	public static inline var SOPHIE_ANGRY_AT_PC_COUNTER:String                                  = "sophiemadcounter"; 			// Sophie Pissed Off Counter
	public static inline var TIMES_PISSED_OFF_SOPHIE_COUNTER:String                             = "sophiepissedofftimes"; 		// Times Pissed Off Sophie
	public static inline var TIMES_FUCKED_SOPHIE_LESBIAN:String                                 = "sophieinlesbians"; 			// Times Lesbo-Sexed
	public static inline var CORRUPT_MARAE_FOLLOWUP_ENCOUNTER_STATE:String                      = "corruptmaraestate"; 			// Corrupt Marae Followups

	public static inline var WHITNEY_GEMS_PAID_THIS_WEEK:String                                 = "whitneygemspaid"; 			// Whitney Gems Paid Out That Week

	public static inline var TIMES_MET_SCYLLA_IN_ADDICTION_GROUP:String                         = "timesmetscyllainaddgroup"; 	// SCYLLA:  Times met Addiction Group
	public static inline var TIMES_SCYLLA_ADDICT_GROUP_EXPLOITED:String                         = "scyllatimesaddictgroupused"; // SCYLLA: Times addict group taken advantage of
	public static inline var SCYLLA_TIMES_SHARED_IN_ADDICT_GROUP:String                         = "scyllatimesshared"; 			// SCYLLA: Times given the option to 'share' something
	public static inline var SCYLLA_MILK_THERAPY_TIMES:String                                   = "scyllamilktherapy"; 			// SCYLLA: Milk therapy times
	public static inline var SCYLLA_CUM_THERAPY_TIMES:String                                    = "scyllacumtherapy"; 			// SCYLLA: Cum therapy times
	public static inline var SCYLLA_SEX_THERAPY_TIMES:String                                    = "scyllasextherapy"; 			// SCYLLA: Sex therapy times
	public static inline var TIMES_CAUGHT_URTA_WITH_SCYLLA:String                               = "urtaandscyllacaught"; 		// How Many Times Have You Caught Urta + Scylla?
	
	public static inline var WEEKLY_FAIRY_ORGY_COUNTDOWN:String                                 = "fairyorgycountdown"; 		// Weekly Faerie Orgy Countdown
	public static inline var TIMES_FUCKED_VALA_IN_DUNGEON:String                                = "timesfuckedvaladungeon"; 	// Times Vala Used IN Dungeon
	public static inline var TIME_SINCE_VALA_ATTEMPTED_RAPE_PC:String                           = "timesincevalaatemptrape";	// rape attempt
	public static inline var TIMES_PC_DEFEATED_VALA:String                                      = "timespcdefeatvala"; 			// Times beaten Vala into the dirt
	public static inline var TIMES_PC_DEFEATED_VALA_AND_RAEPED:String                           = "timesrapedvala";				// Times beat AND raped vala

	public static inline var TIMES_EXPLORED_PLAINS:String                                       = "timesexploredplains"; 		// Times Explored Plains
	
	public static inline var RAPHEAL_COUNTDOWN_TIMER:String                                     = "raphealcountdowntimer"; 		// Raphael Countdown (Time till Quiksilver Fawkes.  Set to -1 betrayed, set to -2 if covered for.
	public static inline var RAPHAEL_DRESS_TIMER:String                                         = "raphaeldresstimer"; 			// Dress TImer (Set when dress acquired, will proc event when dress is worn to bed or when counter is up (quiksilver fawkes!))
	public static inline var RAPHAEL_RAPIER_TRANING:String                                      = "raphaelrapiertraining"; 		// Raphael Rapier Training - OR? - Weapon strength post-newgame.
	public static inline var RAPHAEL_INTELLIGENCE_TRAINING:String                               = "raphaelinteltraining"; 		// Raphael Intelligence Training

	public static inline var HELLHOUND_MASTER_PROGRESS:String                                   = "hellhoundprogress"; 			// Hellhound Progression
	
	public static inline var DOMINIKA_STAGE:String                                              = "dominikastage"; 				// Dominika Stage - 0=unknown, 1=met, 2=received, -1=blocked
	public static inline var DOMINIKA_SUCKED_OFF_LARGE_COCKS:String                             = "dominikalargecock"; 			// Times Dominika Sucked off Smaller dicks
	public static inline var DOMINIKA_SUCKED_OFF_SMALL_COCKS:String                             = "dominikasmallcock"; 			// Times Dominika sucked off bigger dicks
	public static inline var DOMINIKA_VAGINAL_ORAL_RECEIVED:String                              = "dominikavagoral";			// Times received vaginal oral from fellatrix
	public static inline var DOMINIKA_LEARNING_COOLDOWN:String                                  = "dominikalearningcooldown"; 	// Dominika learning cooldown

	
}