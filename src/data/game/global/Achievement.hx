package data.game.global;
import data.game.flag.AchievementFlag;
import data.game.flag.Flags;

/**
 * ...
 * @author Funtacles
 */
class Achievement 
{
	public var id:String;
	public var title:String;
	public var lockedDesc:String;
	public var unlockedDesc:String;
	public var isShadow:Bool;
	
	public function new(title:String, id:String, lockedDesc:String, unlockedDesc:String = null, isShadow:Bool = false) 
	{
		this.title = title;
		this.id = id;
		this.lockedDesc = lockedDesc;
		this.unlockedDesc = unlockedDesc;
		this.isShadow = isShadow;
	}
	
	public function getDescription():String {
		var fa = Flags.getAchievement(id);
		
		return fa.earned && !StringUtil.isNullOrEmpty(unlockedDesc) ? unlockedDesc : lockedDesc;
	}
	
	public static function init():Void {
		_achievements = new Map<String, Achievement>();
		
		registerAchievement(new Achievement("Newcomer", Achievement.STORY_NEWCOMER, "Enter the realm of Mareth."));
		registerAchievement(new Achievement("Marae's Savior", Achievement.STORY_MARAE_SAVIOR, "Complete Marae's quest."));
		registerAchievement(new Achievement("Revenge at Last", Achievement.STORY_ZETAZ_REVENGE, "Defeat Zetaz and obtain the map."));
		registerAchievement(new Achievement("Demon Slayer", Achievement.STORY_FINALBOSS, "Defeat Lethice."));
		
		registerAchievement(new Achievement("Explorer", Achievement.ZONE_EXPLORER, "Discover every zone."));
		registerAchievement(new Achievement("Sightseer", Achievement.ZONE_SIGHTSEER, "Discover every place."));
		registerAchievement(new Achievement("Where am I?", Achievement.ZONE_WHERE_AM_I, "Explore for the first time."));
		registerAchievement(new Achievement("Forest Ranger", Achievement.ZONE_FOREST_RANGER, "Explore the forest 100 times."));
		registerAchievement(new Achievement("Vacationer", Achievement.ZONE_VACATIONER, "Explore the lake 100 times."));
		registerAchievement(new Achievement("Dehydrated", Achievement.ZONE_DEHYDRATED, "Explore the desert 100 times."));
		registerAchievement(new Achievement("Mountaineer", Achievement.ZONE_MOUNTAINEER, "Explore the mountains 100 times."));
		registerAchievement(new Achievement("Rolling Hills", Achievement.ZONE_ROLLING_HILLS, "Explore the plains 100 times."));
		registerAchievement(new Achievement("Wet All Over", Achievement.ZONE_WET_ALL_OVER, "Explore the swamp 100 times."));
		registerAchievement(new Achievement("We Need to Go Deeper", Achievement.ZONE_WE_NEED_TO_GO_DEEPER, "Explore the deepwoods 100 times."));
		registerAchievement(new Achievement("Light-headed", Achievement.ZONE_LIGHT_HEADED, "Explore the high mountains 100 times."));
		registerAchievement(new Achievement("All Murky", Achievement.ZONE_ALL_MURKY, "Explore the bog 100 times."));
		registerAchievement(new Achievement("Frozen", Achievement.ZONE_FROZEN, "Explore the glacial rift 100 times."));
		registerAchievement(new Achievement("Roasted", Achievement.ZONE_ROASTED, "Explore the volcanic crag 100 times."));
		registerAchievement(new Achievement("Archaeologist", Achievement.ZONE_ARCHAEOLOGIST, "Explore the town ruins 15 times."));
		registerAchievement(new Achievement("Farmer", Achievement.ZONE_FARMER, "Visit Whitney's farm 30 times."));
		registerAchievement(new Achievement("Sea-Legs", Achievement.ZONE_SEA_LEGS, "Use the lake boat 15 times."));
		
		registerAchievement(new Achievement("Level up!", Achievement.LEVEL_LEVEL_UP, "Get to level 2."));
		registerAchievement(new Achievement("Novice", Achievement.LEVEL_NOVICE, "Get to level 5."));
		registerAchievement(new Achievement("Apprentice", Achievement.LEVEL_APPRENTICE, "Get to level 10."));
		registerAchievement(new Achievement("Journeyman", Achievement.LEVEL_JOURNEYMAN, "Get to level 15."));
		registerAchievement(new Achievement("Expert", Achievement.LEVEL_EXPERT, "Get to level 20."));
		registerAchievement(new Achievement("Master", Achievement.LEVEL_MASTER, "Get to level 30."));
		registerAchievement(new Achievement("Grandmaster", Achievement.LEVEL_GRANDMASTER, "Get to level 45."));
		registerAchievement(new Achievement("Illustrious", Achievement.LEVEL_ILLUSTRIOUS, "Get to level 60."));
		registerAchievement(new Achievement("Are you a god?", Achievement.LEVEL_ARE_YOU_A_GOD, "Get to level 100.", "Get to level 100. (Your powers would have surpassed Marae's by now.)", true));
		
		registerAchievement(new Achievement("My First Companion", Achievement.POPULATION_FIRST, "Have a camp population of 2."));
		registerAchievement(new Achievement("Hamlet", Achievement.POPULATION_HAMLET, "Have a camp population of 5."));
		registerAchievement(new Achievement("Village", Achievement.POPULATION_VILLAGE, "Have a camp population of 10."));
		registerAchievement(new Achievement("Town", Achievement.POPULATION_TOWN, "Have a camp population of 25."));
		registerAchievement(new Achievement("City", Achievement.POPULATION_CITY, "Have a camp population of 100."));
		registerAchievement(new Achievement("Metropolis", Achievement.POPULATION_METROPOLIS, "Have a camp population of 250."));
		registerAchievement(new Achievement("Megalopolis", Achievement.POPULATION_MEGALOPOLIS, "Have a camp population of 500."));
		registerAchievement(new Achievement("City-State", Achievement.POPULATION_CITY_STATE, "Have a camp population of 1,000.", "", true));
		registerAchievement(new Achievement("Kingdom", Achievement.POPULATION_KINGDOM, "Have a camp population of 2,500.", "", true));
		registerAchievement(new Achievement("Empire", Achievement.POPULATION_EMPIRE, "Have a camp population of 5,000.", "", true));
		
		registerAchievement(new Achievement("It's been a month", Achievement.TIME_MONTH, "Get to day 30."));
		registerAchievement(new Achievement("Half-year", Achievement.TIME_HALF_YEAR, "Get to day 180."));
		registerAchievement(new Achievement("Annual", Achievement.TIME_ANNUAL, "Get to day 365. (1 year)"));
		registerAchievement(new Achievement("Biennial", Achievement.TIME_BIENNIAL, "Get to day 730. (2 years)"));
		registerAchievement(new Achievement("Triennial", Achievement.TIME_TRIENNIAL, "Get to day 1,095. (3 years)"));
		registerAchievement(new Achievement("In for the long haul", Achievement.TIME_LONG_HAUL, "Get to day 1,825. (5 years)"));
		registerAchievement(new Achievement("Decade", Achievement.TIME_DECADE, "Get to day 3,650. (10 years)", "Get to day 3,650. (10 years | Okay, you can stop now.)", true));
		registerAchievement(new Achievement("Century", Achievement.TIME_CENTURY, "Get to day 36,500. (100 years)", "Get to day 36,500. (100 years | It's time to stop playing. Go outside.)", true));
		registerAchievement(new Achievement("Time Traveller", Achievement.TIME_TRAVELLER, "Get to day 36,500+ by tampering with save", "", true));
		
		registerAchievement(new Achievement("Delver", Achievement.DUNGEON_DELVER, "Clear any dungeon."));
		registerAchievement(new Achievement("Delver Apprentice", Achievement.DUNGEON_DELVER_APPRENTICE, "Clear 3 dungeons."));
		registerAchievement(new Achievement("Delver Master", Achievement.DUNGEON_DELVER_MASTER, "Clear every dungeon in the game."));
		registerAchievement(new Achievement("Shut Down Everything", Achievement.DUNGEON_SHUT_DOWN_EVERYTHING, "Clear the Factory."));
		registerAchievement(new Achievement("You're in Deep", Achievement.DUNGEON_YOURE_IN_DEEP, "Fully clear the Deep Cave."));
		registerAchievement(new Achievement("End of Reign", Achievement.DUNGEON_END_OF_REIGN, "Fully clear the Lethice Stronghold."));
		registerAchievement(new Achievement("Friend of the Sand Witches", Achievement.DUNGEON_SAND_WITCH_FRIEND, "Fully clear the Desert Cave."));
		registerAchievement(new Achievement("Fall of the Phoenix", Achievement.DUNGEON_PHOENIX_FALL, "Clear the Tower of the Phoenix."));
		registerAchievement(new Achievement("Accomplice", Achievement.DUNGEON_ACCOMPLICE, "Watch Helia kill the Harpy Queen.", "", true));
		registerAchievement(new Achievement("Extremely Celibate Delver", Achievement.DUNGEON_EXTREMELY_CHASTE_DELVER, "Complete Phoenix Tower without ever orgasming from the beginning.", "", true));
		registerAchievement(new Achievement("A Little Hope", Achievement.DUNGEON_A_LITTLE_HOPE, "Clear the accursed Manor."));
		
		registerAchievement(new Achievement("Wannabe Wizard", Achievement.FASHION_WANNABE_WIZARD, "Equip wizard robes and magic staff."));
		registerAchievement(new Achievement("Cosplayer", Achievement.FASHION_COSPLAYER, "Wear 10 different clothings/armors."));
		registerAchievement(new Achievement("Dominatrix", Achievement.FASHION_DOMINATRIX, "Wear any form of kinky clothing and wield any form of whip."));
		registerAchievement(new Achievement("Going Commando", Achievement.FASHION_GOING_COMMANDO, "Wear no undergarments while wearing any clothes or armours."));
		registerAchievement(new Achievement("Bling Bling", Achievement.FASHION_BLING_BLING, "Wear jewelry that is valued over 1,000 gems."));
		
		registerAchievement(new Achievement("Rich", Achievement.WEALTH_RICH, "Have 1,000 gems."));
		registerAchievement(new Achievement("Hoarder", Achievement.WEALTH_HOARDER, "Have 10,000 gems."));
		registerAchievement(new Achievement("Gem Vault", Achievement.WEALTH_GEM_VAULT, "Have 100,000 gems."));
		registerAchievement(new Achievement("Millionaire", Achievement.WEALTH_MILLIONAIRE, "Have 1,000,000 gems.", "Have 1,000,000 gems. What are you going to spend these gems on?", true));
		registerAchievement(new Achievement("Item Vault", Achievement.WEALTH_ITEM_VAULT, "Fill up your inventory, chest, jewelry box, weapon and armor racks."));
		
		registerAchievement(new Achievement("Wizard", Achievement.COMBAT_WIZARD, "Learn all black and white spells from spell books."));
		registerAchievement(new Achievement("Cum Cannon", Achievement.COMBAT_CUM_CANNON, "Cum in the middle of battle."));
		registerAchievement(new Achievement("How Do I Shot Web?", Achievement.COMBAT_SHOT_WEB, "Fire your webbings at your opponent."));
		registerAchievement(new Achievement("Pain", Achievement.COMBAT_PAIN, "Deal 50 damage in one hit."));
		registerAchievement(new Achievement("Fractured Limbs", Achievement.COMBAT_FRACTURED_LIMBS, "Deal 100 damage in one hit."));
		registerAchievement(new Achievement("Broken Bones", Achievement.COMBAT_BROKEN_BONES, "Deal 250 damage in one hit."));
		registerAchievement(new Achievement("Overkill", Achievement.COMBAT_OVERKILL, "Deal 500 damage in one hit.")); //Actually POSSIBLE
		registerAchievement(new Achievement("Damage Sponge", Achievement.COMBAT_DAMAGE_SPONGE, "Take a total of 10,000 damage."));
		registerAchievement(new Achievement("Bloodletter", Achievement.COMBAT_BLOOD_LETTER, "Deal a total of 50,000 damage."));
		registerAchievement(new Achievement("Revengeance", Achievement.COMBAT_REVENGEANCE, "Defeat an enemy with a katana counter."));
		
		registerAchievement(new Achievement("Egg Hunter", Achievement.HOLIDAY_EGG_HUNTER, "Find 10 eggs as random drops during Easter event.", "", true));
		registerAchievement(new Achievement("Happy Birthday, Helia!", Achievement.HOLIDAY_HELIA_BIRTHDAY, "Participate into Helia's birthday event. (August)", "", true));
		registerAchievement(new Achievement("Thankslutting", Achievement.HOLIDAY_THANKSGIVING_I, "Meet the Piggy-Slut (Thanksgiving)", "", true));
		registerAchievement(new Achievement("Gobble Gobble", Achievement.HOLIDAY_THANKSGIVING_II, "Meet the Cockgobbler (Thanksgiving)", "", true));
		registerAchievement(new Achievement("Pump-kin-kin-kin", Achievement.HOLIDAY_HALLOWEEN_I, "Find the pumpkin (Halloween)", "", true));
		registerAchievement(new Achievement("Fera's Wonderland", Achievement.HOLIDAY_HALLOWEEN_II, "Free Fera/Visit her wonderland (Halloween)", "", true));
		registerAchievement(new Achievement("Naughty or Nice", Achievement.HOLIDAY_CHRISTMAS_I, "Meet the X-mas Elf (Christmas)", "", true));
		registerAchievement(new Achievement("A Christmas Carol", Achievement.HOLIDAY_CHRISTMAS_II, "Complete Carol's mini-quest (Christmas)", "", true));
		registerAchievement(new Achievement("The Lovable Snowman", Achievement.HOLIDAY_CHRISTMAS_III, "Have Nieve as lover (Christmas/Winter)", "", true));
		registerAchievement(new Achievement("Will You Be My Valentine?", Achievement.HOLIDAY_VALENTINE, "Visit the Wet Bitch during Valentine's day. (Valentine)", "", true));
		
		registerAchievement(new Achievement("Tastes Like Chicken", Achievement.REALISTIC_TASTES_LIKE_CHICKEN, "Refill your hunger for the first time."));
		registerAchievement(new Achievement("Champion Needs Food Badly", Achievement.REALISTIC_CHAMPION_NEEDS_FOOD, "Instantly refill your hunger from 0 to 100 in one go."));
		registerAchievement(new Achievement("Gourmand", Achievement.REALISTIC_GOURMAND, "Refill hunger from 5 different sources."));
		registerAchievement(new Achievement("Glutton", Achievement.REALISTIC_GLUTTON, "Eat while hunger is above 90."));
		registerAchievement(new Achievement("Fasting", Achievement.REALISTIC_FASTING, "Keep hunger below 25 for a week but don't let it reach 0."));
		
		registerAchievement(new Achievement("Portal Defender", Achievement.GENERAL_PORTAL_DEFENDER, "Defeat 25 demons and sleep 10 times."));
		registerAchievement(new Achievement("Bad Ender", Achievement.GENERAL_BAD_ENDER, "Cause or witness 3 Bad Ends to various NPCs."));
		registerAchievement(new Achievement("Game Over!", Achievement.GENERAL_GAME_OVER, "Get a Bad End."));
		registerAchievement(new Achievement("Urine Trouble", Achievement.GENERAL_URINE_TROUBLE, "Urinate at least once in the realm of Mareth."));
		registerAchievement(new Achievement("Smashed", Achievement.GENERAL_SMASHED, "Get so drunk that you end up urinating.", "", true));
		registerAchievement(new Achievement("What's Happening to Me?", Achievement.GENERAL_WHATS_HAPPENING_TO_ME, "Transform for the first time."));
		registerAchievement(new Achievement("Transformer", Achievement.GENERAL_TRANSFORMER, "Transform 10 times."));
		registerAchievement(new Achievement("Shapeshifty", Achievement.GENERAL_SHAPESHIFTY, "Transform 25 times."));
		registerAchievement(new Achievement("Fapfapfap", Achievement.GENERAL_FAPFAPFAP, "Masturbate for the first time."));
		registerAchievement(new Achievement("Faptastic", Achievement.GENERAL_FAPTASTIC, "Masturbate 10 times."));
		registerAchievement(new Achievement("Master-bation", Achievement.GENERAL_FAPSTER, "Masturbate 100 times."));
		
		registerAchievement(new Achievement("Helspawn", Achievement.GENERAL_HELSPAWN, "Have Helia give birth to Helspawn and raise her until adulthood."));
		registerAchievement(new Achievement("Goo Armor", Achievement.GENERAL_GOO_ARMOR, "Wear the goo armor."));
		registerAchievement(new Achievement("Urta's True Lover", Achievement.GENERAL_URTA_TRUE_LOVER, "Complete Urta's infertility quest then have her give birth to a baby fox."));
		registerAchievement(new Achievement("Dress-tacular", Achievement.GENERAL_DRESSTACULAR, "Give Rubi every outfit available."));
		registerAchievement(new Achievement("Godslayer", Achievement.GENERAL_GODSLAYER, "Defeat corrupted Marae.", "", true));
		registerAchievement(new Achievement("Follow the Leader", Achievement.GENERAL_FOLLOW_THE_LEADER, "Get every follower in the game."));
		registerAchievement(new Achievement("Gotta Love 'Em All", Achievement.GENERAL_GOTTA_LOVE_THEM_ALL, "Get every lover in the game. (Nieve optional)"));
		registerAchievement(new Achievement("Meet Your Master", Achievement.GENERAL_MEET_YOUR_MASTER, "Get every slave in the game. (Corrupt Jojo & Amily, and Bimbo Sophie optional.)"));
		registerAchievement(new Achievement("Slaver", Achievement.GENERAL_MEET_YOUR_MASTER_TRUE, "Get every slave in the game, including corrupt Jojo and Amily, and Bimbo Sophie.", "", true));
		registerAchievement(new Achievement("All Your People are Belong to Me", Achievement.GENERAL_ALL_UR_PPLZ_R_BLNG_2_ME, "Obtain every follower, lover, and slave. (Excluding mutual exclusivity)"));
		registerAchievement(new Achievement("Scholar", Achievement.GENERAL_SCHOLAR, "Fill out all codex entries available in the game."));
		registerAchievement(new Achievement("Freeloader", Achievement.GENERAL_FREELOADER, "Visit the Kitsune's mansion 3 times."));
		registerAchievement(new Achievement("Schizophrenic", Achievement.GENERAL_SCHIZO, "Go between pure and corrupt 4 times. (Threshold of 20 and 80 corruption)"));
		registerAchievement(new Achievement("Clean Slate", Achievement.GENERAL_CLEAN_SLATE, "Go from 100 corruption to zero for the first time."));
		registerAchievement(new Achievement("Perky", Achievement.GENERAL_PERKY, "Have at least 20 perks."));
		registerAchievement(new Achievement("Super Perky", Achievement.GENERAL_SUPER_PERKY, "Have at least 35 perks."));
		registerAchievement(new Achievement("Ultra Perky", Achievement.GENERAL_ULTRA_PERKY, "Have at least 50 perks."));
		registerAchievement(new Achievement("Jack of All Trades", Achievement.GENERAL_STATS_50, "Have at least 50 of each stat. (Libido, sensitivity, corruption optional)"));
		registerAchievement(new Achievement("Incredible Stats", Achievement.GENERAL_STATS_100, "Have at least 100 of each stat. (Libido, sensitivity, corruption optional)"));
		registerAchievement(new Achievement("Like Chuck Norris", Achievement.GENERAL_LIKE_CHUCK_NORRIS, "Defeat the Frost Giant without any equipment.", "Defeat the Frost Giant without any equipment. Way to be a badass!"));
		registerAchievement(new Achievement("Tentacle Beast Slayer", Achievement.GENERAL_TENTACLE_BEAST_SLAYER, "Slay your first Tentacle Beast."));
		registerAchievement(new Achievement("Hammer Time", Achievement.GENERAL_HAMMER_TIME, "Buy a total of 300 nails."));
		registerAchievement(new Achievement("Nail Scavenger", Achievement.GENERAL_NAIL_SCAVENGER, "Scavenge a total of 200 nails from the library wreckage"));
		registerAchievement(new Achievement("I'm No Lumberjack", Achievement.GENERAL_IM_NO_LUMBERJACK, "Buy a total of 100 wood."));
		registerAchievement(new Achievement("Deforester", Achievement.GENERAL_DEFORESTER, "Cut down 100 wood pieces."));
		registerAchievement(new Achievement("Yabba Dabba Doo", Achievement.GENERAL_YABBA_DABBA_DOO, "Buy a total of 100 stones."));
		registerAchievement(new Achievement("AntWorks", Achievement.GENERAL_ANTWORKS, "Gather a total of 200 stones with Phylla help."));
		registerAchievement(new Achievement("Home Sweet Home", Achievement.GENERAL_HOME_SWEET_HOME, "Finish the cabin and complete it with furnishings."));
		registerAchievement(new Achievement("Getaway", Achievement.GENERAL_GETAWAY, "Spend the night outside your camp."));
		registerAchievement(new Achievement("My Tent's (not) Better Than Yours", Achievement.GENERAL_MY_TENT_NOT_BETTER, "Sleep in Arian's tent."));
		registerAchievement(new Achievement("Divine Intervention", Achievement.GENERAL_MINERVA_PURIFICATION, "Complete Minerva's purification process.", "", true));
		registerAchievement(new Achievement("Fencer", Achievement.GENERAL_FENCER, "Complete rapier training from Raphael.", "", true));
		registerAchievement(new Achievement("Now You're Fucking With Portals", Achievement.GENERAL_FUCK_WITH_PORTALS, "Engage in portal sex with Ceraph.", "", true));
		registerAchievement(new Achievement("Getting Wood", Achievement.GENERAL_GETTING_WOOD, "Punch a tree until wood falls out... Wait, what?", "", true));
		registerAchievement(new Achievement("Dick Banisher", Achievement.GENERAL_DICK_BANISHER, "Remove cocks from at least three dedickable NPCs. Don't you think they'll miss having their own cocks?", "", true));
		registerAchievement(new Achievement("You Bastard", Achievement.GENERAL_YOU_BASTARD, "Perform something only someone who's evil would do. Like corrupting NPCs or removing dick from at least 7 dedickable NPCs.", "", true));
		registerAchievement(new Achievement("Up to Eleven", Achievement.GENERAL_UP_TO_11, "Take your height up to 11 feet."));
		registerAchievement(new Achievement("Off With Her Head!", Achievement.GENERAL_OFF_WITH_HER_HEAD, "You've managed to behead Lethice and show her head to the demons!", "", true));
		registerAchievement(new Achievement("NOOOOOOOOOOOO!", Achievement.GENERAL_NOOOOOOO, "You've managed to kill yourself before Lethice takes you as her slave.", "", true));
		registerAchievement(new Achievement("Make Mareth Great Again", Achievement.GENERAL_MAKE_MARETH_GREAT_AGAIN, "Build a wall around your camp to defend from those pesky imps."));
		registerAchievement(new Achievement("Parasite Queen", Achievement.GENERAL_PARASITE_QUEEN, "Host a massive amount of Eel Parasites."));
	}
	
	private static var _achievements:Map<String, Achievement>;
	public static function registerAchievement(a:Achievement):Void {
		if (_achievements == null)
			throw "Acheivements have not been initialized. Call init before trying to register acheivements.";
		
		if (_achievements.exists(a.id))
			throw "Acheivement already exists in acheivement map: " + a.id;
		
		_achievements.set(a.id, a);
		
		Flags.registerFlag(new AchievementFlag(a.id));
	}
	
	/**
	 * Awards the achievement. Will display a blue text if achievement hasn't been earned.
	 * @param	title The name of the achievement.
	 * @param	achievement The achievement to be awarded.
	 * @param	display Determines if achievement earned should be displayed.
	 * @param	nl Inserts a new line before the achievement text.
	 * @param	nl2 Inserts a new line after the achievement text.
	 */
	public static function awardAchievement(id:String):Void {
		if (_achievements[id] == null)
			throw "Invalid achievement!";
		
		var fa = Flags.getAchievement(id);
		if (!fa.earned) {
			fa.earned = true;
			var ac = _achievements[id];
			UI.showAchievement(ac.title, ac.getDescription());
			Flags.saveFlags();
		}
	}
	
	//Storyline Achievements 
	public static inline var STORY_NEWCOMER:String					= "newcomer"; 		//Enter the realm of Mareth.
	public static inline var STORY_MARAE_SAVIOR:String				= "maraesavior"; 	//Complete Marae’s quest.
	public static inline var STORY_ZETAZ_REVENGE:String				= "zetazrevenge"; 	//Defeat Zetaz and obtain the map.
	public static inline var STORY_FINALBOSS:String					= "finalboss"; 		//Defeat Lethice.
	
	//Zone Achievements 
	public static inline var ZONE_EXPLORER:String					= "explorer"; 		//Discover every zone.
	public static inline var ZONE_SIGHTSEER:String					= "sightseer"; 		//Discover every place.
	public static inline var ZONE_WHERE_AM_I:String					= "whereami"; 		//Explore for the first time.
	public static inline var ZONE_FOREST_RANGER:String				= "forestranger"; 	//Explore the forest 100 times.
	public static inline var ZONE_VACATIONER:String					= "vacationer"; 	//Explore the lake 100 times.
	public static inline var ZONE_DEHYDRATED:String					= "dehydrated"; 	//Explore the desert 100 times.
	public static inline var ZONE_MOUNTAINEER:String				= "mountaineer"; 	//Explore the mountain 100 times.
	public static inline var ZONE_WE_NEED_TO_GO_DEEPER:String		= "weneedtogodeeper"; //Explore the deepwoods 100 times.
	public static inline var ZONE_ROLLING_HILLS:String				= "rollinghills"; 	//Explore the plains 100 times.
	public static inline var ZONE_WET_ALL_OVER:String				= "wetallover"; 	//Explore the swamp 100 times.
	public static inline var ZONE_LIGHT_HEADED:String				= "lightheaded"; 	//Explore the high mountain 100 times.
	public static inline var ZONE_ALL_MURKY:String					= "allmurky"; 		//Explore the bog 100 times.
	public static inline var ZONE_FROZEN:String						= "frozen"; 		//Explore the glacial rift 100 times.
	public static inline var ZONE_ARCHAEOLOGIST:String				= "archaeologist"; 	//Visit the town ruins 15 times.
	public static inline var ZONE_FARMER:String						= "farmer"; 		//Visit the farm 30 times.
	public static inline var ZONE_SEA_LEGS:String					= "sealegs"; 		//Use the boat 15 times.
	public static inline var ZONE_ROASTED:String					= "roasted"; 		//Explore the volcanic crag 50 times.
	
	//Level Achievements 
	public static inline var LEVEL_LEVEL_UP:String					= "levelup"; 		//Level 2
	public static inline var LEVEL_NOVICE:String					= "levelnovice"; 	//Level 5
	public static inline var LEVEL_APPRENTICE:String				= "levelapprentice";//Level 10
	public static inline var LEVEL_JOURNEYMAN:String				= "leveljourneyman";//Level 15
	public static inline var LEVEL_EXPERT:String					= "levelexpert"; 	//Level 20
	public static inline var LEVEL_MASTER:String					= "levelmaster"; 	//Level 30
	public static inline var LEVEL_GRANDMASTER:String				= "levelgrandmaster";//Level 40
	public static inline var LEVEL_ILLUSTRIOUS:String				= "levelillustrious";//Level 50
	public static inline var LEVEL_OVERLORD:String					= "leveloverlord"; 	//Level 60
	public static inline var LEVEL_ARE_YOU_A_GOD:String				= "levelyouareagod";//Level 100 (shadow achievement)
	
	//Population Achievements
	public static inline var POPULATION_FIRST:String				= "populationfirst"; 	//Population 2
	public static inline var POPULATION_HAMLET:String				= "populationhamlet"; 	//Population 5
	public static inline var POPULATION_VILLAGE:String				= "populationvillage"; 	//Population 10
	public static inline var POPULATION_TOWN:String					= "populationtown"; 	//Population 25
	public static inline var POPULATION_CITY:String					= "populationcity"; 	//Population 100
	public static inline var POPULATION_METROPOLIS:String			= "populationmetropolis";//Population 250
	public static inline var POPULATION_MEGALOPOLIS:String			= "populationmegalopolis"; //Population 500
	public static inline var POPULATION_CITY_STATE:String			= "populationcitystate"; //Population 1,000 (shadow achievement)
	public static inline var POPULATION_KINGDOM:String				= "populationkingdom"; 	//Population 2,500 (shadow achievement)
	public static inline var POPULATION_EMPIRE:String				= "populationempire"; 	//Population 5,000 (shadow achievement)
	
	//Time Achievements
	public static inline var TIME_MONTH:String						= "timemonth"; 		//30 days
	public static inline var TIME_HALF_YEAR:String					= "timehalfyear"; 	//180 days
	public static inline var TIME_ANNUAL:String						= "timeannual"; 	//365 days
	public static inline var TIME_BIENNIAL:String					= "timebiennual"; 	//730 days
	public static inline var TIME_TRIENNIAL:String					= "timetriennial"; 	//1095 days
	public static inline var TIME_LONG_HAUL:String					= "longhaul"; 		//1825 days
	public static inline var TIME_DECADE:String						= "decade"; 		//3650 days
	public static inline var TIME_CENTURY:String					= "century"; 		//36,500 days (shadow achievement)
	public static inline var TIME_MILLENNIUM:String					= "millennium";
	public static inline var TIME_TRAVELLER:String					= "timetraveller";
	
	//Dungeon Achievements
	public static inline var DUNGEON_DELVER:String					= "dungeondelver";
	public static inline var DUNGEON_DELVER_MASTER:String			= "dungeondelvermaster";
	public static inline var DUNGEON_SHUT_DOWN_EVERYTHING:String	= "shutdowneverything";
	public static inline var DUNGEON_YOURE_IN_DEEP:String			= "youreindeep";
	public static inline var DUNGEON_SAND_WITCH_FRIEND:String		= "sandwitchfriend";
	public static inline var DUNGEON_PHOENIX_FALL:String			= "phoenixfall";
	public static inline var DUNGEON_ACCOMPLICE:String				= "accomplice"; 			//shadow achievement
	public static inline var DUNGEON_EXTREMELY_CHASTE_DELVER:String	= "extremelychastedelver"; 	//shadow achievement
	public static inline var DUNGEON_DELVER_APPRENTICE:String		= "delverapprentice";
	public static inline var DUNGEON_END_OF_REIGN:String			= "endofreign";
	
	//Fashion and Wealth Achievements
	public static inline var FASHION_WANNABE_WIZARD:String			= "wannabewizard";
	public static inline var FASHION_COSPLAYER:String				= "cosplayer";
	public static inline var FASHION_DOMINATRIX:String				= "dominatrix";
	public static inline var FASHION_GOING_COMMANDO:String			= "goingcommando";
	public static inline var FASHION_BLING_BLING:String				= "blingbling";
	public static inline var WEALTH_RICH:String						= "rich";
	public static inline var WEALTH_HOARDER:String					= "hoarder";
	public static inline var WEALTH_GEM_VAULT:String				= "gemvault";
	public static inline var WEALTH_MILLIONAIRE:String				= "millionaire";
	public static inline var WEALTH_ITEM_VAULT:String				= "itemvault";
	
	//Combat Achievements
	public static inline var COMBAT_WIZARD:String					= "combatwizard"; 	//Learn all black and white spells.
	public static inline var COMBAT_CUM_CANNON:String				= "cumcannon"; 		//Cum in the middle of battle.
	public static inline var COMBAT_PAIN:String						= "combatpain"; 	//50 damage
	public static inline var COMBAT_FRACTURED_LIMBS:String			= "fracturedlimbs"; //100 damage
	public static inline var COMBAT_BROKEN_BONES:String				= "brokenbones"; 	//250 damage
	public static inline var COMBAT_OVERKILL:String					= "overkill"; 		//500 damage (shadow achievement?)
	public static inline var COMBAT_SHOT_WEB:String					= "shotweb"; 		//How do I shot web?
	public static inline var COMBAT_DAMAGE_SPONGE:String			= "damagesponge";
	public static inline var COMBAT_BLOOD_LETTER:String				= "bloodletter";
	public static inline var COMBAT_PACIFIST:String					= "pacifist";		
	
	//Holiday Achievements
	public static inline var HOLIDAY_EGG_HUNTER:String				= "egghunter";
	public static inline var HOLIDAY_HELIA_BIRTHDAY:String			= "heliabirthday";
	public static inline var HOLIDAY_THANKSGIVING_I:String			= "thanksgiving1";
	public static inline var HOLIDAY_THANKSGIVING_II:String			= "thanksgiving2";
	public static inline var HOLIDAY_HALLOWEEN_I:String				= "halloween1";
	public static inline var HOLIDAY_HALLOWEEN_II:String			= "halloween2";
	public static inline var HOLIDAY_CHRISTMAS_I:String				= "christmas1";
	public static inline var HOLIDAY_CHRISTMAS_II:String			= "christmas2";
	public static inline var HOLIDAY_CHRISTMAS_III:String			= "christmas3";
	public static inline var HOLIDAY_VALENTINE:String				= "valentine";
	
	//Hunger Achievements
	public static inline var REALISTIC_TASTES_LIKE_CHICKEN:String	= "tasteslikechicken";
	public static inline var REALISTIC_CHAMPION_NEEDS_FOOD:String	= "championneedsfood"; //Champion needs food badly!
	public static inline var REALISTIC_GOURMAND:String				= "gourmand";
	public static inline var REALISTIC_GLUTTON:String				= "glutton";
	public static inline var REALISTIC_FASTING:String				= "fasting";
	
	//Challenge Achievements
	public static inline var CHALLENGE_ULTIMATE_NOOB:String			= "ultimatenoob"; 		//Defeat Lethice at level 1.

	
	//General Achievements
	public static inline var GENERAL_PORTAL_DEFENDER:String			= "portaldefender"; 	//Defeat 25 demons and sleep 10 times.
	public static inline var GENERAL_BAD_ENDER:String				= "badender"; 			//Cause 3 bad ends to various NPCs.
	public static inline var GENERAL_GAME_OVER:String				= "gameover"; 			//Get a Bad End.
	public static inline var GENERAL_URINE_TROUBLE:String			= "urinetrouble"; 		//Urinate once in the realm of Mareth.
	public static inline var GENERAL_WHATS_HAPPENING_TO_ME:String	= "whatshappeningtome"; //Transform for the first time. 
	public static inline var GENERAL_TRANSFORMER:String				= "transformer"; 		//Transform 10 times.
	public static inline var GENERAL_SHAPESHIFTY:String				= "shapeshifty"; 		//Transform 25 times.
	public static inline var GENERAL_FAPFAPFAP:String				= "fapfapfap"; 			//Masturbate for the first time.
	public static inline var GENERAL_FAPTASTIC:String				= "faptastic"; 			//Masturbate 10 times.
	public static inline var GENERAL_FAPSTER:String					= "fapster"; 			//Masturbate 100 times.
	public static inline var GENERAL_HELSPAWN:String				= "helspawn";
	public static inline var GENERAL_GOO_ARMOR:String				= "gooarmor";
	public static inline var GENERAL_URTA_TRUE_LOVER:String			= "urtatruelover";
	public static inline var GENERAL_DRESSTACULAR:String			= "dresstacular";
	public static inline var GENERAL_GODSLAYER:String				= "godslayer";
	public static inline var GENERAL_GOTTA_LOVE_THEM_ALL:String		= "gottalovethemall";
	public static inline var GENERAL_FOLLOW_THE_LEADER:String		= "followtheleader";
	public static inline var GENERAL_MEET_YOUR_MASTER:String		= "meetyourmaster";
	public static inline var GENERAL_MEET_YOUR_MASTER_TRUE:String	= "meetyourmastertrue";
	public static inline var GENERAL_ALL_UR_PPLZ_R_BLNG_2_ME:String	= "allurpplsrblng2me";
	public static inline var GENERAL_SCHOLAR:String					= "scholar";
	public static inline var GENERAL_HOLEY_BEING:String				= "holeybeing";
	public static inline var GENERAL_TRANSGENDER:String				= "transgender";
	public static inline var GENERAL_FREELOADER:String				= "freeloader";
	public static inline var GENERAL_PERKY:String					= "perky";
	public static inline var GENERAL_SUPER_PERKY:String				= "superperky";
	public static inline var GENERAL_STATS_50:String				= "stats50";
	public static inline var GENERAL_STATS_100:String				= "stats100";
	public static inline var GENERAL_SCHIZO:String					= "schizo";
	public static inline var GENERAL_CLEAN_SLATE:String				= "cleanslate";
	public static inline var GENERAL_EVERYBODY_LOVES_ME:String		= "everybodylovesme";
	public static inline var GENERAL_RETURN_TO_INGNAM:String		= "returntoignam";
	public static inline var GENERAL_LIKE_CHUCK_NORRIS:String		= "likechucknoris";
	public static inline var GENERAL_FAIRY_FRENZY:String			= "fairyfrenzy";
	public static inline var GENERAL_TENTACLE_BEAST_SLAYER:String	= "tentaclebeastslayer";
	public static inline var GENERAL_HOME_SWEET_HOME:String			= "homesweethome";
	public static inline var GENERAL_GETAWAY:String					= "getaway";
	public static inline var GENERAL_CONVERSATIONALIST:String		= "conversationalist";
	public static inline var GENERAL_PEOPLE_PERSON:String			= "peopleperson";
	public static inline var GENERAL_RESTART_THE_RACES:String		= "restartheraces";
	public static inline var GENERAL_OLD_TIMES:String				= "oldtimes";
	public static inline var GENERAL_REGULAR:String					= "regular";
	public static inline var GENERAL_DRUNK:String					= "drunk";
	public static inline var GENERAL_SMASHED:String					= "smashed";
	public static inline var GENERAL_IM_NO_LUMBERJACK:String		= "imnolumberjack";
	public static inline var GENERAL_DEFORESTER:String				= "deforester";
	public static inline var GENERAL_HAMMER_TIME:String				= "hammertime";
	public static inline var GENERAL_NAIL_SCAVENGER:String			= "nailscavenger";
	public static inline var GENERAL_FREE_ALL_THE_SLAVES:String		= "freealltheslaves";
	public static inline var GENERAL_YIN_YANG:String				= "yinyang";
	public static inline var GENERAL_MY_TENT_NOT_BETTER:String		= "mytentnotbetter";
	public static inline var GENERAL_MINERVA_PURIFICATION:String	= "minervapurification"; //Shadow achievement
	public static inline var GENERAL_FENCER:String					= "fencer"; //Shadow achievement
	public static inline var GENERAL_GAME_BREAKER:String			= "gamebreaker"; //Might be awarded for breaking the game.
	public static inline var GENERAL_FUCK_WITH_PORTALS:String		= "fuckwithportals";
	public static inline var GENERAL_GETTING_WOOD:String			= "gettingwood"; //Minecraft reference
	public static inline var GENERAL_DICK_BANISHER:String			= "dickbanisher";
	public static inline var GENERAL_ULTRA_PERKY:String				= "ultraperky";
	public static inline var GENERAL_UP_TO_11:String				= "uptoeleven"; //This one goes to eleven.
	public static inline var GENERAL_YOU_BASTARD:String				= "youbastard";
	public static inline var GENERAL_YABBA_DABBA_DOO:String			= "yabbadabbadoo";
	public static inline var GENERAL_ANTWORKS:String				= "antworks";
	public static inline var GENERAL_OFF_WITH_HER_HEAD:String		= "offwithherhead"; //Awarded for beheading Lethice.
	public static inline var GENERAL_NOOOOOOO:String				= "nooooooo"; //Insert Darth Vader voice here.
	public static inline var GENERAL_KAIZO_TRAP:String				= "kaizotrap"; //Fall victim to Kaizo Trap. NOT USED.
	public static inline var GENERAL_SAVE_SCUMMER:String			= "savescummer"; //Save scum. NOT USED.
	public static inline var GENERAL_MAKE_MARETH_GREAT_AGAIN:String	= "makemarethgreatagain"; //Make Mareth Great Again indeed!
	
	//OtherCoCAnon achievements
	public static inline var GENERAL_PARASITE_QUEEN:String			= "parasitequeen"; 	//Host 10 eel parasites at once. Not sure if this is even possible!
	public static inline var COMBAT_REVENGEANCE:String				= "revengence";		//Kill someone with a katana counter.
	public static inline var DUNGEON_A_LITTLE_HOPE:String			= "alittlehope";	//Completed the dullahan's request. A little hope, however desperate, is never without worth.
}