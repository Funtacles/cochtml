package data.game.perk;
import data.Player;

/**
 * @author Funtacles
 */
typedef PerkReq =
{
	var eval:Player -> Bool;
	var desc:String;
	var type:Int;
}