package data.game.perk;
import data.Player;
import enums.Stat;

class Perk 
{
	private var _id:String;
	private var _name:String;
	private var _desc:String;
	private var _longDesc:String;
	private var _keepOnAscension:Bool;

	public function id():String
	{
		return _id;
	}

	public function name():String
	{
		return _name;
	}

	/**
	 * Short description used in list
	 */
	public function desc():String
	{
		if (!Util.isNullOrUndefined(_dynDesc))
			return _dynDesc(this);
		
		return _desc;
	}
	
	/**
	 * Returns the content of _desc regardless of whether or not a dynamicDesc has been specified.
	 * @return
	 */
	public function rawDesc():String {
		return _desc;
	}
	
	private var _dynDesc:Perk -> String;
	public function addDynamicDesc(desc:Perk -> String):Perk {
		_dynDesc = desc;
		return this;
	}

	/**
	 * Long description used during level up
	 */
	public function longDesc():String
	{
		return _longDesc;
	}

	public function keepOnAscension(respec:Bool = false):Bool
	{
		return _keepOnAscension;
	}

	public function new(id:String, name:String, desc:String, longDesc:String = null, keepOnAscension:Bool = false)
	{
		this._id = id;
		this._name = name;
		this._desc = desc;
		this._longDesc = longDesc != null ? longDesc : _desc;
		this._keepOnAscension = keepOnAscension;
		
		this.requirements = new Array<PerkReq>();
	}

	public var requirements:Array<PerkReq>;

	/**
	 * Returns a string containing the text descriptions of all requirements.
	 */
	public function requirementsList():String {
		var list = new Array<String>();
		for (r in requirements) {
			list.push(r.desc);
		}
		
		return list.join(", ");
	}
	
	public function available(player:Player):Bool {
		for (c in requirements) {
			if (!c.eval(player)) 
				return false;
		}
		
		return true;
	}
	
	public static inline var AttributeLevelType:Int = 0;
	public static inline var StatValueType:Int = 1;
	public static inline var PlayerLevelType:Int = 2;
	public static inline var PerkUnlockedType:Int = 3;
	
	
	public function addRequirement(pr:PerkReq):Perk {
		requirements.push(pr);
		return this;
	}
	
	public function requireLevel(value:Int):Perk {
		requirements.push({
			eval  : function(player:Player):Bool {
				return player.level >= value;
			},
			desc: "Level " + value,
			type: PlayerLevelType
		});
		return this;
	}

	/**
	 * Adds a stat requirment to the perk for a minimum value of the specified stat.
	 * @param	stat	The stat value to check.
	 * @param	value	The minimum allowed value, inclusive.
	 * @return	This perk to allow for chaining.
	 */
	public function requireStatMin(stat:Stat, value:Int):Perk {
		requirements.push({
			eval  : function(player:Player):Bool {
				return player.stats[stat] >= value;
			},
			desc: stat.getName() + " above " + value,
			type: StatValueType
		});
		return this;
	}
	
	/**
	 * Adds a stat requirment to the perk for a maximum value of the specified stat.
	 * @param	stat	The stat value to check.
	 * @param	value	The maximum allowed value, inclusive.
	 * @return	This perk to allow for chaining.
	 */
	public function requireStatMax(stat:Stat, value:Int):Perk {
		requirements.push({
			eval  : function(player:Player):Bool {
				return player.stats[stat] <= value;
			},
			desc:stat.getName() + " below " + value,
			type: StatValueType
		});
		return this;
	}
	
	public function requireAttribute(attr:String, value:Int):Perk {
		requirements.push({
			eval  : function(player:Player):Bool {
				return Reflect.field(player, attr) >= value;
			},
			desc: StringUtil.capitalize(attr) + " " + value,
			type: PlayerLevelType
		});
		return this;
	}
	
	public function requireMaxAttribute(attr:String, value:Int):Perk {
		requirements.push({
			eval  : function(player:Player):Bool {
				return Reflect.field(player, attr) < value;
			},
			desc: StringUtil.capitalize(attr) + " " + value,
			type: AttributeLevelType
		});
		return this;
	}

	//public function requireStatusEffect(effect:StatusEffectType, text:String):Perk {
		//requirements.push({
			//fn  : function (player:Player):Boolean {
				//return player.hasStatusEffect(effect);
			//},
			//text: text,
			//type: "effect",
			//effect: effect
		//});
		//return this;
	//}
	
	public function requirePerk(perkId:String):Perk {
		var perk = PerkStore.getPerk(perkId);
		
		requirements.push({
			eval  : function (player:Player):Bool {
				return player.findPerk(perkId) >= 0;
			},
			desc: perk.name(),
			type: PerkUnlockedType
		});
		return this;
	}
	//public function requireAnyPerk(perks:Array<String>):Perk {
		//
		//var perkList:Array<String> = perks.map<Perk>(PerkStore.getPerk).map();
		//
		//requirements.push({
			//fn  : function (player:Player):Bool {
				//for (perk in perks) {
					//if (player.findPerk(perk) >= 0)
						//return true;
				//}
				//return false;
			//},
			//desc: perkList.join(" or "),
			//type: PerkUnlockedType,
		//});
		//return this;
	//}
	
}