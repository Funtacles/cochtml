package data.game.perk;
import enums.Stat;

/**
 * ...
 * @author Funtacles
 */
class PerkStore 
{
		// Player creation perks
		public static inline var Fast:String = "fast";
		public static inline var Lusty:String = "lusty";
		public static inline var Pervert:String = "pervert";
		public static inline var Sensitive:String = "sensitive";
		public static inline var Smart:String = "smart";
		public static inline var Strong:String = "strong";
		public static inline var Tough:String = "tough";
		// Female creation perks
		public static inline var BigClit:String = "bigclit";
		public static inline var BigTits:String = "bigtits";
		public static inline var Fertile:String = "fertile";
		public static inline var WetPussy:String = "wetpussy";
		// Male creation perks
		public static inline var BigCock:String = "bigcock";
		public static inline var MessyOrgasms:String = "messyorgasms";
				
		// Ascension perks
		public static inline var AscensionDesires:String = "ascensiondesires";
		public static inline var AscensionEndurance:String = "ascensionendurance";
		public static inline var AscensionFertility:String = "ascensionfertility";
		public static inline var AscensionFortune:String = "ascensionfortune";
		public static inline var AscensionMoralShifter:String = "ascensionmoralshifter";
		public static inline var AscensionMysticality:String = "ascensionmysticality";
		public static inline var AscensionTolerance:String = "ascensiontolerance";
		public static inline var AscensionVirility:String = "ascensionvirility";
		public static inline var AscensionWisdom:String = "ascensionwisdom";
		public static inline var AscensionMartiality:String = "ascensionmartiality";
	    public static inline var AscensionSeduction:String = "ascensionseduction";
		// History perks
		public static inline var HistoryAlchemist:String = "historyalchemist";
		public static inline var HistoryFighter:String = "historyfighter";
		public static inline var HistoryFortune:String = "historyfortune";
		public static inline var HistoryHealer:String = "historyhealer";
		public static inline var HistoryReligious:String = "historyreligious";
		public static inline var HistoryScholar:String = "historyscholar";
		public static inline var HistorySlacker:String = "historyslacker";
		public static inline var HistorySlut:String = "historyslut";
		public static inline var HistorySmith:String = "historysmith";
		public static inline var HistoryWhore:String = "historywhore";
		
		
		//// Ordinary (levelup) perks
		//public static inline var Acclimation:String = mk("Acclimation", "Acclimation",
				//"Reduces lust gain by 15%.",
				//"You choose the 'Acclimation' perk, making your body 15% more resistant to lust, up to a maximum of 75%.");
		//public static inline var Agility:String = mk("Agility", "Agility",
				//"Boosts armor points by a portion of your speed on light/medium armors.",
				//"You choose the 'Agility' perk, increasing the effectiveness of Light/Medium armors by a portion of your speed.");
		//public static inline var Archmage:String = mk("Archmage", "Archmage",
				//"[if (player.inte>=75)" +
						//"Increases base spell strength by 50%." +
						//"|" +
						//"<b>You are too dumb to gain benefit from this perk.</b>" +
						//"]",
				//"You choose the 'Archmage' perk, increasing base spell strength by 50%.");
		//public static inline var ArousingAura:String = mk("Arousing Aura", "Arousing Aura",
				//"Exude a lust-inducing aura (Req's corruption of 70 or more)",
				//"You choose the 'Arousing Aura' perk, causing you to radiate an aura of lust when your corruption is over 70.");
		//public static inline var Battlemage:String = mk("Battlemage", "Battlemage",
				//"Start every battle with Might enabled, if you meet Black Magic requirements before it starts.",
				//"You choose the 'Battlemage' perk. You start every battle with Might effect, as long as your Lust is sufficient to cast it before battle.");
		//public static inline var Berzerker:String = mk("Berzerker", "Berserker",
				//"[if (player.str>=75)" +
						//"Grants 'Berserk' ability." +
						//"|" +
						//"<b>You aren't strong enough to benefit from this anymore.</b>" +
						//"]",
				//"You choose the 'Berserker' perk, which unlocks the 'Berserk' magical ability.  Berserking increases attack and lust resistance but reduces physical defenses.");
		//public static inline var Blademaster:String = mk("Blademaster", "Blademaster",
				//"Gain +5% to critical strike chance when wielding a sword and not using a shield.",
				//"You choose the 'Blademaster' perk.  Your chance of critical hit is increased by 5% as long as you're wielding a sword and not using a shield.");
		//public static inline var Brawler:String = mk("Brawler", "Brawler",
				//"Brawling experience allows you to make two unarmed attacks in a turn.",
				//"You choose the 'Brawler' perk, allowing you to make two unarmed attacks in a turn!");
		//public static inline var BrutalBlows:String = mk("Brutal Blows", "Brutal Blows",
				//"[if (player.str>=75)" +
						//"Reduces enemy armor with each hit." +
						//"|" +
						//"<b>You aren't strong enough to benefit from this anymore.</b>" +
						//"]",
				//"You choose the 'Brutal Blows' perk, which reduces enemy armor with each hit.");
		//public static inline var Channeling:String = mk("Channeling", "Channeling",
				//"Increases base spell strength by 50%.",
				//"You choose the 'Channeling' perk, boosting the strength of your spellcasting!");
		//public static inline var ColdBlooded:String = mk("Cold Blooded", "Cold Blooded",
				//"Reduces minimum lust by up to 20, down to min of 20. Caps min lust at 80.",
				//"You choose the 'Cold Blooded' perk.  Thanks to increased control over your desires, your minimum lust is reduced! (Caps minimum lust at 80. Won't reduce minimum lust below 20 though.)");
		//public static inline var ColdFury:String = mk("Cold Fury", "Cold Fury",
				//"Berserking halves your defense instead of reducing to zero.",
				//"You choose the 'Cold Fury' perk, causing Berserking to only reduce your armor by half instead of completely reducing to zero.");
		//public static inline var CorruptedLibido:String = mk("Corrupted Libido", "Corrupted Libido",
				//"Reduces lust gain by 10%.",
				//"You choose the 'Corrupted Libido' perk.  As a result of your body's corruption, you've become a bit harder to turn on. (Lust gain reduced by 10%!)");
		public static inline var DoubleAttack:String = "doubleattack";
		public static inline var Evade:String = "evade";
		//public static inline var FertilityMinus:String = mk("Fertility-", "Fertility-",
				//"Decreases fertility rating by 15 and cum volume by up to 30%. (Req's libido of less than 25.)",
				//"You choose the 'Fertility-' perk, making it harder to get pregnant.  It also decreases your cum volume by up to 30% (if appropriate)!");
		//public static inline var FertilityPlus:String = mk("Fertility+", "Fertility+",
				//"Increases fertility rating by 15 and cum volume by up to 50%.",
				//"You choose the 'Fertility+' perk, making it easier to get pregnant.  It also increases your cum volume by up to 50% (if appropriate)!");
		//public static inline var FocusedMind:String = mk("Focused Mind", "Focused Mind",
				//"Black Magic is less likely to backfire and White Magic threshold is increased.",
				//"You choose the 'Focused Mind' perk. Black Magic is less likely to backfire and White Magic threshold is increased.");
		//public static inline var HoldWithBothHands:String = mk("Hold With Both Hands", "Hold With Both Hands",
				//"Gain +20% strength modifier with melee weapons when not using a shield.",
				//"You choose the 'Hold With Both Hands' perk.  As long as you're wielding a melee weapon and you're not using a shield, you gain 20% strength modifier to damage.");
		//public static inline var HotBlooded:String = mk("Hot Blooded", "Hot Blooded",
				//"Raises minimum lust by up to 20.",
				//"You choose the 'Hot Blooded' perk.  As a result of your enhanced libido, your lust no longer drops below 20! (If you already have some minimum lust, it will be increased by 10)");
		//public static inline var ImmovableObject:String = mk("Immovable Object", "Immovable Object",
				//"[if (player.tou>=75)" +
						//"Grants 10% physical damage reduction.</b>" +
						//"|" +
						//"<b>You aren't tough enough to benefit from this anymore.</b>" +
						//"]",
				//"You choose the 'Immovable Object' perk, granting 10% physical damage reduction.</b>");
		//public static inline var ImprovedEndurance:String = mk("Improved Endurance", "Improved Endurance",
				//"Increases maximum fatigue by 20.",
				//"You choose the 'Improved Endurance' perk. Thanks to your physical conditioning, your maximum fatigue has been increased by 20!</b>");
		//public static inline var ImprovedSelfControl:String = mk("Improved Self-Control", "Improved Self-Control",
				//"Increases maximum lust by 20.",
				//"You choose the 'Improved Self-Control' perk. Thanks to your mental conditioning, your maximum lust has been increased by 20!</b>");
		//public static inline var IronFists:String = mk("Iron Fists", "Iron Fists",
				//"Hardens your fists to increase attack rating by 5.",
				//"You choose the 'Iron Fists' perk, hardening your fists. This increases attack power by 5.");
		//public static inline var IronFists2:String = mk("Iron Fists 2", "Iron Fists 2",
				//"Further hardens your fists to increase attack rating by another 3.",
				//"You choose the 'Iron Fists 2' perk, further hardening your fists. This increases attack power by another 3.");
		//public static inline var IronFists3:String = mk("Iron Fists 3", "Iron Fists 3",
				//"Even more hardens your fists to increase attack rating again by 3.",
				//"You choose the 'Iron Fists 3' perk, even further hardening your fists. This increases attack power again by 3.");				
		//public static inline var IronMan:String = mk("Iron Man", "Iron Man",
				//"Reduces the fatigue cost of physical specials by 50%.",
				//"You choose the 'Iron Man' perk, reducing the fatigue cost of physical special attacks by 50%");
		//public static inline var Juggernaut:String = mk("Juggernaut", "Juggernaut",
				//"When wearing heavy armor, you have extra 10 armor points and are immune to damage from being inline varricted/squeezed.",
				//"You choose the 'Juggernaut' perk, granting extra 10 armor points when wearing heavy armor and immunity to damage from been inline varricted/squeezed.");
		//public static inline var LightningStrikes:String = mk("Lightning Strikes", "Lightning Strikes",
				//"[if (player.spe>=60)" +
						//"Increases the attack damage for non-heavy weapons.</b>" +
						//"|" +
						//"<b>You are too slow to benefit from this perk.</b>" +
						//"]",
				//"You choose the 'Lightning Strikes' perk, increasing the attack damage for non-heavy weapons.</b>");
		//public static inline var LungingAttacks:String = mk("Lunging Attacks", "Lunging Attacks",
				//"[if (player.spe>=75)" +
						//"Grants 50% armor penetration for standard attacks." +
						//"|" +
						//"<b>You are too slow to benefit from this perk.</b>" +
						//"]",
				//"You choose the 'Lunging Attacks' perk, granting 25% armor penetration for standard attacks.");
		//public static inline var Mage:String = mk("Mage", "Mage",
				//"Increases base spell strength by 50%.",
				//"You choose the 'Mage' perk.  You are able to focus your magical abilities even more keenly, boosting your base spell effects by 50%.");
		//public static inline var Masochist:String = mk("Masochist", "Masochist",
				//"Take 20% less physical damage but gain lust when you take damage.",
				//"You choose the 'Masochist' perk, reducing the damage you take but raising your lust each time!  This perk only functions while your libido is at or above 60!");
		//public static inline var Medicine:String = mk("Medicine", "Medicine",
				//"Grants 15% chance per round of cleansing poisons/drugs from your body. Increases HP restoration on rest.",
				//"You choose the 'Medicine' perk, giving you a chance to remove debilitating poisons automatically! Also, increases HP restoration on rest.");
		//public static inline var Nymphomania:String = mk("Nymphomania", "Nymphomania",
				//"Raises minimum lust by up to 30.",
				//"You've chosen the 'Nymphomania' perk.  Due to the incredible amount of corruption you've been exposed to, you've begun to live in a state of minor inline varant arousal.  Your minimum lust will be increased by as much as 30 (If you already have minimum lust, the increase is 10-15).");
		//public static inline var Parry:String = mk("Parry", "Parry",
				//"[if (player.spe>=50)" +
						//"Increases deflect chance by up to 10% while wielding a weapon. (Speed-based)." +
						//"|" +
						//"<b>You are not durable enough to gain benefit from this perk.</b>" +
						//"]",
				//"You choose the 'Parry' perk, giving you a chance to deflect blow with your weapon. (Speed-based).");
		//public static inline var Precision:String = mk("Precision", "Precision",
				//"Reduces enemy armor by 10. (Req's 25+ Intelligence)",
				//"You've chosen the 'Precision' perk.  Thanks to your intelligence, you're now more adept at finding and striking an enemy's weak points, reducing their damage resistance from armor by 10.  If your intelligence ever drops below 25 you'll no longer be smart enough to benefit from this perk.");
		//public static inline var RagingInferno:String = mk("Raging Inferno", "Raging Inferno",
				//"Cumulative 20% damage increase for every subsequent fire spell without interruption.",
				//"You choose the 'Raging Inferno' perk. Cumulative 20% damage increase for every subsequent fire spell without interruption.");
		//public static inline var Regeneration:String = "regeneration";
		//public static inline var Regeneration2:String = "regeneration2";
		//public static inline var Resistance:String = mk("Resistance", "Resistance",
				//"Reduces lust gain by 10%.",
				//"You choose the 'Resistance' perk, reducing the rate at which your lust increases by 10%.");
		//public static inline var Resolute:String = mk("Resolute", "Resolute",
				//"[if (player.tou>=75)" +
						//"Grants immunity to stuns and some statuses.</b>" +
						//"|" +
						//"<b>You aren't tough enough to benefit from this anymore.</b>" +
						//"]",
				//"You choose the 'Resolute' perk, granting immunity to stuns and some statuses.</b>");
		public static inline var Runner:String = "runner";
		//public static inline var Sadist:String = mk("Sadist", "Sadist",
				//"Deal 20% more damage, but gain lust at the same time.",
				//"You choose the 'Sadist' perk, increasing damage by 20 percent but causing you to gain lust from dealing damage.");
		//public static inline var Seduction:String = mk("Seduction", "Seduction",
				//"Upgrades your tease attack, making it more effective.",
				//"You choose the 'Seduction' perk, upgrading the 'tease' attack with a more powerful damage and a higher chance of success.");
		//public static inline var ShieldMastery:String = mk("Shield Mastery", "Shield Mastery",
				//"[if (player.tou>=50)" +
						//"Increases block chance by up to 10% while using a shield (Toughness-based)." +
						//"|" +
						//"<b>You are not durable enough to gain benefit from this perk.</b>" +
						//"]",
				//"You choose the 'Shield Mastery' perk, increasing block chance by up to 10% as long as you're wielding a shield (Toughness-based).");
		//public static inline var ShieldSlam:String = mk("Shield Slam", "Shield Slam",
				//"Reduces shield bash diminishing returns by 50% and increases bash damage by 20%.",
				//"You choose the 'Shield Slam' perk.  Stun diminishing returns is reduced by 50% and shield bash damage is increased by 20%.");
		//public static inline var SpeedyRecovery:String = mk("Speedy Recovery", "Speedy Recovery",
				//"Regain fatigue 50% faster.",
				//"You choose the 'Speedy Recovery' perk, boosting your fatigue recovery rate!");
		//public static inline var Spellpower:String = mk("Spellpower", "Spellpower",
				//"Increases base spell strength by 50%.",
				//"You choose the 'Spellpower' perk.  Thanks to your sizeable intellect and willpower, you are able to more effectively use magic, boosting base spell effects by 50%.");
		//public static inline var Spellsword:String = mk("Spellsword", "Spellsword",
				//"Start every battle with Charge Weapon enabled, if you meet White Magic requirements before it starts.",
				//"You choose the 'Spellsword' perk. You start every battle with Charge Weapon effect, as long as your Lust is not preventing you from casting it before battle.");
		//public static inline var Survivalist:String = mk("Survivalist", "Survivalist",
				//"Slows hunger rate by 20%.",
				//"You choose the 'Survivalist' perk.  With this perk, your hunger rate is reduced by 20%.");
		//public static inline var Survivalist2:String = mk("Survivalist 2", "Survivalist 2",
				//"Slows hunger rate by further 20%.",
				//"You choose the 'Survivalist 2' perk.  With this perk, your hunger rate is reduced by another 20%.");
		//public static inline var StaffChanneling:String = mk("Staff Channeling", "Staff Channeling",
				//"Basic attack with wizard's staff is replaced with ranged magic bolt.",
				//"You choose the 'Staff Channeling' perk. Basic attack with wizard's staff is replaced with ranged magic bolt.");
		//public static inline var StrongBack:String = mk("Strong Back", "Strong Back",
				//"Enables fourth item slot.",
				//"You choose the 'Strong Back' perk, enabling a fourth item slot.");
		//public static inline var StrongBack2:String = mk("Strong Back 2: Strong Harder", "Strong Back 2: Strong Harder",
				//"Enables fifth item slot.",
				//"You choose the 'Strong Back 2: Strong Harder' perk, enabling a fifth item slot.");
		//public static inline var Tactician:String = mk("Tactician", "Tactician",
				//"[if (player.inte>=50)" +
						//"Increases critical hit chance by up to 10% (Intelligence-based)." +
						//"|" +
						//"<b>You are too dumb to gain benefit from this perk.</b>" +
						//"]",
				//"You choose the 'Tactician' perk, increasing critical hit chance by up to 10% (Intelligence-based).");
		public static inline var Tank:String = "tank";
		public static inline var Tank2:String = "tank2";
		//public static inline var ThunderousStrikes:String = mk("Thunderous Strikes", "Thunderous Strikes",
				//"+20% 'Attack' damage while strength is at or above 80.",
				//"You choose the 'Thunderous Strikes' perk, increasing normal damage by 20% while your strength is over 80.");
		//public static inline var Unhindered:String = mk("Unhindered", "Unhindered",
				//"Increases chances of evading enemy attacks when you are naked. (Undergarments won't disable this perk.)",
				//"You choose the 'Unhindered' perk, granting chance to evade when you are naked.");
		//public static inline var WeaponMastery:String = mk("Weapon Mastery", "Weapon Mastery",
				//"[if (player.str>60)" +
						//"Doubles damage bonus of weapons classified as 'Large'." +
						//"|" +
						//"<b>You aren't strong enough to benefit from this anymore.</b>" +
						//"]",
				//"You choose the 'Weapon Mastery' perk, doubling the effectiveness of large weapons.");
		//public static inline var WellAdjusted:String = mk("Well Adjusted", "Well Adjusted",
				//"You gain half as much lust as time passes in Mareth.",
				//"You choose the 'Well Adjusted' perk, reducing the amount of lust you naturally gain over time while in this strange land!");
//
		//// Needlework perks
		//public static inline var ChiReflowAttack:String = mk("Chi Reflow - Attack", "Chi Reflow - Attack",
				//"Regular attacks boosted, but damage resistance decreased.");
		//public static inline var ChiReflowDefense:String = mk("Chi Reflow - Defense", "Chi Reflow - Defense",
				//"Passive damage resistance, but caps speed");
		//public static inline var ChiReflowLust:String = mk("Chi Reflow - Lust", "Chi Reflow - Lust",
				//"Lust resistance and Tease are enhanced, but Libido and Sensitivity gains increased.");
		//public static inline var ChiReflowMagic:String = mk("Chi Reflow - Magic", "Chi Reflow - Magic",
				//"Magic attacks boosted, but regular attacks are weaker.");
		//public static inline var ChiReflowSpeed:String = mk("Chi Reflow - Speed", "Chi Reflow - Speed",
				//"Speed reductions are halved but caps strength");
//
		//// Piercing perks
		//public static inline var PiercedCrimstone:String = "piercedcrimstone";
		//public static inline var PiercedIcestone:String = "piercedicestone";
		//public static inline var PiercedFertite:String = "piercedfertite";
		//public static inline var PiercedFurrite:String = mk("Pierced: Furrite", "Pierced: Furrite",
				//"Increases chances of encountering 'furry' foes.");
		//public static inline var PiercedLethite:String = mk("Pierced: Lethite", "Pierced: Lethite",
				//"Increases chances of encountering demonic foes.");

		//// Armor perks
		//public static inline var BloodMage:String = mk("Blood Mage", "Blood Mage",
				//"Spellcasting now consumes health instead of fatigue!",null,true);
		//public static inline var SluttySeduction:String = "sluttyseduction";
		//public static inline var WizardsEndurance:String = "wizardsendurance";
		//public static inline var WellspringOfLust:String = mk("Wellspring of Lust", "Wellspring of Lust",
				//"At the beginning of combat, lust raises to black magic threshold if lust is below black magic threshold.");
//
		//// Weapon perks
		//public static inline var WizardsFocus:String = "wizardsfocus";
		//public static inline var Cunning:String = mk("Cunning", "Cunning", "Increases critical chance, but reduces critical damage.");
//
		//// Achievement perks
		//public static inline var BroodMother:String = mk("Brood Mother", "Brood Mother",
				//"Pregnancy moves twice as fast as a normal woman's.");
		//public static inline var SpellcastingAffinity:String = "spellcastingaffinity";
		//public static inline var KillerInstinct:String = "killerinstinct";
//
		//// Mutation perks
		//public static inline var Androgyny:String = mk("Androgyny", "Androgyny",
				//"No gender limits on facial masculinity or femininity.");
		//public static inline var BasiliskWomb:String = mk("Basilisk Womb", "Basilisk Womb",
				//"Enables your eggs to be properly fertilized into basilisks of both genders!");
		//public static inline var BeeOvipositor:String = mk("Bee Ovipositor", "Bee Ovipositor",
				//"Allows you to lay eggs through a special organ on your insect abdomen, though you need at least 10 eggs to lay.");
		//public static inline var BimboBody:String = mk("Bimbo Body", "Bimbo Body",
				//"Gives the body of a bimbo.  Tits will never stay below a 'DD' cup, libido is raised, lust resistance is raised, and upgrades tease.");
		//public static inline var BimboBrains:String = mk("Bimbo Brains", "Bimbo Brains",
				//"Now that you've drank bimbo liquer, you'll never, like, have the attention span and intelligence you once did!  But it's okay, 'cause you get to be so horny an' stuff!");
		//public static inline var BroBody:String = mk("Bro Body", "Bro Body",
				//"Grants an ubermasculine body that's sure to impress.");
		//public static inline var BroBrains:String = mk("Bro Brains", "Bro Brains",
				//"Makes thou... thin... fuck, that shit's for nerds.");
		//public static inline var BunnyEggs:String = mk("Bunny Eggs", "Bunny Eggs",
				//"Laying eggs has become a normal part of your bunny-body's routine.");
		//public static inline var CorruptedNinetails:String = mk("Corrupted Nine-tails", "Corrupted Nine-tails",
				//"The mystical energy of the nine-tails surges through you, filling you with phenomenal cosmic power!  Your boundless magic allows you to recover quickly after casting spells, but your method of attaining it has corrupted the transformation, preventing you from achieving true enlightenment.",null,true);
		//public static inline var Diapause:String = mk("Diapause", "Diapause",
				//"Pregnancy does not advance normally, but develops quickly after taking in fluids.");
		public static inline var Dragonfire:String = "dragonfire";
		//public static inline var EnlightenedNinetails:String = mk("Enlightened Nine-tails", "Enlightened Nine-tails",
				//"The mystical energy of the nine-tails surges through you, filling you with phenomenal cosmic power!  Your boundless magic allows you to recover quickly after casting spells.",null,true);
		//public static inline var Feeder:String = mk("Feeder", "Feeder",
				//"Lactation does not decrease and gives a compulsion to breastfeed others.");
		//public static inline var Flexibility:String = mk("Flexibility", "Flexibility",
				//"Grants cat-like flexibility.  Useful for dodging and 'fun'.");
		//public static inline var FutaFaculties:String = mk("Futa Faculties", "Futa Faculties",
				//"It's super hard to think about stuff that like, isn't working out or fucking!");
		//public static inline var FutaForm:String = mk("Futa Form", "Futa Form",
				//"Ensures that your body fits the Futa look (Tits DD+, Dick 8\"+, & Pussy).  Also keeps your lusts burning bright and improves the tease skill.");
		//public static inline var HarpyWomb:String = mk("Harpy Womb", "Harpy Womb",
				//"Increases all laid eggs to large size so long as you have harpy legs and a harpy tail.");
		//public static inline var Incorporeality:String = mk("Incorporeality", "Incorporeality",
				//"Allows you to fade into a ghost-like state and temporarily possess others.");
		//public static inline var Lustzerker:String = mk("Lustserker", "Lustserker",
				//"Grants 'Lustserk' ability.");
		//public static inline var MilkMaid:String = "milkmaid";
		//public static inline var MinotaurCumAddict:String = mk("Minotaur Cum Addict", "Minotaur Cum Addict",
				//"Causes you to crave minotaur cum frequently.  You cannot shake this addiction.");
		//public static inline var MinotaurCumResistance:String = mk("Minotaur Cum Resistance", "Minotaur Cum Resistance",
				//"You can never become a Minotaur Cum Addict. Grants immunity to Minotaur Cum addiction.");
		//public static inline var Oviposition:String = mk("Oviposition", "Oviposition",
				//"Causes you to regularly lay eggs when not otherwise pregnant.");
		//public static inline var PurityBlessing:String = mk("Purity Blessing", "Purity Blessing",
				//"Reduces the rate at which your corruption, libido, and lust increase. Reduces minimum libido slightly.");
		//public static inline var RapierTraining:String = mk("Rapier Training", "Rapier Training",
				//"After finishing of your training, increase attack power of any rapier you're using.");
		//public static inline var SatyrSexuality:String = mk("Satyr Sexuality", "Satyr Sexuality",
				//"Thanks to your satyr biology, you now have the ability to impregnate both vaginas and asses. Also increases your virility rating. (Anal impregnation not implemented yet)");
		//public static inline var SlimeCore:String = mk("Slime Core", "Slime Core",
				//"Grants more control over your slimy body, allowing you to go twice as long without fluids.");
		public static inline var SpiderOvipositor:String = "spiderovipositor";
		//public static inline var ThickSkin:String = mk("Thick Skin", "Thick Skin",
				//"Toughens your dermis to provide 2 points of armor.");
		//public static inline var TransformationResistance:String = mk("Transformation Resistance", "Transformation Resistance",
				//"Reduces the likelihood of undergoing a transformation. Disables Bad Ends from transformative items.");
		//public static inline var LoliliciousBody:String = mk("Lolilicious Body", "Lolilicious Body",
				//"Gives the body of a loli.  Stature is smaller than an adult, with small breasts.");
		//public static inline var ParasiteMusk:String = mk("Parasite Musk", "Parasite Musk",
				//"The Bog parasite inline varantly releases pheromones that boost cum production and allow for a special move in combat.");		
	//
				//
		//// Quest, Event & NPC perks
		//public static inline var AChristmasCarol:String = mk("A Christmas Carol", "A Christmas Carol",
				//"Grants year-round access to Christmas event. Note that some events are only accessible once per year.", null, true);
		//public static inline var BasiliskResistance:String = mk("Basilisk Resistance", "Basilisk Resistance",
				//"Grants immunity to Basilisk's paralyzing gaze. Disables Basilisk Bad End.");
		//public static inline var BulgeArmor:String = mk("Bulge Armor", "Bulge Armor",
				//"Grants a 5 point damage bonus to dick-based tease attacks.");
		public static inline var Cornucopia:String = "cornucopia";
		//public static inline var CounterAB:String = mk("Counter Stance", "Counter Stance",
				//"The Dullahan's teachings allows you to enter a countering stance in combat.", null, true);			
		//public static inline var ElvenBounty:String = "elvenbounty";
		//public static inline var FerasBoonAlpha:String = mk("Fera's Boon - Alpha", "Fera's Boon - Alpha",
				//"Increases the rate your cum builds up and cum production in general.", null, true);
		//public static inline var FerasBoonBreedingBitch:String = mk("Fera's Boon - Breeding Bitch", "Fera's Boon - Breeding Bitch",
				//"Increases fertility and reduces the time it takes to birth young.", null, true);
		//public static inline var FerasBoonMilkingTwat:String = mk("Fera's Boon - Milking Twat", "Fera's Boon - Milking Twat",
				//"Keeps your pussy from ever getting too loose and increases pregnancy speed.");
		//public static inline var FerasBoonSeeder:String = mk("Fera's Boon - Seeder", "Fera's Boon - Seeder",
				//"Increases cum output by 1,000 mLs.", null, true);
		//public static inline var FerasBoonWideOpen:String = mk("Fera's Boon - Wide Open", "Fera's Boon - Wide Open",
				//"Keeps your pussy permanently gaped and increases pregnancy speed.");
		//public static inline var FireLord:String = mk("Fire Lord", "Fire Lord",
				//"Akbal's blessings grant the ability to breathe burning green flames.");
		//public static inline var Hellfire:String = mk("Hellfire", "Hellfire",
				//"Grants a corrupted fire breath attack, like the hellhounds in the mountains.");
		//public static inline var LuststickAdapted:String = mk("Luststick Adapted", "Luststick Adapted",
				//"Grants immunity to the lust-increasing effects of lust-stick and allows its use.");
		//public static inline var MagicalFertility:String = mk("Magical Fertility", "Magical Fertility",
				//"10% higher chance of pregnancy and increased pregnancy speed.");
		//public static inline var MagicalVirility:String = mk("Magical Virility", "Magical Virility",
				//"200 mLs more cum per orgasm and enhanced virility.");
		//public static inline var MaraesGiftButtslut:String = mk("Marae's Gift - Buttslut", "Marae's Gift - Buttslut",
				//"Makes your anus provide lubrication when aroused.");
		//public static inline var MaraesGiftFertility:String = mk("Marae's Gift - Fertility", "Marae's Gift - Fertility",
				//"Greatly increases fertility and halves base pregnancy speed.");
		//public static inline var MaraesGiftProfractory:String = mk("Marae's Gift - Profractory", "Marae's Gift - Profractory",
				//"Causes your cum to build up at 3x the normal rate.");
		//public static inline var MaraesGiftStud:String = mk("Marae's Gift - Stud", "Marae's Gift - Stud",
				//"Increases your cum production and potency greatly.");
		//public static inline var MarbleResistant:String = mk("Marble Resistant", "Marble Resistant",
				//"Provides resistance to the addictive effects of bottled LaBova milk.");
		//public static inline var MarblesMilk:String = mk("Marble's Milk", "Marble's Milk",
				//"Requires you to drink LaBova milk frequently or eventually die.  You cannot shake this addiction.");
		//public static inline var Misdirection:String = mk("Misdirection", "Misdirection",
				//"Grants additional evasion chances while wearing Raphael's red bodysuit.");
		//public static inline var OmnibusGift:String = mk("Omnibus' Gift", "Omnibus' Gift",
				//"Increases minimum lust but provides some lust resistance.");
		//public static inline var OneTrackMind:String = mk("One Track Mind", "One Track Mind",
				//"Your inline varant desire for sex causes your sexual organs to be able to take larger insertions and disgorge greater amounts of fluid.", null, true);
		//public static inline var PilgrimsBounty:String = mk("Pilgrim's Bounty", "Pilgrim's Bounty",
				//"Causes you to always cum as hard as if you had max lust.", null, true);
		//public static inline var PureAndLoving:String = mk("Pure and Loving", "Pure and Loving",
				//"Your caring attitude towards love and romance makes you slightly more resistant to lust and corruption.", null, true);
		//public static inline var PotentProstate:String = mk("Potent Prostate", "Potent Prostate",
				//"Whenever you have a dick and no balls(or tiny balls), produce cum equivalent to a pair of large balls.", null, true);	
		//public static inline var MysticLearnings:String = mk("Mystic Learnings", "Mystic Learnings",
				//"Reading and fully comprehending the Heptarchia Mystica allows you to cast stronger spells.", null, true);	
		//public static inline var PotentPregnancy:String = mk("Potent Pregnancy", "Potent Pregnancy",
				//"Whenever you are pregnant, you gain strength and toughness bonuses.", null, true);
		//public static inline var ParasiteQueen:String = mk("Parasite Queen", "Parasite Queen",
				//"Allow you to sacrifice a parasite to boost your stats in battle.", null, true);
		//public static inline var Revelation:String = mk("Revelation", "Revelation",
				//"You have seen things beyond mortal comprehension. Peering into the abyss has a lower chance of breaking your psyche.", null, true);
		//public static inline var SensualLover:String = mk("Sensual Lover", "Sensual Lover",
				//"Your sensual attitude towards love and romance makes your tease ability slightly more effective.", null, true);
		//public static inline var Whispered:String = mk("Whispered", "Whispered",
				//"Akbal's blessings grant limited telepathy that can induce fear.");
				
		public static inline var ControlledBreath:String = "controlledbreath";
		public static inline var CleansingPalm:String = "cleansingpalm";
		public static inline var Enlightened:String = "enlightened";
		

		// Monster perks
		public static inline var Acid:String = "acidperk";
		public static inline var PoisonImmune:String = "poisonimmune";
		public static inline var BleedImmune:String = "bleedImmune";

		private static var _perks:Map<String, Perk>;
		public static function init():Void {
			_perks = new Map<String, Perk>();
			
			
			registerPerk(Fast, "Fast", "Gains speed faster.", null, true);
			registerPerk(Lusty, "Lusty", "Gains lust 25% faster.", null, true);
			registerPerk(Pervert, "Pervert", "Gains corruption 25% faster. Reduces corruption requirement for high-corruption variant of scenes.", null, true);
			registerPerk(Sensitive, "Sensitive", "Gains sensitivity 25% faster.", null, true);
			registerPerk(Smart, "Smart", "Gains intelligence faster.", null, true);
			registerPerk(Strong, "Strong", "Gains strength faster.", null, true);
			registerPerk(Tough, "Tough", "Gains toughness faster.", null, true);
			registerPerk(BigClit, "Big Clit", "Allows your clit to grow larger more easily and faster.", null, true);
			registerPerk(BigTits, "Big Tits", "Makes your tits grow larger more easily.", null, true);
			registerPerk(Fertile, "Fertile", "Makes you 15% more likely to become pregnant.", null, true);
			registerPerk(WetPussy, "Wet Pussy", "Keeps your pussy wet and provides a bonus to capacity.", null, true);
			registerPerk(BigCock, "Big Cock", "Gains cock size 25% faster and with less limitations.", null, true);
			registerPerk(MessyOrgasms, "Messy Orgasms", "Produces 50% more cum volume.", null, true);
			
			registerPerk(HistoryAlchemist, "History: Alchemist", "Alchemical experience makes items more reactive to your body.", null, true);
			registerPerk(HistoryFighter, "History: Fighter", "A Past full of conflict increases physical damage dealt by 10%.", null, true);
			registerPerk(HistoryFortune, "History: Fortune", "Your luck and skills at gathering currency allows you to get 15% more gems from victories.", null, true);
			registerPerk(HistoryHealer, "History: Healer", "Healing experience increases HP gains by 20%.", null, true);
			registerPerk(HistoryReligious, "History: Religious", "Replaces masturbate with meditate when corruption less than or equal to 66. Reduces minimum libido slightly.", null, true);
			registerPerk(HistoryScholar, "History: Scholar", "Time spent focusing your mind makes spellcasting 20% less fatiguing.", null, true);
			registerPerk(HistorySlacker, "History: Slacker", "Regenerate fatigue 20% faster.", null, true);
			registerPerk(HistorySlut, "History: Slut", "Sexual experience has made you more able to handle large insertions and more resistant to stretching.", null, true);
			registerPerk(HistorySmith, "History: Smith", "Knowledge of armor and fitting increases armor effectiveness by roughly 10%.", null, true);
			registerPerk(HistoryWhore, "History: Whore","Seductive experience causes your tease attacks to be 15% more effective.", null, true);
			
			registerPerk(Tank, "Tank", "Raises max HP by 50.", "You choose the 'Tank' perk, giving you an additional 50 HP!");
			registerPerk(Tank2, "Tank 2", "+1 extra HP per point of toughness.", "You choose the 'Tank 2' perk, granting an extra maximum HP for each point of toughness.");
			
			//Evade.
			////slot 3 - run perk
			//Runner.requireSpe(25);
			
			registerPerk(Runner, "Runner", "Increases chances of escaping combat.", "You choose the 'Runner' perk, increasing your chances to escape from your foes when fleeing!")
				.requireStatMin(Stat.Speed, 25);
			registerPerk(Evade, "Evade", "Increases chances of evading enemy attacks.", "You choose the 'Evade' perk, allowing you to avoid enemy attacks more often!")
				.requireStatMin(Stat.Speed, 25);
			registerPerk(DoubleAttack, "Double Attack", "Allows you to perform two melee attacks per round.",
				"You choose the 'Double Attack' perk.  This allows you to make two attacks so long as your strength is at 60 or below.  By default your effective strength " 
				+ " will be reduced to 60 if it is too high when double attacking.  <b>You can enter the perks menu at any time to toggle options as to how you will use this perk.</b>")
				.addDynamicDesc(function(perk:Perk):String {
					if (Game.player.stats[Stat.Speed] < 50)
						return "<b>You're too slow to double attack!</b>"
					else if (Game.player.stats[Stat.Strength] > 60)
						return "<b>You are stronger than double attack allows.  To choose between reduced strength double-attacks and a single strong attack, access \"Dbl Options\" in the perks menu.</b>";
					else
						return "Allows you to perform two melee attacks per round.";
				})
				.requireStatMin(Stat.Speed, 50)
				.requirePerk(PerkStore.Evade)
				.requirePerk(PerkStore.Runner);
				
			registerPerk(Dragonfire, "Dragonfire", "Allows access to a dragon breath attack.");
			
			registerPerk(Cornucopia, "Cornucopia", "Vaginal and Anal capacities increased by 30.", null, true);
			
			registerPerk(SpiderOvipositor, "Spider Ovipositor", "Allows you to lay eggs through a special organ on your arachnid abdomen, though you need at least 10 eggs to lay.");
			
			registerPerk(CleansingPalm, "Cleansing Palm", 
				"A ranged fighting technique of Jojo’s order, allows you to blast your enemies with waves of pure spiritual energy, weakening them and hurting the corrupt.")
				.addDynamicDesc(function (perk:Perk) {
					if (Game.player.stats[Stat.Corruption] >= (10 + Game.player.corruptionTolerance())) 
						return "<b>DISABLED</b> - Corruption too high!";
					else 
						return perk.rawDesc();
				});
				
			registerPerk(ControlledBreath, "Controlled Breath", "Jojo’s training allows you to recover more quickly. Increases rate of fatigue regeneration by 10%")
				.addDynamicDesc(function (perk:Perk) {
					if (Game.player.stats[Stat.Corruption] >= (30 + Game.player.corruptionTolerance())) 
						return "<b>DISABLED</b> - Corruption too high!";
					else 
						return perk.rawDesc();
				});
			
			registerPerk(Acid, "Acid", "");
			registerPerk(PoisonImmune, "Poison Immune", "Add this to make a monster be immune to poisons.");
			registerPerk(BleedImmune, "BleedImmune", "Add this to make a monster be immune to bleed.");
		}
		
		public static function registerPerk(id:String, name:String, desc:String, longDesc:String = null, keepOnAscension:Bool = false):Perk {
			if (_perks.exists(id))
				throw "Cannot register perk with the id \"" + id + "\" as another perk has already been registered with that id.";
			
			var perk = new Perk(id, name, desc, longDesc, keepOnAscension);
			
			_perks.set(id, perk);
			
			return perk;
		}
		
		public static function getPerk(id:String):Perk {
			return _perks[id];
		}
		
		
		private static function initRequirements():Void {
			//------------
			// STRENGTH
			//------------
			//StrongBack.requireStr(25);
			//StrongBack2.requireStr(25)
					   //.requirePerk(StrongBack);
			////Tier 1 Strength Perks
			////Thunderous Strikes - +20% basic attack damage while str > 80.
			//ThunderousStrikes.requireStr(80)
							 //.requireLevel(6);
			////Weapon Mastery - Doubles weapon damage bonus of 'large' type weapons. (Minotaur Axe, M. Hammer, etc)
			//WeaponMastery.requireStr(60)
						 //.requireLevel(6);
			//BrutalBlows.requireStr(75)
					   //.requireLevel(6);
			//IronFists.requireStr(50)
					 //.requireLevel(6);
			//IronFists2.requireStr(65)
					  //.requireLevel(6)
					  //.requireNGPlus(1)
					  //.requirePerk(IronFists);
			//IronFists3.requireStr(80)
					  //.requireLevel(6)
					  //.requireNGPlus(1)
					  //.requirePerk(IronFists2);
			//Parry.requireStr(50)
				 //.requireSpe(50)
				 //.requireLevel(6);
			////Tier 2 Strength Perks
			//Berzerker.requireStr(75)
					 //.requireLevel(12);
			//HoldWithBothHands.requireStr(80)
							 //.requireLevel(12);
			//ShieldSlam.requireStr(80)
					  //.requireTou(60)
					  //.requireLevel(12);
			////Tier 3 Strength Perks
			//ColdFury.requireStr(75)
					//.requireLevel(18)
					//.requirePerk(Berzerker)
					//.requirePerk(ImprovedSelfControl);
			////------------
			//// TOUGHNESS
			////------------
			////slot 2 - toughness perk 1
			//Tank.requireTou(25);
			////slot 2 - regeneration perk
			//Regeneration.requireTou(50)
						//.requirePerk(Tank);
			//ImprovedEndurance.requireStr(50)
							 //.requireTou(50);
			////Tier 1 Toughness Perks
			//Tank2.requireTou(60)
				 //.requireLevel(6)
				 //.requirePerk(Tank);
			//Regeneration2.requireTou(70)
						 //.requireLevel(6)
						 //.requirePerk(Regeneration);
			//ImmovableObject.requireTou(75)
						   //.requireLevel(6);
			//ShieldMastery.requireTou(50)
						 //.requireLevel(6);
			////Tier 2 Toughness Perks
			//Resolute.requireTou(75)
					//.requireLevel(12);
			//Juggernaut.requireTou(75)
					  //.requireLevel(12);
			//IronMan.requireTou(60)
				   //.requireLevel(12);
			////Tier 1 Speed Perks
			////Speedy Recovery - Regain Fatigue 50% faster speed.
			//SpeedyRecovery.requireSpe(60)
						  //.requireLevel(6)
						  //.requirePerk(Evade);
			////Agility - A small portion of your speed is applied to your defense rating when wearing light armors.
			//Agility.requireSpe(75)
				   //.requireLevel(6)
				   //.requirePerk(Runner);
			//Unhindered.requireSpe(75)
					  //.requireLevel(6)
					  //.requirePerk(Evade)
					  //.requirePerk(Agility);
			//LightningStrikes.requireSpe(60)
							//.requireLevel(6);
			///*
			 //Brawler.requireStr(60).requireSpe(60);
			 //*/ //Would it be fitting to have Urta teach you?
			////Tier 2 Speed Perks
			//LungingAttacks.requireSpe(75)
						  //.requireLevel(12);
			//Blademaster.requireStr(60)
					   //.requireSpe(80)
					   //.requireLevel(12);
			////------------
			//// INTELLIGENCE
			////------------
			////Slot 4 - precision - -10 enemy toughness for damage calc
			//Precision.requireInt(25);
			////Spellpower - boosts spell power
			//Spellpower.requireInt(50);
			//Mage.requireInt(50)
				//.requirePerk(Spellpower);
			////Tier 1 Intelligence Perks
			//Tactician.requireInt(50)
					 //.requireLevel(6);
			//Channeling.requireInt(60)
					  //.requireLevel(6)
					  //.requirePerk(Spellpower)
					  //.requirePerk(Mage)
					  //.requireCustomFunction(function (player:Player):Boolean {
						  //return player.spellCount() > 0;
					  //}, "Any Spell");
			//Medicine.requireInt(60)
					//.requireLevel(6);
			//StaffChanneling.requireInt(60)
						   //.requireLevel(6)
						   //.requirePerk(Channeling);
			////Tier 2 Intelligence perks
			//Archmage.requireInt(75)
					//.requireLevel(12)
					//.requirePerk(Mage);
			//FocusedMind.requireInt(75)
					   //.requireLevel(12)
					   //.requirePerk(Mage);
//
			//RagingInferno.requireInt(75)
						 //.requireLevel(12)
						 //.requirePerk(Archmage)
						 //.requirePerk(Channeling)
						 //.requireCustomFunction(function (player:Player):Boolean {
							 //return player.hasStatusEffect(StatusEffects.KnowsWhitefire)
									 //|| player.hasStatusEffect(StatusEffects.KnowsBlackfire)
									 //|| player.hasPerk(FireLord)
									 //|| player.hasPerk(Hellfire)
									 //|| player.hasPerk(EnlightenedNinetails)
									 //|| player.hasPerk(CorruptedNinetails);
						 //}, "Any Fire Spell");
			//// Spell-boosting perks
			//// Battlemage: auto-use Might
			//Battlemage.requireInt(80)
					  //.requireLevel(12)
					  //.requirePerk(Channeling)
					  //.requireStatusEffect(StatusEffects.KnowsMight, "Might Spell");
			//// Spellsword: auto-use Charge Weapon
			//Spellsword.requireInt(80)
					  //.requireLevel(12)
					  //.requirePerk(Channeling)
					  //.requireStatusEffect(StatusEffects.KnowsCharge, "Charge Weapon Spell");
//
			////------------
			//// LIBIDO
			////------------
			////slot 5 - libido perks
//
			////Slot 5 - Fertile+ increases cum production and fertility (+15%)
			//FertilityPlus.requireLib(25);
			//FertilityPlus.defaultValue1 = 15;
			//FertilityPlus.defaultValue2 = 1.75;
			//ImprovedSelfControl.requireInt(50).requireLib(25);
			////Slot 5 - minimum libido
			//ColdBlooded.requireMinLust(20);
			//ColdBlooded.defaultValue1 = 20;
			//HotBlooded.requireLib(50);
			//HotBlooded.defaultValue1 = 20;
			////Tier 1 Libido Perks
			////Slot 5 - minimum libido
			////Slot 5 - Fertility- decreases cum production and fertility.
			//FertilityMinus.requireLevel(6)
						  //.requireLibLessThan(25);
			//FertilityMinus.defaultValue1 = 15;
			//FertilityMinus.defaultValue2 = 0.7;
			//WellAdjusted.requireLevel(6).requireLib(60);
			////Slot 5 - minimum libido
			//Masochist.requireLevel(6).requireLib(60).requireCor(50);
			////------------
			//// SENSITIVITY
			////------------
			////Nope.avi
			////------------
			//// CORRUPTION
			////------------
			////Slot 7 - Corrupted Libido - lust raises 10% slower.
			//CorruptedLibido.requireCor(25);
			//CorruptedLibido.defaultValue1 = 20;
			////Slot 7 - Seduction (Must have seduced Jojo
			//Seduction.requireCor(50);
			////Slot 7 - Nymphomania
			//Nymphomania.requirePerk(CorruptedLibido)
					   //.requireCor(75);
			////Slot 7 - UNFINISHED :3
			//Acclimation.requirePerk(CorruptedLibido)
					   //.requireMinLust(20)
					   //.requireCor(50);
			////Tier 1 Corruption Perks - acclimation over-rides
			//Sadist.requireLevel(6)
				  //.requirePerk(CorruptedLibido)
				  //.requireCor(60);
			//ArousingAura.requireLevel(6)
						//.requirePerk(CorruptedLibido)
						//.requireCor(70);
			////Tier 1 Misc Perks
			//Resistance.requireLevel(6);
			//Survivalist.requireLevel(6)
					   //.requireHungerEnabled();
			////Tier 2 Misc Perks
			//Survivalist2.requireLevel(12)
						//.requirePerk(Survivalist)
						//.requireHungerEnabled();
		}
}