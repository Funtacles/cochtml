package data.game.statuseffect;

/**
 * @author Funtacles
 */
interface IStatusEffect 
{
	var id:String;
	
	var stackable:Bool;
	
	var value:Float;
	
	var duration:Int;
}