package data.game.statuseffect;

/**
 * ...
 * @author Funtacles
 */
class StatusEffects 
{
	private static var _statusEffects:Map<String, IStatusEffect>;
	
	public static function init():Void 
	{
		_statusEffects = new Map<String, IStatusEffect>();
		
		
	}
	
	public static function register(f:IStatusEffect):Void {
		if (_statusEffects == null)
			throw "Status effect system has not been initialized. Call init before trying to register status effects.";
		
		if (_statusEffects.exists(f.id))
			throw "Status effect already exists in StatusEffects: " + f.id;
		
		_statusEffects.set(f.id, f);
	}
	
	public static function getWorldEffect(id:String): WorldStatusEffect {
		if (!_statusEffects.exists(id))
			throw "Status effect has not been registered in status effects: " + id;
		
		if (!Std.is(_statusEffects.get(id), WorldStatusEffect))
			throw '"$id" is not a world status effect';
		
		return cast(_statusEffects.get(id), WorldStatusEffect);
	}
	
	public static function getCombatEffect(id:String):CombatStatusEffect {
		if (!_statusEffects.exists(id))
			throw "Status effect has not been registered in status effects: " + id;
		
		if (!Std.is(_statusEffects.get(id), CombatStatusEffect))
			throw '"$id" is not a combat status effect';
		
		return cast(_statusEffects.get(id), CombatStatusEffect);
	}

	
}