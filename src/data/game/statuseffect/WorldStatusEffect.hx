package data.game.statuseffect;

/**
 * ...
 * @author Funtacles
 */
class WorldStatusEffect implements IStatusEffect 
{
	public var id:String;
	
	public var stackable:Bool;
	
	public var value:Float;
	
	public var duration:Int;
	
	// Non-combat player perks
	public static inline var AllNaturalOnaholeUsed:String = "all-natural onahole used";
	public static inline var AndysSmoke:String = "Andy's Smoke"; //v1: Hours; v2: Speed; v3: Intelligence
	public static inline var AteEgg:String = "ateEgg";
	public static inline var AnemoneArousal:String = "Anemone Arousal";
	public static inline var BimboChampagne:String = "Bimbo Champagne";
	public static inline var Birthed:String = "Birthed";
	public static inline var BirthedImps:String = "Birthed Imps";
	public static inline var BlackCatBeer:String = "Black Cat Beer";
	public static inline var BlackNipples:String = "Black Nipples";
	public static inline var BlowjobOn:String = "BlowjobOn";
	public static inline var BoatDiscovery:String = "Boat Discovery";
	public static inline var BonusACapacity:String = "Bonus aCapacity";
	public static inline var BonusVCapacity:String = "Bonus vCapacity";
	public static inline var BottledMilk:String = "Bottled Milk";
	public static inline var BreastsMilked:String = "Breasts Milked";
	public static inline var BSwordBroken:String = "BSwordBroken";
	public static inline var BuiltMilker:String = "BUILT: Milker";
	public static inline var BurpChanged:String = "Burp Changed";
	public static inline var ButtStretched:String = "ButtStretched";
	public static inline var CampAnemoneTrigger:String = "Camp Anemone Trigger";
	public static inline var CampMarble:String = "Camp Marble";
	public static inline var CampRathazul:String = "Camp Rathazul";
	public static inline var ClaraCombatRounds:String = "Clara Combat Rounds";
	public static inline var ClaraFoughtInCamp:String = "Clara Fought In Camp";
	public static inline var CockPumped:String = "Cock Pumped";
	public static inline var Contraceptives:String = "Contraceptives";
	public static inline var CuntStretched:String = "CuntStretched"; //Used only for compatibility with old save files, replaced with attribute on vagina class
	public static inline var DefenseCanopy:String = "Defense: Canopy";
	public static inline var DeluxeOnaholeUsed:String = "deluxe onahole used";
	public static inline var DogWarning:String = "dog warning";
	public static inline var DragonBreathBoost:String = "Dragon Breath Boost";
	public static inline var DragonBreathCooldown:String = "Dragon Breath Cooldown";
	public static inline var Dysfunction:String = "dysfunction";
	public static inline var Edryn:String = "Edryn";
	public static inline var Eggchest:String = "eggchest";
	public static inline var Eggs:String = "eggs";
	public static inline var EmberFuckCooldown:String = "ember fuck cooldown";
	public static inline var EmberMilk:String = "Ember's Milk";
	public static inline var EmberNapping:String = "Ember Napping";
	public static inline var EverRapedJojo:String = "Ever Raped Jojo";
	public static inline var Exgartuan:String = "Exgartuan";
	public static inline var ExploredDeepwoods:String = "exploredDeepwoods";
	public static inline var FaerieFemFuck:String = "Faerie Fem Fuck";
	public static inline var FaerieFucked:String = "Faerie Fucked";
	public static inline var FappedGenderless:String = "fapped genderless";
	public static inline var Feeder:String = "Feeder";
	public static inline var Fertilized:String = "Fertilized";
	public static inline var FetishOn:String = "fetishON";
	public static inline var FuckedMarble:String = "FuckedMarble";
	public static inline var Fullness:String = "Fullness"; //Alternative to hunger
	public static inline var Goojob:String = "GOOJOB";
	public static inline var GooStuffed:String = "gooStuffed";
	public static inline var Groundpound:String = "Groundpound";
	public static inline var HairdresserMeeting:String = "hairdresser meeting";
	public static inline var Hangover:String = "Hangover";
	public static inline var Heat:String = "heat";
	public static inline var HorseWarning:String = "horse warning";
	public static inline var ImmolationSpell:String = "Immolation Spell";
	public static inline var ImpGangBang:String = "Imp GangBang";
	public static inline var Infested:String = "infested";
	public static inline var IzmaBlowing:String = "IzmaBlowing";
	public static inline var IzumisPipeSmoke:String = "Izumis Pipe Smoke";
	public static inline var JerkingIzma:String = "JerkingIzma";
	public static inline var Jizzpants:String = "Jizzpants";
	public static inline var JojoMeditationCount:String = "Jojo Meditation Count";
	public static inline var JojoNightWatch:String = "JojoNightWatch";
	public static inline var JojoTFOffer:String = "JojoTFOffer";
	public static inline var Kelt:String = "Kelt";
	public static inline var KeltBJ:String = "KeltBJ";
	public static inline var KeltBadEndWarning:String = "Kelt Bad End Warning";
	public static inline var KeltOff:String = "KeltOff";
	public static inline var KnowsArouse:String = "Knows Arouse";
	public static inline var KnowsBlind:String = "Knows Blind";
	public static inline var KnowsCharge:String = "Knows Charge";
	public static inline var KnowsHeal:String = "Knows Heal";
	public static inline var KnowsMight:String = "Knows Might";
	public static inline var KnowsWhitefire:String = "Knows Whitefire";
	public static inline var KnowsTKBlast:String = "Knows TK Blast";
	public static inline var KnowsBlackfire:String = "Knows Blackfire";
	public static inline var KnowsLeech:String = "Knows Leech";
	public static inline var LactationEndurance:String = "Lactation Endurance";
	public static inline var LactationReduction:String = "Lactation Reduction";
	public static inline var LactationReduc0:String = "Lactation Reduc0";
	public static inline var LactationReduc1:String = "Lactation Reduc1";
	public static inline var LactationReduc2:String = "Lactation Reduc2";
	public static inline var LactationReduc3:String = "Lactation Reduc3";
	public static inline var LootEgg:String = "lootEgg";
	public static inline var LostVillagerSpecial:String = "lostVillagerSpecial";
	public static inline var Luststick:String = "Luststick";
	public static inline var LustStickApplied:String = "Lust Stick Applied";
	public static inline var LustyTongue:String = "LustyTongue";
	public static inline var MalonVisitedPostAddiction:String = "Malon Visited Post Addiction";
	public static inline var Marble:String = "Marble";
	public static inline var MarbleHasItem:String = "MarbleHasItem";
	public static inline var MarbleItemCooldown:String = "MarbleItemCooldown";
	public static inline var MarbleRapeAttempted:String = "Marble Rape Attempted";
	public static inline var MarblesMilk:String = "Marbles Milk";
	public static inline var MarbleSpecials:String = "MarbleSpecials";
	public static inline var MarbleWithdrawl:String = "MarbleWithdrawl";
	public static inline var Meditated:String = "Meditated"; // DEPRECATED
	public static inline var MeanToNaga:String = "MeanToNaga";
	public static inline var MeetWanderer:String = "meet wanderer";
	public static inline var MetRathazul:String = "metRathazul";
	public static inline var MetWorms:String = "metWorms";
	public static inline var MetWhitney:String = "Met Whitney";
	public static inline var Milked:String = "Milked";
	public static inline var MinoPlusCowgirl:String = "Mino + Cowgirl";
	public static inline var Naga:String = "Naga";
	public static inline var NakedOn:String = "NakedOn";
	public static inline var NoJojo:String = "noJojo";
	public static inline var NoMoreMarble:String = "No More Marble";
	public static inline var Oswald:String = "Oswald";
	public static inline var ParasiteSlug:String = "infected by slug parasite";
	public static inline var ParasiteSlugReproduction:String = "slug parasite reproducing";
	public static inline var ParasiteSlugMet:String = "met a parasite in the bog";
	public static inline var ParasiteSlugMatureDay:String = "sets a delay before parasite can reproduce after maturing";
	public static inline var ParasiteEel:String = "infected by eel parasite";
	public static inline var ParasiteEelNeedCum:String = "ParasiteEel needs specific cum";
	public static inline var ParasiteEelReproduction:String = "ParasiteEel ready to reproduce";
	public static inline var PlainOnaholeUsed:String = "plain onahole used";
	public static inline var PhoukaWhiskeyAffect:String = "PhoukaWhiskeyAffect";
	public static inline var PostAkbalSubmission:String = "Post Akbal Submission";
	public static inline var PostAnemoneBeatdown:String = "Post Anemone Beatdown";
	public static inline var PureCampJojo:String = "PureCampJojo";
	public static inline var RathazulArmor:String = "RathazulArmor";
	public static inline var RepeatSuccubi:String = "repeatSuccubi";
	public static inline var Rut:String = "rut";
	public static inline var SharkGirl:String = "Shark-Girl";
	public static inline var ShieldingSpell:String = "Shielding Spell";
	public static inline var SlimeCraving:String = "Slime Craving";
	public static inline var SlimeCravingFeed:String = "Slime Craving Feed";
	public static inline var SlimeCravingOutput:String = "Slime Craving Output";
	public static inline var SuccubiFirst:String = "SuccubiFirst";
	public static inline var SuccubiNight:String = "succubiNight";
	public static inline var TakenGroPlus:String = "TakenGro+";
	public static inline var TakenLactaid:String = "TakenLactaid";
	public static inline var Tamani:String = "Tamani";									//Used only for compatibility with old save files, otherwise no longer in use
	public static inline var TamaniFemaleEncounter:String = "Tamani Female Encounter";	//Used only for compatibility with old save files, otherwise no longer in use
	public static inline var TelAdre:String = "Tel'Adre";
	public static inline var TentacleBadEndCounter:String = "TentacleBadEndCounter";
	public static inline var TentacleJojo:String = "Tentacle Jojo";
	public static inline var TensionReleased:String = "TensionReleased";
	public static inline var TransformationDoses:String = "Transformation stuff"; //tracks TF item usage to decrease grind a bit, maybe not really
	public static inline var TF2:String = "TF2";
	public static inline var TookBlessedSword:String = "Took Blessed Sword";
	
	public static inline var UmasMassage:String = "Uma's Massage"; //v1 = bonus index; v2 = bonus value; v3 = remaining time
	public static inline var Uniball:String = "Uniball";
	public static inline var UsedNaturalSelfStim:String = "used natural self-stim";
	public static inline var used_self_dash_stim:String = "used self-stim";
	public static inline var Victoria:String = "Victoria";
	public static inline var VoluntaryDemonpack:String = "Voluntary Demonpack";
	public static inline var WormOffer:String = "WormOffer";
	public static inline var WormPlugged:String = "worm plugged";
	public static inline var WormsHalf:String = "wormsHalf";
	public static inline var WormsOff:String = "wormsOff";
	public static inline var WormsOn:String = "wormsOn";
	public static inline var WandererDemon:String = "wanderer demon";
	public static inline var WandererHuman:String = "wanderer human";
	public static inline var Yara:String = "Yara";

	// monster
	public static inline var Attacks:String = "attacks";
	public static inline var BimboBrawl:String = "bimboBrawl";
	public static inline var BowCooldown:String = "Bow Cooldown";
	public static inline var BowDisabled:String = "Bow Disabled";
	public static inline var Charged:String = "Charged";
	public static inline var Climbed:String = "Climbed";
	public static inline var Concentration:String = "Concentration";
	public static inline var Constricted:String = "Constricted";
	public static inline var CoonWhip:String = "Coon Whip";
	public static inline var Counter:String = "Counter";
	public static inline var DomFight:String = "domfight";
	public static inline var DrankMinoCum:String = "drank mino cum";
	public static inline var DrankMinoCum2:String = "drank mino cum2";
	public static inline var Drunk:String = "Drunk";
	public static inline var Earthshield:String = "Earthshield";
	public static inline var Fear:String = "Fear";
	//public static inline var FearCounter:String = "FearCounter";
	public static inline var GenericRunDisabled:String = "Generic Run Disabled";
	public static inline var Gigafire:String = "Gigafire";
	public static inline var GottaOpenGift:String = "Gotta Open Gift";
	public static inline var HolliBurning:String = "Holli Burning";
	public static inline var Illusion:String = "Illusion";
	public static inline var ImpSkip:String = "ImpSkip";
	public static inline var ImpUber:String = "ImpUber";
	public static inline var JojoIsAssisting:String = "Jojo Is Assisting";
	public static inline var JojoPyre:String = "Jojo Pyre";
	public static inline var Keen:String = "keen";
	public static inline var Level:String = "level";
	public static inline var KitsuneFight:String = "Kitsune Fight";
	public static inline var LethicesRapeTentacles:String = "Lethices Rape Tentacles";
	public static inline var LustAura:String = "Lust Aura";
	public static inline var LustStick:String = "LustStick";
	public static inline var Milk:String = "milk";
	public static inline var MilkyUrta:String = "Milky Urta";
	public static inline var MinoMilk:String = "Mino Milk";
	public static inline var MinotaurEntangled:String = "Minotaur Entangled";

	public static inline var MissFirstRound:String = "miss first round";
	public static inline var NoLoot:String = "No Loot";
	public static inline var OnFire:String = "On Fire";
	public static inline var PCTailTangle:String = "PCTailTangle";
	public static inline var PeachLootLoss:String = "Peach Loot Loss";

	// @aimozg i don't know and do not fucking care if these two should be merged
	public static inline var PhyllaFight:String = "PhyllaFight";
	public static inline var phyllafight:String = "phyllafight";
	public static inline var Platoon:String = "platoon";
	public static inline var QueenBind:String = "QueenBind";

	//Plantgirl
	public static inline var happy:String = "happy";
	public static inline var horny:String = "horny";
	public static inline var grouchy:String = "grouchy";
	
}