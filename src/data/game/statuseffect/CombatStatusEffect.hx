package data.game.statuseffect;

/**
 * ...
 * @author Funtacles
 */
class CombatStatusEffect implements IStatusEffect 
{
	public static inline var AcidSlap:String = "Acid Slap";
	public static inline var AkbalSpeed:String = "Akbal Speed";
	public static inline var AmilyVenom:String = "Amily Venom";
	public static inline var AnemoneVenom:String = "Anemone Venom";
	public static inline var ArmorRent:String = "Armor Rent";
	public static inline var AttackDisabled:String = "Attack Disabled";
	public static inline var BasiliskCompulsion:String = "Basilisk Compulsion";
	public static inline var BasiliskSlow:String = "BasiliskSlow";
	public static inline var Berzerking:String = "Berzerking";
	public static inline var Blind:String = "Blind";
	public static inline var Bound:String = "Bound";
	public static inline var CalledShot:String = "Called Shot";
	public static inline var ChargeWeapon:String = "Charge Weapon";
	public static inline var Chokeslam:String = "Chokeslam";
	public static inline var CorrWitchBind:String = "Corrupted Witch headlock";
	public static inline var Confusion:String = "Confusion";
	public static inline var CounterAB:String = "Revengeance";
	public static inline var DemonSeed:String = "DemonSeed";
	public static inline var Disarmed:String = "Disarmed";
	public static inline var DriderKiss:String = "Drider Kiss";
	public static inline var GuardAB:String = "Targetguarded";
	public static inline var Marked:String = "Player is marked, less armor";
	public static inline var Resolve:String = "Test your resolve. A buff, or a debuff.";
	public static inline var Revelation:String = "Player gained a crumb of cosmic truth";
	
	public static inline var GiantBoulder:String = "Giant Boulder";
	public static inline var Round:String = "Round";
	public static inline var round:String = "round";
	public static inline var RunDisabled:String = "Run Disabled";
	public static inline var Shell:String = "Shell";
	public static inline var SirenSong:String = "Siren Song";
	public static inline var Spar:String = "spar";
	public static inline var Sparring:String = "sparring";
	public static inline var spiderfight:String = "spiderfight";
	public static inline var StunCooldown:String = "Stun Cooldown";
	public static inline var TentacleCoolDown:String = "TentacleCoolDown";
	public static inline var TimesBashed:String = "TimesBashed";
	public static inline var Uber:String= "Uber";
	public static inline var VolcanicUberHEAL:String = "Volcanic Golem's uberheal";
	public static inline var UrtaSecondWinded:String = "Urta Second Winded";
	public static inline var UsedTitsmother:String = "UsedTitsmother";
	public static inline var Vala:String= "vala";

	public static inline var FirstAttack:String = "FirstAttack";
	public static inline var GiantGrabbed:String = "Giant Grabbed";
	public static inline var GiantStrLoss:String = "GiantStrLoss";
	public static inline var GnollSpear:String = "Gnoll Spear";
	public static inline var GooArmorBind:String = "GooArmorBind";
	public static inline var GooArmorSilence:String = "GooArmorSilence";
	public static inline var GooBind:String = "GooBind";
	public static inline var HarpyBind:String = "HarpyBind";
	public static inline var HolliConstrict:String = "Holli Constrict";
	public static inline var InfestAttempted:String = "infestAttempted";
	public static inline var IsabellaStunned:String = "Isabella Stunned";
	public static inline var IzmaBleed:String = "Izma Bleed";
	public static inline var KissOfDeath:String = "Kiss of Death";
	public static inline var Leeching:String = "Player is stealing health with every regular attack";
	public static inline var LizanBlowpipe:String = "Lizan Blowpipe";
	public static inline var LustStones:String = "lust stones";
	public static inline var lustvenom:String = "lust venom";
	public static inline var Lustzerking:String = "Lustzerking";
	public static inline var Might:String = "Might";
	public static inline var NagaBind:String = "Naga Bind";
	public static inline var NagaVenom:String = "Naga Venom";
	public static inline var NagaSentVenom:String = "Naga Sentinel Venom";
	public static inline var Nothingness:String = "Player doesn't exist";
	public static inline var NoFlee:String = "NoFlee";
	public static inline var ParalyzeVenom:String = "paralyze venom";
	public static inline var ParasiteSlugMusk:String = "Smell that musk man";
	public static inline var ParasiteQueen:String = "Being boosted by your parasites!";
	public static inline var PhoenixOrder:String = "Phoenix Commander issued order";//it's weird, but it's simpler to use this to alter the Platoon AI.
	public static inline var PhysicalDisabled:String = "Physical Disabled";
	public static inline var Poison:String = "Poison";
	public static inline var Refashioned:String = "Player has been remade";
	public static inline var Sandstorm:String = "sandstorm";
	public static inline var Sealed:String = "Sealed";
	public static inline var SheilaOil:String = "Sheila Oil";
	public static inline var Shielding:String = "Sheilding";
	public static inline var StoneLust:String = "Stone Lust";
	public static inline var Stunned:String = "Stunned";
	public static inline var TailWhip:String = "Tail Whip";
	public static inline var TemporaryHeat:String = "Temporary Heat";
	public static inline var TentacleBind:String = "TentacleBind";
	public static inline var ThroatPunch:String = "Throat Punch";
	public static inline var Titsmother:String = "Titsmother";
	public static inline var TwuWuv:String = "Twu Wuv";
	public static inline var UBERWEB:String = "UBERWEB";
	public static inline var VolcanicFistProblem:String = "Golem stuck its fist in the ground.";
	public static inline var VolcanicFrenzy:String = "Golem is pissed off.";
	public static inline var VolcanicArmorRed:String = "Armor temporarily reduced";
	public static inline var VolcanicWeapRed:String = "Weapon damage temporarily reduced";
	public static inline var Web:String = "Web";
	public static inline var WebSilence:String = "Web-Silence";
	public static inline var Whispered:String = "Whispered";
	
	public static inline var RemovedArmor:String = "Removed Armor";
	public static inline var JCLustLevel:String = "JC Lust Level";
	public static inline var MirroredAttack:String = "Mirrored Attack";
	public static inline var KnockedBack:String = "Knocked Back";
	public static inline var Tentagrappled:String = "Tentagrappled";
	public static inline var TentagrappleCooldown:String = "Tentagrapple Cooldown";
	public static inline var ShowerDotEffect:String = "Shower Dot Effect";
	public static inline var GardenerSapSpeed:String = "Sap Speed";
	public static inline var VineHealUsed:String = "Vine Heal Used";
	
	public static inline var DriderIncubusVenom:String = "Drider Incubus Venom";
	public static inline var PurpleHaze:String = "PurpleHaze";
	public static inline var TaintedMind:String = "Tainted Mind";
	public static inline var MinotaurKingMusk:String = "Minotaur King Musk";
	public static inline var MinotaurKingsTouch:String = "Minotaur Kings Touch";
	public static inline var PigbysHands:String = "Pigbys Hands";
	public static inline var WhipSilence:String = "Whip-Silence";

	public function new() 
	{
		
	}
	
}