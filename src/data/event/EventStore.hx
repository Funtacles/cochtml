package data.event;
import storage.tml.TMLDocuments;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class EventStore 
{
	private static var _docMap:Map<String, String>;
	private static var _events:Map<String, Event>;
	
	public static function init():Void {
		_events = new Map<String, Event>();
		_docMap = new Map<String, String>();
		
		for (doc in TMLDocuments.getDocuments())
		{
			var evts = TMLDocuments.getAllTags(doc, "Event");
			
			if (evts == null)
				continue;
			
			for (evt in evts)
			{
				if (StringUtil.isNullOrEmpty(evt.asString()))
					throw 'Event with empty id in "$doc"';
				
				if (_events.exists(evt.asString()))
					throw 'Duplicate event definition "${evt.asString()}" in "$doc"';
				
				try {
					validateEvent(evt);
				} catch (e:String) {
					throw 'In document "$doc.tml" - $e';
				}
				
				var eventObj = new Event();
				eventObj.setFromTML(evt);
				_events.set(eventObj.id, eventObj);
				_docMap.set(eventObj.id, doc);
			}
		}
	}
	
	public static function getEvent(id:String):Event {
		if (!_events.exists(id))
			throw 'Event "$id" does not exist.';
		
		return _events.get(id);
	}
	
	public static function validateEvent(tml:TMLElement):Void {
		var tags = tml.getChildren();
		for (i in 0...tags.length)
		{
			var tag = tags[i];
			
			switch(tag.name)
			{
				case Event.TextLabel:
					if (tag.getChildren().length > 0)
						throw '"Text" tag on line ${tag.lineNumber} has one or more children but should have zero.';
				case Event.IfLabel:
					if (tag.getChildren().length == 0)
						throw '"If" tag on line ${tag.lineNumber} has no child tags. Needs at least one child tag to be valid.';
				case Event.ElseIfLabel:
					if (tags[i - 1].name != Event.IfLabel && tags[i - 1].name != Event.ElseIfLabel)
						throw '"ElseIf" tag on line ${tag.lineNumber} has no preceeding If or ElseIf tag.';
					if (tag.getChildren().length == 0)
						throw '"ElseIf" tag on line ${tag.lineNumber} has no child tags. Needs at least one child tag to be valid.';
				case Event.ElseLabel:
					if (tags[i - 1].name != Event.IfLabel && tags[i - 1].name != Event.ElseIfLabel)
						throw '"Else" tag on line ${tag.lineNumber} has no preceeding If or ElseIf tag.';
					if (tag.asString().length > 0)
						throw '"Else" tag on line ${tag.lineNumber} found with condition or content. All content should be placed in child tags.';
					if (tag.getChildren().length == 0)
						throw '"Else" tag on line ${tag.lineNumber} has no child tags. Needs at least one child tag to be valid.';
			}
		}
	}
	
	public static function getContainingDocument(eventId:String):String {
		return _docMap[eventId];
	}
	
}