package data.event;
import data.Character;
import data.Player;
import storage.tml.TMLDocuments;
import storage.tml.TMLElement;
import system.ParserContext;
import system.Parser;
import data.game.global.Achievement;
import data.body.enums.Gender;
import enums.Stat;
import data.game.flag.Flags;

/**
 * ...
 * @author Funtacles
 */
class Event 
{
	public var id:String;
	public var content:Array<TMLElement>;
	
	public function new() 
	{
		
	}
	
	public function setFromTML(tml:TMLElement):Void {
		id = tml.asString();
		
		content = tml.getChildren();
	}
	
	public function getText(context:ParserContext):String {
		return getTextFromElement(content, context);
	}
	
	private function getTextFromElement(elements:Array<TMLElement>, context:ParserContext):String {
		var text:String = "";
		var prevCondTrue:Bool = false;
		
		for (elem in elements)
		{
			try {
				switch(elem.name) {
					case TextLabel:
						text += Parser.parse(elem.asString(), context);
					case IfLabel:
						prevCondTrue = false;
						var cond = elem.asString();
						prevCondTrue = evalIfStatement(cond, context);
						
						if (prevCondTrue)
							text += getTextFromElement(elem.getChildren(), context);
					case ElseIfLabel:
						if (prevCondTrue)
							continue;
						
						var cond = elem.asString();
						prevCondTrue = evalIfStatement(cond, context);
						
						if (prevCondTrue)
							text += getTextFromElement(elem.getChildren(), context);
					case ElseLabel:
						if (prevCondTrue)
							continue;
						
						text += getTextFromElement(elem.getChildren(), context);
				}
			} catch (e:String) {
				throw '$e on line ${elem.lineNumber} in "${EventStore.getContainingDocument(id)}"';
			}
		}
		
		return text;
	}
	
	private function evalIfStatement(statement:String, context:ParserContext):Bool {
		var tree = buildExpressionTree(statement);
		
		return evalExpressionTree(tree, context);
	}
	
	private function evalExpressionTree(expr:ExprTreeNode, context:ParserContext):Bool {
		if (Util.isNullOrUndefined(expr.rhs) && Util.isNullOrUndefined(expr.lhs)) {	// is leaf node
			var parts = ~/\s+/g.split(expr.content);
			
			var func:ParserContext -> Bool = null;
			if (parts.length == 1){
				func = boolArgConverters[expr.content];
				
				if (func == null)
					throw "Unknown property \"" + expr.content +"\"";
				
				return func(context);
			} else if (parts.length == 3){
				var prop = parts[0];
				var cond = parts[1];
				var value = parts[2];
				
				if (!numberArgConverters.exists(prop))
					throw "Unknown property \"" + prop +"\"";
				
				var propValue = numberArgConverters[prop](context);
				var compValue = Std.parseFloat(value);
				
				return comparisonFunctions[cond](propValue, compValue);
			}
			
			throw "Bad If expression \"" + expr.content +"\"";
		}
		
		return false;
	}
	
	private function buildExpressionTree(statement:String):ExprTreeNode {
		//if (statement.indexOf("AND") == -1 && statement.indexOf("OR") == -1)
			return {content: StringTools.trim(statement)};
		
	}
	
	public function applyEffects(player:Player):Void {
		UI.hideStatArrows();
		
		applyEffectsInternal(content, {player:player, other: new Character()});
	}
	
	private function applyEffectsInternal(elements:Array<TMLElement>, context:ParserContext) {
		var prevCondTrue:Bool = false;
		
		for (elem in elements)
		{
			switch(elem.name) {
				case SetTimeLabel:
					var parts = elem.asString().split(":");
					Game.gameTime.setTime(Std.parseInt(parts[0]), Std.parseInt(parts[1]));
				case AddTimeLabel:
					var parts = elem.asString().split(":");
					Game.gameTime.addTime(Std.parseInt(parts[0]), Std.parseInt(parts[1]));
				case AwardAchievementLabel:
					Achievement.awardAchievement(elem.asString());
				case SetFlagLabel:
					Flags.getTrigger(elem.asString()).set = true;
				case ClearFlagLabel:
					Flags.getTrigger(elem.asString()).set = false;
				case StatChangeLabel:
					for (change in elem.getChildren())
					{
						var stat = Stat.createByName(change.name);
						Game.changeStat(stat, change.asInt());
					}
				case IfLabel:
					prevCondTrue = false;
					var cond = elem.asString();
					prevCondTrue = evalIfStatement(cond, context);
					
					if (prevCondTrue)
						applyEffectsInternal(elem.getChildren(), context);
				case ElseIfLabel:
					if (prevCondTrue)
						continue;
					
					var cond = elem.asString();
					prevCondTrue = evalIfStatement(cond, context);
					
					if (prevCondTrue)
						applyEffectsInternal(elem.getChildren(), context);
				case ElseLabel:
					if (prevCondTrue)
						continue;
					
					applyEffectsInternal(elem.getChildren(), context);
			}
		}
	}
	
	public static inline var TextLabel:String = "Text";
	
	public static inline var IfLabel:String = "If";
	public static inline var ElseIfLabel:String = "ElseIf";
	public static inline var ElseLabel:String = "Else";
	
	public static inline var StatChangeLabel:String = "StatChange";
	public static inline var SetTimeLabel:String = "SetTime";
	public static inline var AddTimeLabel:String = "AddTime";
	public static inline var AwardAchievementLabel = "AwardAchievement";
	public static inline var GiveItemFlag:String = "GiveItem";
	public static inline var SetFlagLabel:String = "SetFlag";
	public static inline var ClearFlagLabel:String = "ClearFlag";
	
	public static inline var NextLabel:String = "Next";
	public static inline var ExitLabel:String = "Exit";
	
	public static var boolArgConverters:Map<String, ParserContext -> Bool> = 
	[
		"isFemale"					=> function(context:ParserContext):Bool { return context.player.getGender() == Gender.Female; },
		"isHerm"					=> function(context:ParserContext):Bool { return context.player.getGender() == Gender.Herm; },
		"isMale"				    => function(context:ParserContext):Bool { return context.player.getGender() == Gender.Male;  },
	];
	
	public static var numberArgConverters:Map<String, ParserContext -> Float> = 
	[
		"anallooseness"				=> function(context:ParserContext):Float { return context.player.ass.analLooseness.getIndex(); },
		"analwetness"				=> function(context:ParserContext):Float { return context.player.ass.analWetness.getIndex(); },
		"corruption"				=> function(context:ParserContext):Float { return context.player.stats[Stat.Corruption]; },
		"cumvolume"					=> function(context:ParserContext):Float { return context.player.stats[Stat.Corruption]; },
		"libido"					=> function(context:ParserContext):Float { return context.player.stats[Stat.Libido]; },
		"sensitivity"				=> function(context:ParserContext):Float { return context.player.stats[Stat.Sensitivity]; },
		"strength"					=> function(context:ParserContext):Float { return context.player.stats[Stat.Strength]; },
	];
	
	public static var comparisonFunctions:Map<String, Float -> Float -> Bool> = 
	[
		"equal" 					=> function (prop:Float, val:Float):Bool { return prop == val; },
		"below" 					=> function (prop:Float, val:Float):Bool { return prop < val; },
		"above" 					=> function (prop:Float, val:Float):Bool { return prop > val; },
 	];
}

typedef ExprTreeNode = {
	var content:String;
	@:optional var lhs:ExprTreeNode;
	@:optional var rhs:ExprTreeNode;
	@:optional var parent:ExprTreeNode;
}