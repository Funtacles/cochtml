package data;
import data.body.Womb;
import data.body.enums.HipSize;
import data.body.enums.FaceType;
import data.body.enums.PregnancyType;

/**
 * ...
 * @author Funtacles
 */
class Character extends Creature
{
	//Used for hip ratings
	public var thickness:Int;
	
	public function pregnancyType():PregnancyType { return womb.pregType; }

	public function pregnancyIncubation():Int { return womb.gestation; }

	public function buttPregnancyType():PregnancyType { return womb.buttPregType; }

	public function buttPregnancyIncubation():Int { return womb.buttGestation; }
	
	public var womb:Womb;
	
	public var keyItems:Array<String>;

	
	public function Character()
	{
		womb = new Womb();
		keyItems = new Array<String>();
	}

	public function hasBeard():Bool
	{
		return beardLength > 0;
	}
	
	public function beard():String
	{
		if (hasBeard())
			return "beard";
		else
		{
			//CoC_Settings.error("");
			return "ERROR: NO BEARD! <b>YOU ARE NOT A VIKING AND SHOULD TELL KITTEH IMMEDIATELY.</b>";
		}
	}

	public function faceDesc():String
	{
		var text:String = "";
		//0-10
		if (femininity < 10)
		{
			text = "a square chin";
			if (!hasBeard())
				text += " and chiseled jawline";
			else
				text += ", chiseled jawline, and " + beard();
		}
		//10+ -20
		else if (femininity < 20)
		{
			text = "a rugged looking " + faceTypeDesc() + " ";
			if (hasBeard())
				text += "and " + beard();
			text += "that's surely handsome";
		}
		//21-28
		else if (femininity < 28)
			text = "a well-defined jawline and a fairly masculine profile";
		//28+-35 
		else if (femininity < 35)
			text = "a somewhat masculine, angular jawline";
		//35-45
		else if (femininity < 45)
			text = "the barest hint of masculinity on its features";
		//45-55
		else if (femininity <= 55)
			text = "an androgynous set of features that would look normal on a male or female";
		//55+-65
		else if (femininity <= 65)
			text = "a tiny touch of femininity to it, with gentle curves";
		//65+-72
		else if (femininity <= 72)
			text = "a nice set of cheekbones and lips that have the barest hint of pout";
		//72+-80
		else if (femininity <= 80)
			text = "a beautiful, feminine shapeliness that's sure to draw the attention of males";
		//81-90
		else if (femininity <= 90)
			text = "a gorgeous profile with full lips, a button nose, and noticeable eyelashes";
		//91-100
		else
			text = "a jaw-droppingly feminine shape with full, pouting lips, an adorable nose, and long, beautiful eyelashes";
		return text;
	}
	
	public function faceTypeDesc():String
	{
		var text:String = "";
		var muzzleWord = function():String {
			switch(Std.random(3)) {
				case 0:
					return "muzzle";
				case 1:
					return "snout";
				case 2:
					return "face";
				default:
					return "face";
			}
		};
		
		switch(faceType){
			case FaceType.Human:
				text = "face";
			case FaceType.Horse:
				text = "long " + muzzleWord();
			case FaceType.Cat:
				text = "feline " + muzzleWord();
			case FaceType.Rhino:
				text = "rhino " + muzzleWord();
			case FaceType.Lizard, FaceType.Dragon:
				text = "long " + muzzleWord();
			case FaceType.Dog, FaceType.Wolf, FaceType.Fox:
				text = "canine " + muzzleWord();
			case FaceType.Cow:
				text = "bovine " + muzzleWord();
			case FaceType.SharkTeeth:
				text = "angular face";
			case FaceType.Boar:
				text = "boar-like " + StringUtil.randomWord(["snout", "face"]);
			case FaceType.Pig:
				text = "pig-like " + StringUtil.randomWord(["snout", "face"]); 
			default:
				text = "face";
		}
		
		return text;
	}
	//
	//public function hasLongTail():Bool
	//{
		////7 - shark tail!
		////8 - catTAIIIIIL
		////9 - lizard tail
		////10 - bunbuntail
		////11 - harpybutt
		////12 - rootail
		////13 - foxtail
		////14 - dagron tail
		//if (isNaga())
			//return true;
		//if (tailType == 2 || tailType == 3 || tailType == 4 || tailType == 7 || tailType == 8 || tailType == 9 || tailType == 12 || tailType == 13 || tailType == 14 || tailType == 15 || tailType == 16 || tailType == 17 || tailType == 18 || tailType == 20)
			//return true;
		//return false;
	//}
//
	//public function isPregnant():Bool { return _pregnancyType != 0; }
//
	//public function isButtPregnant():Bool { return _buttPregnancyType != 0; }
//
	//
	///**
	 //* Impregnate the character with the given pregnancy type if the total fertility 
	 //* is greater or equal to the roll.
	 //* @param	type the type of pregnancy (@see PregnancyStore.PREGNANCY_xxx)
	 //* @param	incubationDuration the incubation duration
	 //* @param	maxRoll the possible maximum roll for an impregnation check
	 //* @param	forcePregnancy specify a large bonus or malus to fertility (0 = no bonus, positive number = guaranteed pregnancy, negative number = no pregnancy)
	 //*/
	//public function knockUp(type:Int = 0, incubationDuration:Int = 0, maxRoll:Int = 100, forcePregnancy:Int = 0):Void
	//{
		////TODO push this down into player?
		//if(hasStatusEffect(StatusEffects.ParasiteEel)){
		//if (statusEffectv2(StatusEffects.ParasiteEelNeedCum) == type || (statusEffectv2(StatusEffects.ParasiteEelNeedCum) == 4 && (type == 11 || type == 29))){
			//if(type == 2) addStatusValue(StatusEffects.ParasiteEelNeedCum, 3, -1);//Minotaurs cum a lot.
			//addStatusValue(StatusEffects.ParasiteEelNeedCum, 3, -1); 
			//if (statusEffectv3(StatusEffects.ParasiteEelNeedCum) <= 0){
				//removeStatusEffect(StatusEffects.ParasiteEelNeedCum);
				//addStatusValue(StatusEffects.ParasiteEel, 2, 1);
			//}
			//
		//}
		//return;
		//}
		////Contraceptives cancel!
		//if (hasStatusEffect(StatusEffects.Contraceptives) && forcePregnancy < 1)
			//return;
			//
		//var bonus:Int = 0;
		//
		//// apply fertility bonus or malus
		//if (forcePregnancy >= 1)
			//bonus = 9000;
		//if (forcePregnancy <= -1)
			//bonus = -9000;
			//
		////If not pregnant and fertility wins out:
		//if (pregnancyIncubation == 0 && totalFertility() + bonus > Math.floor(Math.random() * maxRoll) && hasVagina())
		//{
			//knockUpForce(type, incubationDuration);
			//trace("PC Knocked up with pregnancy type: " + type + " for " + incubationDuration + " incubation.");
		//}
		//
		////Chance for eggs fertilization - ovi elixir and imps excluded!
		//if (type != PregnancyStore.PREGNANCY_IMP && type != PregnancyStore.PREGNANCY_OVIELIXIR_EGGS && type != PregnancyStore.PREGNANCY_ANEMONE)
		//{
			//if (findPerk(PerkLib.SpiderOvipositor) >= 0 || findPerk(PerkLib.BeeOvipositor) >= 0)
			//{
				//if (totalFertility() + bonus > Math.floor(Math.random() * maxRoll))
				//{
					//fertilizeEggs();
				//}
			//}
		//}
//
	//}
//
//
	//
	 ///**
	  //* Forcefully override the characters pregnancy. If no pregnancy type is provided,
	  //* any current pregancy is cleared.
	  //* 
	  //* Note: A more complex pregnancy function used by the character is Character.knockUp
	  //* The character doesn't need to be told of the last event triggered, so the code here is quite a bit simpler than that in PregnancyStore.
	  //* @param	type the type of pregnancy (@see PregnancyStore.PREGNANCY_xxx)
	  //* @param	incubationDuration the incubation duration
	  //*/
	//public function knockUpForce(type:Int = 0, incubationDuration:Int = 0):Void
	//{
		////TODO push this down into player?
		//_pregnancyType = type;
		//_pregnancyIncubation = (type == 0 ? 0 : incubationDuration); //Won't allow incubation time without pregnancy type
	//}
//
	////fertility must be >= random(0-beat)
	//public function buttKnockUp(type:Int = 0, incubation:Int = 0, beat:Int = 100, arg:Int = 0):Void
	//{
		////Contraceptives cancel!
		//if (hasStatusEffect(StatusEffects.Contraceptives) && arg < 1)
			//return;
		//var bonus:Int = 0;
		////If arg = 1 (always pregnant), bonus = 9000
		//if (arg >= 1)
			//bonus = 9000;
		//if (arg <= -1)
			//bonus = -9000;
		////If unpregnant and fertility wins out:
		//if (buttPregnancyIncubation == 0 && totalFertility() + bonus > Math.floor(Math.random() * beat))
		//{
			//buttKnockUpForce(type, incubation);
			//trace("PC Butt Knocked up with pregnancy type: " + type + " for " + incubation + " incubation.");
		//}
	//}
//
	////The more complex buttKnockUp function used by the player is defined in Character.as
	//public function buttKnockUpForce(type:Int = 0, incubation:Int = 0):Void
	//{
		//_buttPregnancyType = type;
		//_buttPregnancyIncubation = (type == 0 ? 0 : incubation); //Won't allow incubation time without pregnancy type
	//}
//
	//public function pregnancyAdvance():Bool {
		//if (_pregnancyIncubation > 0) _pregnancyIncubation--;
		//if (_pregnancyIncubation < 0) _pregnancyIncubation = 0;
		//if (_buttPregnancyIncubation > 0) _buttPregnancyIncubation--;
		//if (_buttPregnancyIncubation < 0) _buttPregnancyIncubation = 0;
		//return pregnancyUpdate();
	//}
//
	//public function pregnancyUpdate():Bool { return false; }
//
	////Create a keyItem
	//public function createKeyItem(keyName:String, value1:Number, value2:Number, value3:Number, value4:Number):Void
	//{
		//var newKeyItem:KeyItemClass = new KeyItemClass();
		////used to denote that the array has already had its new spot pushed on.
		//var arrayed:Bool = false;
		////used to store where the array goes
		//var keySlot:Number = 0;
		//var counter:Number = 0;
		////Start the array if its the first bit
		//if (keyItems.length == 0)
		//{
			////trace("New Key Item Started Array! " + keyName);
			//keyItems.push(newKeyItem);
			//arrayed = true;
			//keySlot = 0;
		//}
		////If it belongs at the end, push it on
		//if (keyItems[keyItems.length - 1].keyName < keyName && !arrayed)
		//{
			////trace("New Key Item Belongs at the end!! " + keyName);
			//keyItems.push(newKeyItem);
			//arrayed = true;
			//keySlot = keyItems.length - 1;
		//}
		////If it belongs in the beginning, splice it in
		//if (keyItems[0].keyName > keyName && !arrayed)
		//{
			////trace("New Key Item Belongs at the beginning! " + keyName);
			//keyItems.splice(0, 0, newKeyItem);
			//arrayed = true;
			//keySlot = 0;
		//}
		////Find the spot it needs to go in and splice it in.
		//if (!arrayed)
		//{
			////trace("New Key Item using alphabetizer! " + keyName);
			//counter = keyItems.length;
			//while (counter > 0 && !arrayed)
			//{
				//counter--;
				////If the current slot is later than new key
				//if (keyItems[counter].keyName > keyName)
				//{
					////If the earlier slot is earlier than new key && a real spot
					//if (counter - 1 >= 0)
					//{
						////If the earlier slot is earlier slot in!
						//if (keyItems[counter - 1].keyName <= keyName)
						//{
							//arrayed = true;
							//keyItems.splice(counter, 0, newKeyItem);
							//keySlot = counter;
						//}
					//}
					////If the item after 0 slot is later put here!
					//else
					//{
						////If the next slot is later we are go
						//if (keyItems[counter].keyName <= keyName)
						//{
							//arrayed = true;
							//keyItems.splice(counter, 0, newKeyItem);
							//keySlot = counter;
						//}
					//}
				//}
			//}
		//}
		////Fallback
		//if (!arrayed)
		//{
			////trace("New Key Item Belongs at the end!! " + keyName);
			//keyItems.push(newKeyItem);
			//keySlot = keyItems.length - 1;
		//}
		//
		//keyItems[keySlot].keyName = keyName;
		//keyItems[keySlot].value1 = value1;
		//keyItems[keySlot].value2 = value2;
		//keyItems[keySlot].value3 = value3;
		//keyItems[keySlot].value4 = value4;
		////trace("NEW KEYITEM FOR PLAYER in slot " + keySlot + ": " + keyItems[keySlot].keyName);
	//}
	//
	////Remove a key item
	//public function removeKeyItem(itemName:String):Void
	//{
		//var counter:Number = keyItems.length;
		////Various Errors preventing action
		//if (keyItems.length <= 0)
		//{
			////trace("ERROR: KeyItem could not be removed because player has no key items.");
			//return;
		//}
		//while (counter > 0)
		//{
			//counter--;
			//if (keyItems[counter].keyName == itemName)
			//{
				//keyItems.splice(counter, 1);
				//trace("Attempted to remove \"" + itemName + "\" keyItem.");
				//counter = 0;
			//}
		//}
	//}
	//
	//public function addKeyValue(statusName:String, statusValueNum:Number = 1, newNum:Number = 0):Void
	//{
		//var counter:Number = keyItems.length;
		////Various Errors preventing action
		//if (keyItems.length <= 0)
		//{
			//return;
				////trace("ERROR: Looking for keyitem '" + statusName + "' to change value " + statusValueNum + ", and player has no key items.");
		//}
		//while (counter > 0)
		//{
			//counter--;
			////Find it, change it, quit out
			//if (keyItems[counter].keyName == statusName)
			//{
				//if (statusValueNum < 1 || statusValueNum > 4)
				//{
					////trace("ERROR: AddKeyValue called with invalid key value number.");
					//return;
				//}
				//if (statusValueNum == 1)
					//keyItems[counter].value1 += newNum;
				//if (statusValueNum == 2)
					//keyItems[counter].value2 += newNum;
				//if (statusValueNum == 3)
					//keyItems[counter].value3 += newNum;
				//if (statusValueNum == 4)
					//keyItems[counter].value4 += newNum;
				//return;
			//}
		//}
		////trace("ERROR: Looking for keyitem '" + statusName + "' to change value " + statusValueNum + ", and player does not have the key item.");
	//}
	//
	//public function keyItemv1(statusName:String):Number
	//{
		//var counter:Number = keyItems.length;
		////Various Errors preventing action
		//if (keyItems.length <= 0)
		//{
			//return 0;
				////trace("ERROR: Looking for keyItem '" + statusName + "', and player has no key items.");
		//}
		//while (counter > 0)
		//{
			//counter--;
			//if (keyItems[counter].keyName == statusName)
				//return keyItems[counter].value1;
		//}
		////trace("ERROR: Looking for key item '" + statusName + "', but player does not have it.");
		//return 0;
	//}
	//
	//public function keyItemv2(statusName:String):Number
	//{
		//var counter:Number = keyItems.length;
		////Various Errors preventing action
		//if (keyItems.length <= 0)
		//{
			//return 0;
				////trace("ERROR: Looking for keyItem '" + statusName + "', and player has no key items.");
		//}
		//while (counter > 0)
		//{
			//counter--;
			//if (keyItems[counter].keyName == statusName)
				//return keyItems[counter].value2;
		//}
		////trace("ERROR: Looking for key item '" + statusName + "', but player does not have it.");
		//return 0;
	//}
	//
	//public function keyItemv3(statusName:String):Number
	//{
		//var counter:Number = keyItems.length;
		////Various Errors preventing action
		//if (keyItems.length <= 0)
		//{
			//return 0;
				////trace("ERROR: Looking for keyItem '" + statusName + "', and player has no key items.");
		//}
		//while (counter > 0)
		//{
			//counter--;
			//if (keyItems[counter].keyName == statusName)
				//return keyItems[counter].value3;
		//}
		////trace("ERROR: Looking for key item '" + statusName + "', but player does not have it.");
		//return 0;
	//}
	//
	//public function keyItemv4(statusName:String):Number
	//{
		//var counter:Number = keyItems.length;
		////Various Errors preventing action
		//if (keyItems.length <= 0)
		//{
			//return 0;
				////trace("ERROR: Looking for keyItem '" + statusName + "', and player has no key items.");
		//}
		//while (counter > 0)
		//{
			//counter--;
			//if (keyItems[counter].keyName == statusName)
				//return keyItems[counter].value4;
		//}
		////trace("ERROR: Looking for key item '" + statusName + "', but player does not have it.");
		//return 0;
	//}
	//
	//public function removeKeyItems():Void
	//{
		//var counter:Number = keyItems.length;
		//while (counter > 0)
		//{
			//counter--;
			//keyItems.splice(counter, 1);
		//}
	//}
	
	public function hasKeyItem(itemId:String):Bool
	{
		return keyItems.indexOf(itemId) != -1;
	}
	
	//public function viridianChange():Bool
	//{
		//var count:Int = cockTotal();
		//if (count == 0)
			//return false;
		//while (count > 0)
		//{
			//count--;
			//if (cocks[count].sock == "amaranthine" && cocks[count].cockType != CockTypesEnum.DISPLACER)
				//return true;
		//}
		//return false;
	//}
	//
	//public function hasKnot(arg:Int = 0):Bool
	//{
		//if (arg > cockTotal() - 1 || arg < 0)
			//return false;
		//return cocks[arg].hasKnot();
	//}
	//
	//
	//public function maxHunger():Number
	//{
		//return 100;
	//}
	//
	//public function growHair(amount:Number = .1):Bool {
		////Grow hair!
		//var tempHair:Number = hairLength;
		//if (hairType == HAIR_BASILISK_SPINES) return false;
		//hairLength += amount;
		//if (hairType == HAIR_BASILISK_PLUME && hairLength > 8) hairLength = 8;
		//if (hairLength > 0 && tempHair == 0) {
			//game.outputText("\n<b>You are no longer bald.  You now have " + hairDescript() + " coating your head.\n</b>");
			//return true;
		//}
		//else if (
			//(hairLength >= 1 && tempHair < 1) ||
			//(hairLength >= 3 && tempHair < 3) ||
			//(hairLength >= 6 && tempHair < 6) ||
			//(hairLength >= 10 && tempHair < 10) ||
			//(hairLength >= 16 && tempHair < 16) ||
			//(hairLength >= 26 && tempHair < 26) ||
			//(hairLength >= 40 && tempHair < 40) ||
			//(hairLength >= 40 && hairLength >= tallness && tempHair < tallness)
		//) {
			//game.outputText("\n<b>Your hair's growth has reached a new threshold, giving you " + hairDescript() + ".\n</b>");
			//return true;
		//}
		//return false;
	//}
//
	//public function growBeard(amount:Number = .1):Bool {
		////Grow beard!
		//var tempBeard:Number = beardLength;
		//beardLength += amount;
		//if (beardLength > 0 && tempBeard == 0) {
			//game.outputText("\n<b>You feel a tingling in your cheeks and chin.  You now have " + beardDescript() + " coating your cheeks and chin.\n</b>");
			//return true;
		//}
		//else if (beardLength >= 0.2 && tempBeard < 0.2) {
			//game.outputText("\n<b>Your beard's growth has reached a new threshold, giving you " + beardDescript() + ".\n</b>");
			//return true;
		//}
		//else if (beardLength >= 0.5 && tempBeard < 0.5) {
			//game.outputText("\n<b>Your beard's growth has reached a new threshold, giving you " + beardDescript() + ".\n</b>");
			//return true;
		//}
		//else if (beardLength >= 1.5 && tempBeard < 1.5) {
			//game.outputText("\n<b>Your beard's growth has reached a new threshold, giving you " + beardDescript() + ".\n</b>");
			//return true;
		//}
		//else if (beardLength >= 3 && tempBeard < 3) {
			//game.outputText("\n<b>Your beard's growth has reached a new threshold, giving you " + beardDescript() + ".\n</b>");
			//return true;
		//}
		//else if (beardLength >= 6 && tempBeard < 6) {
			//game.outputText("\n<b>Your beard's growth has reached a new threshold, giving you " + beardDescript() + ".\n</b>");
			//return true;
		//}
//
		//return false;
	//}
}