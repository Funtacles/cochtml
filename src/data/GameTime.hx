package data;

/**
 * ...
 * @author Funtacles
 */
class GameTime 
{
	public var hours(default, null):Int;
	public var minutes(default, null):Int;
	
	public var day(default, null):Int;
	
	public function new() 
	{
		hours = 0;
		minutes = 0;
		day = 0;
		
		_updateListeners = new Array<GameTime -> Void>();
	}
	
	public function setHours(hours:Int):Void {
		setHoursInternal(hours);
		
		triggerUpdate();
	}
	
	private function setHoursInternal(h:Int):Void {
		hours = h;
		
		if (hours > 23)
			hours = 23;
		else if (hours < 0)
			hours = 0;
	}
	
	public function setMinutes(minutes:Int):Void {
		setMinutesInternal(minutes);
		
		triggerUpdate();
	}
	
	private function setMinutesInternal(m:Int):Void {
		minutes = m;
		
		if (minutes > 59)
			minutes = 59;
		else if (minutes < 0)
			minutes = 0;
	}
	
	public function setTime(hours:Int, minutes:Int):Void {
		setHoursInternal(hours);
		setMinutesInternal(minutes);
		
		triggerUpdate();
	}
	
	public function addTime(h:Int, m:Int):Void {
		hours += addMinutesInternal(m);
		day += addHoursInternal(h);
		
		triggerUpdate();
	}
	
	private function addMinutesInternal(m:Int):Int {
		if (m < 0)
			return 0;
		
		var h:Int = 0;
		
		minutes += m;
		
		if (minutes > 59)
		{
			h = Std.int(minutes / 60);
			minutes %= 60;
		}
		
		return h;
	}
	
	private function addHoursInternal(h:Int):Int {
		if (h < 0)
			return 0;
		
		var d:Int = 0;
		
		hours += h;
		
		if (hours > 23)
		{
			d = Std.int(hours / 24);
			hours %= 24;
		}
		
		return d;
	}
	
	private var _updateListeners:Array<GameTime -> Void>;
	public function addUpdateListener(func:GameTime -> Void):Void {
		_updateListeners.push(func);
	}
	
	public function triggerUpdate():Void {
		for (listener in _updateListeners)
			listener(this); 
	}
}