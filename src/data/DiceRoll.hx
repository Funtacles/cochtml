package data;

/**
 * Used to generate random numbers within a specified range.
 * @author Funtacles
 */
class DiceRoll 
{
	private var _numberOfDice:Int;
	private var _valueOfDice:Int;
	private var _modifier:Int;
	
	public function new(number:Int, value:Int, modifier:Int = 0) 
	{
		_numberOfDice = number;
		_valueOfDice = value;
		_modifier = modifier;
	}
	
	public function roll():Int {
		var result:Int = 0;
		for (i in 0..._numberOfDice) {
			result += Std.random(_valueOfDice);
		}
		
		return result + _modifier;
	}
	
	public function getNumberOfDice():Int {
		return _numberOfDice;
	}
	
	public function getValueOfDice(): Int {
		return _valueOfDice;
	}
	
	public function getModifier():Int {
		return _modifier;
	}
	
	public static function parse(s:String): DiceRoll {
		if (!StringUtil.isDiceRoll(s))
			return null;
		
		var results = s.split("d");
		var number = Std.parseInt(results[0]);
		
		var modSign:String = null;
		if (results[1].indexOf("+") != -1)
			modSign = "+";
		else if (results[1].indexOf("-") != -1)
			modSign = "-";
		
		var value:Int, mod:Int = 0;
		if (modSign == null) {
			value = Std.parseInt(results[1]);
		} else {
			results = results[1].split(modSign);
			value = Std.parseInt(StringTools.trim(results[0]));
			mod = Std.parseInt(StringTools.trim(results[1]));
			
			if (modSign == "-")
				mod *= -1;
		}
		
		return new DiceRoll(number, value, mod);
	}
}