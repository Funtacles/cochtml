package data.nav;
import StringUtil;
import data.game.flag.*;
import menu.EventMenu;
import enums.Stat;

/**
 * ...
 * @author Funtacles
 */
class Camp 
{
	public static function init():Void {
		
	}
	
	public function returnToCamp(timeUsed:Int):Void {
		UI.clearMainText();
		if (timeUsed == 1)
			UI.write("An hour passes...<br>");
		else 
			UI.write(StringUtil.numberToWord(timeUsed) + " hours pass...<br>");
	}
	
	public function returnToCampUseOneHour():Void { returnToCamp(1); }
	public function returnToCampUseTwoHours():Void { returnToCamp(2); }
	public function returnToCampUseFourHours():Void { returnToCamp(4); } 
	public function returnToCampUseEightHours():Void { returnToCamp(8); }
	
	public static function getCampDescription():String {
		var text = "";
		
		if (Flags.getTrigger(TriggerFlag.ISABELLA_FOLLOWER_ACCEPTED).set) {
			text += "Your campsite got a lot more comfortable once Isabella moved in.  Carpets cover up much of the barren ground, simple awnings tied to the rocks provide shade, and hand-made wooden furniture provides comfortable places to sit and sleep.  ";
		} else {
			if (Game.gameTime.day < 10) 
				text += "Your campsite is fairly simple at the moment.  Your tent and bedroll are set in front of the rocks that lead to the portal.  You have a small fire pit as well.  ";
			else if (Game.gameTime.day < 20) 
				text += "Your campsite is starting to get a very 'lived-in' look.  The fire-pit is well defined with some rocks you've arranged around it, and your bedroll and tent have been set up in the area most sheltered by rocks.  ";
			else 
			{
				//if (!isabellaFollower()) outputText("Your new home is as comfy as a camp site can be.  ");
				text += "The fire-pit ";
				/*if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0) outputText("is ");
				else*/ 
					text += "and tent are both ";
				text += "set up perfectly, and in good repair.  ";
			}
		}
		if (Game.gameTime.day >= 20) 
			text += "You've even managed to carve some artwork into the rocks around the camp's perimeter.<br><br>";
		//if (flags[kFLAGS.CAMP_CABIN_PROGRESS] == 7) outputText("There's an unfinished wooden structure. As of right now, it's just frames nailed together.<br><br>");
		//if (flags[kFLAGS.CAMP_CABIN_PROGRESS] == 8) outputText("There's an unfinished cabin. It's currently missing windows and door.<br><br>");
		//if (flags[kFLAGS.CAMP_CABIN_PROGRESS] == 9) outputText("There's a nearly-finished cabin. It looks complete from the outside but inside, it's missing flooring.<br><br>");
		//if (flags[kFLAGS.CAMP_CABIN_PROGRESS] >= 10) outputText("Your cabin is situated near the edge of camp.<br><br>");
		//if (flags[kFLAGS.CLARA_IMPRISONED] > 0) 
		//{
			//marblePurification.claraCampAddition();
		//}
		//Nursery
		//if (flags[kFLAGS.MARBLE_NURSERY_CONSTRUCTION] == 100 && player.hasStatusEffect(StatusEffects.CampMarble)) {
			//outputText("Marble has built a fairly secure nursery amongst the rocks to house your ");
			//if (flags[kFLAGS.MARBLE_KIDS] == 0) outputText("future children");
			//else {
				//outputText(num2Text(flags[kFLAGS.MARBLE_KIDS]) + " child");
				//if (flags[kFLAGS.MARBLE_KIDS] > 1) outputText("ren");
			//}
			//outputText(".<br><br>");
		//}
		//HARPY ROOKERY
		//if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] > 0) {
			////Rookery Descriptions (Short)
			////Small (1 mature daughter)
			//if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] == 1) {
				//outputText("There's a smallish harpy nest that your daughter has built up with rocks piled high near the fringes of your camp.  It's kind of pathetic, but she seems proud of her accomplishment.  ");
			//}
			////Medium (2-3 mature daughters)
			//else if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] <= 3) {
				//outputText("There's a growing pile of stones built up at the fringes of your camp.  It's big enough to be considered a small hill by this point, dotted with a couple small harpy nests just barely big enough for two.  ");
			//}
			////Big (4 mature daughters)
			//else if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] <= 4) {
				//outputText("The harpy rookery at the edge of camp has gotten pretty big.  It's taller than most of the standing stones that surround the portal, and there's more nests than harpies at this point.  Every now and then you see the four of them managing a boulder they dragged in from somewhere to add to it.  ");
			//}
			////Large (5-10 mature daughters)
			//else if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] <= 10) {
				//outputText("The rookery has gotten quite large.  It stands nearly two stories tall at this point, dotted with nests and hollowed out places in the center.  It's surrounded by the many feathers the assembled harpies leave behind.  ");
			//}
			////Giant (11-20 mature daughters)
			//else if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] <= 20) {
				//outputText("A towering harpy rookery has risen up at the fringes of your camp, filled with all of your harpy brood.  It's at least three stories tall at this point, and it has actually begun to resemble a secure structure.  These harpies are always rebuilding and adding onto it.  ");
			//}
			////Massive (21-50 mature daughters)
			//else if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] <= 50) {
				//outputText("A massive harpy rookery towers over the edges of your camp.  It's almost entirely built out of stones that are fit seamlessly into each other, with many ledges and overhangs for nests.  There's a constant hum of activity over there day or night.  ");
			//}
			////Immense (51+ Mature daughters)
			//else {
				//outputText("An immense harpy rookery dominates the edge of your camp, towering over the rest of it.  Innumerable harpies flit around it, always working on it, assisted from below by the few sisters unlucky enough to be flightless.  ");
			//}
			//outputText("<br><br>");
		//}
		//Traps
		//if (player.hasStatusEffect(StatusEffects.DefenseCanopy)) {
			//outputText("A thorny tree has sprouted near the center of the camp, growing a protective canopy of spiky vines around the portal and your camp.  ");
		//}
		//if (flags[kFLAGS.CAMP_WALL_PROGRESS] >= 20 && flags[kFLAGS.CAMP_WALL_PROGRESS] < 100) {
			//if (flags[kFLAGS.CAMP_WALL_PROGRESS] / 20 == 0) outputText("A thick wooden wall has been erected to provide a small amount of defense.  ");
			//else outputText("Thick wooden walls have been erected to provide some defense.  ");
		//}
		//else if (flags[kFLAGS.CAMP_WALL_PROGRESS] >= 100) {
			//outputText("Thick wooden walls have been erected; they surround one half of your camp perimeter and provide good defense, leaving the another half open for access to the stream.  ");
			//if (flags[kFLAGS.CAMP_WALL_GATE] > 0) outputText("A gate has been constructed in the middle of the walls; it gets closed at night to keep any invaders out.  ");
			//if (flags[kFLAGS.CAMP_WALL_SKULLS] > 0) {
				//if (flags[kFLAGS.CAMP_WALL_SKULLS] == 1) outputText("A single imp skull has been mounted near the gateway");
				//else if (flags[kFLAGS.CAMP_WALL_SKULLS] >= 2 && flags[kFLAGS.CAMP_WALL_SKULLS] < 5) outputText("Few imp skulls have been mounted near the gateway");
				//else if (flags[kFLAGS.CAMP_WALL_SKULLS] >= 5 && flags[kFLAGS.CAMP_WALL_SKULLS] < 15) outputText("Several imp skulls have been mounted near the gateway");
				//else outputText("Many imp skulls decorate the gateway and wall, some even impaled on wooden spikes");
				//outputText(" to serve as deterrence.  ");
				//if (flags[kFLAGS.CAMP_WALL_SKULLS] == 1) outputText("There is currently one skull.  ");
				//else outputText("There are currently " + num2Text(flags[kFLAGS.CAMP_WALL_SKULLS]) + " skulls.  ");
			//}
			//if (flags[kFLAGS.CAMP_WALL_STATUES] > 0) {
				//if (flags[kFLAGS.CAMP_WALL_STATUES] == 1)
					//output.text("Looking around the perimeter of your camp you spy a single marble imp statue.  ");
				//else
					//output.text("Dotted around and on the wall that surrounds your camp you spy "
								//+ num2Text(flags[kFLAGS.CAMP_WALL_STATUES]) + " marble imp statues.  ");
			//}
			//outputText("<br><br>");
		//}
		//else outputText("You have a number of traps surrounding your makeshift home, but they are fairly simple and may not do much to deter a demon.  ");
		//outputText("The portal shimmers in the background as it always does, looking menacing and reminding you of why you came.");
		//if (flags[kFLAGS.ANT_KIDS] > 1000) outputText(" Really close to it there is a small entrance to the underground maze created by your ant children. And due to Phylla wish from time to time one of your children coming out this entrance to check on the situation near portal. You feel a little more safe now knowing that it will be harder for anyone to go near the portal without been noticed or...if someone came out of the portal.");
		//outputText("<br><br>");
//
		////Ember's anti-minotaur crusade!
		//if (flags[kFLAGS.EMBER_CURRENTLY_FREAKING_ABOUT_MINOCUM] == 1) {
			////Modified Camp Description
			//outputText("Since Ember began " + emberMF("his","her") + " 'crusade' against the minotaur population, skulls have begun to pile up on either side of the entrance to " + emberScene.emberMF("his","her") + " den.  There're quite a lot of them.<br><br>");
		//}
		////Dat tree!
		//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] >= 4 && flags[kFLAGS.FUCK_FLOWER_KILLED] == 0) {
			//outputText("On the outer edges, half-hidden behind a rock, is a large, very healthy tree.  It grew fairly fast, but seems to be fully developed now.  Holli, Marae's corrupt spawn, lives within.<br><br>");
		//}
		//
		////Display NPCs
		//campFollowers(true);
		//
		////MOUSEBITCH
		//if (amilyScene.amilyFollower() && flags[kFLAGS.AMILY_FOLLOWER] == 1) {
			//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] >= 4 && flags[kFLAGS.FUCK_FLOWER_KILLED] == 0) outputText("Amily has relocated her grass bedding to the opposite side of the camp from the strange tree; every now and then, she gives it a suspicious glance, as if deciding whether to move even further.<br><br>");
			//else outputText("A surprisingly tidy nest of soft grasses and sweet-smelling herbs has been built close to your " + (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 ? "cabin": "bedroll") + ". A much-patched blanket draped neatly over the top is further proof that Amily sleeps here. She changes the bedding every few days, to ensure it stays as nice as possible.<br><br>");
		//}
		
		return text;
	}
	
	public static function onReturnToCamp():Void {
		if (Flags.getTrigger(TriggerFlag.ERLKING_CANE_OBTAINED).notset && Game.player.hasKeyItem("GoldenAntlers")) {
			EventMenu.startEvent("ErlkingCane");
			return;
		}
		
		if (Flags.getTrigger(TriggerFlag.MARBLE_LEFT_OVER_CORRUPTION).set && Game.player.stats[Stat.Corruption] <= (40 + Game.player.corruptionTolerance()))
		{
			EventMenu.startEvent("MarbleReturnsToCamp");
			return;
		}
	}



		//if (marbleScene.marbleFollower())
		//{
			////Cor < 50
			////No corrupt: Jojo, Amily, or Vapula
			////Purifying Murble
			//if (player.cor < (50 + player.corruptionTolerance()) && !campCorruptJojo() && !amilyScene.amilyCorrupt() && !vapulaSlave() 
				//&& flags[kFLAGS.MARBLE_PURIFICATION_STAGE] == 0 && flags[kFLAGS.MARBLE_COUNTUP_TO_PURIFYING] >= 200
				//&& player.findPerk(PerkLib.MarblesMilk) < 0)
			//{
				//hideMenus();
				//marblePurification.BLUHBLUH();
				//return;
			//}
			//if (flags[kFLAGS.MARBLE_PURIFICATION_STAGE] >= 5)
			//{
				//if (flags[kFLAGS.MARBLE_WARNED_ABOUT_CORRUPTION] == 0 && player.cor >= (50 + player.corruptionTolerance()))
				//{
					//hideMenus();
					//marblePurification.marbleWarnsPCAboutCorruption();
					//return;
				//}
				//if (flags[kFLAGS.MARBLE_WARNED_ABOUT_CORRUPTION] == 1 && flags[kFLAGS.MARBLE_LEFT_OVER_CORRUPTION] == 0 && player.cor >= (60 + player.corruptionTolerance()))
				//{
					//hideMenus();
					//marblePurification.marbleLeavesThePCOverCorruption();
					//return;
				//}
			//}
			//if (flags[kFLAGS.MARBLE_RATHAZUL_COUNTER_1] == 1 && (time.hours == 6 || time.hours == 7))
			//{
				//hideMenus();
				//marblePurification.rathazulsMurbelReport();
				//return;
			//}
			//if (flags[kFLAGS.MARBLE_RATHAZUL_COUNTER_2] == 1)
			//{
				//hideMenus();
				//marblePurification.claraShowsUpInCampBECAUSESHESACUNT();
				//return;
			//}
		//}
		//if (arianFollower() && flags[kFLAGS.ARIAN_MORNING] == 1) {
			//hideMenus();
			//arianScene.wakeUpAfterArianSleep();
			//return;
		//}
		//if (arianFollower() && flags[kFLAGS.ARIAN_EGG_EVENT] >= 30) {
			//hideMenus();
			//arianScene.arianEggingEvent();
			//return;
		//}
		//if (arianFollower() && flags[kFLAGS.ARIAN_EGG_COUNTER] >= 24 && flags[kFLAGS.ARIAN_VAGINA] > 0) {
			//hideMenus();
			//arianScene.arianLaysEggs();
			//return;
		//}
		//if (flags[kFLAGS.JOJO_BIMBO_STATE] >= 3 && flags[kFLAGS.JOY_NIGHT_FUCK] == 1) {
			//joyScene.wakeUpWithJoyPostFuck();
			//return;
		//}
		//if (flags[kFLAGS.EMBER_MORNING] > 0 && ((flags[kFLAGS.BENOIT_CLOCK_BOUGHT] > 0 && model.time.hours >= flags[kFLAGS.BENOIT_CLOCK_ALARM]) || (flags[kFLAGS.BENOIT_CLOCK_BOUGHT] <= 0 && model.time.hours >= 6))) {
			//hideMenus();
			//emberScene.postEmberSleep();
			//return;
		//}
		//if (flags[kFLAGS.JACK_FROST_PROGRESS] > 0) {
			//hideMenus();
			//kGAMECLASS.xmas.jackFrost.processJackFrostEvent();
			//return;
		//}
		//if (player.hasKeyItem("Super Reducto") < 0 && milkSlave() && player.hasStatusEffect(StatusEffects.CampRathazul) && player.statusEffectv2(StatusEffects.MetRathazul) >= 4) {
			//hideMenus();
			//milkWaifu.ratducto();
			//return;
		//}
		//if (kGAMECLASS.xmas.xmasMisc.nieveHoliday() && model.time.hours == 6) {
			//if (player.hasKeyItem("Nieve's Tear") >= 0 && flags[kFLAGS.NIEVE_STAGE] != 5)
			//{
				//kGAMECLASS.xmas.xmasMisc.returnOfNieve();
				//hideMenus();
				//return;
			//}
			//else if (flags[kFLAGS.NIEVE_STAGE] == 0) {
				//hideMenus();
				//kGAMECLASS.xmas.xmasMisc.snowLadyActive();
				//return;
			//}
			//else if (flags[kFLAGS.NIEVE_STAGE] == 4) {
				//hideMenus();
				//kGAMECLASS.xmas.xmasMisc.nieveComesToLife();
				//return;
			//}
		//}
		//if (kGAMECLASS.helScene.followerHel()) {
			//if (helFollower.isHeliaBirthday() && flags[kFLAGS.HEL_FOLLOWER_LEVEL] >= 2 && flags[kFLAGS.HELIA_BIRTHDAY_OFFERED] == 0) {
				//hideMenus();
				//helFollower.heliasBirthday();
				//return;
			//}
			//if (kGAMECLASS.helScene.pregnancy.isPregnant) {
				//switch (kGAMECLASS.helScene.pregnancy.eventTriggered()) {
					//case 2: hideMenus();
							//helSpawnScene.bulgyCampNotice();
							//return;
					//case 3: hideMenus();
							//helSpawnScene.heliaSwollenNotice();
							//return;
					//case 4: hideMenus();
							//helSpawnScene.heliaGravidity();
							//return;
					//default:
							//if (kGAMECLASS.helScene.pregnancy.incubation == 0 && (model.time.hours == 6 || model.time.hours == 7)) {
								//hideMenus();
								//helSpawnScene.heliaBirthtime();
								//return;
							//}
				//}
			//}
		//}
		//if (flags[kFLAGS.HELSPAWN_AGE] == 1 && flags[kFLAGS.HELSPAWN_GROWUP_COUNTER] == 7) {
			//hideMenus();
			//helSpawnScene.helSpawnGraduation();
			//return;
		//}
		//if (model.time.hours >= 10 && model.time.hours <= 18 && (model.time.days % 20 == 0 || model.time.hours == 12) && flags[kFLAGS.HELSPAWN_DADDY] == 2 && helSpawnScene.helspawnFollower()) {
			//hideMenus();
			//helSpawnScene.maiVisitsHerKids();
			//return;
		//}
		//if (model.time.hours == 6 && flags[kFLAGS.HELSPAWN_DADDY] == 1 && model.time.days % 30 == 0 && flags[kFLAGS.SPIDER_BRO_GIFT] == 0 && helSpawnScene.helspawnFollower())
		//{
			//hideMenus();
			//helSpawnScene.spiderBrosGift();
			//return;
		//}
		//if (model.time.hours >= 10 && model.time.hours <= 18 && (model.time.days % 15 == 0 || model.time.hours == 12) && helSpawnScene.helspawnFollower() && flags[kFLAGS.HAKON_AND_KIRI_VISIT] == 0) {
			//hideMenus();
			//helSpawnScene.hakonAndKiriComeVisit();
			//return;
		//}
		//if (flags[kFLAGS.HELSPAWN_AGE] == 2 && flags[kFLAGS.HELSPAWN_DISCOVER_BOOZE] == 0 && (rand(10) == 0 || flags[kFLAGS.HELSPAWN_GROWUP_COUNTER] == 6)) {
			//hideMenus();
			//helSpawnScene.helspawnDiscoversBooze();
			//return;
		//}
		//if (flags[kFLAGS.HELSPAWN_AGE] == 2 && flags[kFLAGS.HELSPAWN_WEAPON] == 0 && flags[kFLAGS.HELSPAWN_GROWUP_COUNTER] >= 3 && model.time.hours >= 10 && model.time.hours <= 18) {
			//hideMenus();
			//helSpawnScene.helSpawnChoosesAFightingStyle();
			//return;
		//}
		//if (flags[kFLAGS.HELSPAWN_AGE] == 2 && (model.time.hours == 6 || model.time.hours == 7) && flags[kFLAGS.HELSPAWN_GROWUP_COUNTER] >= 7 && flags[kFLAGS.HELSPAWN_FUCK_INTERRUPTUS] == 1) {
			//helSpawnScene.helspawnAllGrownUp();
			//return;
		//}
		//if ((sophieFollower() || bimboSophie()) && flags[kFLAGS.SOPHIE_DAUGHTER_MATURITY_COUNTER] == 1) {
			//flags[kFLAGS.SOPHIE_DAUGHTER_MATURITY_COUNTER] = 0;
			//sophieBimbo.sophieKidMaturation();
			//hideMenus();
			//return;
		//}
		////Bimbo Sophie Move In Request!
		//if (bimboSophie() && flags[kFLAGS.SOPHIE_BROACHED_SLEEP_WITH] == 0 && sophieScene.pregnancy.event >= 2)
		//{
			//hideMenus();
			//sophieBimbo.sophieMoveInAttempt();
			//return;
		//}
		//if (!kGAMECLASS.xmas.xmasMisc.nieveHoliday() && model.time.hours == 6 && flags[kFLAGS.NIEVE_STAGE] > 0) {
			//kGAMECLASS.xmas.xmasMisc.nieveIsOver();
			//return;
		//}
		////Amily followup!
		//if (flags[kFLAGS.PC_PENDING_PREGGERS] == 1) {
			//kGAMECLASS.amilyScene.postBirthingEndChoices();
			//flags[kFLAGS.PC_PENDING_PREGGERS] = 2;
			//return;
		//}
		//if (timeQ > 0) {
			//if (!campQ) {
				//clearOutput();
				//outputText("More time passes...<br>");
				//goNext(timeQ, false);
				//return;
			//}
			//else {
				//if (model.time.hours < 6 || model.time.hours > 20) {
					//doSleep();
				//}
				//else {
					//rest();
				//}
				//return;
			//}
		//}
		//if (flags[kFLAGS.FUCK_FLOWER_KILLED] == 0 && flags[kFLAGS.CORRUPT_MARAE_FOLLOWUP_ENCOUNTER_STATE] > 0 && (flags[kFLAGS.IN_PRISON] == 0 && flags[kFLAGS.IN_INGNAM] == 0)) {
			//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] == 0 && flags[kFLAGS.FUCK_FLOWER_GROWTH_COUNTER] >= 8) {
				//holliScene.getASprout();
				//hideMenus();
				//return;
			//}
			//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] == 1 && flags[kFLAGS.FUCK_FLOWER_GROWTH_COUNTER] >= 7) {
				//holliScene.fuckPlantGrowsToLevel2();
				//hideMenus();
				//return;
			//}
			//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] == 2 && flags[kFLAGS.FUCK_FLOWER_GROWTH_COUNTER] >= 25) {
				//holliScene.flowerGrowsToP3();
				//hideMenus();
				//return;
			//}
			////Level 4 growth
			//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] == 3 && flags[kFLAGS.FUCK_FLOWER_GROWTH_COUNTER] >= 40) {
				//holliScene.treePhaseFourGo();
				//hideMenus();
				//return;
			//}
		//}
		////Jojo treeflips!
		//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] >= 4 && flags[kFLAGS.FUCK_FLOWER_KILLED] == 0 && player.hasStatusEffect(StatusEffects.PureCampJojo) && flags[kFLAGS.JOJO_BIMBO_STATE] < 3) {
			//holliScene.JojoTransformAndRollOut();
			//hideMenus();
			//return;
		//}
		////Amily flips out
		//if (amilyScene.amilyFollower() && !amilyScene.amilyCorrupt() && flags[kFLAGS.FUCK_FLOWER_LEVEL] >= 4 && flags[kFLAGS.FUCK_FLOWER_KILLED] == 0 && flags[kFLAGS.AMILY_TREE_MADEUPBULLSHIT] < 1) {
			//holliScene.amilyHatesTreeFucking();
			//hideMenus();
			//return;
		//}
		//if (flags[kFLAGS.FUCK_FLOWER_KILLED] == 1 && flags[kFLAGS.AMILY_TREE_FLIPOUT] == 1 && !amilyScene.amilyFollower() && flags[kFLAGS.AMILY_VISITING_URTA] == 0) {
			//holliScene.amilyComesBack();
			//flags[kFLAGS.AMILY_TREE_FLIPOUT] = 2;
			//hideMenus();
			//return;
		//}
		////Anemone birth followup!
		//if (player.hasStatusEffect(StatusEffects.CampAnemoneTrigger)) {
			//player.removeStatusEffect(StatusEffects.CampAnemoneTrigger);
			//anemoneScene.anemoneKidBirthPtII();
			//hideMenus();
			//return;
		//}
		////Exgartuan clearing
		//if (player.statusEffectv1(StatusEffects.Exgartuan) == 1 && (player.cockArea(0) < 100 || player.cocks.length == 0)) {
			//exgartuanCampUpdate();
			//return;
		//}
		//else if (player.statusEffectv1(StatusEffects.Exgartuan) == 2 && player.biggestTitSize() < 12) {
			//exgartuanCampUpdate();
			//return;
		//}
		////Izzys tits asplode
		//if (isabellaFollower() && flags[kFLAGS.ISABELLA_MILKED_YET] >= 10 && player.hasKeyItem("Breast Milker - Installed At Whitney's Farm") >= 0) {
			//isabellaFollowerScene.milktasticLacticLactation();
			//hideMenus();
			//return;
		//}
		////Isabella and Valeria sparring.
		//if (isabellaFollower() && flags[kFLAGS.VALARIA_AT_CAMP] > 0 && flags[kFLAGS.ISABELLA_VALERIA_SPARRED] == 0) {
			//valeria.isabellaAndValeriaSpar();
			//return;
		//}
		////Marble meets follower izzy when moving in
		//if (flags[kFLAGS.ISABELLA_MURBLE_BLEH] == 1 && isabellaFollower() && player.hasStatusEffect(StatusEffects.CampMarble)) {
			//isabellaFollowerScene.angryMurble();
			//hideMenus();
			//return;
		//}
		////Cotton preg freakout
		//if (player.pregnancyIncubation <= 280 && player.pregnancyType == PregnancyStore.PREGNANCY_COTTON &&
			//flags[kFLAGS.COTTON_KNOCKED_UP_PC_AND_TALK_HAPPENED] == 0 && (model.time.hours == 6 || model.time.hours == 7)) {
			//kGAMECLASS.telAdre.cotton.goTellCottonShesAMomDad();
			//hideMenus();
			//return;
		//}
		////Bimbo Sophie finds ovi elixer in chest!
		//if (bimboSophie() && hasItemInStorage(consumables.OVIELIX) && rand(5) == 0 && flags[kFLAGS.TIMES_SOPHIE_HAS_DRUNK_OVI_ELIXIR] == 0 && player.gender > 0) {
			//sophieBimbo.sophieEggApocalypse();
			//hideMenus();
			//return;
		//}
		////Amily + Urta freakout!
		//if (!kGAMECLASS.urtaQuest.urtaBusy() && flags[kFLAGS.AMILY_VISITING_URTA] == 0 && rand(10) == 0 && flags[kFLAGS.URTA_DRINK_FREQUENCY] >= 0 && flags[kFLAGS.URTA_BANNED_FROM_SCYLLA] == 0 && flags[kFLAGS.AMILY_NEED_TO_FREAK_ABOUT_URTA] == 1 && amilyScene.amilyFollower() && flags[kFLAGS.AMILY_FOLLOWER] == 1 && !amilyScene.pregnancy.isPregnant) {
			//finter.amilyUrtaReaction();
			//hideMenus();
			//return;
		//}
		////Find jojo's note!
		//if (flags[kFLAGS.JOJO_FIXED_STATUS] == 1  && flags[kFLAGS.JOJO_READ_FIXED_NOTE] == 0 && flags[kFLAGS.AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO] == 0) {
			//finter.findJojosNote();
			//hideMenus();
			//return;
		//}
		////Bimbo Jojo warning
		//if (player.hasStatusEffect(StatusEffects.PureCampJojo) && inventory.hasItemInStorage(consumables.BIMBOLQ) && flags[kFLAGS.BIMBO_LIQUEUR_STASH_COUNTER_FOR_JOJO] >= 72 && flags[kFLAGS.JOJO_BIMBO_STATE] == 0) {
			//joyScene.jojoPromptsAboutThief();
			//hideMenus();
			//return;
		//}
		////Jojo gets bimbo'ed!
		//if (player.hasStatusEffect(StatusEffects.PureCampJojo) && flags[kFLAGS.BIMBO_LIQUEUR_STASH_COUNTER_FOR_JOJO] >= 24 && flags[kFLAGS.JOJO_BIMBO_STATE] == 2) {
			//joyScene.jojoGetsBimbofied();
			//hideMenus();
			//return;
		//}
		////Joy gives birth!
		//if (flags[kFLAGS.JOJO_BIMBO_STATE] >= 3 && jojoScene.pregnancy.type == PregnancyStore.PREGNANCY_PLAYER && jojoScene.pregnancy.incubation == 0) {
			//joyScene.joyGivesBirth();
			//return;
		//}
		////Rathazul freaks out about jojo
		//if (flags[kFLAGS.RATHAZUL_CORRUPT_JOJO_FREAKOUT] == 0 && rand(5) == 0 && player.hasStatusEffect(StatusEffects.CampRathazul) && campCorruptJojo()) {
			//finter.rathazulFreaksOverJojo();
			//hideMenus();
			//return;
		//}
		////Izma/Marble freakout - marble moves in
		//if (flags[kFLAGS.IZMA_MARBLE_FREAKOUT_STATUS] == 1) {
			//izmaScene.newMarbleMeetsIzma();
			//hideMenus();
			//return;
		//}
		////Izma/Amily freakout - Amily moves in
		//if (flags[kFLAGS.IZMA_AMILY_FREAKOUT_STATUS] == 1) {
			//izmaScene.newAmilyMeetsIzma();
			//hideMenus();
			//return;
		//}
		////Amily/Marble Freakout
		//if (flags[kFLAGS.AMILY_NOT_FREAKED_OUT] == 0 && player.hasStatusEffect(StatusEffects.CampMarble) && flags[kFLAGS.AMILY_FOLLOWER] == 1 && amilyScene.amilyFollower() && marbleScene.marbleAtCamp()) {
			//finter.marbleVsAmilyFreakout();
			//hideMenus();
			//return;
		//}
		////Amily and/or Jojo freakout about Vapula!!
		//if (vapulaSlave() && ((player.hasStatusEffect(StatusEffects.PureCampJojo) && flags[kFLAGS.KEPT_PURE_JOJO_OVER_VAPULA] <= 0) || (amilyScene.amilyFollower() && !amilyScene.amilyCorrupt() && flags[kFLAGS.KEPT_PURE_AMILY_OVER_VAPULA] <= 0))) {
			////Jojo but not Amily (Must not be bimbo!)
			//if ((player.hasStatusEffect(StatusEffects.PureCampJojo)) && !(amilyScene.amilyFollower() && !amilyScene.amilyCorrupt()) && flags[kFLAGS.KEPT_PURE_JOJO_OVER_VAPULA] == 0)
				//vapula.mouseWaifuFreakout(false, true);
			////Amily but not Jojo
			//else if ((amilyScene.amilyFollower() && !amilyScene.amilyCorrupt()) && !player.hasStatusEffect(StatusEffects.PureCampJojo) && flags[kFLAGS.KEPT_PURE_AMILY_OVER_VAPULA] == 0) {
				//vapula.mouseWaifuFreakout(true, false);
			//}
			////Both
			//else
				//vapula.mouseWaifuFreakout(true, true);
			//hideMenus();
			//return;
		//}
		//if (followerKiha() && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] == 144) {
			//kihaFollower.kihaTellsChildrenStory();
			//return;
		//}
		////Go through Helia's first time move in interactions if  you haven't yet.
		//if (flags[kFLAGS.HEL_FOLLOWER_LEVEL] == 2 && kGAMECLASS.helScene.followerHel() && flags[kFLAGS.HEL_INTROS_LEVEL] == 0) {
			//helFollower.helFollowersIntro();
			//hideMenus();
			//return;
		//}
		////If you've gone through Hel's first time actions and Issy moves in without being okay with threesomes.
		//if (flags[kFLAGS.HEL_INTROS_LEVEL] > 9000 && kGAMECLASS.helScene.followerHel() && isabellaFollower() && flags[kFLAGS.HEL_ISABELLA_THREESOME_ENABLED] == 0) {
			//helFollower.angryHelAndIzzyCampHelHereFirst();
			//hideMenus();
			//return;		
		//}
		////Reset.
		//flags[kFLAGS.CAME_WORMS_AFTER_COMBAT] = 0;
		//campQ = false;
		////Clear stuff
		//if (player.hasStatusEffect(StatusEffects.SlimeCravingOutput)) player.removeStatusEffect(StatusEffects.SlimeCravingOutput);
		////Reset luststick display status (see event parser)
		//flags[kFLAGS.PC_CURRENTLY_LUSTSTICK_AFFECTED] = 0;
		////Display Proper Buttons
		//mainView.showMenuButton( MainView.MENU_APPEARANCE );
		//mainView.showMenuButton( MainView.MENU_PERKS );
		//mainView.showMenuButton( MainView.MENU_STATS );
		//mainView.showMenuButton( MainView.MENU_DATA );
		//showStats();
		////Change settings of new game buttons to go to main menu
		//mainView.setMenuButton( MainView.MENU_NEW_MAIN, "Main Menu", kGAMECLASS.mainMenu.mainMenu );
		//mainView.newGameButton.toolTipText = "Return to main menu.";
		//mainView.newGameButton.toolTipHeader = "Main Menu";
		////clear up/down arrows
		//hideUpDown();
		////Level junk
		//if (setLevelButton()) return;
		////Build main menu
		//var exploreEvent:Function = getGame().exploration.doExplore;
		//var placesEvent:Function = (placesKnown() ? places : null);
		//clearOutput();
		//updateAchievements();
		//
//
		//
		//
		//campLoversMenu(true);
		//
		//campSlavesMenu(true);
//
		////Clear bee-status
		//if (player.hasStatusEffect(StatusEffects.ParalyzeVenom)) {
			//dynStats("str", player.statusEffectv1(StatusEffects.ParalyzeVenom),"spe", player.statusEffectv2(StatusEffects.ParalyzeVenom));
			//player.removeStatusEffect(StatusEffects.ParalyzeVenom);
			//outputText("<b>You feel quicker and stronger as the paralyzation venom in your veins wears off.</b><br><br>");
		//}
		//
		////Hunger check!
		//if (flags[kFLAGS.HUNGER_ENABLED] > 0 && player.hunger < 25)
		//{
			//outputText("<b>You have to eat something; your stomach is growling " + (player.hunger < 1 ? "painfully": "loudly") + ". </b>");
			//if (player.hunger < 10) {
				//outputText("<b>You are getting thinner and you're losing muscles. </b>");
			//}
			//if (player.hunger <= 0) {
				//outputText("<b>You are getting weaker due to starvation. </b>");
			//}
			//outputText("<br><br>");
		//}
		//
		////The uber horny
		//if (player.lust >= player.maxLust()) {
			//if (player.hasStatusEffect(StatusEffects.Dysfunction)) {
				//outputText("<b>You are debilitatingly aroused, but your sexual organs are so numbed the only way to get off would be to find something tight to fuck or get fucked...</b><br><br>");
			//}
			//else if (flags[kFLAGS.UNABLE_TO_MASTURBATE_BECAUSE_CENTAUR] > 0 && player.isTaur()) {
				//outputText("<b>You are delibitatingly aroused, but your sex organs are so difficult to reach that masturbation isn't at the forefront of your mind.</b><br><br>");
			//}
			//else {
				//outputText("<b>You are debilitatingly aroused, and can think of doing nothing other than masturbating.</b><br><br>");
				//exploreEvent = null;
				//placesEvent = null;
				////This once disabled the ability to rest, sleep or wait, but ir hasn't done that for many many builds
			//}
		//}
		////Set up rest stuff
		////Night
		//if (model.time.hours < 6 || model.time.hours > 20) {
			//if (flags[kFLAGS.GAME_END] == 0) { //Lethice not defeated
				//outputText("It is dark out, made worse by the lack of stars in the sky.  A blood-red moon hangs in the sky, seeming to watch you, but providing little light. It's far too dark to leave camp.<br><br>");
			//}
			//else { //Lethice defeated, proceed with weather
				//switch(flags[kFLAGS.CURRENT_WEATHER]) {
					//case 0:
					//case 1:
						//outputText("It is dark out. Stars dot the night sky. A blood-red moon hangs in the sky, seeming to watch you, but providing little light. It's far too dark to leave camp.<br><br>");
						//break;
					//case 2:
						//outputText("It is dark out. The sky is covered by clouds and you could faintly make out the red spot in the clouds which is presumed to be the moon. It's far too dark to leave camp.<br><br>");
						//break;
					//case 3:
						//outputText("It is dark out. The sky is covered by clouds raining water upon the ground. It's far too dark to leave camp.<br><br>");
						//break;
					//case 4:
						//outputText("It is dark out. The sky is covered by clouds raining water upon the ground and occasionally the sky flashes with lightning. It's far too dark to leave camp.<br><br>");
						//break;
					//default:
						//outputText("It is dark out. Stars dot the night sky. A blood-red moon hangs in the sky, seeming to watch you, but providing little light. It's far too dark to leave camp.<br><br>");
				//}
			//}
			//if (companionsCount() > 0 && !(model.time.hours > 4 && model.time.hours < 23)) {
				//outputText("Your camp is silent as your companions are sleeping right now.<br>");
			//}
			//exploreEvent = null;
			//placesEvent = null;
		//}
		////Day Time!
		//else {
			//if (flags[kFLAGS.GAME_END] > 0) { //Lethice defeated
				//switch(flags[kFLAGS.CURRENT_WEATHER]) {
					//case 0:
						//outputText("The sun shines brightly, illuminating the now-blue sky. ");
						//break;
					//case 1:
						//outputText("The sun shines brightly, illuminating the now-blue sky. Occasional clouds dot the sky, appearing to form different shapes. ");
						//break;
					//case 2:
						//outputText("The sky is light gray as it's covered by the clouds. ");
						//break;
					//case 3:
						//outputText("The sky is fairly dark as it's covered by the clouds that rain water upon the lands. ");
						//break;
					//case 4:
						//outputText("The sky is dark as it's thick with dark grey clouds that rain and occasionally the sky flashes with lightning. ");
						//break;
				//}
			//}
			//if (model.time.hours == 19) {
				//if (flags[kFLAGS.CURRENT_WEATHER] < 2)
					//outputText("The sun is close to the horizon, getting ready to set. ");
				//else
					//outputText("Though you cannot see the sun, the sky near the horizon began to glow orange. ");
			//}
			//if (model.time.hours == 20) {
				//if (flags[kFLAGS.CURRENT_WEATHER] < 2)
					//outputText("The sun has already set below the horizon. The sky glows orange. ");
				//else
					//outputText("Even with the clouds, the sky near the horizon is glowing bright orange. The sun may have already set at this point. ");
			//}
			//outputText("It's light outside, a good time to explore and forage for supplies with which to fortify your camp.<br>");
		//}

		//Unlock cabin.
		//if (Flags.getCounter(CounterFlag.CAMP_CABIN_PROGRESS).value <= 0 && Game.gameTime.day >= 14)
		//{
			//Flags.getCounter(CounterFlag.CAMP_CABIN_PROGRESS).value = 1;
			//clearOutput();
			//outputText("You realize that you have spent two weeks sleeping in tent every night. You think of something so you can sleep nicely and comfortably. Perhaps a cabin will suffice?");
			//doNext(playerMenu);
			//return;
		//}
		////Unlock something in character creation.
		//if (flags[kFLAGS.NEW_GAME_PLUS_BONUS_UNLOCKED_HERM] == 0)
		//{
			//if (player.gender == GENDER_HERM)
			//{
				//flags[kFLAGS.NEW_GAME_PLUS_BONUS_UNLOCKED_HERM] = 1;
				//outputText("<br><br><b>Congratulations! You have unlocked hermaphrodite option on character creation, accessible from New Game Plus!</b>");
				//kGAMECLASS.saves.savePermObject(false);
			//}
		//}
		//
		//dynStats(); // workaround for #484 'statbars do not fit in their place'
		//
		////Menu
		//menu();
		//addButton(0, "Explore", exploreEvent, null, null, null, "Explore to find new regions and visit any discovered regions.");
		//addButton(1, "Places", placesEvent, null, null, null, "Visit any places you have discovered so far.");
		//addButton(2, "Inventory", inventory.inventoryMenu, null, null, null, "The inventory allows you to use an item.  Be careful as this leaves you open to a counterattack when in combat.");
		//if (inventory.showStash()) addButton(3, "Stash", inventory.stash, null, null, null, "The stash allows you to store your items safely until you need them later.");
		//addButton(4, "Camp Actions", campActions, null, null, null, "Interact with the camp surroundings and also read your codex.");
		//if (followersCount() > 0) addButton(5, "Followers", campFollowers, null, null, null, "Check up on any followers or companions who are joining you in or around your camp.  You'll probably just end up sleeping with them.");
		//if (loversCount() > 0) addButton(6, "Lovers", campLoversMenu, null, null, null, "Check up on any lovers you have invited so far to your camp and interact with them.");
		//if (slavesCount() > 0) addButton(7, "Slaves", campSlavesMenu, null, null, null, "Check up on any slaves you have received and interact with them.");
		//var canFap:Bool = !player.hasStatusEffect(StatusEffects.Dysfunction) && (flags[kFLAGS.UNABLE_TO_MASTURBATE_BECAUSE_CENTAUR] == 0 && !player.isTaur());
		//if (player.lust >= 30) {
			//addButton(8, "Masturbate", kGAMECLASS.masturbation.masturbateMenu);
			//if ((((player.findPerk(PerkLib.HistoryReligious) >= 0 || player.findPerk(PerkLib.HistoryReligious2) >= 0) && player.cor <= (66 + player.corruptionTolerance())) || (player.findPerk(PerkLib.Enlightened) >= 0 && player.cor < 10)) && !(player.hasStatusEffect(StatusEffects.Exgartuan) && player.statusEffectv2(StatusEffects.Exgartuan) == 0) || flags[kFLAGS.SFW_MODE] >= 1) addButton(8, "Meditate", kGAMECLASS.masturbation.masturbateMenu);
		//}
		//addButton(9, "Wait", doWait, null, null, null, "Wait for four hours.<br><br>Shift-click to wait until the night comes.");
		//if (player.fatigue > 40 || player.HP / player.maxHP() <= .9) addButton(9, "Rest", rest, null, null, null, "Rest for four hours.<br><br>Shift-click to rest until fully healed or night comes.");
		//if (model.time.hours >= 21 || model.time.hours < 6) addButton(9, "Sleep", doSleep, null, null, null, "Turn yourself in for the night.");
//
		//if (isAprilFools()) addButton(12, "Cash Shop", getGame().aprilFools.pay2WinSelection, null, null, null, "Need more gems? Want to buy special items to give you the edge? Purchase with real money!");
		//
		////Remove buttons according to conditions.
		//if (model.time.hours >= 21 || model.time.hours < 6) {
			//removeButton(0); //Explore
			//removeButton(1); //Places
			//if (model.time.hours >= 23 || model.time.hours < 5) {
				//removeButton(4); //Camp Actions
				//removeButton(5); //Followers
				//removeButton(6); //Lovers
				//removeButton(7); //Slaves
			//}
		//}
		//if (player.lust >= player.maxLust() && canFap) {
			//removeButton(0); //Explore
			//removeButton(1); //Places
		//}
		////Massive Balls Bad End (Realistic Mode only)
		//if (flags[kFLAGS.HUNGER_ENABLED] >= 1 && player.ballSize > (18 + (player.str / 2) + (player.tallness / 4))) {
			//badEndGIANTBALLZ();
			//return;
		//}
		////Hunger Bad End
		//if (flags[kFLAGS.HUNGER_ENABLED] > 0 && player.hunger <= 0)
		//{
			////Bad end at 0 HP!
			//if (player.HP <= 0 && (player.str + player.tou) < 30)
			//{
				//badEndHunger();
				//return;
			//}
		//}
		////Min Lust Bad End (Must not have any removable/temporary min lust.)
		//if (player.minLust() >= player.maxLust() && !flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= 168 && !player.eggs() >= 20 && !player.hasStatusEffect(StatusEffects.BimboChampagne) && !player.hasStatusEffect(StatusEffects.Luststick) && player.jewelryEffectId != 1)
		//{
			//badEndMinLust();
			//return;
		//}
	//}
	
	//public function hasCompanions():Bool {
		//return companionsCount() > 0;
	//}
//
	//public function companionsCount():Int {
		//return followersCount() + slavesCount() + loversCount();
	//}
//
	//public function followersCount():Int {
		//var counter:Int = 0;
		//if (emberScene.followerEmber()) counter++;
		//if (flags[kFLAGS.VALARIA_AT_CAMP] == 1 || player.armor == armors.GOOARMR) counter++;
		//if (player.hasStatusEffect(StatusEffects.PureCampJojo)) counter++;
		//if (player.hasStatusEffect(StatusEffects.CampRathazul)) counter++;
		//if (followerShouldra()) counter++;
		//if (sophieFollower() && flags[kFLAGS.FOLLOWER_AT_FARM_SOPHIE] == 0) counter++;
		//if (helspawnFollower()) counter++;
		//return counter;
	//}

	//public function slavesCount():Int {
		//var counter:Number = 0;
		//if (latexGooFollower() && flags[kFLAGS.FOLLOWER_AT_FARM_LATEXY] == 0) counter++;
		//if (vapulaSlave() && flags[kFLAGS.FOLLOWER_AT_FARM_VAPULA] == 0) counter++;
		//if (campCorruptJojo() && flags[kFLAGS.FOLLOWER_AT_FARM_JOJO] == 0) counter++;
		//if (amilyScene.amilyFollower() && amilyScene.amilyCorrupt() && flags[kFLAGS.FOLLOWER_AT_FARM_AMILY] == 0) counter++;
		////Bimbo sophie
		//if (bimboSophie() && flags[kFLAGS.FOLLOWER_AT_FARM_SOPHIE] == 0) counter++;
		//if (ceraphIsFollower()) counter++;
		//if (milkSlave() && flags[kFLAGS.FOLLOWER_AT_FARM_BATH_GIRL] == 0) counter++;
		//return counter;
	//}

	//public function loversCount():Int {
		//var counter:Number = 0;
		//if (arianScene.arianFollower()) counter++;
		//if (followerHel()) counter++;
		////Izma!
		//if (flags[kFLAGS.IZMA_FOLLOWER_STATUS] == 1 && flags[kFLAGS.FOLLOWER_AT_FARM_IZMA] == 0) counter++;
		//if (isabellaFollower() && flags[kFLAGS.FOLLOWER_AT_FARM_ISABELLA] == 0) counter++;
		//if (player.hasStatusEffect(StatusEffects.CampMarble) && flags[kFLAGS.FOLLOWER_AT_FARM_MARBLE] == 0) counter++;
		//if (amilyScene.amilyFollower() && !amilyScene.amilyCorrupt()) counter++;
		//if (followerKiha()) counter++;
		//if (flags[kFLAGS.NIEVE_STAGE] == 5) counter++;
		//if (flags[kFLAGS.ANT_WAIFU] > 0) counter++;
		//return counter;
	//}
//
	////-----------------
	////-- COMPANIONS 
	////-----------------
	//public function campLoversMenu(descOnly:Bool = false):Void {
		//if (!descOnly) {
			//hideMenus();
			//spriteSelect(null);
			//clearOutput();
			//getGame().inCombat = false;
			//menu();
		//}
		//if (isAprilFools() && flags[kFLAGS.DLC_APRIL_FOOLS] == 0 && !descOnly) {
			//getGame().aprilFools.DLCPrompt("Lovers DLC", "Get the Lovers DLC to be able to interact with them and have sex! Start families! The possibilities are endless!", "$4.99", doCamp);
			//return;
		//}
		////AMILY
		//if (amilyScene.amilyFollower() && flags[kFLAGS.AMILY_FOLLOWER] == 1 && flags[kFLAGS.AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO] == 0 && !descOnly) {
			//outputText("Amily is currently strolling around your camp, ");
			//temp = rand(6);
			//if (temp == 0) {
				//outputText("dripping water and stark naked from a bath in the stream");
				//if (player.hasStatusEffect(StatusEffects.CampRathazul)) outputText(".  Rathazul glances over and immediately gets a nosebleed");
			//}
			//else if (temp == 1) outputText("slouching in the shade of some particularly prominent rocks, whittling twigs to create darts for her blowpipe");
			//else if (temp == 2) outputText("dipping freshly-made darts into a jar of something that looks poisonous");
			//else if (temp == 3) outputText("eating some of your supplies");
			//else if (temp == 4) outputText("and she flops down on her nest to have a rest");
			//else outputText("peeling the last strips of flesh off of an imp's skull and putting it on a particularly flat, sun-lit rock to bleach as a trophy");
			//outputText(".<br><br>");
			//addButton(0, "Amily", amilyScene.amilyFollowerEncounter);
		//}
		////Amily out freaking Urta?
		//else if (flags[kFLAGS.AMILY_VISITING_URTA] == 1 || flags[kFLAGS.AMILY_VISITING_URTA] == 2) {
			//outputText("Amily's bed of grass and herbs lies empty, the mouse-woman still absent from her sojourn to meet your other lover.<br><br>");
		//}
		////Arian
		//if (arianScene.arianFollower()) {
			//outputText("Arian's tent is here, if you'd like to go inside.<br><br>");
			//addButton(1, "Arian", arianScene.visitAriansHouse);
		//}
		////Helia
		//if (kGAMECLASS.helScene.followerHel()) {
			//if (flags[kFLAGS.HEL_FOLLOWER_LEVEL] == 2) {
				////Hel @ Camp: Follower Menu
				////(6-7) 
				//if (model.time.hours <= 7) outputText("Hel is currently sitting at the edge of camp, surrounded by her scraps of armor, sword, and a few half-empty bottles of vodka.  By the way she's grunting and growling, it looks like she's getting ready to flip her shit and go running off into the plains in her berserker state.<br><br>");
				////(8a-5p) 
				//else if (model.time.hours <= 17) outputText("Hel's out of camp at the moment, adventuring on the plains.  You're sure she'd be on hand in moments if you needed her, though.<br><br>");
				////5-7) 
				//else if (model.time.hours <= 19) outputText("Hel's out visiting her family in Tel'Adre right now, though you're sure she's only moments away if you need her.<br><br>");
				////(7+)
				//else outputText("Hel is fussing around her hammock, checking her gear and sharpening her collection of blades.  Each time you glance her way, though, the salamander puts a little extra sway in her hips and her tail wags happily.<br><br>");
			//}
			//else if (flags[kFLAGS.HEL_FOLLOWER_LEVEL] == 1) {
				//if (flags[kFLAGS.HEL_HARPY_QUEEN_DEFEATED] == 1) {
					//outputText("Hel has returned to camp, though for now she looks a bit bored.  Perhaps she is waiting on something.<br><br>");
				//}
				//else {
					//outputText("<b>You see the salamander Helia pacing around camp, anxiously awaiting your departure to the harpy roost. Seeing you looking her way, she perks up, obviously ready to get underway.</b><br><br>");
				//}
			//}
			//addButton(2, "Helia", helFollower.heliaFollowerMenu);
		//}
		////Isabella
		//if (isabellaFollower() && flags[kFLAGS.FOLLOWER_AT_FARM_ISABELLA] == 0) {
			//if (model.time.hours >= 21 || model.time.hours <= 5) outputText("Isabella is sound asleep in her bunk and quietly snoring.");
			//else if (model.time.hours == 6) outputText("Isabella is busy eating some kind of grain-based snack for breakfast.  The curly-haired cow-girl gives you a smile when she sees you look her way.");
			//else if (model.time.hours == 7) outputText("Isabella, the red-headed cow-girl, is busy with a needle and thread, fixing up some of her clothes.");
			//else if (model.time.hours == 8) outputText("Isabella is busy cleaning up the camp, but when she notices you looking her way, she stretches up and arches her back, pressing eight bullet-hard nipples into the sheer silk top she prefers to wear.");
			//else if (model.time.hours == 9) outputText("Isabella is out near the fringes of your campsite.  She has her massive shield in one hand and appears to be keeping a sharp eye out for intruders or demons.  When she sees you looking her way, she gives you a wave.");
			//else if (model.time.hours == 10) outputText("The cow-girl warrioress, Isabella, is sitting down on a chair and counting out gems from a strange pouch.  She must have defeated someone or something recently.");
			//else if (model.time.hours == 11) outputText("Isabella is sipping from a bottle labelled 'Lactaid' in a shaded corner.  When she sees you looking she blushes, though dark spots appear on her top and in her skirt's middle.");
			//else if (model.time.hours == 12) outputText("Isabella is cooking a slab of meat over the fire.  From the smell that's wafting this way, you think it's beef.  Idly, you wonder if she realizes just how much like her chosen food animal she has become.");
			//else if (model.time.hours == 13) {
				//outputText("Isabella ");
				//var izzyCreeps:Array = [];
				////Build array of choices for izzy to talk to
				//if (player.hasStatusEffect(StatusEffects.CampRathazul))
					//izzyCreeps[izzyCreeps.length] = 0;
				//if (player.hasStatusEffect(StatusEffects.PureCampJojo))
					//izzyCreeps[izzyCreeps.length] = 1;
				//if (amilyScene.amilyFollower() && flags[kFLAGS.AMILY_FOLLOWER] == 1 && flags[kFLAGS.AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO] == 0)
					//izzyCreeps[izzyCreeps.length] = 2;
				//if (amilyScene.amilyFollower() && flags[kFLAGS.AMILY_FOLLOWER] == 2 && flags[kFLAGS.AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO] == 0 && flags[kFLAGS.FOLLOWER_AT_FARM_AMILY] == 0)
					//izzyCreeps[izzyCreeps.length] = 3;
				//if (flags[kFLAGS.IZMA_FOLLOWER_STATUS] == 1 && flags[kFLAGS.FOLLOWER_AT_FARM_IZMA] == 0)
					//izzyCreeps[izzyCreeps.length] = 4;
				////Base choice - book
				//izzyCreeps[izzyCreeps.length] = 5;
				////Select!
				//var choice:Int = rand(izzyCreeps.length);
					//
				//if (izzyCreeps[choice] == 0) outputText("is sitting down with Rathazul, chatting amiably about the weather.");
				//else if (izzyCreeps[choice] == 1) outputText("is sitting down with Jojo, smiling knowingly as the mouse struggles to keep his eyes on her face.");
				//else if (izzyCreeps[choice] == 2) outputText("is talking with Amily, sharing stories of the fights she's been in and the enemies she's faced down.  Amily seems interested but unimpressed.");
				//else if (izzyCreeps[choice] == 3) outputText("is sitting down chatting with Amily, but the corrupt mousette is just staring at Isabella's boobs and masturbating.  The cow-girl is pretending not to notice.");
				//else if (izzyCreeps[choice] == 4) outputText("is sitting down with Izma and recounting some stories, somewhat nervously.  Izma keeps flashing her teeth in a predatory smile.");
				//else outputText("is sitting down and thumbing through a book.");
			//}
			//else if (model.time.hours == 14) outputText("Isabella is working a grindstone and sharpening her tools.  She even hones the bottom edge of her shield into a razor-sharp cutting edge.  The cow-girl is sweating heavily, but it only makes the diaphanous silk of her top cling more alluringly to her weighty chest.");
			//else if (model.time.hours == 15) outputText("The warrior-woman, Isabella is busy constructing dummies of wood and straw, then destroying them with vicious blows from her shield.  Most of the time she finishes by decapitating them with the sharp, bottom edge of her weapon.  She flashes a smile your way when she sees you.");
			//else if (model.time.hours == 16) outputText("Isabella is sitting down with a knife, the blade flashing in the sun as wood shavings fall to the ground.  Her hands move with mechanical, practiced rhythm as she carves a few hunks of shapeless old wood into tools or art.");
			//else if (model.time.hours == 17) outputText("Isabella is sitting against one of the large rocks near the outskirts of your camp, staring across the wasteland while idly munching on what you assume to be a leg of lamb.  She seems lost in thought, though that doesn't stop her from throwing a wink and a goofy food-filled grin toward you.");
			//else if (model.time.hours == 18) outputText("The dark-skinned cow-girl, Isabella, is sprawled out on a carpet and stretching.  She seems surprisingly flexible for someone with hooves and oddly-jointed lower legs.");
			//else if (model.time.hours == 19) {
				////[(Izzy Milked Yet flag = -1)
				//if (flags[kFLAGS.ISABELLA_MILKED_YET] == -1) outputText("Isabella has just returned from a late visit to Whitney's farm, bearing a few filled bottles and a small pouch of gems.");
				//else outputText("Isabella was hidden behind a rock when you started looking for her, but as soon as you spot her in the darkness, she jumps, a guilty look flashing across her features.  She turns around and adjusts her top before looking back your way, her dusky skin even darker from a blush.  The cow-girl gives you a smile and walks back to her part of camp.  A patch of white decorates the ground where she was standing - is that milk?  Whatever it is, it's gone almost as fast as you see it, devoured by the parched, wasteland earth.");
			//}
			//else if (model.time.hours == 20) outputText("Your favorite chocolate-colored cowgirl, Isabella, is moving about, gathering all of her scattered belongings and replacing them in her personal chest.  She yawns more than once, indicating her readiness to hit the hay, but her occasional glance your way lets you know she wouldn't mind some company before bed.");
			//else outputText("Isabella looks incredibly bored right now.");
			//if (isabellaScene.totalIsabellaChildren() > 0) {
				//var babiesList:Array = [];
				//if (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_BOYS) > 0) {
					//babiesList.push((isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_BOYS) == 1 ? "a" : num2Text(isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_BOYS))) + " human son" + (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_BOYS) == 1 ? "" : "s"));
				//}
				//if (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_GIRLS) > 0) {
					//babiesList.push((isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_GIRLS) == 1 ? "a" : num2Text(isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_GIRLS))) + " human daughter" + (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_GIRLS) == 1 ? "" : "s"));
				//}
				//if (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_HERMS) > 0) {
					//babiesList.push((isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_HERMS) == 1 ? "a" : num2Text(isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_HERMS))) + " human herm" + (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_HUMAN_HERMS) == 1 ? "" : "s"));
				//}
				//if (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWGIRLS) > 0) {
					//babiesList.push((isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWGIRLS) == 1 ? "a" : num2Text(isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWGIRLS))) + " cow girl" + (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWGIRLS) == 1 ? "" : "s"));
				//}
				//if (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWFUTAS) > 0) {
					//babiesList.push((isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWFUTAS) == 1 ? "a" : num2Text(isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWFUTAS))) + " cow herm" + (isabellaScene.getIsabellaChildType(IsabellaScene.OFFSPRING_COWFUTAS) == 1 ? "" : "s"));
				//}
				//outputText("  Isabella has set up a small part of her \"corner\" in the camp as a nursery. She has sawn a " + (Math.ceil(isabellaScene.totalIsabellaChildren() / 2) == 1 ? "barrel" : "number of barrels") + " in half and lined " + (Math.ceil(isabellaScene.totalIsabellaChildren() / 2) == 1 ? "it" : "them") + " with blankets and pillows to serve as rocking cribs. "); 
				//outputText("You have " + formatStringArray(babiesList) + " with her, all living here; unlike native Marethians, they will need years and years of care before they can go out into the world on their own.");
			//}
			//outputText("<br><br>");
			//addButton(3, "Isabella", isabellaFollowerScene.callForFollowerIsabella);
		//}
		////Izma
		//if (izmaFollower() && flags[kFLAGS.FOLLOWER_AT_FARM_IZMA] == 0) {
			//if (flags[kFLAGS.IZMA_BROFIED] > 0) {
				//if (rand(6) == 0 && camp.vapulaSlave() && flags[kFLAGS.VAPULA_HAREM_FUCK] > 0) outputText("Izmael is standing a short distance away with an expression of unadulterated joy on his face, Vapula knelt in front of him and fellating him with noisy enthusiasm.  The shark morph dreamily opens his eyes to catch you staring, and proceeds to give you a huge grin and two solid thumbs up.");
				//else if (model.time.hours >= 6 && model.time.hours <= 12) outputText("You keep hearing the sound of objects hitting water followed by peals of male laughter coming from the stream. It sounds as if Izmael is throwing large rocks into the stream and finding immense gratification from the results. In fact, you’re pretty sure that’s exactly what he’s doing.");
				//else if (model.time.hours <= 16) outputText("Izmael is a short distance away doing squat thrusts, his body working like a piston, gleaming with sweat. He keeps bobbing his head up to see if anybody is watching him.");
				//else if (model.time.hours <= 19) outputText("Izmael is sat against his book chest, masturbating furiously without a care in the world. Eyes closed, both hands pumping his immense shaft, there is an expression of pure, childish joy on his face.");
				//else if (model.time.hours <= 22) outputText("Izmael has built a fire and is flopped down next to it. You can’t help but notice that he’s used several of his books for kindling. His eyes are locked on the flames, mesmerized by the dancing light and heat.");
				//else outputText("Izmael is currently on his bedroll, sleeping for the night.");
				//outputText("<br><br>");
				//addButton(4, "Izmael", izmaScene.izmaelScene.izmaelMenu);
			//}
			//else {
				//outputText("Neatly laid near the base of your own is a worn bedroll belonging to Izma, your tigershark lover. It's a snug fit for her toned body, though it has some noticeable cuts and tears in the fabric. Close to her bed is her old trunk, almost as if she wants to have it at arms length if anyone tries to rob her in her sleep.<br><br>");
				//switch(rand(3)) {
					//case 0: outputText("Izma's lazily sitting on the trunk beside her bedroll, reading one of the many books from inside it. She smiles happily when your eyes linger on her, and you know full well she's only half-interested in it."); break;
					//case 1: outputText("You notice Izma isn't around right now. She's probably gone off to the nearby stream to get some water. Never mind, she comes around from behind a rock, still dripping wet."); break;
					//case 2: outputText("Izma is lying on her back near her bedroll. You wonder at first just why she isn't using her bed, but as you look closer you notice all the water pooled beneath her and the few droplets running down her arm, evidence that she's just returned from the stream."); break;
				//}
				//outputText("<br><br>");
				//addButton(4, "Izma", izmaScene.izmaFollowerMenu);
			//}
//
		//}
		////Kiha!
		//if (followerKiha()) {
			////(6-7) 
			//if (model.time.hours < 7) outputText("Kiha is sitting near the fire, her axe laying across her knees as she polishes it.<br><br>");
			//else if (model.time.hours < 19) {
				//if (kihaFollower.totalKihaChildren() > 0 && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] > 160 && (flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] % 3 == 0 || model.time.hours == 17)) outputText("Kiha is breastfeeding her offspring right now.<br><br>");
				//else if (kihaFollower.totalKihaChildren() > 0 && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] > 80 && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] <= 160 && (flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] % 7 == 0 || model.time.hours == 17)) outputText("Kiha is telling stories to her draconic child" + (kihaFollower.totalKihaChildren() == 1 ? "" : "ren") + " right now.<br><br>");
				//else outputText("Kiha's out right now, likely patrolling for demons to exterminate.  You're sure a loud call could get her attention.<br><br>");
			//}
			//else {
				//if (kihaFollower.totalKihaChildren() > 0 && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] > 160 && (flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] % 3 == 0 || model.time.hours == 20)) outputText("Kiha is breastfeeding her offspring right now.<br><br>");
				//else if (kihaFollower.totalKihaChildren() > 0 && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] > 80 && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] <= 160 && (flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] % 7 == 0 || model.time.hours == 20)) outputText("Kiha is telling stories to her draconic child" + (kihaFollower.totalKihaChildren() == 1 ? "" : "ren") + " right now.<br><br>");
				//else if (kihaFollower.totalKihaChildren() > 0 && flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] <= 80 && (flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] % 3 == 0 || model.time.hours == 20)) {
					//outputText("Kiha is training her " + (kihaFollower.totalKihaChildren() == 1 ? "child to become a strong warrior" : "children to become strong warriors") + ". ");
					//if (rand(2) == 0) outputText("Right now, she's teaching various techniques.");
					//else outputText("Right now, she's teaching her child" + (kihaFollower.totalKihaChildren() == 1 ? "" : "ren") + " how to make use of axes.<br><br>");
				//}
				//else {
					//outputText("Kiha is utterly decimating a set of practice dummies she's set up out on the edge of camp.  All of them have crudely drawn horns. ");
					//if (kihaFollower.totalKihaChildren() > 0 && (kihaFollower.totalKihaChildren() >= 2 || flags[kFLAGS.KIHA_CHILD_MATURITY_COUNTER] <= 60)) outputText("Some of them are saved for her child" + (kihaFollower.totalKihaChildren() == 1 ? "" : "ren") + " to train on. ");
					//outputText("Most of them are on fire.<br><br>");
				//}
			//}
			//addButton(5, "Kiha", kihaScene.encounterKiha);
		//}
		////MARBLE
		//if (player.hasStatusEffect(StatusEffects.CampMarble) && flags[kFLAGS.FOLLOWER_AT_FARM_MARBLE] == 0) {
			//temp = rand(5);
			//outputText("A second bedroll rests next to yours; a large two handed hammer sometimes rests against it, depending on whether or not its owner needs it at the time.  ");
			////Normal Murbles
			//if (flags[kFLAGS.MARBLE_PURIFICATION_STAGE] == 4) outputText("Marble isn’t here right now; she’s still off to see her family.");
			////requires at least 1 kid, time is just before sunset, this scene always happens at this time if the PC has at least one kid.
			//else if (flags[kFLAGS.MARBLE_KIDS] >= 1 && (model.time.hours == 19 || model.time.hours == 20)) {
				//outputText("Marble herself is currently in the nursery, putting your ");
				//if (flags[kFLAGS.MARBLE_KIDS] == 1) outputText("child");
				//else outputText("children");
				//outputText(" to bed.");
			//}
			////at 6-7 in the morning, scene always displays at this time
			//else if (model.time.hours == 6 || model.time.hours == 7) outputText("Marble is off in an open area to the side of your camp right now.  She is practicing with her large hammer, going through her daily training.");
			////after nightfall, scene always displays at this time unless PC is wormed
			//else if (model.time.hours >= 21 && !player.hasStatusEffect(StatusEffects.Infested)) {
				//outputText("Marble is hanging around her bedroll waiting for you to come to bed.  However, sometimes she lies down for a bit, and sometimes she paces next to it.");
				//if (flags[kFLAGS.MARBLE_LUST] > 30) outputText("  She seems to be feeling antsy.");
			//}
			//else if (flags[kFLAGS.MARBLE_KIDS] > 0 && model.time.hours < 19 && model.time.hours > 7) {
				////requires at least 6 kids, and no other parental characters in camp
				//if (rand(2) == 0 && flags[kFLAGS.MARBLE_KIDS] > 5) outputText("Marble is currently tending to your kids, but she looks a bit stressed out right now.  It looks like " + num2Text(flags[kFLAGS.MARBLE_KIDS]) + " might just be too many for her to handle on her own...");
				////requires at least 4 kids
				//else if (rand(3) == 0 && flags[kFLAGS.MARBLE_KIDS] > 3) outputText("Marble herself is in the camp right now, telling a story about her travels around the world to her kids as they gather around her.  The children are completely enthralled by her words.  You can't help but smile.");
				////Requires 2 boys
				//else if (rand(3) == 0 && flags[kFLAGS.MARBLE_BOYS] > 1)
				//{
					//outputText("Marble herself is currently refereeing a wrestling match between two of your sons.  It seems like it's a contest to see which one of them gets to go for a ride between her breasts in a game of <i>Bull Blasters</i>, while the loser has to sit on her shoulders.");
				//}
				////requires at least 2 kids
				//else if (rand(3) == 0 && flags[kFLAGS.MARBLE_KIDS] - flags[kFLAGS.MARBLE_BOYS] > 1) outputText("Marble herself is involved in a play fight with two of your kids brandishing small sticks.  It seems that the <i>mommy monster</i> is terrorising the camp and needs to be stopped by the <i>Mighty Moo and her sidekick Bovine Lass</i>.");
				//else if (rand(3) == 0 && flags[kFLAGS.MARBLE_KIDS] > 1) outputText("Marble herself is out right now; she's taken her kids to go visit Whitney.  You're sure though that she'll be back within the hour, so you could just wait if you needed her.");
				//else {
					////requires at least 1 kid
					//if (rand(2) == 0) 
					//{
						//outputText("Marble herself is nursing ");
						//if (flags[kFLAGS.MARBLE_KIDS] > 1) outputText("one of your cow-girl children");
						//else outputText("your cow-girl child");
						//outputText(" with a content look on her face.");
					//}
					//else 
					//{
						//outputText("Marble herself is watching your kid");
						//if (flags[kFLAGS.MARBLE_KIDS] > 0) outputText("s");
						//outputText(" playing around the camp right now.");
					//}
				//}
			//}
			////(Choose one of these at random to display each hour)
			//else if (temp == 0) outputText("Marble herself has gone off to Whitney's farm to get milked right now.");
			//else if (temp == 1) outputText("Marble herself has gone off to Whitney's farm to do some chores right now.");
			//else if (temp == 2) outputText("Marble herself isn't at the camp right now; she is probably off getting supplies, though she'll be back soon enough.");
			//else if (temp == 3) {
				//outputText("Marble herself is resting on her bedroll right now.");
			//}
			//else if (temp == 4) {
				//outputText("Marble herself is wandering around the camp right now.");
			//}
			//if (temp < 3) {
				//outputText("  You're sure she'd be back in moments if you needed her.");
			//}
			////Out getting family
			////else outputText("Marble is out in the wilderness right now, searching for a relative.");
			//outputText("<br><br>");
			//if (flags[kFLAGS.MARBLE_PURIFICATION_STAGE] != 4) addButton(6, "Marble", marbleScene.interactWithMarbleAtCamp, null, null, null, "Go to Marble the cowgirl for talk and companionship.");
		//}
		////Nieve
		//if (flags[kFLAGS.NIEVE_STAGE] == 5) {
			//kGAMECLASS.xmas.xmasMisc.nieveCampDescs();
			//outputText("<br><br>");
			//addButton(7, "Nieve", getGame().xmas.xmasMisc.approachNieve);
		//}
		////Phylla
		//if (flags[kFLAGS.ANT_WAIFU] > 0) {
			//outputText("You see Phylla's anthill in the distance.  Every now and then you see");
			////If PC has children w/ Phylla:
			//if (flags[kFLAGS.ANT_KIDS] > 0 && flags[kFLAGS.ANT_KIDS] <= 250) outputText(" one of your children exit the anthill to unload some dirt before continuing back down into the colony.  It makes you feel good knowing your offspring are so productive.");
			//if (flags[kFLAGS.ANT_KIDS] > 250 && flags[kFLAGS.ANT_KIDS] <= 1000) outputText(" few of your many children exit the anthill to unload some dirt before vanishing back inside.  It makes you feel good knowing your offspring are so productive.");
			//if (flags[kFLAGS.ANT_KIDS] > 1000) outputText(" some of your children exit the anthill using main or one of the additionally entrances to unload some dirt. Some of them instead of unloading dirt coming out to fulfill some other task that their mother gave them.  You feel a little nostalgic seeing how this former small colony grown to such a magnificent size.");
			//else outputText(" Phylla appears out of the anthill to unload some dirt.  She looks over to your campsite and gives you an excited wave before heading back into the colony.  It makes you feel good to know she's so close.");
			//outputText("<br><br>");
			//addButton(8,"Phylla", getGame().desert.antsScene.introductionToPhyllaFollower);
		//}
		//addButton(14, "Back", playerMenu);
	//}

//public function campSlavesMenu(descOnly:Bool = false):Void {
	//if (!descOnly) {
		//hideMenus();
		//spriteSelect(null);
		//clearOutput();
		//getGame().inCombat = false;
		//menu();
	//}
	//if (isAprilFools() && flags[kFLAGS.DLC_APRIL_FOOLS] == 0 && !descOnly) {
		//getGame().aprilFools.DLCPrompt("Slaves DLC", "Get the Slaves DLC to be able to interact with them. Show them that you're dominating!", "$4.99", doCamp);
		//return;
	//}
	//if (latexGooFollower() && flags[kFLAGS.FOLLOWER_AT_FARM_LATEXY] == 0) {
		//outputText(flags[kFLAGS.GOO_NAME] + " lurks in a secluded section of rocks, only venturing out when called for or when she needs to gather water from the stream.<br><br>");
		//addButton(0, flags[kFLAGS.GOO_NAME], latexGirl.approachLatexy);
	//}
	//if (milkSlave() && flags[kFLAGS.FOLLOWER_AT_FARM_BATH_GIRL] == 0) {
		//outputText("Your well-endowed, dark-skinned milk-girl is here.  She flicks hopeful eyes towards you whenever she thinks she has your attention.<br><br>");
		//addButton(1, flags[kFLAGS.MILK_NAME], milkWaifu.milkyMenu);
	//}
	////Ceraph
	//if (ceraphIsFollower()) {
		//addButton(5, "Ceraph", ceraphFollowerScene.ceraphFollowerEncounter);
	//}
	////Vapula
	//if (vapulaSlave() && flags[kFLAGS.FOLLOWER_AT_FARM_VAPULA] == 0) {
		//vapula.vapulaSlaveFlavorText();
		//outputText("<br><br>");
		//addButton(6, "Vapula", vapula.callSlaveVapula);
	//}
	////Modified Camp/Follower List Description:
	//if (amilyScene.amilyFollower() && flags[kFLAGS.AMILY_FOLLOWER] == 2 && flags[kFLAGS.AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO] == 0 && flags[kFLAGS.FOLLOWER_AT_FARM_AMILY] == 0) {
		//outputText("Sometimes you hear a faint moan from not too far away. No doubt the result of your slutty toy mouse playing with herself.<br><br>");
		//addButton(10, "Amily", amilyScene.amilyFollowerEncounter);
	//}
	////JOJO
	////If Jojo is corrupted, add him to the masturbate menu.
	//if (campCorruptJojo() && flags[kFLAGS.FOLLOWER_AT_FARM_JOJO] == 0) {
		//outputText("From time to time you can hear movement from around your camp, and you routinely find thick puddles of mouse semen.  You are sure Jojo is here if you ever need to sate yourself.<br><br>");
		//addButton(11, "Jojo", jojoScene.corruptCampJojo, null, null, null, "Call your corrupted pet into camp in order to relieve your desires in a variety of sexual positions?  He's ever so willing after your last encounter with him.");
	//}
	////Bimbo Sophie
	//if (bimboSophie() && flags[kFLAGS.FOLLOWER_AT_FARM_SOPHIE] == 0) {
		//sophieBimbo.sophieCampLines();
		//addButton(12, "Sophie", sophieBimbo.approachBimboSophieInCamp);
	//}
	//addButton(14, "Back", playerMenu);
//}
//
//public function campFollowers(descOnly:Bool = false):Void {
	//if (!descOnly) {
		//hideMenus();
		//spriteSelect(null);
		//clearOutput();
		//getGame().inCombat = false;
		////ADD MENU FLAGS/INDIVIDUAL FOLLOWER TEXTS
		//menu();
	//}
	////Ember
	//if (emberScene.followerEmber()) {
		//emberScene.emberCampDesc();
		//addButton(0, "Ember", emberScene.emberCampMenu, null, null, null, "Check up on Ember the dragon-" + (flags[kFLAGS.EMBER_ROUNDFACE] == 0 ? "morph" : flags[kFLAGS.EMBER_GENDER] == 1 ? "boy" : "girl" ) + "");
	//}
	////Sophie
	//if (sophieFollower() && flags[kFLAGS.FOLLOWER_AT_FARM_SOPHIE] == 0) {
		//if (rand(5) == 0) outputText("Sophie is sitting by herself, applying yet another layer of glittering lip gloss to her full lips.<br><br>");
		//else if (rand(4) == 0) outputText("Sophie is sitting in her nest, idly brushing out her feathers.  Occasionally, she looks up from her work to give you a sultry wink and a come-hither gaze.<br><br>");
		//else if (rand(3) == 0) outputText("Sophie is fussing around in her nest, straightening bits of straw and grass, trying to make it more comfortable.  After a few minutes, she flops down in the middle and reclines, apparently satisfied for the moment.<br><br>");
		//else if (rand(2) == 0 || flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] == 0) {
			//if (flags[kFLAGS.SOPHIE_BIMBO] > 0) outputText("Your platinum-blonde harpy, Sophie, is currently reading a book - a marked change from her bimbo-era behavior.  Occasionally, though, she glances up from the page and gives you a lusty look.  Some things never change....<br><br>");
			//else outputText("Your pink harpy, Sophie, is currently reading a book.  She seems utterly absorbed in it, though you question how she obtained it.  Occasionally, though, she'll glance up from the pages to shoot you a lusty look.<br><br>");
		//}
		//else {
			//outputText("Sophie is sitting in her nest, ");
			//if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] < 5) {
				//outputText("across from your daughter");
				//if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] > 1) outputText("s");
			//}
			//else outputText("surrounded by your daughters");
			//outputText(", apparently trying to teach ");
			//if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT] == 1) outputText("her");
			//else outputText("them");
			//outputText(" about hunting and gathering techniques.  Considering their unusual upbringing, it can't be as easy for them...<br><br>");
		//}
		//addButton(1, "Sophie", sophieFollowerScene.followerSophieMainScreen, null, null, null, "Check up on Sophie the harpy.");
	//}
	////Pure Jojo
	//if (player.hasStatusEffect(StatusEffects.PureCampJojo)) {
		//if (flags[kFLAGS.JOJO_BIMBO_STATE] >= 3) {
			//outputText("Joy's tent is set up in a quiet corner of the camp, close to a boulder. Inside the tent, you can see a chest holding her belongings, as well as a few clothes and books spread about her bedroll. ");
			//if (flags[kFLAGS.JOJO_LITTERS] > 0 && model.time.hours >= 16 && model.time.hours < 19) outputText("You spot the little mice you had with Joy playing about close to her tent.");
			//else outputText("Joy herself is nowhere to be found, she's probably out frolicking about or sitting atop the boulder.");
			//outputText("<br><br>");
			//addButton(2, "Joy", joyScene.approachCampJoy, null, null, null, "Go find Joy around the edges of your camp and meditate with her or have sex with her.");
		//}
		//else {
			//outputText("There is a small bedroll for Jojo near your own");
			//if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0) outputText(" cabin");
			//if (!(model.time.hours > 4 && model.time.hours < 23)) outputText(" and the mouse is sleeping on it right now.<br><br>");
			//else outputText(", though the mouse is probably hanging around the camp's perimeter.<br><br>");
			//addButton(2, "Jojo", jojoScene.jojoCamp, null, null, null, "Go find Jojo around the edges of your camp and meditate with him or talk about watch duty.");
		//}
	//}
	////Helspawn
	//if (helspawnFollower()) {
		//addButton(3, flags[kFLAGS.HELSPAWN_NAME], helSpawnScene.helspawnsMainMenu);
	//}
	////RATHAZUL
	////if rathazul has joined the camp
	//if (player.hasStatusEffect(StatusEffects.CampRathazul)) {
		//if (flags[kFLAGS.RATHAZUL_SILK_ARMOR_COUNTDOWN] <= 1) {
			//outputText("Tucked into a shaded corner of the rocks is a bevy of alchemical devices and equipment.  ");
			//if (!(model.time.hours > 4 && model.time.hours < 23)) outputText("The alchemist is absent from his usual work location. He must be sleeping right now.");
			//else outputText("The alchemist Rathazul looks to be hard at work with his chemicals, working on who knows what.");
			//if (flags[kFLAGS.RATHAZUL_SILK_ARMOR_COUNTDOWN] == 1) outputText("  Some kind of spider-silk-based equipment is hanging from a nearby rack.  <b>He's finished with the task you gave him!</b>");
			//outputText("<br><br>");
		//}
		//else outputText("Tucked into a shaded corner of the rocks is a bevy of alchemical devices and equipment.  The alchemist Rathazul looks to be hard at work on the silken equipment you've commissioned him to craft.<br><br>");
		//addButton(4, "Rathazul", kGAMECLASS.rathazul.returnToRathazulMenu, null, null, null, "Visit with Rathazul to see what alchemical supplies and services he has available at the moment.");
	//}
	//else
	//{
		//if (flags[kFLAGS.RATHAZUL_SILK_ARMOR_COUNTDOWN] == 1)
		//{
			//outputText("There is a note on your ");
			//if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0) outputText("bed inside your cabin.");
			//else outputText("bedroll");
			//outputText(". It reads: \"<i>Come see me at the lake. I've finished your spider-silk ");
			//switch(flags[kFLAGS.RATHAZUL_SILK_ARMOR_TYPE]) {
				//case 1: 
					//outputText("armor");
					//break;
				//case 2: 
					//outputText("robes");
					//break;
				//case 3: 
					//outputText("bra");
					//break;
				//case 4: 
					//outputText("panties");
					//break;
				//case 5: 
					//outputText("loincloth");
					//break;
				//default: outputText("robes");
			//}
			//outputText(". -Rathazul</i>\".<br><br>");
		//}
	//}
	////Shouldra
	//if (followerShouldra()) {
		//addButton(5, "Shouldra", shouldraFollower.shouldraFollowerScreen, null, null, null, "Talk to Shouldra. She is currently residing in your body.");
	//}
	////Valeria
	//if (flags[kFLAGS.VALARIA_AT_CAMP] == 1) {
		//addButton(6, "Valeria", valeria.valeriaFollower, null, null, null, "Visit Valeria the goo-girl. You can even take and wear her as goo armor if you like.");
	//}
	//if (player.armor == armors.GOOARMR) {
		//addButtonDisabled(6, "Valeria", "You are currently wearing Valeria. Unequip from your Inventory menu if you want to interact with her.");
	//}
	//addButton(14,"Back",playerMenu);
//}
//
////-----------------
////-- CAMP ACTIONS 
////-----------------
//private function campActions():Void {
	//hideMenus();
	//menu();
	//clearOutput();
	//outputText("What would you like to do?");
	//addButton(0, "SwimInStream", swimInStream, null, null, null, "Swim in stream and relax to pass time.", "Swim In Stream");
	//addButton(1, "ExaminePortal", examinePortal, null, null, null, "Examine the portal. This scene is placeholder.", "Examine Portal"); //Examine portal.
	//if (model.time.hours == 19) {
		//addButton(2, "Watch Sunset", watchSunset, null, null, null, "Watch the sunset and relax."); //Relax and watch at the sunset.
	//}
	//else if (model.time.hours >= 20 ) {
		//if (flags[kFLAGS.LETHICE_DEFEATED] > 0) addButton(2, "Stargaze", watchStars, null, null, null, "Look at the starry night sky."); //Stargaze. Only available after Lethice is defeated.
		//if (flags[kFLAGS.MANOR_PROGRESS] & 1024) addButton(8, "Void", enterVoid, null, null, null, "Observe the infinite expanse of the Cosmos and accept realities you once ignored.");
	//}
	//else {
		//addButtonDisabled(2, "Watch Sky", "The option to watch sunset is available at 7pm.");
	//}
	//if (flags[kFLAGS.CAMP_CABIN_PROGRESS] > 0 && flags[kFLAGS.CAMP_CABIN_PROGRESS] < 10) addButton(3, "Build Cabin", cabinProgress.initiateCabin, null, null, null, "Work on your cabin."); //Work on cabin.
	//if (flags[kFLAGS.CAMP_CABIN_PROGRESS] >= 10 || flags[kFLAGS.CAMP_BUILT_CABIN] >= 1) addButton(3, "Enter Cabin", cabinProgress.initiateCabin, null, null, null, "Enter your cabin."); //Enter cabin for furnish.
	//addButton(4, "Read Codex", codex.accessCodexMenu, null, null, null, "Read any codex entries you have unlocked.");
	//if (player.hasKeyItem("Carpenter's Toolbox") >= 0 && flags[kFLAGS.CAMP_WALL_PROGRESS] < 100 && getCampPopulation() >= 4) addButton(5, "Build Wall", buildCampWallPrompt, null, null, null, "Build a wall around your camp to defend from the imps." + (flags[kFLAGS.CAMP_WALL_PROGRESS] >= 20 ? "<br><br>Progress: " + (flags[kFLAGS.CAMP_WALL_PROGRESS]/20) + "/5 complete": "") + "");
	//if (player.hasKeyItem("Carpenter's Toolbox") >= 0 && flags[kFLAGS.CAMP_WALL_PROGRESS] >= 100 && flags[kFLAGS.CAMP_WALL_GATE] <= 0) addButton(5, "Build Gate", buildCampGatePrompt, null, null, null, "Build a gate to complete your camp defense.");
	//if (flags[kFLAGS.CAMP_WALL_PROGRESS] >= 100 && player.hasItem(useables.IMPSKLL, 1)) addButton(6, "AddImpSkull", promptHangImpSkull, null, null, null, "Add an imp skull to decorate the wall and to serve as deterrent for imps.", "Add Imp Skull");
	//if (flags[kFLAGS.LETHICE_DEFEATED] > 0) addButton(7, "Ascension", promptAscend, null, null, null, "Perform an ascension? This will restart your adventures with your levels, items, and gems carried over. The game will also get harder.");
	//addButton(14, "Back", playerMenu);
//}
//
//public function enterVoid():Void{
	//clearOutput();
	//outputText("You stare at the sky, the abyss that covers the entirety of Mareth.");
	//outputText("<br><br>You are warped across time and space once again.");
	//inDungeon = true;
	//kGAMECLASS.dungeonLoc = DungeonCore.DUNGEON_MANOR_INFINITY4;
	//doNext(playerMenu);
//}
//
	//private function swimInStream():Void {
		//clearOutput();
		//outputText("You ponder over the nearby stream that's flowing. Deciding you'd like a dip, ");
		//if (player.armorName == "slutty swimwear") outputText("you are going to swim while wearing just your swimwear. ");
		//else outputText("you strip off your [armor] until you are completely naked. ");
		//outputText("You step into the flowing waters. You shiver at first but you step in deeper. Incredibly, it's not too deep. ");
		//if (player.tallness < 60) outputText("Your feet aren't even touching the riverbed. ");
		//if (player.tallness >= 60 && player.tallness < 72) outputText("Your feet are touching the riverbed and your head is barely above the water. ");
		//if (player.tallness >= 72) outputText("Your feet are touching touching the riverbed and your head is above water. You bend down a bit so you're at the right height. ");
		//outputText("<br><br>You begin to swim around and relax. ");
		////Izma!
		//if (rand(2) == 0 && camp.izmaFollower())
		//{
			//outputText("<br><br>Your tiger-shark beta, Izma, joins you. You are frightened at first when you saw the fin protruding from the water and the fin approaches you! ");
			//outputText("As the fin approaches you, the familiar figure comes up. \"<i>I was going to enjoy my daily swim, alpha,</i>\" she says.");
		//}
		////Helia!
		//if (rand(2) == 0 && camp.followerHel() && flags[kFLAGS.HEL_CAN_SWIM])
		//{
			//outputText("<br><br>Helia, your salamander lover, joins in for a swim. \"<i>Hey, lover mine!</i>\" she says. As she enters the waters, the water seems to become warmer until it begins to steam like a sauna.");
		//}
		////Marble!
		//if (rand(2) == 0 && camp.marbleFollower() && flags[kFLAGS.MARBLE_PURIFICATION_STAGE] != 4)
		//{
			//outputText("<br><br>Your cow-girl lover Marble strips herself naked and joins you. \"<i>Sweetie, you enjoy swimming, don't you?</i>\" she says.");
		//}
		////Amily! (Must not be corrupted and must have given Slutty Swimwear.)
		//if (rand(2) == 0 && camp.amilyFollower() && flags[kFLAGS.AMILY_FOLLOWER] == 1 && flags[kFLAGS.AMILY_OWNS_BIKINI] > 0)
		//{
			//outputText("<br><br>Your mouse-girl lover Amily is standing at the riverbank. She looks flattering in her bikini");
			//if (flags[kFLAGS.AMILY_WANG_LENGTH] > 0) outputText(", especially when her penis is exposed");
			//outputText(". She walks into the waters and swims.  ");
		//}
		////Ember 
		//if (rand(4) == 0 && camp.followerEmber())
		//{
			//outputText("<br><br>You catch a glimpse of Ember taking a daily bath.");
		//}
		////Rathazul (RARE)
		//if (rand(10) == 0 && player.hasStatusEffect(StatusEffects.CampRathazul))
		//{
			//outputText("<br><br>You spot Rathazul walking into the shallow section of stream, most likely taking a bath to get rid of the smell.");
		//}
		//else doNext(swimInStreamFinish);
		//
	//}
//
//private function swimInStreamFinish():Void {
	//clearOutput();
	////Blown up factory? Corruption gains.
	//if (flags[kFLAGS.FACTORY_SHUTDOWN] == 2 && player.cor < 50) 
	//{
		//outputText("You feel a bit dirtier after swimming in the tainted waters. <br><br>");
		//dynStats("cor", 0.5);
		//dynStats("lust", 15, "resisted", true);
	//}
	//outputText("Eventually, you swim back to the riverbank and dry yourself off");
	//if (player.armorName != "slutty swimwear") outputText(" before you re-dress yourself in your " + player.armorName);
	//outputText(".");
	//doNext(camp.returnToCampUseOneHour);
//}
//
//private function examinePortal():Void {
	//if (flags[kFLAGS.CAMP_PORTAL_PROGRESS] <= 0) {
		//clearOutput();
		//outputText("You walk over to the portal, reminded by how and why you came. You wonder if you can go back to Ingnam. You start by picking up a small pebble and throw it through the portal. It passes through the portal. As you walk around the portal, you spot the pebble at the other side. Seems like you can't get back right now.");
		//flags[kFLAGS.CAMP_PORTAL_PROGRESS] = 1;
		//doNext(camp.returnToCampUseOneHour);
		//return;
	//}
	//else {
		//clearOutput();
		//outputText("You walk over to the portal, reminded by how and why you came. You let out a sigh, knowing you can't return to Ingnam.");
	//}
	//doNext(playerMenu);
//}
//
//private function watchSunset():Void {
	//clearOutput();
	//outputText("You pick a location where the sun is clearly visible from that particular spot and sit down. The sun is just above the horizon, ready to set. It's such a beautiful view. <br><br>");
	//var randText:Number = rand(3);
	////Childhood nostalgia GO!
	//if (randText == 0)
	//{
		//if (player.cor < 33) 
		//{
			//outputText("A wave of nostalgia washes over you as you remember your greatest moments from your childhood.");
			//dynStats("cor", -1, "lib", -1, "lust", -30, "resisted", false);
		//}
		//if (player.cor >= 33 && player.cor < 66) 
		//{
			//outputText("A wave of nostalgia washes over you as you remember your greatest moments from your childhood. Suddenly, your memories are somewhat twisted from some of the perverted moments. You shake your head and just relax.");
			//dynStats("cor", -0.5, "lib", -1, "lust", -20, "resisted", false);
		//}
		//if (player.cor >= 66) 
		//{
			//outputText("A wave of nostalgia washes over you as you remember your greatest moments from your childhood. Suddenly, your memories twist into some of the dark and perverted moments. You chuckle at that moment but you shake your head and focus on relaxing.");
			//dynStats("cor", 0, "lib", -1, "lust", -10, "resisted", false);
		//}
	//}
	////Greatest moments GO!
	//if (randText == 1)
	//{
		//if (player.cor < 33) 
		//{
			//outputText("You reflect back on your greatest adventures and how curiosity got the best of you. You remember some of the greatest places you discovered.");
			//dynStats("lust", -30, "resisted", false);
		//}
		//if (player.cor >= 33 && player.cor < 66) 
		//{
			//outputText("You reflect back on your greatest adventures. Of course, some of them involved fucking and getting fucked by the denizens of Mareth. You suddenly open your eyes from the memory and just relax, wondering why you thought of that in the first place.");
			//dynStats("lust", -20, "resisted", false);
		//}
		//if (player.cor >= 66) 
		//{
			//outputText("You reflect back on your greatest adventures. You chuckle at the moments you were dominating and the moments you were submitting. You suddenly open your eyes from the memory and just relax.");
			//dynStats("lust", -10, "resisted", false);
		//}
	//}
	////Greatest moments GO!
	//if (randText >= 2)
	//{
		//outputText("You think of what you'd like to ");
		//if (rand(2) == 0) outputText("do");
		//else outputText("accomplish");
		//outputText(" before you went through the portal. You felt a bit sad that you didn't get to achieve your old goals.");
		//dynStats("lust", -30, "resisted", false);
//
	//}
	//outputText("<br><br>After the thought, you spend a good while relaxing and watching the sun setting. By now, the sun has already set below the horizon. The sky is glowing orange after the sunset. It looks like you could explore more for a while.");
	//doNext(camp.returnToCampUseOneHour);
//}
//
//private function watchStars():Void {
	//clearOutput();
	//outputText("You pick a location not far from your " + homeDesc() + " and lay on the ground, looking up at the starry night sky.");
	//outputText("<br><br>Ever since the fall of Lethice, the stars are visible.");
	//outputText("<br><br>You relax and point at various constellations.");
	//var consellationChoice:Int = rand(4);
	//switch(consellationChoice) {
		//case 0:
			//outputText("<br><br>One of them even appears to be phallic. You blush at the arrangement.");
			//break;
		//case 1:
			//outputText("<br><br>One of them even appears to be arranged like breasts. You blush at the arrangement.");
			//break;
		//case 2:
			//outputText("<br><br>One of the constellations have the stars arranged to form the shape of a centaur. Interesting.");
			//break;
		//case 3:
			//outputText("<br><br>Ah, the familiar Big Dipper. Wait a minute... you remember that constellation back in Ingnam. You swear the star arrangements are nearly the same.");
			//break;
		//default:
			//outputText("<br><br>Somehow, one of them spells out \"ERROR\". Maybe you should let Kitteh6660 know?");
	//}
	//outputText("<br><br>You let your mind wander and relax.");
	//dynStats("lus", -15, "resisted", false);
	//doNext(camp.returnToCampUseOneHour);
//}
//
//
////-----------------
////-- REST
////-----------------
//public function rest():Void {
	//campQ = true;
	//clearOutput();
	////Fatigue recovery
	//var multiplier:Number = 1.0;
	//var fatRecovery:Number = 4;
	//var hpRecovery:Number = 10;
	//
	//if (player.findPerk(PerkLib.Medicine) >= 0) hpRecovery *= 1.5;
	//
	//if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && !ingnam.inIngnam)
		//multiplier += 0.5;
	////Marble withdrawal
	//if (player.hasStatusEffect(StatusEffects.MarbleWithdrawl))
		//multiplier /= 2;
	////Hungry
	//if (flags[kFLAGS.HUNGER_ENABLED] > 0 && player.hunger < 25)
		//multiplier /= 2;
		//
	//if (timeQ == 0) {
		//var hpBefore:Int = player.HP;
		//if (flags[kFLAGS.SHIFT_KEY_DOWN] > 0) { //Rest until fully healed, midnight or hunger wake.
			//while (player.HP < player.maxHP() || player.fatigue > 0) {
				//timeQ += 1;
				//HPChange(hpRecovery * multiplier, false); // no display since it is meant to be full rest anyway
				//player.changeFatigue( -fatRecovery * multiplier); 
				//if (timeQ + model.time.hours == 24 || flags[kFLAGS.HUNGER_ENABLED] > 0 && player.hunger < 5)
					//break;
			//}	
			//if (timeQ == 0) timeQ = 1;
			//if (timeQ > 21 - model.time.hours) timeQ = 21 - model.time.hours;
		//} else {
			//timeQ = Math.min(4, 21 - model.time.hours);
			//HPChange(timeQ * hpRecovery * multiplier, false);
			//player.changeFatigue(timeQ * -fatRecovery * multiplier); 
		//}
		//
		//
		//if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && !ingnam.inIngnam)
		//{
			//if (timeQ != 1) outputText("You head into your cabin to rest. You lie down on your bed to rest for " + num2Text(timeQ) + " hours.<br>");
			//else outputText("You head into your cabin to rest. You lie down on your bed to rest for an hour.<br>");
		//}
		//else 
		//{
			//if (timeQ != 1) outputText("You lie down to rest for " + num2Text(timeQ) + " hours.<br>");
			//else outputText("You lie down to rest for an hour.<br>");
		//}
		////Marble withdrawal
		//if (player.hasStatusEffect(StatusEffects.MarbleWithdrawl)) {
			//outputText("<br>Your rest is very troubled, and you aren't able to settle down.  You get up feeling tired and unsatisfied, always thinking of Marble's milk.<br>");
			//dynStats("tou", -.1, "int", -.1);
		//}
		////Bee cock
		//if (player.hasCock() && player.cocks[0].cockType == CockTypesEnum.BEE) {
			//outputText("<br>The desire to find the bee girl that gave you this cursed " + player.cockDescript(0) + " and have her spread honey all over it grows with each passing minute<br>");
		//}
		////Starved goo armor
		//if (player.armor == armors.GOOARMR && flags[kFLAGS.VALERIA_FLUIDS] <= 0 && valeria.valeriaFluidsEnabled()) {
			//outputText("<br>You feel the fluid-starved goo rubbing all over your groin as if Valeria wants you to feed her.<br>");
		//}
		////Hungry
		//if (flags[kFLAGS.HUNGER_ENABLED] > 0 && player.hunger < 25)
		//{
			//outputText("<br>You have difficulty resting as you toss and turn with your stomach growling.<br>");
		//}
		//
		//kGAMECLASS.HPChangeNotify(player.HP - hpBefore);
	//}
	//else {
		//clearOutput();
		//if (timeQ != 1) outputText("You continue to rest for " + num2Text(timeQ) + " more hours.<br>");
		//else outputText("You continue to rest for another hour.<br>");
	//}
	//goNext(timeQ,true);
//}
//
////-----------------
////-- WAIT
////-----------------
//public function doWait():Void {
	//campQ = true;
	//clearOutput();
	////Fatigue recovery
	//var fatRecovery:Number = 2;
	//if (player.findPerk(PerkLib.SpeedyRecovery) >= 0) fatRecovery *= 1.5;
	//if (player.findPerk(PerkLib.ControlledBreath) >= 0) fatRecovery *= 1.1;
	//if (timeQ == 0) {
		//timeQ = 4;
		//if (flags[kFLAGS.SHIFT_KEY_DOWN] > 0) timeQ = 21 - model.time.hours;
		//outputText("You wait " + num2Text(timeQ) + " hours...<br>");
		////Marble withdrawl
		//if (player.hasStatusEffect(StatusEffects.MarbleWithdrawl)) {
			//outputText("<br>Your time spent waiting is very troubled, and you aren't able to settle down.  You get up feeling tired and unsatisfied, always thinking of Marble's milk.<br>");
			////fatigue
			//fatRecovery /= 2;
			//player.changeFatigue(-fatRecovery * timeQ);
		//}
		////Bee cock
		//if (player.hasCock() && player.cocks[0].cockType == CockTypesEnum.BEE) {
			//outputText("<br>The desire to find the bee girl that gave you this cursed " + player.cockDescript(0) + " and have her spread honey all over it grows with each passing minute<br>");
		//}
		////Starved goo armor
		//if (player.armor == armors.GOOARMR && flags[kFLAGS.VALERIA_FLUIDS] <= 0) {
			//outputText("<br>You feel the fluid-starved goo rubbing all over your groin as if Valeria wants you to feed her.<br>");
		//}
		////REGULAR HP/FATIGUE RECOVERY
		//else {
			////fatigue
			//player.changeFatigue(-fatRecovery * timeQ); 
		//}
	//}
	//else {
		//if (timeQ != 1) outputText("You continue to wait for " + num2Text(timeQ) + " more hours.<br>");
		//else outputText("You continue to wait for another hour.<br>");
	//}
	//goNext(timeQ,true);
//}
////-----------------
////-- SLEEP
////-----------------
//public function doSleep(clrScreen:Bool = true):Void {
	//if (kGAMECLASS.urta.pregnancy.incubation == 0 && kGAMECLASS.urta.pregnancy.type == PregnancyStore.PREGNANCY_PLAYER && model.time.hours >= 20 && model.time.hours < 2) {
		//urtaPregs.preggoUrtaGivingBirth();
		//return;
	//}
	//campQ = true;
	//if (timeQ == 0) {
		//model.time.minutes = 0;
		//if (model.time.hours == 21) timeQ = 9;
		//if (model.time.hours == 22) timeQ = 8;
		//if (model.time.hours >= 23) timeQ = 7;
		//if (model.time.hours == 0) timeQ = 6;
		//if (model.time.hours == 1) timeQ = 5;
		//if (model.time.hours == 2) timeQ = 4;
		//if (model.time.hours == 3) timeQ = 3;
		//if (model.time.hours == 4) timeQ = 2;
		//if (model.time.hours == 5) timeQ = 1;
		//if (flags[kFLAGS.BENOIT_CLOCK_ALARM] > 0 && flags[kFLAGS.IN_PRISON] == 0) {
			//timeQ += (flags[kFLAGS.BENOIT_CLOCK_ALARM] - 6);
		//}
		////Autosave stuff		
		//if (player.slotName != "VOID" && player.autoSave && mainView.getButtonText( 0 ) != "Game Over") 
		//{
			//trace("Autosaving to slot: " + player.slotName);
			//
			//getGame().saves.saveGame(player.slotName);
		//}
		////Clear screen
		//if (clrScreen) clearOutput();
		///******************************************************************/
		///*       ONE TIME SPECIAL EVENTS                                  */
		///******************************************************************/
		////HEL SLEEPIES!
		//if (helFollower.helAffection() >= 70 && flags[kFLAGS.HEL_REDUCED_ENCOUNTER_RATE] == 0 && flags[kFLAGS.HEL_FOLLOWER_LEVEL] == 0) {
			//getGame().dungeons.heltower.heliaDiscovery();
			//sleepRecovery(false);
			//return;
		//}
		////Shouldra xgartuan fight
		//if (player.hasCock() && followerShouldra() && player.statusEffectv1(StatusEffects.Exgartuan) == 1) {
			//if (flags[kFLAGS.SHOULDRA_EXGARTUDRAMA] == 0) {
				//shouldraFollower.shouldraAndExgartumonFightGottaCatchEmAll();
				//sleepRecovery(false);
				//return;
			//}
			//else if (flags[kFLAGS.SHOULDRA_EXGARTUDRAMA] == 3) {
				//shouldraFollower.exgartuMonAndShouldraShowdown();
				//sleepRecovery(false);
				//return;
			//}
		//}
		//if (player.hasCock() && followerShouldra() && flags[kFLAGS.SHOULDRA_EXGARTUDRAMA] == -0.5) {
			//shouldraFollower.keepShouldraPartIIExgartumonsUndeatH();
			//sleepRecovery(false);
			//return;
		//}
		///******************************************************************/
		///*       SLEEP WITH SYSTEM GOOOO                                  */
		///******************************************************************/
		//if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && (flags[kFLAGS.SLEEP_WITH] == "" || flags[kFLAGS.SLEEP_WITH] == "Marble"))
		//{
			//outputText("You enter your cabin to turn yourself in for the night. ");
		//}
		////Marble Sleepies
		//if (marbleScene.marbleAtCamp() && player.hasStatusEffect(StatusEffects.CampMarble) && flags[kFLAGS.SLEEP_WITH] == "Marble" && flags[kFLAGS.FOLLOWER_AT_FARM_MARBLE] == 0) {
			//if (marbleScene.marbleNightSleepFlavor()) {
				//sleepRecovery(false);
				//return;
			//}
		//}
		//else if (flags[kFLAGS.SLEEP_WITH] == "Arian" && arianScene.arianFollower()) {
			//arianScene.sleepWithArian();
			//return;
		//}
		//else if (flags[kFLAGS.SLEEP_WITH] == "Ember" && flags[kFLAGS.EMBER_AFFECTION] >= 75 && followerEmber()) {
			//if (flags[kFLAGS.TIMES_SLEPT_WITH_EMBER] > 3) {
				//outputText("You curl up next to Ember, planning to sleep for " + num2Text(timeQ) + " hour. Ember drapes one of " + emberScene.emberMF("his", "her") + " wing over you, keeping you warm. ");
			//}
			//else {
				//emberScene.sleepWithEmber();
				//return;
			//}
		//}
		//else if (flags[kFLAGS.JOJO_BIMBO_STATE] >= 3 && jojoScene.pregnancy.isPregnant && jojoScene.pregnancy.event == 4 && player.hasCock() && flags[kFLAGS.SLEEP_WITH] == 0) {
			//joyScene.hornyJoyIsPregnant();
			//return;
		//}
		//else if (flags[kFLAGS.SLEEP_WITH] == "Sophie" && (bimboSophie() || sophieFollower()) && flags[kFLAGS.FOLLOWER_AT_FARM_SOPHIE] == 0) {
			////Night Time Snuggle Alerts!*
			////(1) 
			//if (rand(4) == 0) {
				//outputText("You curl up next to Sophie, planning to sleep for " + num2Text(timeQ) + " hour");
				//if (timeQ > 1 ) outputText("s");
				//outputText(".  She wraps her feathery arms around you and nestles her chin into your shoulder.  Her heavy breasts cushion flat against your back as she gives you a rather chaste peck on the cheek and drifts off towards dreamland...");
			//}
			////(2) 
			//else if (rand(3) == 0) {
				//outputText("While you're getting ready for bed, you see that Sophie has already beaten you there.  She's sprawled out on her back with her arms outstretched, making little beckoning motions towards the valley of her cleavage.  You snuggle in against her, her pillowy breasts supporting your head and her familiar heartbeat drumming you to sleep for " + num2Text(timeQ) + " hour");
				//if (timeQ > 1) outputText("s");
				//outputText(".");
			//}
			////(3)
			//else if (rand(2) == 0) {
				//outputText("As you lay down to sleep for " + num2Text(timeQ) + " hour");
				//if (timeQ > 1) outputText("s");
				//outputText(", you find the harpy-girl, Sophie, snuggling herself under her blankets with you.  She slips in between your arms and guides your hands to her enormous breasts, her backside already snug against your loins.  She whispers, \"<i>Something to think about for next morning...  Sweet dreams.</i>\" as she settles in for the night.");
			//}
			////(4)
			//else {
				//outputText("Sophie climbs under the sheets with you when you go to sleep, planning on resting for " + num2Text(timeQ) + " hour");
				//if (timeQ > 1) outputText("s");
				//outputText(".  She sleeps next to you, just barely touching you.  You rub her shoulder affectionately before the two of you nod off.");
			//}
			//outputText("<br>");
		//}
		//else {
			//if (flags[kFLAGS.SLEEP_WITH] == "Helia" && kGAMECLASS.helScene.followerHel()) {
				//outputText("You curl up next to Helia, planning to sleep for " + num2Text(timeQ) + " ");
			//}
			////Normal sleep message
			//else outputText("You curl up, planning to sleep for " + num2Text(timeQ) + " ");
			//if (timeQ == 1) outputText("hour.<br>");
			//else outputText("hours.<br>");
		//}
		//sleepRecovery(true);
	//}
	//else {
		//clearOutput();
		//if (timeQ != 1) outputText("You lie down to resume sleeping for the remaining " + num2Text(timeQ) + " hours.<br>");
		//else outputText("You lie down to resume sleeping for the remaining hour.<br>");
	//}
	//goNext(timeQ, true);
//}
////For shit that breaks normal sleep processing.
//public function sleepWrapper():Void {
	//if (model.time.hours == 16) timeQ = 14;
	//if (model.time.hours == 17) timeQ = 13;
	//if (model.time.hours == 18) timeQ = 12;
	//if (model.time.hours == 19) timeQ = 11;
	//if (model.time.hours == 20) timeQ = 10;
	//if (model.time.hours == 21) timeQ = 9;
	//if (model.time.hours == 22) timeQ = 8;
	//if (model.time.hours >= 23) timeQ = 7;
	//if (model.time.hours == 0) timeQ = 6;
	//if (model.time.hours == 1) timeQ = 5;
	//if (model.time.hours == 2) timeQ = 4;
	//if (model.time.hours == 3) timeQ = 3;
	//if (model.time.hours == 4) timeQ = 2;
	//if (model.time.hours == 5) timeQ = 1;
	//if (flags[kFLAGS.BENOIT_CLOCK_ALARM] > 0 && (flags[kFLAGS.SLEEP_WITH] == "Ember" || flags[kFLAGS.SLEEP_WITH] == 0)) timeQ += (flags[kFLAGS.BENOIT_CLOCK_ALARM] - 6);
	//clearOutput();
	//if (timeQ != 1) outputText("You lie down to resume sleeping for the remaining " + num2Text(timeQ) + " hours.<br>");
	//else outputText("You lie down to resume sleeping for the remaining hour.<br>");
	//sleepRecovery(true);
	//goNext(timeQ, true);
//}
//
//public function sleepRecovery(display:Bool = false):Void {
	//var multiplier:Number = 1.0;
	//var fatRecovery:Number = 20;
	//var hpRecovery:Number = 20;
	//if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && (flags[kFLAGS.SLEEP_WITH] == "" || flags[kFLAGS.SLEEP_WITH] == "Marble"))
	//{
		//multiplier += 0.5;
	//}
	//if (flags[kFLAGS.HUNGER_ENABLED] > 0)
	//{
		//if (player.hunger < 25)
		//{
			//outputText("<br>You have difficulty sleeping as your stomach is growling loudly.<br>");
			//multiplier *= 0.5;
		//}
	//}	
	////Marble withdrawl
	//if (player.hasStatusEffect(StatusEffects.MarbleWithdrawl)) {
		//if (display) outputText("<br>Your sleep is very troubled, and you aren't able to settle down.  You get up feeling tired and unsatisfied, always thinking of Marble's milk.<br>");
		//multiplier *= 0.5;
		//dynStats("tou", -.1, "int", -.1);
	//}
	////Mino withdrawal
	//else if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] == 3) {
		//if (display) outputText("<br>You spend much of the night tossing and turning, aching for a taste of minotaur cum.<br>");
		//multiplier *= 0.75;
	//}
	////Bee cock
	//if (player.hasCock() && player.cocks[0].cockType == CockTypesEnum.BEE) {
		//outputText("<br>The desire to find the bee girl that gave you this cursed " + player.cockDescript(0) + " and have her spread honey all over it grows with each passing minute<br>");
	//}
	////Starved goo armor
	//if (player.armor == armors.GOOARMR && flags[kFLAGS.VALERIA_FLUIDS] <= 0) {
		//outputText("<br>You feel the fluid-starved goo rubbing all over your groin as if Valeria wants you to feed her.<br>");
	//}
	////Tentacle Bark armor is annoying when sleeping.
	//if (player.armor == armors.TBARMOR) {
		//outputText("<br>The living, corrupted tentacles on your armor tease and poke every erogenous zone in your body over the night, leading to ");
		//if (player.cor < 33){
			//outputText("restless sleep and troubling dreams.");
			//multiplier *= 0.75;
		//}
		//if (player.cor >= 33 && player.cor < 66){
			//outputText("restless sleep.");
			//multiplier *= 0.9;
		//}
		//if (player.cor >= 66) outputText("wonderful dreams of being ravaged endlessly by several tentacle beasts.");
//
	//}
	////REGULAR HP/FATIGUE RECOVERY
	//HPChange(timeQ * hpRecovery * multiplier, display);
	////fatigue
	//player.changeFatigue(-(timeQ * fatRecovery * multiplier));
//}
//
////Bad End if your balls are too big. Only happens in Realistic Mode.
//public function badEndGIANTBALLZ():Void {
	//clearOutput();
	//outputText("You suddenly fall over due to your extremely large " + player.ballsDescriptLight() + ".  You struggle to get back up but the size made it impossible.  Panic spreads throughout your mind and your heart races.<br><br>");
	//outputText("You know that you can't move and you're aware that you're going to eventually starve to death.");
	//menu();
	//if (player.hasItem(consumables.REDUCTO, 1)) {
		//outputText("<br><br>Fortunately, you have some Reducto.  You can shrink your balls and get back to your adventures!");
		//addButton(1, "Reducto", applyReductoAndEscapeBadEnd);
	//}
	//if (player.hasStatusEffect(StatusEffects.CampRathazul)) {
		//outputText("<br><br>You could call for Rathazul to help you.");
		//addButton(2, "Rathazul", callRathazulAndEscapeBadEnd);		
	//}
	//if (shouldraFollower.followerShouldra()) {
		//outputText("<br><br>You could call for Shouldra to shrink your monstrous balls.");
		//addButton(3, "Shouldra", shouldraFollower.shouldraReductosYourBallsUpInsideYa, true);		
	//}
	//else getGame().gameOver();
//}
//private function applyReductoAndEscapeBadEnd():Void {
	//clearOutput();
	//outputText("You smear the foul-smelling paste onto your " + player.sackDescript() + ".  It feels cool at first but rapidly warms to an uncomfortable level of heat.<br><br>");
	//player.ballSize -= (4 + rand(6));
	//if (player.ballSize < 1) player.ballSize = 1;
	//if (player.ballSize > 18 + (player.str / 2) + (player.tallness / 4)) player.ballSize = 17 + (player.str / 2) + (player.tallness / 4);
	//outputText("You feel your scrotum shift, shrinking down along with your " + player.ballsDescriptLight() + ".  ");
	//outputText("Within a few seconds the paste has been totally absorbed and the shrinking stops.  ");
	//dynStats("lib", -2, "lus", -10);
	//player.consumeItem(consumables.REDUCTO, 1);
	//doNext(camp.returnToCampUseOneHour);
//}
//private function callRathazulAndEscapeBadEnd():Void {
	//clearOutput();
	//outputText("You shout as loud as you can to call Rathazul.  Your call is answered as the alchemist walks up to you.<br><br>");
	//outputText("\"<i>My, my... Look at yourself! Don't worry, I can help, </i>\" he says.  He rushes to his alchemy equipment and mixes ingredients.  He returns to you with a Reducto.<br><br>");
	//outputText("He rubs the paste all over your massive balls. It's incredibly effective. <br><br>");
	//player.ballSize -= (4 + rand(6));
	//if (player.ballSize < 1) player.ballSize = 1;
	//if (player.ballSize > 18 + (player.str / 2) + (player.tallness / 4)) player.ballSize = 16 + (player.str / 2) + (player.tallness / 4);
	//outputText("You feel your scrotum shift, shrinking down along with your " + player.ballsDescriptLight() + ".  ");
	//outputText("Within a few seconds the paste has been totally absorbed and the shrinking stops.  ");
	//outputText("\"<i>Try not to make your balls bigger. If it happens, make sure you have Reducto,</i>\" he says.  He returns to his alchemy equipment, working on who knows what.<br><br>");
	//doNext(camp.returnToCampUseOneHour);
//}
//
////Bad End if you starved to death.
//public function badEndHunger():Void {
	//clearOutput();
	//player.hunger = 0.1; //For Easy Mode/Debug Mode.
	//outputText("Too weak to be able to stand up, you collapse onto the ground. Your vision blurs as the world around you finally fades to black. ");
	//if (companionsCount() > 0) {
		//outputText("<br><br>");
		//if (companionsCount() > 1) {
			//outputText("Your companions gather to mourn over your passing.");
		//}
		//else {
			//outputText("Your fellow companion mourns over your passing.");
		//}
	//}
	//player.HP = 0;
	//getGame().gameOver();
	//removeButton(1); //Can't continue, you're dead!
//}
////Bad End if you have 100 min lust.
//public function badEndMinLust():Void {
	//clearOutput();
	//outputText("The thought of release overwhelms you. You frantically remove your " + player.armorName + " and begin masturbating furiously.  The first orgasm hits you but the desire persists.  You continue to masturbate but unfortunately, no matter how hard or how many times you orgasm, your desires will not go away.  Frustrated, you keep masturbating furiously but you are unable to stop.  Your minimum lust is too high.  No matter how hard you try, you cannot even satisfy your desires.");
	//outputText("<br><br>You spend the rest of your life masturbating, unable to stop.");
	//player.orgasm('Generic');
	//getGame().gameOver();
	//removeButton(1); //Can't wake up, must load.
//}
//
//public function allNaturalSelfStimulationBeltContinuation():Void {
	//clearOutput();
	//outputText("In shock, you scream as you realize the nodule has instantly grown into a massive, organic dildo. It bottoms out easily and rests against your cervix as you recover from the initial shock of its penetration. As the pangs subside, the infernal appendage begins working itself. It begins undulating in long, slow strokes. It takes great care to adjust itself to fit every curve of your womb. Overwhelmed, your body begins reacting against your conscious thought and slowly thrusts your pelvis in tune to the thing.<br><br>");
	//outputText("As suddenly as it penetrated you, it shifts into a different phase of operation. It buries itself as deep as it can and begins short, rapid strokes. The toy hammers your insides faster than any man could ever hope to do. You orgasm immediately and produce successive climaxes. Your body loses what motor control it had and bucks and undulates wildly as the device pistons your cunt without end. You scream at the top of your lungs. Each yell calls to creation the depth of your pleasure and lust.<br><br>");
	//outputText("The fiendish belt shifts again. It buries itself as deep as it can go and you feel pressure against the depths of your womanhood. You feel a hot fluid spray inside you. Reflexively, you shout, \"<b>IT'S CUMMING! IT'S CUMMING INSIDE ME!</b>\" Indeed, each push of the prodding member floods your box with juice. It cums... and cums... and cums... and cums...<br><br>");
	//outputText("An eternity passes, and your pussy is sore. It is stretched and filled completely with whatever this thing shoots for cum. It retracts itself from your hole and you feel one last pang of pressure as your body now has a chance to force out all of the spunk that it cannot handle. Ooze sprays out from the sides of the belt and leaves you in a smelly, sticky mess. You feel the belt's tension ease up as it loosens. The machine has run its course. You immediately pass out.");
	//player.slimeFeed();
	//player.orgasm('Vaginal');
	//dynStats("lib", 1, "sen", (-0.5));
	//doNext(camp.returnToCampUseOneHour);
//}
//
//public function allNaturalSelfStimulationBeltBadEnd():Void {
	//spriteSelect(SpriteDb.s_giacomo);
	//clearOutput();
	//outputText("Whatever the belt is, whatever it does, it no longer matters to you.  The only thing you want is to feel the belt and its creature fuck the hell out of you, day and night.  You quickly don the creature again and it begins working its usual lustful magic on your insatiable little box.  An endless wave of orgasms take you.  All you now know is the endless bliss of an eternal orgasm.<br><br>");
	//outputText("Your awareness hopelessly compromised by the belt and your pleasure, you fail to notice a familiar face approach your undulating form.  It is the very person who sold you this infernal toy.  The merchant, Giacomo.<br><br>");
	//outputText("\"<i>Well, well,</i>\" Giacomo says.  \"<i>The Libertines are right.  The creature's fluids are addictive. This poor " + player.mf("man", "woman") + " is a total slave to the beast!</i>\"<br><br>");
	//outputText("Giacomo contemplates the situation as you writhe in backbreaking pleasure before him.  His sharp features brighten as an idea strikes him.<br><br>");
	//outputText("\"<i>AHA!</i>\" the hawkish purveyor cries.  \"<i>I have a new product to sell! I will call it the 'One Woman Show!'</i>\"<br><br>");
	//outputText("Giacomo cackles smugly at his idea.  \"<i>Who knows how much someone will pay me for a live " + player.mf("man", "woman") + " who can't stop cumming!</i>\"<br><br>");
	//outputText("Giacomo loads you up onto his cart and sets off for his next sale.  You do not care.  You do not realize what has happened.  All you know is that the creature keeps cumming and it feels... sooooo GODDAMN GOOD!");
	//getGame().gameOver();
//}
//
//private function dungeonFound():Bool { //Returns true as soon as any known dungeon is found
	//if (flags[kFLAGS.DISCOVERED_DUNGEON_2_ZETAZ] > 0) return true;
	//if (flags[kFLAGS.FACTORY_FOUND] > 0) return true;
	//if (flags[kFLAGS.DISCOVERED_WITCH_DUNGEON] > 0) return true;
	//if (flags[kFLAGS.D3_DISCOVERED] > 0) return true;
	//if (kGAMECLASS.dungeons.checkPhoenixTowerClear()) return true;
	//if (flags[kFLAGS.FOUND_MANOR] > 0) return true;
	//return false;
//}
//
//private function farmFound():Bool {
	//if (player.hasStatusEffect(StatusEffects.MetWhitney) && player.statusEffectv1(StatusEffects.MetWhitney) > 1) {
		//if (flags[kFLAGS.FARM_DISABLED] == 0) return true;
		//if (player.cor >= (70 - player.corruptionTolerance()) && player.level >= 12 && getGame().farm.farmCorruption.corruptFollowers() >= 2 && flags[kFLAGS.FARM_CORRUPTION_DISABLED] == 0) return true;
	//}
	//if (flags[kFLAGS.FARM_CORRUPTION_STARTED]) return true;
	//return false;
//}
//
////-----------------
////-- PLACES MENU
////-----------------
//private function placesKnown():Bool { //Returns true as soon as any known place is found
	//if (placesCount() > 0) return true;
	//return false;
//}
//
//public function placesCount():Int {
	//var places:Int = 0;
	//if (flags[kFLAGS.BAZAAR_ENTERED] > 0) places++;
	//if (player.hasStatusEffect(StatusEffects.BoatDiscovery)) places++;
	//if (flags[kFLAGS.FOUND_CATHEDRAL] > 0) places++;
	//if (flags[kFLAGS.FACTORY_FOUND] > 0 || flags[kFLAGS.DISCOVERED_DUNGEON_2_ZETAZ] > 0 || flags[kFLAGS.DISCOVERED_WITCH_DUNGEON] > 0) places++;
	//if (farmFound()) places++; 
	//if (flags[kFLAGS.OWCA_UNLOCKED] > 0) places++;
	//if (player.hasStatusEffect(StatusEffects.HairdresserMeeting)) places++;
	//if (player.statusEffectv1(StatusEffects.TelAdre) >= 1) places++;
	//if (flags[kFLAGS.AMILY_VILLAGE_ACCESSIBLE] > 0) places++;
	//if (flags[kFLAGS.MET_MINERVA] >= 4) places++;
	//if (flags[kFLAGS.PRISON_CAPTURE_COUNTER] > 0) places++;
	//return places;
//}
//
////All cleaned up!
//public function places():Bool {
	//hideMenus();
	//clearOutput();
	//outputText("Which place would you like to visit?");
	////if (flags[kFLAGS.PLACES_PAGE] != 0)
	////{
	////	placesPage2();
	////	return;
	////}
	////Build menu
	//menu();
	//if (flags[kFLAGS.BAZAAR_ENTERED] > 0) addButton(0, "Bazaar", kGAMECLASS.bazaar.enterTheBazaar, null, null, null, "Visit the Bizarre Bazaar where the demons and corrupted beings hang out.");
	//if (player.hasStatusEffect(StatusEffects.BoatDiscovery)) addButton(1, "Boat", kGAMECLASS.boat.boatExplore, null, null, null, "Get on the boat and explore the lake. <br><br>Recommended level: 4");
	//if (flags[kFLAGS.FOUND_CATHEDRAL] > 0) {
		//if (flags[kFLAGS.GAR_NAME] == 0) addButton(2, "Cathedral", kGAMECLASS.gargoyle.gargoylesTheShowNowOnWBNetwork, null, null, null, "Visit the ruined cathedral you've recently discovered.");
		//else addButton(2, "Cathedral", kGAMECLASS.gargoyle.returnToCathedral, null, null, null, "Visit the ruined cathedral where " + flags[kFLAGS.GAR_NAME] + " resides.");
	//}
	//if (dungeonFound()) addButton(3, "Dungeons", dungeons, null, null, null, "Delve into dungeons.");
	//
	//if (farmFound()) addButton(5, "Farm", kGAMECLASS.farm.farmExploreEncounter, null, null, null, "Visit Whitney's farm.");
	//if (flags[kFLAGS.OWCA_UNLOCKED] == 1) addButton(6, "Owca", kGAMECLASS.owca.gangbangVillageStuff, null, null, null, "Visit the sheep village of Owca, known for its pit where a person is hung on the pole weekly to be gang-raped by the demons.");
	//if (flags[kFLAGS.MET_MINERVA] >= 4) addButton(7, "Oasis Tower", kGAMECLASS.highMountains.minervaScene.encounterMinerva, null, null, null, "Visit the ruined tower in the high mountains where Minerva resides.");
	//if (player.hasStatusEffect(StatusEffects.HairdresserMeeting)) addButton(8, "Salon", kGAMECLASS.mountain.salon.salonGreeting, null, null, null, "Visit the salon for hair services.");
	//if (flags[kFLAGS.LUMI_MET] > 0) addButton(9, "Lumi's Lab", kGAMECLASS.lumi.lumiEncounter, null, null, null, "Visit Lumi's lab.");
	//if (getGame().telAdre.isAllowedInto()) addButton(10, "Tel'Adre", kGAMECLASS.telAdre.telAdreMenu, null, null, null, "Visit the city of Tel'Adre in desert, easily recognized by the massive tower.");
	//if (flags[kFLAGS.AMILY_VILLAGE_ACCESSIBLE] > 0) addButton(11, "Town Ruins", kGAMECLASS.townRuins.exploreVillageRuin, null, null, null, "Visit the village ruins.");
	//
	////addButton(4, "Next", placesPage2);
	//addButton(14, "Back", playerMenu);
	//return true;
//}
//
///*private function placesPage2():Void {
	//menu();
	//flags[kFLAGS.PLACES_PAGE] = 1;
	//addButton(9, "Previous", placesToPage1);
	//addButton(14, "Back", playerMenu);
//}
//
//private function placesToPage1():Void {
	//flags[kFLAGS.PLACES_PAGE] = 0;
	//places();
//}*/
//
//private function dungeons():Void {
	//menu();
	////Turn on dungeon 1
	//if (flags[kFLAGS.FACTORY_FOUND] > 0) addButton(0, "Factory", getGame().dungeons.factory.enterDungeon, null, null, null, "Visit the demonic factory in the mountains." + (flags[kFLAGS.FACTORY_SHUTDOWN] > 0 ? "<br><br>You've managed to shut down the factory." : "The factory is still running. Marae wants you to shut down the factory!") + (kGAMECLASS.dungeons.checkFactoryClear() ? "<br><br>CLEARED!" : ""));
	////Turn on dungeon 2
	//if (flags[kFLAGS.DISCOVERED_DUNGEON_2_ZETAZ] > 0) addButton(1, "Deep Cave", getGame().dungeons.deepcave.enterDungeon, null, null, null, "Visit the cave you've found in the Deepwoods." + (flags[kFLAGS.DEFEATED_ZETAZ] > 0 ? "<br><br>You've defeated Zetaz, your old rival." : "") + (kGAMECLASS.dungeons.checkDeepCaveClear() ? "<br><br>CLEARED!" : ""));
	////Turn on dungeon 3
	//if (flags[kFLAGS.D3_DISCOVERED] > 0) addButton(2, "Stronghold", kGAMECLASS.d3.enterD3, null, null, null, "Visit the stronghold in the high mountains that belongs to Lethice, the demon queen." + (flags[kFLAGS.LETHICE_DEFEATED] > 0 ? "<br><br>You have defeated Lethice and put an end to the demonic threats. Congratulations, you've beaten the main story!" : "") + (kGAMECLASS.dungeons.checkLethiceStrongholdClear() ? "<br><br>CLEARED!" : ""));
	////Side dungeons
	//if (flags[kFLAGS.DISCOVERED_WITCH_DUNGEON] > 0) addButton(5, "Desert Cave", getGame().dungeons.desertcave.enterDungeon, null, null, null, "Visit the cave you've found in the desert." + (flags[kFLAGS.SAND_WITCHES_COWED] + flags[kFLAGS.SAND_WITCHES_FRIENDLY] > 0 ? "<br><br>From what you've known, this is the source of the Sand Witches." : "") + (kGAMECLASS.dungeons.checkSandCaveClear() ? "<br><br>CLEARED!" : ""));
	//if (kGAMECLASS.dungeons.checkPhoenixTowerClear()) addButton(6, "Phoenix Tower", getGame().dungeons.heltower.returnToHeliaDungeon, null, null, null, "Re-visit the tower you went there as part of Helia's quest." + (kGAMECLASS.dungeons.checkPhoenixTowerClear() ? "<br><br>You've helped Helia in the quest and resolved the problems. <br><br>CLEARED!" : ""));
		////Fetish Church?
		////Hellhound Dungeon?
	//if (flags[kFLAGS.FOUND_MANOR] > 0) addButton(7, "Old Manor", getGame().dungeons.manor.enterDungeon, null, null, null, "Visit the accursed manor overlooking the forested valley." + (flags[kFLAGS.MANOR_PROGRESS] & 128 ? "<br><br>You have slain the Necromancer and put an end to his unspeakable transgressions of nature.<br><br>CLEARED!" : "")); 
	////Non-hostile dungeons
	//if (flags[kFLAGS.ANZU_PALACE_UNLOCKED] > 0) addButton(10, "Anzu's Palace", getGame().dungeons.palace.enterDungeon, null, null, null, "Visit the palace in the Glacial Rift where Anzu the avian deity resides.");
	//addButton(14, "Back", places);
//}
//
//private function exgartuanCampUpdate():Void {
	////Update Exgartuan stuff
	//if (player.hasStatusEffect(StatusEffects.Exgartuan))
	//{
		//trace("EXGARTUAN V1: " + player.statusEffectv1(StatusEffects.Exgartuan) + " V2: " + player.statusEffectv2(StatusEffects.Exgartuan));
		////if too small dick, remove him
		//if (player.statusEffectv1(StatusEffects.Exgartuan) == 1 && (player.cockArea(0) < 100 || player.cocks.length == 0))
		//{
			//clearOutput();
			//outputText("<b>You suddenly feel the urge to urinate, and stop over by some bushes.  It takes wayyyy longer than normal, and once you've finished, you realize you're alone with yourself for the first time in a long time.");
			//if (player.hasCock()) outputText("  Perhaps you got too small for Exgartuan to handle?</b><br>");
			//else outputText("  It looks like the demon didn't want to stick around without your manhood.</b><br>");
			//player.removeStatusEffect(StatusEffects.Exgartuan);
			//awardAchievement("Urine Trouble", kACHIEVEMENTS.GENERAL_URINE_TROUBLE, true);
		//}
		////Tit removal
		//else if (player.statusEffectv1(StatusEffects.Exgartuan) == 2 && player.biggestTitSize() < 12)
		//{
			//clearOutput();
			//outputText("<b>Black milk dribbles from your " + player.nippleDescript(0) + ".  It immediately dissipates into the air, leaving you feeling alone.  It looks like you became too small for Exgartuan!<br></b>");
			//player.removeStatusEffect(StatusEffects.Exgartuan);
		//}		
	//}
	//doNext(playerMenu);
//}
//
////Wake up from a bad end.
//public function wakeFromBadEnd():Void {
	//clearOutput();
	//trace("Escaping bad end!");
	//outputText("No, it can't be.  It's all just a dream!  You've got to wake up!");
	//outputText("<br><br>You wake up and scream.  You pull out a mirror and take a look at yourself.  Yep, you look normal again.  That was the craziest dream you ever had.");
	//if (flags[kFLAGS.TIMES_BAD_ENDED] >= 2) { //FOURTH WALL BREAKER
		//outputText("<br><br>You mumble to yourself \"<i>Another goddamn bad-end.</i>\"");
	//}
	//if (marbleFollower()) outputText("<br><br>\"<i>Are you okay, sweetie?</i>\" Marble asks.  You assure her that you're fine; you've just had a nightmare.");
	//if (flags[kFLAGS.HUNGER_ENABLED] > 0) player.hunger = 40;
	//if (flags[kFLAGS.HUNGER_ENABLED] >= 1 && player.ballSize > (18 + (player.str / 2) + (player.tallness / 4))) {
		//outputText("<br><br>You realize the consequences of having oversized balls and you NEED to shrink it right away. Reducto will do.");
		//player.ballSize = (14 + (player.str / 2) + (player.tallness / 4));
	//}
	//if (flags[kFLAGS.EASY_MODE_ENABLE_FLAG] > 0)
		//outputText("<br><br>You get up, still feeling confused from the nightmares.");
	//else
		//outputText("<br><br>You get up, still feeling traumatized from the nightmares.");
	////Skip time forward
	//model.time.days++;
	//if (flags[kFLAGS.BENOIT_CLOCK_BOUGHT] > 0) model.time.hours = flags[kFLAGS.BENOIT_CLOCK_ALARM];
	//else model.time.hours = 6;
	////Set so you're in camp.
	//inDungeon = false;
	//inRoomedDungeon = false;
	//inRoomedDungeonResume = null;
	//if (getGame().inCombat) {
		//player.clearStatuses(false);
		//if (monsterArray.length != 0) monsterArray.length = 0;
		//getGame().inCombat = false;
	//}
	////Restore stats
	//player.HP = player.maxHP();
	//player.fatigue = 0;
	//statScreenRefresh();
	////PENALTY!
	//var penaltyMultiplier:Int = 1;
	//penaltyMultiplier += flags[kFLAGS.GAME_DIFFICULTY] * 0.5;
	//if (flags[kFLAGS.EASY_MODE_ENABLE_FLAG] > 0 ) penaltyMultiplier = 0;
	////Deduct XP and gems.
	//player.gems -= int((player.gems / 10) * penaltyMultiplier);
	//player.XP -= int((player.level * 10) * penaltyMultiplier);
	//if (player.gems < 0) player.gems = 0;
	//if (player.XP < 0) player.XP = 0;
	////Deduct attributes.
	//if (player.str100 > 20) dynStats("str", Math.ceil(-player.str * 0.02) * penaltyMultiplier);
	//if (player.tou100 > 20) dynStats("tou", Math.ceil(-player.tou * 0.02) * penaltyMultiplier);
	//if (player.spe100 > 20) dynStats("spe", Math.ceil(-player.spe * 0.02) * penaltyMultiplier);
	//if (player.inte100 > 20) dynStats("inte", Math.ceil(-player.inte * 0.02) * penaltyMultiplier);
	//menu();
	//addButton(0, "Next", playerMenu);
//}
//
////Camp wall
//private function buildCampWallPrompt():Void {
	//clearOutput();
	//if (player.fatigue >= player.maxFatigue() - 50) {
		//outputText("You are too exhausted to work on your camp wall!");
		//doNext(doCamp);
		//return;
	//}
	//if (flags[kFLAGS.CAMP_WALL_PROGRESS] == 0) {
		//outputText("A feeling of unrest grows within you as the population of your camp is growing. Maybe it's time you build a wall to secure the perimeter?<br><br>");
		//flags[kFLAGS.CAMP_WALL_PROGRESS] = 1;
	//}
	//else {
		//outputText("You can continue work on building the wall that surrounds your camp.<br><br>");
		//outputText("Segments complete: " + Math.floor(flags[kFLAGS.CAMP_WALL_PROGRESS] / 20) + "/5<br><br>");
	//}
	//kGAMECLASS.camp.cabinProgress.checkMaterials();
	//outputText("<br><br>It will cost 50 nails, 50 stones and 100 wood to work on a segment of the wall.<br><br>");
	//if (flags[kFLAGS.CAMP_CABIN_WOOD_RESOURCES] >= 100 && player.keyItemv1("Carpenter's Toolbox") >= 50 && flags[kFLAGS.CAMP_CABIN_STONE_RESOURCES] >= 50) {
		//doYesNo(buildCampWall, doCamp);
	//}
	//else {
		//outputText("<br><b>Unfortunately, you do not have sufficient resources.</b>");
		//doNext(doCamp);
	//}
//}
//
//private function buildCampWall():Void {
	//var helpers:Int = 0;
	//var helperArray:Array = [];
	//if (marbleFollower()) {
		//helperArray[helperArray.length] = "Marble";
		//helpers++;
	//}
	//if (followerHel()) {
		//helperArray[helperArray.length] = "Helia";
		//helpers++;
	//}
	//if (followerKiha()) {
		//helperArray[helperArray.length] = "Kiha";
		//helpers++;
	//}
	//flags[kFLAGS.CAMP_CABIN_STONE_RESOURCES] -= 50;
	//flags[kFLAGS.CAMP_CABIN_WOOD_RESOURCES] -= 100;
	//player.addKeyValue("Carpenter's Toolbox", 1, -50);
	//clearOutput();
	//if (flags[kFLAGS.CAMP_WALL_PROGRESS] == 1) {
		//outputText("You pull out a book titled \"Carpenter's Guide\" and flip pages until you come across instructions on how to build a wall. You spend minutes looking at the instructions and memorize the procedures.");
		//flags[kFLAGS.CAMP_WALL_PROGRESS] = 20;
	//}
	//else {
		//outputText("You remember the procedure for building a wall.");
		//flags[kFLAGS.CAMP_WALL_PROGRESS] += 20;
	//}
	//outputText("<br><br>You dig four holes, six inches deep and one foot wide each, before putting up wood posts, twelve feet high and one foot thick each. You take the wood from supplies, saw the wood and cut them into planks before nailing them to the wooden posts.");
	//if (helpers > 0) {
		//outputText("<br><br>" + formatStringArray(helperArray));
		//outputText(" " + (helpers == 1 ? "assists" : "assist") + " you with building the wall, helping to speed up the process and make construction less fatiguing.");
	//}
	////Gain fatigue.
	//var fatigueAmount:Int = 100;
	//fatigueAmount -= player.str / 5;
	//fatigueAmount -= player.tou / 10;
	//fatigueAmount -= player.spe / 10;
	//if (player.findPerk(PerkLib.IronMan) >= 0) fatigueAmount -= 20;
	//fatigueAmount /= (helpers + 1);
	//if (fatigueAmount < 15) fatigueAmount = 15;
	//player.changeFatigue(fatigueAmount);
	//if (helpers >= 2) {
		//outputText("<br><br>Thanks to your assistants, the construction takes only one hour!");
		//doNext(camp.returnToCampUseOneHour);
	//}
	//else if (helpers == 1) {
		//outputText("<br><br>Thanks to your assistant, the construction takes only two hours.");
		//doNext(camp.returnToCampUseTwoHours);
	//}
	//else {
		//outputText("<br><br>It's " + (fatigueAmount >= 75 ? "a daunting" : "an easy") + " task but you eventually manage to finish building a segment of the wall for your camp!");
		//doNext(camp.returnToCampUseFourHours);
	//}
	//if (flags[kFLAGS.CAMP_WALL_PROGRESS] >= 100) {
		//outputText("<br><br><b>Well done! You have finished the wall! You can build a gate and decorate wall with imp skulls to further deter whoever might try to come and rape you.</b>");
		//output.flush();
	//}
//}
//
////Camp gate
//private function buildCampGatePrompt():Void {
	//clearOutput();
	//if (player.fatigue >= player.maxFatigue() - 50) {
		//outputText("You are too exhausted to work on your camp wall!");
		//doNext(doCamp);
		//return;
	//}
	//outputText("You can build a gate to further secure your camp by having it closed at night.<br><br>");
	//kGAMECLASS.camp.cabinProgress.checkMaterials();
	//outputText("<br><br>It will cost 100 nails, 100 stones and 100 wood to build a gate.<br><br>");
	//if (flags[kFLAGS.CAMP_CABIN_WOOD_RESOURCES] >= 100 && player.keyItemv1("Carpenter's Toolbox") >= 100 && flags[kFLAGS.CAMP_CABIN_STONE_RESOURCES] >= 100) {
		//doYesNo(buildCampGate, doCamp);
	//}
	//else {
		//outputText("<br><b>Unfortunately, you do not have sufficient resources.</b>");
		//doNext(doCamp);
	//}
//
//}
//
//private function buildCampGate():Void {
	//var helpers:Int = 0;
	//var helperArray:Array = [];
	//if (marbleFollower()) {
		//helperArray[helperArray.length] = "Marble";
		//helpers++;
	//}
	//if (followerHel()) {
		//helperArray[helperArray.length] = "Helia";
		//helpers++;
	//}
	//if (followerKiha()) {
		//helperArray[helperArray.length] = "Kiha";
		//helpers++;
	//}
	//flags[kFLAGS.CAMP_CABIN_STONE_RESOURCES] -= 100;
	//flags[kFLAGS.CAMP_CABIN_WOOD_RESOURCES] -= 100;
	//player.addKeyValue("Carpenter's Toolbox", 1, -100);
	//clearOutput();
	//outputText("You pull out a book titled \"Carpenter's Guide\" and flip pages until you come across instructions on how to build a gate that can be opened and closed. You spend minutes looking at the instructions and memorize the procedures.");
	//flags[kFLAGS.CAMP_WALL_GATE] = 1;
	//outputText("<br><br>You take the wood from supplies, saw the wood and cut them into planks before nailing them together. ");
	//if (helpers > 0) {
		//outputText("<br><br>" + formatStringArray(helperArray));
		//outputText(" " + (helpers == 1 ? "assists" : "assist") + " you with building the gate, helping to speed up the process and make construction less fatiguing.");
	//}
	//outputText("<br><br>You eventually finish building the gate.");
	////Gain fatigue.
	//var fatigueAmount:Int = 100;
	//fatigueAmount -= player.str / 5;
	//fatigueAmount -= player.tou / 10;
	//fatigueAmount -= player.spe / 10;
	//if (player.findPerk(PerkLib.IronMan) >= 0) fatigueAmount -= 20;
	//fatigueAmount /= (helpers + 1);
	//if (fatigueAmount < 15) fatigueAmount = 15;
	//player.changeFatigue(fatigueAmount);
	//doNext(camp.returnToCampUseOneHour);
//}
//
//private function promptHangImpSkull():Void {
	//clearOutput();
	//if (flags[kFLAGS.CAMP_WALL_SKULLS] >= 100) {
		//outputText("There is no room; you have already hung a total of 100 imp skulls! No imp shall dare approaching you at night!");
		//doNext(doCamp);
		//return;
	//}
	//outputText("Would you like to hang the skull of an imp onto wall? ");
	//if (flags[kFLAGS.CAMP_WALL_SKULLS] > 0) outputText("There " + (flags[kFLAGS.CAMP_WALL_SKULLS] == 1 ? "is" : "are") + " currently " + num2Text(flags[kFLAGS.CAMP_WALL_SKULLS]) + " imp skull" + (flags[kFLAGS.CAMP_WALL_SKULLS] == 1 ? "" : "s") + " hung on the wall, serving to deter any imps who might try to rape you.");
	//doYesNo(hangImpSkull, doCamp);
//}
//
//private function hangImpSkull():Void {
	//clearOutput();
	//outputText("You hang the skull of an imp on the wall. ");
	//player.consumeItem(useables.IMPSKLL, 1);
	//flags[kFLAGS.CAMP_WALL_SKULLS]++;
	//outputText("There " + (flags[kFLAGS.CAMP_WALL_SKULLS] == 1 ? "is" : "are") + " currently " + num2Text(flags[kFLAGS.CAMP_WALL_SKULLS]) + " imp skull" + (flags[kFLAGS.CAMP_WALL_SKULLS] == 1 ? "" : "s") + " hung on the wall, serving to deter any imps who might try to rape you.");
	//doNext(doCamp);
//}
//
//public function homeDesc():String {
	//var textToChoose:String;
	//if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0) {
		//textToChoose = "cabin";
	//}
	//else {
		//textToChoose = "tent";
	//}
	//return textToChoose;
//}

	public static function bedDesc():String {
		if (Flags.getTrigger(TriggerFlag.CAMP_BUILT_CABIN).set && Flags.getTrigger(TriggerFlag.CAMP_CABIN_FURNITURE_BED).set)
			return "bed";
			
		return "bedroll";
	}

//public function setLevelButton():Bool {
	//if ((player.XP >= player.requiredXP() && player.level < kGAMECLASS.levelCap) || player.perkPoints > 0 || player.statPoints > 0) {
		//if (player.XP < player.requiredXP() || player.level >= kGAMECLASS.levelCap)
		//{
			//if (player.statPoints > 0) {
				//mainView.setMenuButton( MainView.MENU_LEVEL, "Stat Up" );
				//mainView.levelButton.toolTipText = "Distribute your stats points. <br><br>You currently have " + String(player.statPoints) + ".";
			//}
			//else {
				//mainView.setMenuButton( MainView.MENU_LEVEL, "Perk Up" );
				//mainView.levelButton.toolTipText = "Spend your perk points on a new perk. <br><br>You currently have " + String(player.perkPoints) + ".";
			//}
		//}
		//else {
			//mainView.setMenuButton( MainView.MENU_LEVEL, "Level Up" );
			//mainView.levelButton.toolTipText = "Level up to increase your maximum HP by 15 and gain 5 attribute points and 1 perk points.";
			//if (flags[kFLAGS.AUTO_LEVEL] > 0) {
				//kGAMECLASS.playerInfo.levelUpGo();
				//return true; //True indicates that you should be routed to level-up.
			//}
			//
		//}
		//mainView.showMenuButton( MainView.MENU_LEVEL );
		//mainView.statsView.showLevelUp();
		//if (player.str >= player.getMaxStats("str") && player.tou >= player.getMaxStats("tou") && player.inte >= player.getMaxStats("int") && player.spe >= player.getMaxStats("spe") && (player.perkPoints <= 0 || kGAMECLASS.playerInfo.buildPerkList().length <= 0) && (player.XP < player.requiredXP() || player.level >= kGAMECLASS.levelCap)) {
			//mainView.statsView.hideLevelUp();
		//}
	//}
	//else {
		//mainView.hideMenuButton( MainView.MENU_LEVEL );
		//mainView.statsView.hideLevelUp();
	//}
	//return false;
//}
//
////Camp population!
//public function getCampPopulation():Int {
	//var pop:Int = 0; //Once you enter Mareth, this will increase to 1.
	//if (flags[kFLAGS.IN_INGNAM] <= 0) pop++; //You count toward the population!
	//pop += companionsCount();
	////------------
	////Misc check!
	//if (ceraphIsFollower()) pop--; //Ceraph doesn't stay in your camp.
	//if (player.armorName == "goo armor") pop++; //Include Valeria if you're wearing her.
	//if (flags[kFLAGS.CLARA_IMPRISONED] > 0) pop++;
	//if (flags[kFLAGS.ANEMONE_KID] > 0) pop++;
	//if (flags[kFLAGS.FUCK_FLOWER_LEVEL] >= 4) pop++;
	////------------
	////Children check!
	////Followers
	//if (followerEmber() && emberScene.emberChildren() > 0) pop += emberScene.emberChildren();
	////Jojo's offsprings don't stay in your camp; they will join with Amily's litters as well.
	//if (sophieFollower()) {
		//if (flags[kFLAGS.SOPHIE_DAUGHTER_MATURITY_COUNTER] > 0) pop++;
		//if (flags[kFLAGS.SOPHIE_ADULT_KID_COUNT]) pop += flags[kFLAGS.SOPHIE_ADULT_KID_COUNT];
	//}
	//
	////Lovers
	////Amily's offsprings don't stay in your camp.
	////Helia can only have 1 child: Helspawn. She's included in companions count.
	//if (isabellaFollower() && isabellaScene.totalIsabellaChildren() > 0) pop += isabellaScene.totalIsabellaChildren();
	//if (izmaFollower() && izmaScene.totalIzmaChildren() > 0) pop += izmaScene.totalIzmaChildren();
	//if (followerKiha() && kihaFollower.totalKihaChildren() > 0) pop += kihaFollower.totalKihaChildren();
	//if (marbleFollower() && flags[kFLAGS.MARBLE_KIDS] > 0) pop += flags[kFLAGS.MARBLE_KIDS];
	//if (flags[kFLAGS.ANT_WAIFU] > 0 && (flags[kFLAGS.ANT_KIDS] > 0 || flags[kFLAGS.PHYLLA_DRIDER_BABIES_COUNT] > 0)) pop += (flags[kFLAGS.ANT_KIDS] + flags[kFLAGS.PHYLLA_DRIDER_BABIES_COUNT]);
	////------------
	////Return number!
	//return pop;
//}

//private function goNextWrapped(time:Number, needNext:Boolean):Boolean  {
		////Inform all time aware classes that a new hour has arrived
		//for (var tac:int = 0; tac < _timeAwareClassList.length; tac++) if (_timeAwareClassList[tac].timeChange()) needNext = true;
		//if (model.time.hours > 23) {
			//model.time.hours = 0;
			//model.time.days++;
		//}
		//else if (model.time.hours == 21) {
			//if (flags[kFLAGS.LETHICE_DEFEATED] <= 0) outputText("\nThe sky darkens as a starless night falls.  The blood-red moon slowly rises up over the horizon.\n");
			//else outputText("\nThe sky darkens as a starry night falls.  The blood-red moon slowly rises up over the horizon.\n");
			//needNext = true;
		//}
		//else if (model.time.hours == 6) {
			//outputText("\nThe sky begins to grow brighter as the moon descends over distant mountains, casting a few last ominous shadows before they burn away in the light.\n");
			//needNext = true;
		//}
		//if (model.time.hours == 2) {
			//if (model.time.days % 30 == 0 && flags[kFLAGS.ANEMONE_KID] > 0 && player.hasCock() && flags[kFLAGS.ANEMONE_WATCH] > 0 && flags[kFLAGS.TAMANI_NUMBER_OF_DAUGHTERS] >= 40) {
				//anemoneScene.goblinNightAnemone();
				//needNext = true;
			//}
			//else if (temp > rand(100) && !player.hasStatusEffect(StatusEffects.DefenseCanopy)) {
				//if (player.gender > 0 && !(player.hasStatusEffect(StatusEffects.JojoNightWatch) && player.hasStatusEffect(StatusEffects.PureCampJojo)) && (flags[kFLAGS.HEL_GUARDING] == 0 || !helFollower.followerHel()) && flags[kFLAGS.ANEMONE_WATCH] == 0 && (flags[kFLAGS.HOLLI_DEFENSE_ON] == 0 || flags[kFLAGS.FUCK_FLOWER_KILLED] > 0) && (flags[kFLAGS.KIHA_CAMP_WATCH] == 0 || !kihaFollower.followerKiha()) && !(flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && (flags[kFLAGS.SLEEP_WITH] == "Marble" || flags[kFLAGS.SLEEP_WITH] == "")) && (flags[kFLAGS.IN_INGNAM] == 0 && flags[kFLAGS.IN_PRISON] == 0)) {
					//impScene.impGangabangaEXPLOSIONS();
					//doNext(playerMenu);
					//return true;
				//}
				//else if (flags[kFLAGS.KIHA_CAMP_WATCH] > 0 && kihaFollower.followerKiha()) {
					//outputText("\n<b>You find charred imp carcasses all around the camp once you wake.  It looks like Kiha repelled a swarm of the little bastards.</b>\n");
					//needNext = true;
				//}
				//else if (flags[kFLAGS.HEL_GUARDING] > 0 && helFollower.followerHel()) {
					//outputText("\n<b>Helia informs you over a mug of beer that she whupped some major imp asshole last night.  She wiggles her tail for emphasis.</b>\n");
					//needNext = true;
				//}
				//else if (player.gender > 0 && player.hasStatusEffect(StatusEffects.JojoNightWatch) && player.hasStatusEffect(StatusEffects.PureCampJojo)) {
					//outputText("\n<b>Jojo informs you that he dispatched a crowd of imps as they tried to sneak into camp in the night.</b>\n");
					//needNext = true;
				//}
				//else if (flags[kFLAGS.HOLLI_DEFENSE_ON] > 0) {
					//outputText("\n<b>During the night, you hear distant screeches of surprise, followed by orgasmic moans.  It seems some imps found their way into Holli's canopy...</b>\n");
					//needNext = true;
				//}
				//else if (flags[kFLAGS.ANEMONE_WATCH] > 0) {
					//outputText("\n<b>Your sleep is momentarily disturbed by the sound of tiny clawed feet skittering away in all directions.  When you sit up, you can make out Kid A holding a struggling, concussed imp in a headlock and wearing a famished expression.  You catch her eye and she sheepishly retreats to a more urbane distance before beginning her noisy meal.</b>\n");
					//needNext = true;
				//}
				//else if (flags[kFLAGS.CAMP_BUILT_CABIN] > 0 && flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && (flags[kFLAGS.SLEEP_WITH] == "Marble" || flags[kFLAGS.SLEEP_WITH] == "") && (player.inte / 5) >= rand(15)) {
					//outputText("\n<b>Your sleep is momentarily disturbed by the sound of imp hands banging against your cabin door. Fortunately, you locked the door before you went to sleep.</b>\n");
					//needNext = true;
				//}
			//}
			////wormgasms
			//else if (flags[kFLAGS.EVER_INFESTED] == 1 && rand(100) <= 4 && player.hasCock() && !player.hasStatusEffect(StatusEffects.Infested)) {
				//if (player.hasCock() && !(player.hasStatusEffect(StatusEffects.JojoNightWatch) && player.hasStatusEffect(StatusEffects.PureCampJojo)) && (flags[kFLAGS.HEL_GUARDING] == 0 || !helFollower.followerHel()) && flags[kFLAGS.ANEMONE_WATCH] == 0 && (flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && flags[kFLAGS.SLEEP_WITH] == "")) {
					//kGAMECLASS.mountain.wormsScene.nightTimeInfestation();
					//return true;
				//}
				//else if (flags[kFLAGS.CAMP_CABIN_FURNITURE_BED] > 0 && flags[kFLAGS.SLEEP_WITH] == "") {
					//outputText("\n<b>You hear the sound of a horde of worms banging against the door. Good thing you locked it before you went to sleep!</b>\n");
					//needNext = true;
				//}
				//else if (flags[kFLAGS.HEL_GUARDING] > 0 && helFollower.followerHel()) {
					//outputText("\n<b>Helia informs you over a mug of beer that she stomped a horde of gross worms into paste.  She shudders after at the memory.</b>\n");
					//needNext = true;
				//}
				//else if (player.gender > 0 && player.hasStatusEffect(StatusEffects.JojoNightWatch) && player.hasStatusEffect(StatusEffects.PureCampJojo)) {
					//outputText("\n<b>Jojo informs you that he dispatched a horde of tiny, white worms as they tried to sneak into camp in the night.</b>\n");
					//needNext = true;
				//}
				//else if (flags[kFLAGS.ANEMONE_WATCH] > 0) {
					//outputText("\n<b>Kid A seems fairly well fed in the morning, and you note a trail of slime leading off in the direction of the lake.</b>\n"); // Yeah, blah blah travel weirdness. Quickfix so it seems logically correct.
					//needNext = true;
				//}
				//}
			//}
	//}

}