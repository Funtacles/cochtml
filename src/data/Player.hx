package data;
import data.inventory.Armor;
import data.body.enums.*;
import data.game.perk.Perk;
import data.game.perk.PerkStore;
import enums.Stat;

/**
 * ...
 * @author Funtacles
 */
class Player extends Character 
{
	public var startingRace:String;
	
	//Autosave
	public var slotName:String = "VOID";
	public var autoSave:Bool = false;

	public var hunger:Int = 0;
	
	public var perks:Array<String>;
	
	//Perks used to store 'queued' perk buys
	public var perkPoints:Int = 0;
	public var statPoints:Int = 0;
	public var ascensionPerkPoints:Int = 0;
	
	public function new() {
		super();
		
		perks = new Array<String>();
		
		startingRace = "human";
		
		hp = 0;
		fatigue = 0;
	}

	
	//Simplish way to track player sexual orientation.
	//50 = bisexual/omnisexual
	//0 = attracted to female characteristics
	//100 = attracted to male characteristics
	public var sexOrientation:Int = 50;
		
		//public var tempStr:Number = 0;
		//public var tempTou:Number = 0;
		//public var tempSpe:Number = 0;
		//public var tempInt:Number = 0;
//
		////Player pregnancy variables and functions
		//override public function pregnancyUpdate():Boolean {
			//return game.pregnancyProgress.updatePregnancy(); //Returns true if we need to make sure pregnancy texts aren't hidden
		//}
//
		//// Inventory
		//public var itemSlot1:ItemSlotClass;
		//public var itemSlot2:ItemSlotClass;
		//public var itemSlot3:ItemSlotClass;
		//public var itemSlot4:ItemSlotClass;
		//public var itemSlot5:ItemSlotClass;
		//public var itemSlot6:ItemSlotClass;
		//public var itemSlot7:ItemSlotClass;
		//public var itemSlot8:ItemSlotClass;
		//public var itemSlot9:ItemSlotClass;
		//public var itemSlot10:ItemSlotClass;
		//public var itemSlots:/*ItemSlotClass*/Array;
		//
		//public var prisonItemSlots:Array = [];
		//public var previouslyWornClothes:/*Armor*/Array = []; //For tracking achievement.
		//
		//private var _weapon:Weapon = WeaponLib.FISTS;
		//private var _armor:Armor = ArmorLib.COMFORTABLE_UNDERCLOTHES;
		//private var _jewelry:Jewelry = JewelryLib.NOTHING;
		////private var _jewelry2:Jewelry = JewelryLib.NOTHING;
		//private var _shield:Shield = ShieldLib.NOTHING;
		//private var _upperGarment:Undergarment = UndergarmentLib.NOTHING;
		//private var _lowerGarment:Undergarment = UndergarmentLib.NOTHING;
		//private var _modArmorName:String = "";
		//
		//public function getBonusStrength():Int{
			//var returnv:Int = 0;
			//if (findPerk(PerkLib.PotentPregnancy) >= 0 && isPregnant()) returnv += 10;
			//if (hasStatusEffect(StatusEffects.Might)) returnv += statusEffectv1(StatusEffects.Might);
			//if (hasStatusEffect(StatusEffects.ParasiteQueen)) returnv += statusEffectv1(StatusEffects.ParasiteQueen);
			//if (hasStatusEffect(StatusEffects.AnemoneVenom)) returnv -= statusEffectv1(StatusEffects.AnemoneVenom);
			//if (hasStatusEffect(StatusEffects.AmilyVenom)) returnv -= statusEffectv1(StatusEffects.AmilyVenom);
			//if (hasStatusEffect(StatusEffects.GiantStrLoss)) returnv -= statusEffectv1(StatusEffects.GiantStrLoss);
			//if (hasStatusEffect(StatusEffects.DriderIncubusVenom)) returnv -= statusEffectv1(StatusEffects.DriderIncubusVenom);
			//if (returnv > 500) returnv = 500; //try to keep things at least a little bit sane.
			//return returnv;
		//}
		//
		//public function getBonusToughness():Int{
			//var returnv:Int = 0;
			//if (findPerk(PerkLib.PotentPregnancy) >= 0 && isPregnant()) returnv += 10;
			//if (hasStatusEffect(StatusEffects.Might)) returnv += statusEffectv1(StatusEffects.Might);
			//if (hasStatusEffect(StatusEffects.ParasiteQueen)) returnv += statusEffectv1(StatusEffects.ParasiteQueen);
			//if (hasStatusEffect(StatusEffects.NagaSentVenom)) returnv -= statusEffectv1(StatusEffects.NagaSentVenom);
			//if (returnv > 500) returnv = 500; //try to keep things at least a little bit sane.
			//return returnv;
		//}
//
		//public function getBonusSpeed():Int{
			//var returnv:Int = 0;
			//if (hasStatusEffect(StatusEffects.ParasiteQueen)) returnv += statusEffectv1(StatusEffects.ParasiteQueen);
			//if (hasStatusEffect(StatusEffects.Web)) returnv -= statusEffectv1(StatusEffects.Web);
			//if (hasStatusEffect(StatusEffects.AnemoneVenom)) returnv -= statusEffectv2(StatusEffects.AnemoneVenom);
			//if (hasStatusEffect(StatusEffects.NagaVenom)) returnv -= statusEffectv1(StatusEffects.NagaVenom);
			//if (hasStatusEffect(StatusEffects.AmilyVenom)) returnv -= statusEffectv2(StatusEffects.AmilyVenom);
			//if (hasStatusEffect(StatusEffects.CalledShot)) returnv -= statusEffectv1(StatusEffects.CalledShot);
			//if (hasStatusEffect(StatusEffects.GnollSpear)) returnv -= statusEffectv1(StatusEffects.GnollSpear);
			//if (hasStatusEffect(StatusEffects.BasiliskSlow)) returnv -= statusEffectv1(StatusEffects.BasiliskSlow);
			//if (hasStatusEffect(StatusEffects.GardenerSapSpeed)) returnv -= statusEffectv1(StatusEffects.GardenerSapSpeed);
			//if (returnv > 500) returnv = 500; //try to keep things at least a little bit sane.
			//return returnv;
		//}
//
		//public function getBonusIntelligence():Number{
			//var returnv:Number = 0;
			//if (hasStatusEffect(StatusEffects.Revelation)) returnv += statusEffectv2(StatusEffects.Revelation);
			//if (statusEffectv1(StatusEffects.Resolve) == 4) returnv -= statusEffectv3(StatusEffects.Resolve);
			//if (kGAMECLASS.monster.hasStatusEffect(StatusEffects.TwuWuv)) returnv -= kGAMECLASS.monster.statusEffectv1(StatusEffects.TwuWuv);
			//if (hasStatusEffect(StatusEffects.NagaSentVenom)) returnv -= statusEffectv1(StatusEffects.NagaSentVenom);
			//if (returnv > 500) returnv = 500; //try to keep things at least a little bit sane.
			//return returnv;
		//}
		//

		
	public function bodyType():String
	{
		var desc:String = "";
		//SUPAH THIN
		if (thickness < 10)
		{
			//SUPAH BUFF
			if (tone > 90)
				desc += "a lithe body covered in highly visible muscles";
			else if (tone > 75)
				desc += "an incredibly thin, well-muscled frame";
			else if (tone > 50)
				desc += "a very thin body that has a good bit of muscle definition";
			else if (tone > 25)
				desc += "a lithe body and only a little bit of muscle definition";
			else
				desc += "a waif-thin body, and soft, forgiving flesh";
		}
		//Pretty thin
		else if (thickness < 25)
		{
			if (tone > 90)
				desc += "a thin body and incredible muscle definition";
			else if (tone > 75)
				desc += "a narrow frame that shows off your muscles";
			else if (tone > 50)
				desc += "a somewhat lithe body and a fair amount of definition";
			else if (tone > 25)
				desc += "a narrow, soft body that still manages to show off a few muscles";
			else
				desc += "a thin, soft body";
		}
		//Somewhat thin
		else if (thickness < 40)
		{
			if (tone > 90)
				desc += "a fit, somewhat thin body and rippling muscles all over";
			else if (tone > 75)
				desc += "a thinner-than-average frame and great muscle definition";
			else if (tone > 50)
				desc += "a somewhat narrow body and a decent amount of visible muscle";
			else if (tone > 25)
				desc += "a moderately thin body, soft curves, and only a little bit of muscle";
			else
				desc += "a fairly thin form and soft, cuddle-able flesh";
		}
		//average
		else if (thickness < 60)
		{
			if (tone > 90)
				desc += "average thickness and a bevy of perfectly defined muscles";
			else if (tone > 75)
				desc += "an average-sized frame and great musculature";
			else if (tone > 50)
				desc += "a normal waistline and decently visible muscles";
			else if (tone > 25)
				desc += "an average body and soft, unremarkable flesh";
			else
				desc += "an average frame and soft, untoned flesh with a tendency for jiggle";
		}
		else if (thickness < 75)
		{
			if (tone > 90)
				desc += "a somewhat thick body that's covered in slabs of muscle";
			else if (tone > 75)
				desc += "a body that's a little bit wide and has some highly-visible muscles";
			else if (tone > 50)
				desc += "a solid build that displays a decent amount of muscle";
			else if (tone > 25)
				desc += "a slightly wide frame that displays your curves and has hints of muscle underneath";
			else
				desc += "a soft, plush body with plenty of jiggle";
		}
		else if (thickness < 90)
		{
			if (tone > 90)
				desc += "a thickset frame that gives you the appearance of a wall of muscle";
			else if (tone > 75)
				desc += "a burly form and plenty of muscle definition";
			else if (tone > 50)
				desc += "a solid, thick frame and a decent amount of muscles";
			else if (tone > 25)
				desc += "a wide-set body, some soft, forgiving flesh, and a hint of muscle underneath it";
			else
			{
				desc += "a wide, cushiony body";
				if (getGender() == Gender.Female || getGender() == Gender.Herm || biggestTitSize().getIndex() > 3 || hipRating > 7 || buttRating > 7)
					desc += " and plenty of jiggle on your curves";
			}
		}
		//Chunky monkey
		else
		{
			if (tone > 90)
				desc += "an extremely thickset frame and so much muscle others would find you harder to move than a huge boulder";
			else if (tone > 75)
				desc += "a very wide body and enough muscle to make you look like a tank";
			else if (tone > 50)
				desc += "an extremely substantial frame packing a decent amount of muscle";
			else if (tone > 25)
			{
				desc += "a very wide body";
				if (getGender() == Gender.Female || getGender() == Gender.Herm || biggestTitSize().getIndex() > 4 || hipRating > 10 || buttRating > 10)
					desc += ", lots of curvy jiggles,";
				desc += " and hints of muscle underneath";
			}
			else
			{
				desc += "a thick";
				if (getGender() == Gender.Female || getGender() == Gender.Herm || biggestTitSize().getIndex() > 4 || hipRating > 10 || buttRating> 10)
					desc += ", voluptuous";
				desc += " body and plush, ";
				if (getGender() == Gender.Female || getGender() == Gender.Herm || biggestTitSize().getIndex() > 4 || hipRating > 10 || buttRating > 10)
					desc += " jiggly curves";
				else
					desc += " soft flesh";
			}
		}
		return desc;
	}
	
	public function getRace():String
	{
		//Determine race type:
		var race:String = "human";
		if (catScore() >= 4) 
		{
			if (isTaur() && lowerBody.type == LowerBodyType.Cat) {
				race = "cat-taur";
				if (faceType == FaceType.Human)
					race = "sphinx-morph"; // no way to be fully feral anyway
			}
			else {
				race = "cat-morph";
				if (faceType == FaceType.Human)
					race = "cat-" + maleOrFemale("boy", "girl");
			}
		}
		if (lizardScore() >= 4)
		{
			if (wings.type == WingType.DraconicLarge || wings.type == WingType.DraconicSmall) //&& hasDragonFire())
				race = eyeType == EyeType.Basilisk ? "dracolisk" : "dragonewt";
			else
				race = eyeType == EyeType.Basilisk ? "basilisk"  : "lizan";
			if (isTaur())
				race += "-taur";
			if (lizardScore() >= 9)
				return race; // High lizardScore? always return lizan-race
		}
		if (dragonScore() >= 6)
		{
			race = "dragon-morph";
			if (faceType == FaceType.Human)
				race = "dragon-" + maleOrFemale("man", "girl");
			if (isTaur())
				race = "dragon-taur";
		}
		if (cockatriceScore() >= 4)
		{
			race = "cockatrice-morph";
			if (cockatriceScore() >= 8)
				race = "cockatrice";
			if (faceType == FaceType.Human)
				race = "cockatrice-" + maleOrFemale("boy", "girl");
			if (isTaur())
				race = "cockatrice-taur";
		}
		if (raccoonScore() >= 4)
		{
			race = "raccoon-morph";
			if (balls > 0 && ballSize > 5)
				race = "tanuki-morph";
			if (isTaur())
				race = "raccoon-taur";
		}
		if (sheepScore() >= 4) {
			if (lowerBody.legCount == 4 && lowerBody.type == LowerBodyType.ClovenHoofed) {
				race = "sheep-taur";
			}
			else if (getGender().getIndex() == 0 || getGender().getIndex() == 3) {
				race = "sheep-morph";
			}
			else if (getGender().getIndex() == 1 && hornType == HornType.Ram) {
				race = "ram-morph";
			}
			else {
				race = "sheep-" +maleOrFemale("boy","girl");
			}
		}
		if (ferretScore() >= 4)
		{
			if (skin.isFurry())
				race = "ferret-morph";
			else
				race = "ferret-" + maleOrFemale("morph", "girl");
		}
		
		if (mutantScore() >= 5 && race == "human")
			race = "corrupted mutant";
		if (humanScore() >= 5 && race == "corrupted mutant")
			race = "somewhat human mutant";
		
		return race;
	}

	//cockatrice rating
	public function cockatriceScore():Int
	{
		var cockatriceCounter:Int = 0;
		if (earType == EarType.Cockatrice)
			cockatriceCounter++;
		if (tail != null && tail.type == TailType.Cockatrice)
			cockatriceCounter++;
		if (lowerBody.type == LowerBodyType.Cockatrice)
			cockatriceCounter++;
		if (faceType == FaceType.Cockatrice)
			cockatriceCounter++;
		if (eyeType == EyeType.Cockatrice)
			cockatriceCounter++;
		if (armType == ArmType.Cockatrice)
			cockatriceCounter++;
		if (antennae == AntennaeType.Cockatrice)
			cockatriceCounter++;
		if (cockatriceCounter > 2) {
			if (tongueType == TongueType.Lizard)
				cockatriceCounter++;
			if (wings.type == WingType.LargeFeathered)
				cockatriceCounter++;
			if (skin.type == SkinType.LizardScales)
				cockatriceCounter++;
			if (getCocksOfType(PenisType.Lizard).length > 0)
				cockatriceCounter++;
		}
		return cockatriceCounter;
	}

	//Determine Human Rating
	public function humanScore():Int
	{
		var humanCounter:Int = 0;
		if (faceType == FaceType.Human)
			humanCounter++;
		if (skin.type == SkinType.Normal)
			humanCounter++;
		if (horns == 0)
			humanCounter++;
		if (tail != null && tail.type == TailType.None)
			humanCounter++;
		if (wings == null || wings.type == WingType.None)
			humanCounter++;
		if (lowerBody.type == LowerBodyType.Human)
			humanCounter++;
		if (getCocksOfType(PenisType.Human).length == 1 && cocks.length == 1)
			humanCounter++;
		if (breastRows.length == 1 && skin.type == SkinType.Normal)
			humanCounter++;
		return humanCounter;
	}

	//Determine Ferret Rating!
	public function ferretScore():Int
	{
		var counter:Int = 0;
		if (faceType == FaceType.FerretMask) counter++;
		if (faceType == FaceType.Ferret) counter+=2;
		if (earType == EarType.Ferret) counter++;
		if (tail != null && tail.type == TailType.Ferret) counter++;
		if (lowerBody.type == LowerBodyType.Ferret) counter++;
		if (skin.isFurry() && counter > 0) counter++;
		return counter;
	}

	public function raccoonScore():Int
	{
		var coonCounter:Int = 0;
		if (faceType == FaceType.RacoonMask)
			coonCounter++;
		if (faceType == FaceType.Racoon)
			coonCounter += 2;
		if (earType == EarType.Racoon)
			coonCounter++;
		if (tail != null && tail.type == TailType.Racoon)
			coonCounter++;
		if (lowerBody.type == LowerBodyType.Racoon)
			coonCounter++;
		if (coonCounter > 0 && balls > 0)
			coonCounter++;
		//Fur only counts if some canine features are present
		if (skin.isFurry() && coonCounter > 0)
			coonCounter++;
		return coonCounter;
	}

	//Determine cat Rating
	public function catScore():Int
	{
		var catCounter:Int = 0;
		if (faceType == FaceType.Cat)
			catCounter++;
		if (earType == EarType.Cat)
			catCounter++;
		if (tail != null && tail.type == TailType.Cat )
			catCounter++;
		if (lowerBody.type == LowerBodyType.Cat)
			catCounter++;
		if (getCocksOfType(PenisType.Cat).length > 0)
			catCounter++;
		if (breastRows.length > 1 && catCounter > 0)
			catCounter++;
		if (breastRows.length == 3 && catCounter > 0)
			catCounter++;
		if (breastRows.length > 3)
			catCounter -= 2;
		//Fur only counts if some canine features are present
		if (skin.isFurry() && catCounter > 0)
			catCounter++;
		return catCounter;
	}

	//Determine lizard rating
	public function lizardScore():Int
	{
		var lizardCounter:Int = 0;
		if (faceType == FaceType.Lizard)
			lizardCounter++;
		if (earType == EarType.Lizard)
			lizardCounter++;
		if (tail != null && tail.type == TailType.Lizard)
			lizardCounter++;
		if (lowerBody.type == LowerBodyType.Lizard)
			lizardCounter++;
		if (hornType == HornType.Draconic2x)
			lizardCounter++;
		if (hornType == HornType.Draconic4x)
			lizardCounter += 2;
		if (armType == ArmType.Predator && clawType == ClawType.Lizard)
			lizardCounter++;
		if (eyeType == EyeType.Lizard)
			lizardCounter++;
		if (lizardCounter > 2) {
			if (tongueType == TongueType.Lizard || tongueType == TongueType.Snake)
				lizardCounter++;
			if (getCocksOfType(PenisType.Lizard).length > 0)
				lizardCounter++;
			if (eyeType == EyeType.Basilisk)
				lizardCounter++;
			if (skin.type == SkinType.DragonScales || skin.type == SkinType.LizardScales)
				lizardCounter++;
		}
		return lizardCounter;
	}

	//Determine Dragon Rating
	public function dragonScore():Int
	{
		var dragonCounter:Int = 0;
		if (faceType == FaceType.Dragon)
			dragonCounter++;
		if (earType == EarType.Dragon)
			dragonCounter++;
		if (tail != null && tail.type == TailType.Draconic)
			dragonCounter++;
		if (tongueType == TongueType.Draconic)
			dragonCounter++;
		if (getCocksOfType(PenisType.Dragon).length > 0)
			dragonCounter++;
		if (wings != null && (wings.type == WingType.DraconicLarge || wings.type == WingType.DraconicSmall))
			dragonCounter++;
		if (lowerBody.type == LowerBodyType.Dragon)
			dragonCounter++;
		if (skin.type == SkinType.DragonScales && dragonCounter > 0)
			dragonCounter++;
		if (hornType == HornType.Draconic2x)
			dragonCounter++;
		if (hornType == HornType.Draconic4x)
			dragonCounter += 2;
		if (hasPerk(PerkStore.Dragonfire))
			dragonCounter++;
		if (armType == ArmType.Predator && clawType == ClawType.Dragon)
			dragonCounter++;
		if (eyeType == EyeType.Dragon)
			dragonCounter++;
		return dragonCounter;
	}


	//Sheep score
	public function sheepScore():Int
	{
		var sheepCounter:Int = 0;
		if (earType == EarType.Sheep)
			sheepCounter++;
		if (hornType == HornType.Sheep)
			sheepCounter++;
		if (hornType == HornType.Ram)
			sheepCounter++;
		if (tail != null && tail.type == TailType.Sheep)
			sheepCounter++;
		if (lowerBody.type == LowerBodyType.ClovenHoofed && lowerBody.legCount == 2)
			sheepCounter++;
		if (hair.type == HairType.Wool)
			sheepCounter++;
		return sheepCounter;
	}

	//Determine Mutant Rating
	public function mutantScore():Int
	{
		var mutantCounter:Int = 0;
		if (faceType != FaceType.Human)
			mutantCounter++;
		if (tail != null && tail.type != TailType.None)
			mutantCounter++;
		if (cocks.length > 1)
			mutantCounter++;
		if (hasCock() && hasVagina())
			mutantCounter++;
		if (hasFuckableNipples())
			mutantCounter++;
		if (breastRows.length > 1)
			mutantCounter++;
		if (mutantCounter > 1 && skin.type == SkinType.Normal)
			mutantCounter++;
		return mutantCounter;
	}
		
		//public function lactationQ():Number
		//{
			//if (biggestLactation() < 1)
				//return 0;
			////(Milk production TOTAL= breastSize x 10 * lactationMultiplier * breast total * milking-endurance (1- default, maxes at 2.  Builds over time as milking as done)
			////(Small   0.01 mLs   Size 1 + 1 Multi)
			////(Large   0.8 - Size 10 + 4 Multi)
			////(HUGE   2.4 - Size 12 + 5 Multi + 4 tits)
			//
			//var total:Number = 0;
			//if (!hasStatusEffect(StatusEffects.LactationEndurance))
				//createStatusEffect(StatusEffects.LactationEndurance, 1, 0, 0, 0);
			//
			//var counter:Number = breastRows.length;
			//while (counter > 0) {
				//counter--;
				//total += 10 * breastRows[counter].breastRating * breastRows[counter].lactationMultiplier * breastRows[counter].breasts * statusEffectv1(StatusEffects.LactationEndurance);
				//
			//}
			//if (hasPerk(PerkLib.MilkMaid))
				//total += 200 + (perkv1(PerkLib.MilkMaid) * 100);
			//if (statusEffectv1(StatusEffects.LactationReduction) >= 48)
				//total = total * 1.5;
			//if (total > Int.MAX_VALUE)
				//total = Int.MAX_VALUE;
			//return total;
		//}
		//
		//public function isLactating():Boolean
		//{
			//if (lactationQ() > 0) return true;
			//return false;
		//}
//
		//public function cuntChange(cArea:Number, display:Boolean, spacingsF:Boolean = false, spacingsB:Boolean = true):Boolean {
			//if (vaginas.length==0) return false;
			//var wasVirgin:Boolean = vaginas[0].virgin;
			//var stretched:Boolean = cuntChangeNoDisplay(cArea);
			//var devirgined:Boolean = wasVirgin && !vaginas[0].virgin;
			//if (devirgined){
				//if (spacingsF) outputText("  ");
				//outputText("<b>Your hymen is torn, robbing you of your virginity.</b>");
				//if (spacingsB) outputText("  ");
			//}
			////STRETCH SUCCESSFUL - begin flavor text if outputting it!
			//if (display && stretched) {
				////Virgins get different formatting
				//if (devirgined) {
					////If no spaces after virgin loss
					//if (!spacingsB) outputText("  ");
				//}
				////Non virgins as usual
				//else if (spacingsF) outputText("  ");
				//if (vaginas[0].vaginalLooseness == VAGINA_LOOSENESS_LEVEL_CLOWN_CAR) outputText("<b>Your " + Appearance.vaginaDescript(this,0)+ " is stretched painfully wide, large enough to accommodate most beasts and demons.</b>");
				//if (vaginas[0].vaginalLooseness == VAGINA_LOOSENESS_GAPING_WIDE) outputText("<b>Your " + Appearance.vaginaDescript(this,0) + " is stretched so wide that it gapes continually.</b>");
				//if (vaginas[0].vaginalLooseness == VAGINA_LOOSENESS_GAPING) outputText("<b>Your " + Appearance.vaginaDescript(this,0) + " painfully stretches, the lips now wide enough to gape slightly.</b>");
				//if (vaginas[0].vaginalLooseness == VAGINA_LOOSENESS_LOOSE) outputText("<b>Your " + Appearance.vaginaDescript(this,0) + " is now very loose.</b>");
				//if (vaginas[0].vaginalLooseness == VAGINA_LOOSENESS_NORMAL) outputText("<b>Your " + Appearance.vaginaDescript(this,0) + " is now a little loose.</b>");
				//if (vaginas[0].vaginalLooseness == VAGINA_LOOSENESS_TIGHT) outputText("<b>Your " + Appearance.vaginaDescript(this,0) + " is stretched out to a more normal size.</b>");
				//if (spacingsB) outputText("  ");
			//}
			//return stretched;
		//}
//
		//public function buttChange(cArea:Number, display:Boolean, spacingsF:Boolean = true, spacingsB:Boolean = true):Boolean
		//{
			//var stretched:Boolean = buttChangeNoDisplay(cArea);
			////STRETCH SUCCESSFUL - begin flavor text if outputting it!
			//if (stretched && display) {
				//if (spacingsF) outputText("  ");
				//buttChangeDisplay();
				//if (spacingsB) outputText("  ");
			//}
			//return stretched;
		//}
//
		///**
		 //* Refills player's hunger. 'amnt' is how much to refill, 'nl' determines if new line should be added before the notification.
		 //* @param	amnt
		 //* @param	nl
		 //*/
		//public function refillHunger(amnt:Number = 0, nl:Boolean = true):Void {
			//if (flags[kFLAGS.HUNGER_ENABLED] > 0 || flags[kFLAGS.IN_PRISON] > 0)
			//{
				//
				//var oldHunger:Number = hunger;
				//var weightChange:Int = 0;
				//
				//hunger += amnt;
				//if (hunger > maxHunger())
				//{
					//while (hunger > 110) {
						//weightChange++;
						//hunger -= 10;
					//}
					//modThickness(100, weightChange);
					//hunger = maxHunger();
				//}
				//if (hunger > oldHunger && flags[kFLAGS.USE_OLD_INTERFACE] == 0) kGAMECLASS.mainView.statsView.showStatUp('hunger');
				////game.dynStats("lus", 0, "resisted");
				//if (nl) outputText("\n");
				////Messages
				//if (hunger < 10) outputText("<b>You still need to eat more. </b>");
				//else if (hunger >= 10 && hunger < 25) outputText("<b>You are no longer starving but you still need to eat more. </b>");
				//else if (hunger >= 25 && hunger100 < 50) outputText("<b>The growling sound in your stomach seems to quiet down. </b>");
				//else if (hunger100 >= 50 && hunger100 < 75) outputText("<b>Your stomach no longer growls. </b>");
				//else if (hunger100 >= 75 && hunger100 < 90) outputText("<b>You feel so satisfied. </b>");
				//else if (hunger100 >= 90) outputText("<b>Your stomach feels so full. </b>");
				//if (weightChange > 0) outputText("<b>You feel like you've put on some weight. </b>");
				//kGAMECLASS.awardAchievement("Tastes Like Chicken ", kACHIEVEMENTS.REALISTIC_TASTES_LIKE_CHICKEN);
				//if (oldHunger < 1 && hunger >= 100) kGAMECLASS.awardAchievement("Champion Needs Food Badly ", kACHIEVEMENTS.REALISTIC_CHAMPION_NEEDS_FOOD);
				//if (oldHunger >= 90) kGAMECLASS.awardAchievement("Glutton ", kACHIEVEMENTS.REALISTIC_GLUTTON);
				//if (hunger > oldHunger) kGAMECLASS.mainView.statsView.showStatUp("hunger");
				//game.dynStats("lus", 0, "resisted", false);
				//kGAMECLASS.statScreenRefresh();
			//}
		//}
		//
		///**
		 //* Damages player's hunger. 'amnt' is how much to deduct.
		 //* @param	amnt
		 //*/
		//public function damageHunger(amnt:Number = 0):Void {
			//var oldHunger:Number = hunger;
			//hunger -= amnt;
			//if (hunger < 0) hunger = 0;
			//if (hunger < oldHunger && flags[kFLAGS.USE_OLD_INTERFACE] == 0) kGAMECLASS.mainView.statsView.showStatDown('hunger');
			//game.dynStats("lus", 0, "resisted", false);
		//}
		//
		public function corruptionTolerance():Int {
			//var temp:Int = perkv1(PerkLib.AscensionTolerance) * 5 * (1 - perkv2(PerkLib.AscensionTolerance));
			//if (flags[kFLAGS.MEANINGLESS_CORRUPTION] > 0) temp += 100;
			//return temp;
			return 0;
		}
//
		//public function buttChangeDisplay():Void
		//{	//Allows the test for stretching and the text output to be separated
			//if (ass.analLooseness == 5) outputText("<b>Your " + Appearance.assholeDescript(this) + " is stretched even wider, capable of taking even the largest of demons and beasts.</b>");
			//if (ass.analLooseness == 4) outputText("<b>Your " + Appearance.assholeDescript(this) + " becomes so stretched that it gapes continually.</b>");
			//if (ass.analLooseness == 3) outputText("<b>Your " + Appearance.assholeDescript(this) + " is now very loose.</b>");
			//if (ass.analLooseness == 2) outputText("<b>Your " + Appearance.assholeDescript(this) + " is now a little loose.</b>");
			//if (ass.analLooseness == 1) outputText("<b>You have lost your anal virginity.</b>");
		//}
//
		//public function slimeFeed():Void{
			//if (hasStatusEffect(StatusEffects.SlimeCraving)) {
				////Reset craving value
				//changeStatusValue(StatusEffects.SlimeCraving,1,0);
				////Flag to display feed update and restore stats in event parser
				//if (!hasStatusEffect(StatusEffects.SlimeCravingFeed)) {
					//createStatusEffect(StatusEffects.SlimeCravingFeed,0,0,0,0);
				//}
			//}
			//if (hasPerk(PerkLib.Diapause)) {
				//flags[kFLAGS.DIAPAUSE_FLUID_AMOUNT] += 3 + Utils.rand(3);
				//flags[kFLAGS.DIAPAUSE_NEEDS_DISPLAYING] = 1;
			//}
//
		//}
//
		//public function minoCumAddiction(raw:Number = 10):Void {
			////Increment minotaur cum intake count
			//flags[kFLAGS.MINOTAUR_CUM_INTAKE_COUNT]++;
			////Fix if variables go out of range.
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] < 0) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0;
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] < 0) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] = 0;
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] > 120) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 120;
//
			////Turn off withdrawal
			////if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] > 1) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] = 1;
			////Reset counter
			//flags[kFLAGS.TIME_SINCE_LAST_CONSUMED_MINOTAUR_CUM] = 0;
			////If highly addicted, rises slower
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] >= 60) raw /= 2;
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] >= 80) raw /= 2;
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] >= 90) raw /= 2;
			//if (hasPerk(PerkLib.MinotaurCumResistance)) raw *= 0;
			////If in withdrawl, readdiction is potent!
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] == 3) raw += 10;
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] == 2) raw += 5;
			//raw = Math.round(raw * 100)/100;
			////PUT SOME CAPS ON DAT' SHIT
			//if (raw > 50) raw = 50;
			//if (raw < -50) raw = -50;
			//flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] += raw;
			////Recheck to make sure shit didn't break
			//if (hasPerk(PerkLib.MinotaurCumResistance)) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0; //Never get addicted!
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] > 120) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 120;
			//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] < 0) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0;
//
		//}
//
		//public function spellMod():Number {
			//var mod:Number = 1;
			//if (hasPerk(PerkLib.Archmage) && inte >= 75) mod += .5;
			//if (hasPerk(PerkLib.Channeling) && inte >= 60) mod += .5;
			//if (hasPerk(PerkLib.Mage) && inte >= 50) mod += .5;
			//if (hasPerk(PerkLib.Spellpower) && inte >= 50) mod += .5;
			//if (findPerk(PerkLib.MysticLearnings) >= 0 && inte > 100) mod += .25;
			//mod += perkv1(PerkLib.WizardsFocus);
			//if (hasPerk(PerkLib.ChiReflowMagic)) mod += UmasShop.NEEDLEWORK_MAGIC_SPELL_MULTI;
			//if (jewelryEffectId == JewelryLib.MODIFIER_SPELL_POWER) mod += (jewelryEffectMagnitude / 100);
			//if (countCockSocks("blue") > 0) mod += (countCockSocks("blue") * .05);
			//if (hasPerk(PerkLib.AscensionMysticality)) mod *= 1 + (perkv1(PerkLib.AscensionMysticality) * 0.05);
			////Powerful. Anger is power - unleash it! 	
			//if (statusEffectv1(StatusEffects.Resolve) == 5) mod *= statusEffectv3(StatusEffects.Resolve);
			////Depressed. There can be no hope in this hell, no hope at all.
			//if (statusEffectv1(StatusEffects.Resolve) == 6) mod *= statusEffectv3(StatusEffects.Resolve);
			//return mod;
		//}
		//
		//public function hasSpells():Bool
		//{
			//return spellCount() > 0;
		//}
		//
		//public function spellCount():Int
		//{
			//return [StatusEffects.KnowsArouse, StatusEffects.KnowsHeal, StatusEffects.KnowsMight, StatusEffects.KnowsCharge, StatusEffects.KnowsBlind, StatusEffects.KnowsWhitefire, StatusEffects.KnowsBlackfire, StatusEffects.KnowsTKBlast, StatusEffects.KnowsLeech].filter(function(item:StatusEffectType, index:Int, array:Array):Boolean {
						//return this.hasStatusEffect(item); }, this).length;
		//}
		
		//public function spellCost(mod:Number):Number {
			////Addiditive mods
			//var costPercent:Number = 100;
			//if (hasPerk(PerkLib.SpellcastingAffinity)) costPercent -= perkv1(PerkLib.SpellcastingAffinity);
			//if (hasPerk(PerkLib.WizardsEndurance)) costPercent -= perkv1(PerkLib.WizardsEndurance);
			//
			////Limiting it and multiplicative mods
			//if (hasPerk(PerkLib.BloodMage) && costPercent < 50) costPercent = 50;
			//
			//mod *= costPercent/100;
			//
			//if (hasPerk(PerkLib.HistoryScholar) || hasPerk(PerkLib.HistoryScholar2)) {
				//if (mod > 2) mod *= .8;
			//}
			//if (hasPerk(PerkLib.BloodMage) && mod < 5) mod = 5;
			//else if (mod < 2) mod = 2;
			//
			//mod = Math.round(mod * 100)/100;
			//return mod;
		//}
		//
		//public function physicalCost(mod:Number):Number {
			//var costPercent:Number = 100;
			//if (hasPerk(PerkLib.IronMan)) costPercent -= 50;
			//mod *= costPercent/100;
			//return mod;
		//}
		//
		////Modify fatigue
		////types:
		////  0 - normal
		////	1 - magic
		////	2 - physical
		////	3 - non-bloodmage magic
		//public function changeFatigue(mod:Number,type:Number  = 0):Void {
			////Spell reductions
			//if (type == 1) {
				//mod = spellCost(mod);
				//
				////Blood mages use HP for spells
				//if (hasPerk(PerkLib.BloodMage)) {
					//takeDamage(mod);
					//return;
				//}                
			//}
			////Physical special reductions
			//if (type == 2) {
				//mod = physicalCost(mod);
			//}
			//if (type == 3) {
				//mod = spellCost(mod);
			//}
			//if (fatigue >= maxFatigue() && mod > 0) return;
			//if (fatigue <= 0 && mod < 0) return;
			////Fatigue restoration buffs!
			//if (mod < 0) {
				//var multi:Number = 1;
				//
				//if (hasPerk(PerkLib.HistorySlacker)) multi *= 1.2;
				//if (hasPerk(PerkLib.ControlledBreath) && cor < (30 + corruptionTolerance())) multi *= 1.1;
				//if (hasPerk(PerkLib.SpeedyRecovery)) multi *= 1.5;
				//
				//mod *= multi;
			//}
			//fatigue += mod;
			//if (mod > 0) {
				//game.mainView.statsView.showStatUp( 'fatigue' );
				//// fatigueUp.visible = true;
				//// fatigueDown.visible = false;
			//}
			//if (mod < 0) {
				//game.mainView.statsView.showStatDown( 'fatigue' );
				//// fatigueDown.visible = true;
				//// fatigueUp.visible = false;
			//}
			//kGAMECLASS.dynStats("lus", 0, "resisted", false); //Force display fatigue up/down by invoking zero lust change.
			//if (fatigue > maxFatigue()) fatigue = maxFatigue();
			//if (fatigue < 0) fatigue = 0;
			//kGAMECLASS.statScreenRefresh();
		//}
		//
		//public function armorDescript(nakedText:String = "gear"):String
		//{
			//var textArray:Array = [];
			//var text:String = "";
			////if (armor != ArmorLib.NOTHING) text += armorName;
			////Join text.
			//if (armor != ArmorLib.NOTHING) textArray.push(armor.name);
			//if (upperGarment != UndergarmentLib.NOTHING) textArray.push(upperGarmentName);
			//if (lowerGarment != UndergarmentLib.NOTHING) textArray.push(lowerGarmentName);
			//if (textArray.length > 0) text = Utils.formatStringArray(textArray);
			////Naked?
			//if (upperGarment == UndergarmentLib.NOTHING && lowerGarment == UndergarmentLib.NOTHING && armor == ArmorLib.NOTHING) text = nakedText;
			//return text;
		//}
		//
		//public function clothedOrNaked(clothedText:String, nakedText:String = ""):String
		//{
			//return (armorDescript() != "gear" ? clothedText : nakedText);
		//}
		//
		//public function clothedOrNakedLower(clothedText:String, nakedText:String = ""):String
		//{
			//return (armorName != "gear" && (armorName != "lethicite armor" && lowerGarmentName == "nothing") && !isTaur() ? clothedText : nakedText);
		//}
		//
		//public function equipItem(armor:Armor):Void {
			//for (i in 0...previouslyWornClothes.length) {
				//if (previouslyWornClothes[i] == armor.shortName) return; //Already have?
			//}
			//previouslyWornClothes.push(armor.shortName);
		//}
		//
		//public function shrinkTits(ignore_hyper_happy:Boolean=false):Void
		//{
			//if (flags[kFLAGS.HYPER_HAPPY] && !ignore_hyper_happy)
			//{
				//return;
			//}
			//if (breastRows.length == 1) {
				//if (breastRows[0].breastRating > 0) {
					////Shrink if bigger than N/A cups
					//var temp:Number;
					//temp = 1;
					//breastRows[0].breastRating--;
					////Shrink again 50% chance
					//if (breastRows[0].breastRating >= 1 && Utils.rand(2) == 0 && !hasPerk(PerkLib.BigTits)) {
						//temp++;
						//breastRows[0].breastRating--;
					//}
					//if (breastRows[0].breastRating < 0) breastRows[0].breastRating = 0;
					////Talk about shrinkage
					//if (temp == 1) outputText("\n\nYou feel a weight lifted from you, and realize your breasts have shrunk!  With a quick measure, you determine they're now " + breastCup(0) + "s.");
					//if (temp == 2) outputText("\n\nYou feel significantly lighter.  Looking down, you realize your breasts are much smaller!  With a quick measure, you determine they're now " + breastCup(0) + "s.");
				//}
			//}
			//else if (breastRows.length > 1) {
				////multiple
				//outputText("\n");
				////temp2 = amount changed
				////temp3 = counter
				//var temp2:Number = 0;
				//var temp3:Number = breastRows.length;
				//while(temp3 > 0) {
					//temp3--;
					//if (breastRows[temp3].breastRating > 0) {
						//breastRows[temp3].breastRating--;
						//if (breastRows[temp3].breastRating < 0) breastRows[temp3].breastRating = 0;
						//temp2++;
						//outputText("\n");
						//if (temp3 < breastRows.length - 1) outputText("...and y");
						//else outputText("Y");
						//outputText("our " + breastDescript(temp3) + " shrink, dropping to " + breastCup(temp3) + "s.");
					//}
					//if (breastRows[temp3].breastRating < 0) breastRows[temp3].breastRating = 0;
				//}
				//if (temp2 == 2) outputText("\nYou feel so much lighter after the change.");
				//if (temp2 == 3) outputText("\nWithout the extra weight you feel particularly limber.");
				//if (temp2 >= 4) outputText("\nIt feels as if the weight of the world has been lifted from your shoulders, or in this case, your chest.");
			//}
		//}
//
		//public function growTits(amount:Number, rowsGrown:Number, display:Boolean, growthType:Number):Void
		//{
			//if (breastRows.length == 0) return;
			////GrowthType 1 = smallest grows
			////GrowthType 2 = Top Row working downward
			////GrowthType 3 = Only top row
			//var temp2:Number = 0;
			//var temp3:Number = 0;
			////Chance for "big tits" perked characters to grow larger!
			//if (hasPerk(PerkLib.BigTits) && Utils.rand(3) == 0 && amount < 1) amount=1;
//
			//// Needs to be a number, since uint will round down to 0 prevent growth beyond a certain point
			//var temp:Number = breastRows.length;
			//if (growthType == 1) {
				////Select smallest breast, grow it, move on
				//while(rowsGrown > 0) {
					////Temp = counter
					//temp = breastRows.length;
					////Temp2 = smallest tits index
					//temp2 = 0;
					////Find smallest row
					//while(temp > 0) {
						//temp--;
						//if (breastRows[temp].breastRating < breastRows[temp2].breastRating) temp2 = temp;
					//}
					////Temp 3 tracks total amount grown
					//temp3 += amount;
					//trace("Breastrow chosen for growth: " + String(temp2) + ".");
					////Reuse temp to store growth amount for diminishing returns.
					//temp = amount;
					//if (!flags[kFLAGS.HYPER_HAPPY])
					//{
						////Diminishing returns!
						//if (breastRows[temp2].breastRating > 3)
						//{
							//if (!hasPerk(PerkLib.BigTits))
								//temp /=1.5;
							//else
								//temp /=1.3;
						//}
//
						//// WHy are there three options here. They all have the same result.
						//if (breastRows[temp2].breastRating > 7)
						//{
							//if (!hasPerk(PerkLib.BigTits))
								//temp /=2;
							//else
								//temp /=1.5;
						//}
						//if (breastRows[temp2].breastRating > 9)
						//{
							//if (!hasPerk(PerkLib.BigTits))
								//temp /=2;
							//else
								//temp /=1.5;
						//}
						//if (breastRows[temp2].breastRating > 12)
						//{
							//if (!hasPerk(PerkLib.BigTits))
								//temp /=2;
							//else
								//temp  /=1.5;
						//}
					//}
//
					////Grow!
					//trace("Growing breasts by ", temp);
					//breastRows[temp2].breastRating += temp;
					//rowsGrown--;
				//}
			//}
//
			//if (!flags[kFLAGS.HYPER_HAPPY])
			//{
				////Diminishing returns!
				//if (breastRows[0].breastRating > 3) {
					//if (!hasPerk(PerkLib.BigTits)) amount/=1.5;
					//else amount/=1.3;
				//}
				//if (breastRows[0].breastRating > 7) {
					//if (!hasPerk(PerkLib.BigTits)) amount/=2;
					//else amount /= 1.5;
				//}
				//if (breastRows[0].breastRating > 12) {
					//if (!hasPerk(PerkLib.BigTits)) amount/=2;
					//else amount /= 1.5;
				//}
			//}
			///*if (breastRows[0].breastRating > 12) {
				//if (hasPerk("Big Tits") < 0) amount/=2;
				//else amount /= 1.5;
			//}*/
			//if (growthType == 2) {
				//temp = 0;
				////Start at top and keep growing down, back to top if hit bottom before done.
				//while(rowsGrown > 0) {
					//if (temp+1 > breastRows.length) temp = 0;
					//breastRows[temp].breastRating += amount;
					//trace("Breasts increased by " + amount + " on row " + temp);
					//temp++;
					//temp3 += amount;
					//rowsGrown--;
				//}
			//}
			//if (growthType == 3) {
				//while(rowsGrown > 0) {
					//rowsGrown--;
					//breastRows[0].breastRating += amount;
					//temp3 += amount;
				//}
			//}
			////Breast Growth Finished...talk about changes.
			//trace("Growth amount = ", amount);
			//if (display) {
				//if (growthType < 3) {
					//if (amount <= 2)
					//{
						//if (breastRows.length > 1) outputText("Your rows of " + breastDescript(0) + " jiggle with added weight, growing a bit larger.");
						//if (breastRows.length == 1) outputText("Your " + breastDescript(0) + " jiggle with added weight as they expand, growing a bit larger.");
					//}
					//else if (amount <= 4)
					//{
						//if (breastRows.length > 1) outputText("You stagger as your chest gets much heavier.  Looking down, you watch with curiosity as your rows of " + breastDescript(0) + " expand significantly.");
						//if (breastRows.length == 1) outputText("You stagger as your chest gets much heavier.  Looking down, you watch with curiosity as your " + breastDescript(0) + " expand significantly.");
					//}
					//else
					//{
						//if (breastRows.length > 1) outputText("You drop to your knees from a massive change in your body's center of gravity.  Your " + breastDescript(0) + " tingle strongly, growing disturbingly large.");
						//if (breastRows.length == 1) outputText("You drop to your knees from a massive change in your center of gravity.  The tingling in your " + breastDescript(0) + " intensifies as they continue to grow at an obscene rate.");
					//}
//
				//}
				//else
				//{
					//if (amount <= 2) {
						//if (breastRows.length > 1) outputText("Your top row of " + breastDescript(0) + " jiggles with added weight as it expands, growing a bit larger.");
						//if (breastRows.length == 1) outputText("Your row of " + breastDescript(0) + " jiggles with added weight as it expands, growing a bit larger.");
					//}
					//if (amount > 2 && amount <= 4) {
						//if (breastRows.length > 1) outputText("You stagger as your chest gets much heavier.  Looking down, you watch with curiosity as your top row of " + breastDescript(0) + " expand significantly.");
						//if (breastRows.length == 1) outputText("You stagger as your chest gets much heavier.  Looking down, you watch with curiosity as your " + breastDescript(0) + " expand significantly.");
					//}
					//if (amount > 4) {
						//if (breastRows.length > 1) outputText("You drop to your knees from a massive change in your body's center of gravity.  Your top row of " + breastDescript(0) + " tingle strongly, growing disturbingly large.");
						//if (breastRows.length == 1) outputText("You drop to your knees from a massive change in your center of gravity.  The tingling in your " + breastDescript(0) + " intensifies as they continue to grow at an obscene rate.");
					//}
				//}
			//}
			//// Nipples
			//if (biggestTitSize() >= 8.5 && nippleLength < 2) 
			//{
				//if (display) outputText("  A tender ache starts at your [nipples] as they grow to match your burgeoning breast-flesh.");
				//nippleLength = 2;
			//}
			//if (biggestTitSize() >= 7 && nippleLength < 1)
			//{
				//if (display) outputText("  A tender ache starts at your [nipples] as they grow to match your burgeoning breast-flesh.");
				//nippleLength = 1;
			//}
			//if (biggestTitSize() >= 5 && nippleLength < .75)
			//{
				//if (display) outputText("  A tender ache starts at your [nipples] as they grow to match your burgeoning breast-flesh.");
				//nippleLength = .75;
			//}
			//if (biggestTitSize() >= 3 && nippleLength < .5)
			//{
				//if (display) outputText("  A tender ache starts at your [nipples] as they grow to match your burgeoning breast-flesh.");
				//nippleLength = .5;
			//}
		//}
//
		////Determine minimum lust
		//public function minLust():Number
		//{
			//var min:Number = 0;
			//var minCap:Number = 100;
			////Bimbo body boosts minimum lust by 40
			//if (hasStatusEffect(StatusEffects.BimboChampagne) || findPerk(PerkLib.BimboBody) >= 0 || findPerk(PerkLib.BroBody) >= 0 || findPerk(PerkLib.FutaForm) >= 0) {
				//if (min > 40) min += 10;
				//else if (min >= 20) min += 20;
				//else min += 40;
				//if (armorName == "bimbo skirt") min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
			//}
			//else if (kGAMECLASS.bimboProgress.ableToProgress()) {
				//if (min > 40) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
				//else if (min >= 20 ) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 2;
				//else min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST];
			//}
			////Omnibus' Gift
			//if (hasPerk(PerkLib.OmnibusGift)) {
				//if (min > 40) min += 10;
				//else if (min >= 20) min += 20;
				//else min += 35;
			//}
			////Nymph perk raises to 30
			//if (hasPerk(PerkLib.Nymphomania)) {
				//if (min >= 40) min += 10;
				//else if (min >= 20) min += 15;
				//else min += 30;
			//}
			////Oh noes anemone!
			//if (hasStatusEffect(StatusEffects.AnemoneArousal)) {
				//if (min >= 40) min += 10;
				//else if (min >= 20) min += 20;
				//else min += 30;
			//}
			////Hot blooded perk raises min lust!
			//if (hasPerk(PerkLib.HotBlooded)) {
				//if (min > 0) min += perk(findPerk(PerkLib.HotBlooded)).value1 / 2;
				//else min += perk(findPerk(PerkLib.HotBlooded)).value1;
			//}
			//if (hasPerk(PerkLib.LuststickAdapted)) {
				//if (min < 50) min += 10;
				//else min += 5;
			//}
			//if (hasStatusEffect(StatusEffects.Infested)) {
				//if (min < 50) min = 50;
			//}
			//if (hasStatusEffect(StatusEffects.ParasiteSlug)){
				//if (min < 50) min += 10;
				//else min += 5;
			//}
			//if (hasStatusEffect(StatusEffects.ParasiteSlugReproduction)){//Hard to ignore.
				//if (min < 80) min = 80;
			//}
			//if (findPerk(PerkLib.ParasiteMusk) >= 0){//Just like worms, but less gross!
				//if (min < 50) min = 50;
			//}
			//if (hasStatusEffect(StatusEffects.ParasiteEelNeedCum)){
				//min += 5 * statusEffectv3(StatusEffects.ParasiteEelNeedCum);
				//trace((5 * statusEffectv3(StatusEffects.ParasiteEelNeedCum)));
			//}
			////Add points for Crimstone
			//min += perkv1(PerkLib.PiercedCrimstone);
			////Subtract points for Icestone!
			//min -= perkv1(PerkLib.PiercedIcestone);
			//min += perkv1(PerkLib.PentUp);
			////Cold blooded perk reduces min lust, to a minimum of 20! Takes effect after piercings. This effectively caps minimum lust at 80.
			//if (hasPerk(PerkLib.ColdBlooded)) {
				//if (min >= 20) {
					//if (min <= 40) min -= (min - 20);
					//else min -= 20;
				//}
				//minCap -= 20;
			//}
			////Harpy Lipstick status forces minimum lust to be at least 50.
			//if (min < 50 && hasStatusEffect(StatusEffects.Luststick)) min = 50;
			////SHOULDRA BOOSTS
			////+20
			//if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -168 && flags[kFLAGS.URTA_QUEST_STATUS] != 0.75) {
				//min += 20;
				//if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -216)
					//min += 30;
			//}
			////SPOIDAH BOOSTS
			//if (eggs() >= 20) {
				//min += 10;
				//if (eggs() >= 40) min += 10;
			//}
			////Jewelry effects
			//if (jewelryEffectId == JewelryLib.MODIFIER_MINIMUM_LUST)
			//{
				//min += jewelryEffectMagnitude;
				//if (min > (minCap - jewelryEffectMagnitude) && jewelryEffectMagnitude < 0)
				//{
					//minCap += jewelryEffectMagnitude;
				//}
			//}
			//if (min < 30 && armorName == "lusty maiden's armor") min = 30;
			//if (min < 20 && armorName == "tentacled bark armor") min = 20;
			////Constrain values
			//if (min < 0) min = 0;
			//if (min > 95) min = 95;
			//if (min > minCap) min = minCap;
			//return min;
		//}
		//
		//public override function getMaxStats(stats:String):Int {
			//var maxStr:Int = 100;
			//var maxTou:Int = 100;
			//var maxSpe:Int = 100;
			//var maxInt:Int = 100;
			////Apply New Game+
			//maxStr += ascensionFactor();
			//maxTou += ascensionFactor();
			//maxSpe += ascensionFactor();
			//maxInt += ascensionFactor();
			////Alter max speed if you have oversized parts. (Realistic mode)
			//if (flags[kFLAGS.HUNGER_ENABLED] >= 1)
			//{
				////Balls
				//var tempSpeedPenalty:Number = 0;
				//var lim:Int = isTaur() ? 9 : 4;
				//if (ballSize > lim && balls > 0) tempSpeedPenalty += Math.round((ballSize - lim) / 2);
				////Breasts
				//lim = isTaur() ? BREAST_CUP_I : BREAST_CUP_G;
				//if (hasBreasts() && biggestTitSize() > lim) tempSpeedPenalty += ((biggestTitSize() - lim) / 2);
				////Cocks
				//lim = isTaur() ? 72 : 24;
				//if (biggestCockArea() > lim) tempSpeedPenalty += ((biggestCockArea() - lim) / 6);
				////Min-cap
				//var penaltyMultiplier:Number = 1;
				//penaltyMultiplier -= str * 0.1;
				//penaltyMultiplier -= (tallness - 72) / 168;
				//if (penaltyMultiplier < 0.4) penaltyMultiplier = 0.4;
				//tempSpeedPenalty *= penaltyMultiplier;
				//maxSpe -= tempSpeedPenalty;
				//if (maxSpe < 50) maxSpe = 50;
			//}
			////Perks ahoy
			//if (hasPerk(PerkLib.BasiliskResistance) && !canUseStare())
			//{
				//maxSpe -= 5;
			//}
			////Uma's Needlework affects max stats. Takes effect BEFORE racial modifiers and AFTER modifiers from body size.
			////Caps strength from Uma's needlework. 
			//if (hasPerk(PerkLib.ChiReflowSpeed))
			//{
				//if (maxStr > UmasShop.NEEDLEWORK_SPEED_STRENGTH_CAP)
				//{
					//maxStr = UmasShop.NEEDLEWORK_SPEED_STRENGTH_CAP;
				//}
			//}
			////Caps speed from Uma's needlework.
			//if (hasPerk(PerkLib.ChiReflowDefense))
			//{
				//if (maxSpe > UmasShop.NEEDLEWORK_DEFENSE_SPEED_CAP)
				//{
					//maxSpe = UmasShop.NEEDLEWORK_DEFENSE_SPEED_CAP;
				//}
			//}
			////Alter max stats depending on race
			//if (impScore() >= 4) {
				//maxSpe += 10;
				//maxInt -= 5;
			//}
			//if (sheepScore() >= 4) {
				//maxSpe += 10;
				//maxInt -= 10;
				//maxTou += 10;
			//}
			//if (wolfScore() >= 4) {
				//maxSpe -= 10;
				//maxInt += 5;
				//maxTou += 10;
				//maxStr += 5;
			//}
			//if (minoScore() >= 4) {
				//maxStr += 20;
				//maxTou += 10;
				//maxInt -= 10;
			//}
			//if (lizardScore() >= 4) {
				//maxInt += 10;
				//if (isBasilisk()) {
					//// Needs more balancing, especially other races, since dracolisks are quite OP right now!
					//maxTou += 5;
					//maxInt += 5;
				//}
			//}
			//if (dragonScore() >= 4) {
				//maxStr += 5;
				//maxTou += 10;
				//maxInt += 10;
			//}
			//if (dogScore() >= 4) {
				//maxSpe += 10;
				//maxInt -= 10;
			//}
			//if (foxScore() >= 4) {
				//maxStr -= 10;
				//maxSpe += 5;
				//maxInt += 5;
			//}
			//if (catScore() >= 4) {
				//maxSpe += 5;
			//}
			//if (bunnyScore() >= 4) {
				//maxSpe += 10;
			//}
			//if (raccoonScore() >= 4) {
				//maxSpe += 15;
			//}
			//if (horseScore() >= 4 && !isTaur() && !isNaga()) {
				//maxSpe += 15;
				//maxTou += 10;
				//maxInt -= 10;
			//}
			//if (gooScore() >= 3) {
				//maxTou += 10;
				//maxSpe -= 10;
			//}
			//if (kitsuneScore() >= 4) {
				//if (tail.type == 13) {
					//if (tailVenom == 1) {
						//maxStr -= 2;
						//maxSpe += 2;
						//maxInt += 1;
					//}
					//else if (tailVenom >= 2 && tailVenom < 9) {
						//maxStr -= tailVenom + 1;
						//maxSpe += tailVenom + 1;
						//maxInt += (tailVenom/2) + 0.5;
					//}
					//else if (tailVenom >= 9) {
						//maxStr -= 10;
						//maxSpe += 10;
						//maxInt += 5;
					//}
				//}
			//}
			//if (beeScore() >= 4) {
				//maxSpe += 5;
				//maxTou += 5;
			//}
			//if (spiderScore() >= 4) {
				//maxInt += 15;
				//maxTou += 5;
				//maxStr -= 10;
			//}
			//if (sharkScore() >= 4) {
				//maxStr += 10;
				//maxSpe += 5;
				//maxInt -= 5;
			//}
			//if (harpyScore() >= 4) {
				//maxSpe += 15;
				//maxTou -= 10;
			//}
			//if (sirenScore() >= 4) {
				//maxStr += 5;
				//maxSpe += 20;
				//maxTou -= 5;
			//}
			//if (demonScore() >= 4) {
				//maxSpe += 5;
				//maxInt += 5;
			//}
			//if (rhinoScore() >= 4) {
				//maxStr += 15;
				//maxTou += 15;
				//maxSpe -= 10;
				//maxInt -= 10;
			//}
			//if (satyrScore() >= 4) {
				//maxStr += 5;
				//maxSpe += 5;
			//}
			//if (salamanderScore() >= 4) {
				//maxStr += 5;
				//maxTou += 5;
			//}
			//if (findPerk(PerkLib.PotentPregnancy) >= 0 && isPregnant())
			//{
				//maxStr += 10;
				//maxTou += 10;
			//}
			//if (isNaga()) maxSpe += 10;
			//if (isTaur() || isDrider()) maxSpe += 20;
			//
			////Age modifiers
			//if (isChild()) {
				//maxStr -= 20;
				//maxTou -= 10;
				//maxSpe += 15;
			//}
			//if (isElder()) {
				//maxStr -= 5;
				//maxTou -= 5;
				//maxInt += 10;
				//maxSpe -= 5;
			//}
			//
			////Might
			//if (hasStatusEffect(StatusEffects.Might)) {
				//maxStr += statusEffectv1(StatusEffects.Might);
				//maxTou += statusEffectv2(StatusEffects.Might);
			//}
			////Parasite - OtherCoCAnon
			//if (hasStatusEffect(StatusEffects.ParasiteQueen)) {
				//maxStr += statusEffectv1(StatusEffects.ParasiteQueen);
				//maxTou += statusEffectv1(StatusEffects.ParasiteQueen);
				//maxSpe += statusEffectv1(StatusEffects.ParasiteQueen);
			//}
			//if (hasStatusEffect(StatusEffects.AndysSmoke)) {
				//maxSpe -= statusEffectv2(StatusEffects.AndysSmoke);
				//maxInt += statusEffectv3(StatusEffects.AndysSmoke);
			//}
			////Revelation - OtherCoCAnon
			//if (hasStatusEffect(StatusEffects.Revelation)){
				//maxInt += statusEffectv2(StatusEffects.Revelation);
			//}
			////Refashioned - OtherCoCAnon
			//if (hasStatusEffect(StatusEffects.Refashioned)){
				//maxStr += 300;
				//maxTou += 300;
				//maxInt += 300;
				//maxSpe += 300;
			//}
			//if (stats == "str" || stats == "strength") return maxStr;
			//else if (stats == "tou" || stats == "toughness") return maxTou;
			//else if (stats == "spe" || stats == "speed") return maxSpe;
			//else if (stats == "inte" || stats == "Int" || stats == "intelligence") return maxInt;
			//else return 100;
		//}
		
		public function requiredXP():Int {
			var temp:Int = level * 100;
			if (temp > 9999) temp = 9999;
			return temp;
		}
		
		public function levelUpAvailable():Bool {
			return xp >= requiredXP();
		}
		
		//public function minotaurAddicted():Bool {
			//return !hasPerk(PerkLib.MinotaurCumResistance) && (hasPerk(PerkLib.MinotaurCumAddict) || flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] >= 1);
		//}
		//public function minotaurNeed():Bool {
			//return !hasPerk(PerkLib.MinotaurCumResistance) && flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] > 1;
		//}
//
		//public function bleed(duration:Int = 3, intensity:Int = 1, ignorePerks:Boolean = false):Void{
			//if (findPerk(PerkLib.BleedImmune) < 0 || ignorePerks){
				//createStatusEffect(StatusEffects.IzmaBleed, duration, intensity, 0, 0);
			//}
		//}
		//
		//public function updateBleed():Void{
			//var totalIntensity:Int = 0;
			//var totalDuration:Int = 0;
			//for (i  in 0..._statusEffects.length){
				//if (statusEffects[i].stype.id == "Izma Bleed") {
				////Countdown to heal
				//statusEffects[i].value1 -= 1;
				//totalDuration += statusEffects[i].value1;
				//totalIntensity += statusEffects[i].value2;
				//if (statusEffects[i].value1 <= 0) {
				//statusEffects.splice(i, 1);
				//}
			//}
			//}
			//if (totalDuration <= 0){
					//outputText("<b>You sigh with relief; your bleeding has slowed considerably.</b>\n\n");
				//}else{	
					//var bleed:Number = ((2 + Utils.rand(4))/100) * totalIntensity;
					//bleed *= HP;
					//bleed = takeDamage(bleed);
					//outputText("<b>You gasp and wince in pain, feeling fresh blood pump from your wounds. (<font color=\"#800000\">" + bleed + "</font>)</b>\n\n");	
			//}
		//}
		//
		//public function purgeBleed():Void{
			//for (i in 0..._statusEffects.length) if(statusEffects[i].stype.id == "Izma Bleed") statusEffects.splice(i, 1);	
		//}
		//

		//public function lengthChange(temp2:Number, ncocks:Number):Void {
		
			//if (temp2 < 0 && flags[kFLAGS.HYPER_HAPPY])  // Early return for hyper-happy cheat if the call was *supposed* to shrink a cock.
			//{
				//return;
			//}
			////DIsplay the degree of length change.
			//if (temp2 <= 1 && temp2 > 0) {
				//if (cocks.length == 1) outputText("Your " + cockDescript(0) + " has grown slightly longer.");
				//if (cocks.length > 1) {
					//if (ncocks == 1) outputText("One of your " + multiCockDescriptLight() + " grows slightly longer.");
					//if (ncocks > 1 && ncocks < cocks.length) outputText("Some of your " + multiCockDescriptLight() + " grow slightly longer.");
					//if (ncocks == cocks.length) outputText("Your " + multiCockDescriptLight() + " seem to fill up... growing a little bit larger.");
				//}
			//}
			//if (temp2 > 1 && temp2 < 3) {
				//if (cocks.length == 1) outputText("A very pleasurable feeling spreads from your groin as your " + cockDescript(0) + " grows permanently longer - at least an inch - and leaks pre-cum from the pleasure of the change.");
				//if (cocks.length > 1) {
					//if (ncocks == cocks.length) outputText("A very pleasurable feeling spreads from your groin as your " + multiCockDescriptLight() + " grow permanently longer - at least an inch - and leak plenty of pre-cum from the pleasure of the change.");
					//if (ncocks == 1) outputText("A very pleasurable feeling spreads from your groin as one of your " + multiCockDescriptLight() + " grows permanently longer, by at least an inch, and leaks plenty of pre-cum from the pleasure of the change.");
					//if (ncocks > 1 && ncocks < cocks.length) outputText("A very pleasurable feeling spreads from your groin as " + Utils.num2Text(ncocks) + " of your " + multiCockDescriptLight() + " grow permanently longer, by at least an inch, and leak plenty of pre-cum from the pleasure of the change.");
				//}
			//}
			//if (temp2 >=3){
				//if (cocks.length == 1) outputText("Your " + cockDescript(0) + " feels incredibly tight as a few more inches of length seem to pour out from your crotch.");
				//if (cocks.length > 1) {
					//if (ncocks == 1) outputText("Your " + multiCockDescriptLight() + " feel incredibly tight as one of their number begins to grow inch after inch of length.");
					//if (ncocks > 1 && ncocks < cocks.length) outputText("Your " + multiCockDescriptLight() + " feel incredibly number as " + Utils.num2Text(ncocks) + " of them begin to grow inch after inch of added length.");
					//if (ncocks == cocks.length) outputText("Your " + multiCockDescriptLight() + " feel incredibly tight as inch after inch of length pour out from your groin.");
				//}
			//}
			////Display LengthChange
			//if (temp2 > 0) {
				//if (cocks[0].cockLength >= 8 && cocks[0].cockLength-temp2 < 8){
					//if (cocks.length == 1) outputText("  <b>Most men would be overly proud to have a tool as long as yours.</b>");
					//if (cocks.length > 1) outputText("  <b>Most men would be overly proud to have one cock as long as yours, let alone " + multiCockDescript() + ".</b>");
				//}
				//if (cocks[0].cockLength >= 12 && cocks[0].cockLength-temp2 < 12) {
					//if (cocks.length == 1) outputText("  <b>Your " + cockDescript(0) + " is so long it nearly swings to your knee at its full length.</b>");
					//if (cocks.length > 1) outputText("  <b>Your " + multiCockDescriptLight() + " are so long they nearly reach your knees when at full length.</b>");
				//}
				//if (cocks[0].cockLength >= 16 && cocks[0].cockLength-temp2 < 16) {
					//if (cocks.length == 1) outputText("  <b>Your " + cockDescript(0) + " would look more at home on a large horse than you.</b>");
					//if (cocks.length > 1) outputText("  <b>Your " + multiCockDescriptLight() + " would look more at home on a large horse than on your body.</b>");
					//if (biggestTitSize() >= BREAST_CUP_C) {
						//if (cocks.length == 1) outputText("  You could easily stuff your " + cockDescript(0) + " between your breasts and give yourself the titty-fuck of a lifetime.");
						//if (cocks.length > 1) outputText("  They reach so far up your chest it would be easy to stuff a few cocks between your breasts and give yourself the titty-fuck of a lifetime.");
					//}
					//else {
						//if (cocks.length == 1) outputText("  Your " + cockDescript(0) + " is so long it easily reaches your chest.  The possibility of autofellatio is now a foregone conclusion.");
						//if (cocks.length > 1) outputText("  Your " + multiCockDescriptLight() + " are so long they easily reach your chest.  Autofellatio would be about as hard as looking down.");
					//}
				//}
				//if (cocks[0].cockLength >= 20 && cocks[0].cockLength-temp2 < 20) {
					//if (cocks.length == 1) outputText("  <b>As if the pulsing heat of your " + cockDescript(0) + " wasn't enough, the tip of your " + cockDescript(0) + " keeps poking its way into your view every time you get hard.</b>");
					//if (cocks.length > 1) outputText("  <b>As if the pulsing heat of your " + multiCockDescriptLight() + " wasn't bad enough, every time you get hard, the tips of your " + multiCockDescriptLight() + " wave before you, obscuring the lower portions of your vision.</b>");
					//if (cor > 40 && cor <= 60) {
						//if (cocks.length > 1) outputText("  You wonder if there is a demon or beast out there that could take the full length of one of your " + multiCockDescriptLight() + "?");
						//if (cocks.length ==1) outputText("  You wonder if there is a demon or beast out there that could handle your full length.");
					//}
					//if (cor > 60 && cor <= 80) {
						//if (cocks.length > 1) outputText("  You daydream about being attacked by a massive tentacle beast, its tentacles engulfing your " + multiCockDescriptLight() + " to their hilts, milking you dry.\n\nYou smile at the pleasant thought.");
						//if (cocks.length ==1) outputText("  You daydream about being attacked by a massive tentacle beast, its tentacles engulfing your " + cockDescript(0) + " to the hilt, milking it of all your cum.\n\nYou smile at the pleasant thought.");
					//}
					//if (cor > 80) {
						//if (cocks.length > 1) outputText("  You find yourself fantasizing about impaling nubile young champions on your " + multiCockDescriptLight() + " in a year's time.");
					//}
				//}
			//}
			////Display the degree of length loss.
			//if (temp2 < 0 && temp2 >= -1) {
				//if (cocks.length == 1) outputText("Your " + multiCockDescriptLight() + " has shrunk to a slightly shorter length.");
				//if (cocks.length > 1) {
					//if (ncocks == cocks.length) outputText("Your " + multiCockDescriptLight() + " have shrunk to a slightly shorter length.");
					//if (ncocks > 1 && ncocks < cocks.length) outputText("You feel " + Utils.num2Text(ncocks) + " of your " + multiCockDescriptLight() + " have shrunk to a slightly shorter length.");
					//if (ncocks == 1) outputText("You feel " + Utils.num2Text(ncocks) + " of your " + multiCockDescriptLight() + " has shrunk to a slightly shorter length.");
				//}
			//}
			//if (temp2 < -1 && temp2 > -3) {
				//if (cocks.length == 1) outputText("Your " + multiCockDescriptLight() + " shrinks smaller, flesh vanishing into your groin.");
				//if (cocks.length > 1) {
					//if (ncocks == cocks.length) outputText("Your " + multiCockDescriptLight() + " shrink smaller, the flesh vanishing into your groin.");
					//if (ncocks == 1) outputText("You feel " + Utils.num2Text(ncocks) + " of your " + multiCockDescriptLight() + " shrink smaller, the flesh vanishing into your groin.");
					//if (ncocks > 1 && ncocks < cocks.length) outputText("You feel " + Utils.num2Text(ncocks) + " of your " + multiCockDescriptLight() + " shrink smaller, the flesh vanishing into your groin.");
				//}
			//}
			//if (temp2 <= -3) {
				//if (cocks.length == 1) outputText("A large portion of your " + multiCockDescriptLight() + "'s length shrinks and vanishes.");
				//if (cocks.length > 1) {
					//if (ncocks == cocks.length) outputText("A large portion of your " + multiCockDescriptLight() + " recedes towards your groin, receding rapidly in length.");
					//if (ncocks == 1) outputText("A single member of your " + multiCockDescriptLight() + " vanishes into your groin, receding rapidly in length.");
					//if (ncocks > 1 && cocks.length > ncocks) outputText("Your " + multiCockDescriptLight() + " tingles as " + Utils.num2Text(ncocks) + " of your members vanish into your groin, receding rapidly in length.");
				//}
			//}
		//}
//
		//public function killCocks(deadCock:Number):Void
		//{
			////Count removal for text bits
			//var removed:Number = 0;
			//var temp:Number;
			////Holds cock index
			//var storedCock:Number = 0;
			////Less than 0 = PURGE ALL
			//if (deadCock < 0) {
				//deadCock = cocks.length;
			//}
			////Double loop - outermost counts down cocks to remove, innermost counts down
			//while (deadCock > 0) {
				////Find shortest cock and prune it
				//temp = cocks.length;
				//while (temp > 0) {
					//temp--;
					////If anything is out of bounds set to 0.
					//if (storedCock > cocks.length - 1) storedCock = 0;
					////If temp index is shorter than stored index, store temp to stored index.
					//if (cocks[temp].cockLength <= cocks[storedCock].cockLength) storedCock = temp;
				//}
				////Smallest cock should be selected, now remove it!
				//removeCock(storedCock, 1);
				//removed++;
				//deadCock--;
				//if (cocks.length == 0) deadCock = 0;
			//}
			////Texts
			//if (removed == 1) {
				//if (cocks.length == 0) {
					//outputText("<b>Your manhood shrinks into your body, disappearing completely.</b>");
					//if (hasStatusEffect(StatusEffects.Infested)) outputText("  Like rats fleeing a sinking ship, a stream of worms squirts free from your withering member, slithering away.");
				//}
				//if (cocks.length == 1) {
					//outputText("<b>Your smallest penis disappears, shrinking into your body and leaving you with just one " + cockDescript(0) + ".</b>");
				//}
				//if (cocks.length > 1) {
					//outputText("<b>Your smallest penis disappears forever, leaving you with just your " + multiCockDescriptLight() + ".</b>");
				//}
			//}
			//if (removed > 1) {
				//if (cocks.length == 0) {
					//outputText("<b>All your male endowments shrink smaller and smaller, disappearing one at a time.</b>");
					//if (hasStatusEffect(StatusEffects.Infested)) outputText("  Like rats fleeing a sinking ship, a stream of worms squirts free from your withering member, slithering away.");
				//}
				//if (cocks.length == 1) {
					//outputText("<b>You feel " + Utils.num2Text(removed) + " cocks disappear into your groin, leaving you with just your " + cockDescript(0) + ".");
				//}
				//if (cocks.length > 1) {
					//outputText("<b>You feel " + Utils.num2Text(removed) + " cocks disappear into your groin, leaving you with " + multiCockDescriptLight() + ".");
				//}
			//}
			////remove infestation if cockless
			//if (cocks.length == 0) removeStatusEffect(StatusEffects.Infested);
			//if (cocks.length == 0 && balls > 0) {
				//outputText("  <b>Your " + sackDescript() + " and " + ballsDescriptLight() + " shrink and disappear, vanishing into your groin.</b>");
				//balls = 0;
				//ballSize = 1;
			//}
		//}
		//public function modCumMultiplier(delta:Float):Float
		//{
			//trace("modCumMultiplier called with: " + delta);
		//
			//if (delta == 0) {
				//trace( "Whoops! modCumMuliplier called with 0... aborting..." );
				//return delta;
			//}
			//else if (delta > 0) {
				//trace("and increasing");
				//if (hasPerk(PerkLib.MessyOrgasms)) {
					//trace("and MessyOrgasms found");
					//delta *= 1.5;
				//}
			//}
			//else if (delta < 0) {
				//trace("and decreasing");
				//if (hasPerk(PerkLib.MessyOrgasms)) {
					//trace("and MessyOrgasms found");
					//delta *= 0.5;
				//}
			//}
//
			//trace("and modifying by " + delta);
			//cumMultiplier += delta;
			//return delta;
		//}
//
		//public function increaseCock(cockNum:Int, lengthDelta:Number):Float
		//{
			//var bigCock:Boolean = false;
	//
			//if (hasPerk(PerkLib.BigCock))
				//bigCock = true;
//
			//return cocks[cockNum].growCock(lengthDelta, bigCock);
		//}
		//
		//public function increaseEachCock(lengthDelta:Float):Float
		//{
			//var totalGrowth:Number = 0;
			//
			//for (i in 0...cocks.length) {
				//trace( "increaseEachCock at: " + i);
				//totalGrowth += increaseCock(i, lengthDelta);
			//}
			//
			//return totalGrowth;
		//}
		//
		//// Attempts to put the player in heat (or deeper in heat).
		//// Returns true if successful, false if not.
		//// The player cannot go into heat if she is already pregnant or is a he.
		//// 
		//// First parameter: boolean indicating if function should output standard text.
		//// Second parameter: intensity, an integer multiplier that can increase the 
		//// duration and intensity. Defaults to 1.
		//public function goIntoHeat(output:Boolean, intensity:Int = 1):Boolean {
			//if (!hasVagina() || pregnancyIncubation != 0) {
				//// No vagina or already pregnant, can't go into heat.
				//return false;
			//}
			//
			////Already in heat, intensify further.
			//if (inHeat) {
				//if (output) {
					//outputText("\n\nYour mind clouds as your " + vaginaDescript(0) + " moistens.  Despite already being in heat, the desire to copulate constantly grows even larger.");
				//}
				//var effect:StatusEffectClass = statusEffectByType(StatusEffects.Heat);
				//effect.value1 += 5 * intensity;
				//effect.value2 += 5 * intensity;
				//effect.value3 += 48 * intensity;
				//game.dynStats("lib", 5 * intensity, "resisted", false, "noBimbo", true);
			//}
			////Go into heat.  Heats v1 is bonus fertility, v2 is bonus libido, v3 is hours till it's gone
			//else {
				//if (output) {
					//outputText("\n\nYour mind clouds as your " + vaginaDescript(0) + " moistens.  Your hands begin stroking your body from top to bottom, your sensitive skin burning with desire.  Fantasies about bending over and presenting your needy pussy to a male overwhelm you as <b>you realize you have gone into heat!</b>");
				//}
				//createStatusEffect(StatusEffects.Heat, 10 * intensity, 15 * intensity, 48 * intensity, 0);
				//game.dynStats("lib", 15 * intensity, "resisted", false, "noBimbo", true);
			//}
			//return true;
		//}
		//
		//// Attempts to put the player in rut (or deeper in heat).
		//// Returns true if successful, false if not.
		//// The player cannot go into heat if he is a she.
		//// 
		//// First parameter: boolean indicating if function should output standard text.
		//// Second parameter: intensity, an integer multiplier that can increase the 
		//// duration and intensity. Defaults to 1.
		//public function goIntoRut(output:Boolean, intensity:Int = 1):Boolean {
			//if (!hasCock()) {
				//// No cocks, can't go into rut.
				//return false;
			//}
			//
			////Has rut, intensify it!
			//if (inRut) {
				//if (output) {
					//outputText("\n\nYour " + cockDescript(0) + " throbs and dribbles as your desire to mate intensifies.  You know that <b>you've sunken deeper into rut</b>, but all that really matters is unloading into a cum-hungry cunt.");
				//}
				//
				//addStatusValue(StatusEffects.Rut, 1, 100 * intensity);
				//addStatusValue(StatusEffects.Rut, 2, 5 * intensity);
				//addStatusValue(StatusEffects.Rut, 3, 48 * intensity);
				//game.dynStats("lib", 5 * intensity, "resisted", false, "noBimbo", true);
			//}
			//else {
				//if (output) {
					//outputText("\n\nYou stand up a bit straighter and look around, sniffing the air and searching for a mate.  Wait, what!?  It's hard to shake the thought from your head - you really could use a nice fertile hole to impregnate.  You slap your forehead and realize <b>you've gone into rut</b>!");
				//}
				//
				////v1 - bonus cum production
				////v2 - bonus libido
				////v3 - time remaining!
				//createStatusEffect(StatusEffects.Rut, 150 * intensity, 5 * intensity, 100 * intensity, 0);
				//game.dynStats("lib", 5 * intensity, "resisted", false, "noBimbo", true);
			//}
			//
			//return true;
		//}
//
		//public function setFurColor(colorArray:Array, underBodyProps:Object = null, doCopySkin:Boolean = false, restoreUnderBody:Boolean = true):Void
		//{
			//_setSkinFurColor("fur", colorArray, underBodyProps, doCopySkin, restoreUnderBody);
		//}
//
		//public function setSkinTone(colorArray:Array, underBodyProps:Object = null, doCopySkin:Boolean = false, restoreUnderBody:Boolean = true):Void
		//{
			//_setSkinFurColor("skin", colorArray, underBodyProps, doCopySkin, restoreUnderBody);
		//}
//
		//private function _setSkinFurColor(what:String, colorArray:Array, underBodyProps:Object = null, doCopySkin:Boolean = false, restoreUnderBody:Boolean = true):Void
		//{
			//var choice:Array = colorArray[Utils.rand(colorArray.length)];
//
			//if (restoreUnderBody)
				//underBody.restore();
//
			//if (what == "fur")
				//furColor = (choice is Array) ? choice[0] : choice;
			//else
				//skinTone = (choice is Array) ? choice[0] : choice;
//
			//if (doCopySkin)
				//copySkinToUnderBody();
//
			//if (what == "fur")
				//underBody.skin.furColor = choice[1];
			//else
				//underBody.skin.tone = choice[1];
//
			//if (underBodyProps != null)
				//underBody.setProps(underBodyProps);
		//}
		//
		//
		//public function hasDifferentUnderBody():Boolean
		//{
			//if ([UNDER_BODY_TYPE_NONE, UNDER_BODY_TYPE_NAGA].indexOf(underBody.type) != -1)
				//return false;
//
			///* // Example for later use
			//if ([UNDER_BODY_TYPE_MERMAID, UNDER_BODY_TYPE_WHATEVER].indexOf(underBody.type) != -1)
				//return false; // The underBody is (mis)used for secondary skin, not for the underBody itself
			//*/
//
			//return underBody.skin.type != skin.type || underBody.skin.tone != skin.tone ||
			       //underBody.skin.adj  != skin.adj  || underBody.skin.desc != skin.desc ||
			       //(underBody.skin.hasFur() && hasFur() && underBody.skin.furColor != skin.furColor);
		//}
//
		//public function hasUnderBody(noSnakes:Boolean = false):Boolean
		//{
			//var normalUnderBodies:Array = [UNDER_BODY_TYPE_NONE];
//
			//if (noSnakes) {
				//normalUnderBodies.push(UNDER_BODY_TYPE_NAGA);
			//}
//
			//return normalUnderBodies.indexOf(underBody.type) == -1;
		//}
//
		//public function hasFurryUnderBody(noSnakes:Boolean = false):Boolean
		//{
			//return hasUnderBody(noSnakes) && underBody.skin.hasFur();
		//}
//
		//public function hasFeatheredUnderBody(noSnakes:Boolean = false):Boolean
		//{
			//return hasUnderBody(noSnakes) && underBody.skin.hasFeathers();
		//}
//

	public function hasReptileEyes():Bool
	{
		return [EyeType.Lizard, EyeType.Dragon, EyeType.Basilisk].indexOf(eyeType) != -1;
	}
	
	//public function canOvipositSpider():Bool
	//{
		//return eggs() >= 10 && hasPerk(PerkStore.SpiderOvipositor) && isDrider() && tailType == TailType.SpiderAbdomen;
	//}
	//
	//public function canOvipositBee():Bool
	//{
		//return eggs() >= 10 && hasPerk(PerkStore.BeeOvipositor) && tailType == TailType.BeeAbdomen;
	//}
	//
	//public function canOviposit():Bool
	//{
		//return canOvipositSpider() || canOvipositBee();
	//}
		
		//public function hasReptileFace():Boolean
		//{
			//return [FACE_SNAKE_FANGS, FACE_LIZARD, FACE_DRAGON].indexOf(faceType) != -1;
		//}
//
		//public function hasReptileUnderBody(withSnakes:Boolean = false):Boolean
		//{
			//var underBodies:Array = [
				//UNDER_BODY_TYPE_REPTILE,
			//];
//
			//if (withSnakes) {
				//underBodies.push(UNDER_BODY_TYPE_NAGA);
			//}
//
			//return underBodies.indexOf(underBody.type) != -1;
		//}
//
		//public function hasCockatriceSkin():Boolean
		//{
			//return skinType == SKIN_TYPE_LIZARD_SCALES && underBody.type == UNDER_BODY_TYPE_COCKATRICE;
		//}
//
		//public function hasNonCockatriceAntennae():Boolean
		//{
			//return [ANTENNAE_NONE, ANTENNAE_COCKATRICE].indexOf(antennae) == -1;
		//}
//
		//public function hasBatLikeWings(large:Boolean = false):Boolean
		//{
			//if (large)
				//return wingType == WING_TYPE_BAT_LIKE_LARGE;
			//else
				//return [WING_TYPE_BAT_LIKE_TINY, WING_TYPE_BAT_LIKE_LARGE].indexOf(wingType) != -1;
		//}
		//public function featheryHairPinEquipped():Boolean
		//{
			//return hasKeyItem("Feathery hair-pin") >= 0 && keyItemv1("Feathery hair-pin") == 1;
		//}
		
	public override function maxHP():Int
	{
		var max:Int = 0;
		max += stats[Stat.Toughness] * 2 + 50;
		if (hasPerk(PerkStore.Tank)) 
		max += 50;
		if (hasPerk(PerkStore.Tank2)) 
			max += stats[Stat.Toughness];
		//if (weaponName == "cursed dagger") 
			//max *= 0.75;
		//if (hasPerk(PerkStore.ChiReflowDefense)) 
			//max += UmasShop.NEEDLEWORK_DEFENSE_EXTRA_HP;
		//if (flags[kFLAGS.GRIMDARK_MODE] >= 1)
			//max += level * 5;
		//else
			max += level * 15;
		//if (jewelryEffectId == JewelryLib.MODIFIER_HP) 
			//max += jewelryEffectMagnitude;
		
		//max *= 1 + (countCockSocks("green") * 0.02);
		//if (isChild() || isElder()) max *= 0.8;
		
		if (max > 9999) max = 9999;
		return max;
	}
	
	/**
	 * Find an array element number for a perk. Useful when you want to work with a PerkClass instance.
	 */
	public function findPerk(perkId:String):Int
	{
		if (perks.length <= 0)
			return -1;
			
		return perks.indexOf(perkId);
	}
	
	/**
	 * Check if this creature has specified perk.
	 */
	public function hasPerk(perkId:String):Bool
	{
		if (perks.length <= 0)
			return false;
			
		return perks.indexOf(perkId) != -1;
	}
	
	public function addPerk(perkId:String):Void
	{
		perks.push(perkId);
	}

	public function removePerk(perkId:String):Void
	{
		perks.remove(perkId);
	}
	
}