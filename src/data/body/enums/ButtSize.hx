package data.body.enums;

/**
 * @author Funtacles
 */
class ButtSize 
{
	public static inline var Buttless:Int = 0;
	public static inline var Tight:Int = 2;
	public static inline var Average:Int = 4;
	public static inline var Noticeable:Int = 6;
	public static inline var Large:Int = 8;
	public static inline var Jiggly:Int = 10;
	public static inline var Expansive:Int = 13;
	public static inline var Huge:Int = 16;
	public static inline var Massive:Int = 20; //Inconceivably big
}