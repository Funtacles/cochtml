package data.body.enums;

/**
 * @author Funtacles
 */
enum AnalLooseness 
{
	Virgin;
	Tight;
	Normal;
	Loose;
	Stretched;
	Gaping;
}