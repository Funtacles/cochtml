package data.body.enums;

/**
 * @author Funtacles
 */
enum ArmType 
{
	Human;
	Harpy;
	Spider;
	Predator;
	Salamander;
	Wolf;
	Cockatrice;
}