package data.body.enums;

/**
 * @author Funtacles
 */
enum TongueType 
{
	Human;
	Snake;
	Demonic;
	Draconic;
	Echidna;
	Lizard;
}