package data.body.enums;

/**
 * @author Funtacles
 */
enum AnalWetness 
{
	Dry;
	Normal;
	Moist;
	Slimy;
	Drooling;
	SlimeDrooling;
}