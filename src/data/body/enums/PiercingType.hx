package data.body.enums;

/**
 * @author Funtacles
 */
enum PiercingType 
{
	None;
	Stud;
	Ring;
	Ladder;
	Hoop;
	Chain;
}