package data.body.enums;

/**
 * @author Funtacles
 */
enum PregnancyStage 
{
	NotPregant;			//The * consts are returned by the size function
	NoSignsUnknown;		//NPC has conceived but doesn’t know she’s pregnant, no visible signs
	NoSignsKnown;		//NPC is in the first trimester, knows she’s pregnant
	StartBulge;			//NPC is in the first trimester, belly is just starting to bulge
	Swollen;			//NPC is in the second trimester, belly is small but definitely swollen
	Sizeable;			//NPC is in the second trimester, belly is now sizable
	Blatant;			//NPC is in the third trimester, belly is blatantly bulging
	FullTerm;			//NPC is in the third trimester, belly is big as it will get for a normal pregnancy
	Overdue;			//NPC is overdue. Usually means a centaur baby, twins or some similar condition. Effectively looks 10 months pregnant
	VeryOverdue;		//NPC is very overdue. Probably triplets or more. Effectively looks 11 months pregnant
}