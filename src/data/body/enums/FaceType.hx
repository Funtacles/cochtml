package data.body.enums;

/**
 * @author Funtacles
 */
enum FaceType 
{

	Human;
	Horse;
	Dog;
	Cow;
	SharkTeeth;
	SnakeFangs;
	Cat;
	Lizard;
	Bunny;
	Kangaroo;
	SpiderFangs;
	Fox;
	Dragon;
	RacoonMask;
	Racoon;
	BuckTeeth;
	Mouse;
	FerretMask;
	Ferret;
	Pig;
	Boar;
	Rhino;
	Echidna;
	Deer;
	Wolf;
	Cockatrice;
	Beak;
}