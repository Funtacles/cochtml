package data.body.enums;

/**
 * @author Funtacles
 */
enum GillType 
{
	None;
	Anemone;
	Fish;
}