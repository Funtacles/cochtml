package data.body.enums;

/**
 * @author Funtacles
 */
enum TailType 
{
	None;
	Horse;
	Dog;
	Demonic;
	Cow;
	SpiderAbdomen;
	BeeAbdomen;
	Shark;
	Cat;
	Lizard;
	Rabbit;
	Harpy;
	Kangaroo;
	Fox;
	Draconic;
	Racoon;
	Mouse;
	Ferret;
	Behemoth;
	Pig;
	Scorpion;
	Goat;
	Rhino;
	Echidna;
	Deer;
	Salamander;
	Wolf;
	Sheep;
	Imp;
	Cockatrice;
}