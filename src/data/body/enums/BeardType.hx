package data.body.enums;

/**
 * @author Funtacles
 */
enum BeardType 
{
	Normal;
	Goatee;
	CleanCut;
	MountainMan;
}