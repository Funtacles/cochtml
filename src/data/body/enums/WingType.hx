package data.body.enums;

/**
 * @author Funtacles
 */
enum WingType 
{
	None;
	SmallBee;
	LargeBee;
	Harpy;
	Imp;
	TinyBatlike;
	LargeBatlike;
	SharkFin;
	LargeFeathered;
	DraconicSmall;
	DraconicLarge;
	GiantDragonfly;
	LargeImp;
}