package data.body.enums;

/**
 * @author Funtacles
 */
class HipSize 
{
	public static inline var Boyish:Int = 0;
	public static inline var Slender:Int = 2;
	public static inline var Average:Int = 4;
	public static inline var Ample:Int = 6;
	public static inline var Curvy:Int = 10;
	public static inline var Fertile:Int = 15;
	public static inline var InhumanWide:Int = 20;
}