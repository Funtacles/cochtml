package data.body.enums;

/**
 * @author Funtacles
 */
enum VaginaLooseness 
{
	Tight;
	Normal;
	Loose;
	Gaping;
	GapingWide;
	ClownCar;
}