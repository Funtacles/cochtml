package data.body.enums;

/**
 * @author Funtacles
 */
enum PregnancyType 
{
	Imp;
	Minotaur;
	Mouse;
	OvielixirEggs;			//also caused by phoenixes apparently
	HellHound;
	Centaur;
	Marble;
	Bunny;
	Anemone;
	Amily;
	Izma;
	Spider;
	Basilisk;
	DriderEggs;
	GooGirl;
	Ember;
	Benoit;
	Satyr;
	Cotton;
	Urta;
	SandWitch;
	Frog_girl;
	Faerie;					//Indicates you are carrying either a phouka or faerie baby. Which one is determined by the corruption flag
	Player;                	//The player is the father. will be used when an npc is able to have children from multiple different fathers.
	Bee_Eggs;
	SandtrapFertile;
	Sandtrap;
	Jojo;					//So we can track them separately from other mouse pregnancies
	Kelt;					//So we can track them separately from other centaur pregnancies
	Taoth;
	GooStuffed;				//When Valeria has a goo girl take up residence. Prevents any other form of pregnancy and does not respond to ovielixirs.
	WormStuffed;			//When the worms take up residence. Prevents any other form of pregnancy and does not respond to ovielixirs.
	Minerva;
	Behemoth;
	Phoenix;
	Andy;					//This is functionally the same as satyr but less corrupt. 10% chance of fauns, if ever implemented.
	Corrwitch;
	TentacleBeastSeed;
}