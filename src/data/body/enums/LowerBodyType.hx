package data.body.enums;

/**
 * @author Funtacles
 */
enum LowerBodyType 
{
	Human;
	Hoofed;
	Dog;
	Naga;
	DemonHighHeels;
	DemonClaws;
	Bee;
	Goo;
	Cat;
	Lizard;
	Pony;
	Bunny;
	Harpy;
	Kangaroo;
	Spider; // Chitinous spider legs
	Drider;
	Fox;
	Dragon;
	Racoon;
	Ferret;
	ClovenHoofed;
	Echidna;
	Salamander;
	Wolf;
	Imp;
	Cockatrice;
}