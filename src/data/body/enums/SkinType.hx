package data.body.enums;

/**
 * @author Funtacles
 */
enum SkinType 
{
	Normal;
	Fur;
	LizardScales;
	Goo;
	Undefined;
	DragonScales;
	FishScales;
	Wool;
	Feathered;
}