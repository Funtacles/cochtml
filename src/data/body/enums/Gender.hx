package data.body.enums;

/**
 * @author Funtacles
 */
enum Gender 
{
	Neuter;
	Male;
	Female;
	Herm;
}