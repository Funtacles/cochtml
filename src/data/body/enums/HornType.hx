package data.body.enums;

/**
 * @author Funtacles
 */
enum HornType 
{
	None;
	Demon;
	Bovine;
	Draconic2x;
	Draconic4x;
	Deer;
	Goat;
	Rhino;
	Sheep;
	Ram;
	Imp;
	Unicorn;
}