package data.body.enums;

/**
 * @author Funtacles
 */
enum ClawType 
{
	Normal;
	Lizard;
	Dragon;
	Salamander;
	Cat;
	Dog;
	Raptor;
	Mantis;
	Imp;
	Cockatrice;
}