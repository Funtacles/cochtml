package data.body.enums;

/**
 * @author Funtacles
 */
enum EarType 
{
	Human;
	Horse;
	Dog;
	Cow;
	Elfin;
	Cat;
	Lizard;
	Bunny;
	Kangaroo;
	Fox;
	Dragon;
	Racoon;
	Mouse;
	Ferret;
	Pig;
	Rhino;
	Echidna;
	Deer;
	Wolf;
	Sheep;
	Imp;
	Cockatrice;
}