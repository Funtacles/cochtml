package data.body.enums;

/**
 * @author Funtacles
 */
enum AntennaeType 
{
	None;
	Bee;
	Cockatrice;
}