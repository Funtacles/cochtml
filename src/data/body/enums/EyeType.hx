package data.body.enums;

/**
 * @author Funtacles
 */
enum EyeType 
{
	Human;
	Spider4x;
	SandtrapBlack;
	Lizard;
	Dragon;
	Basilisk;
	Wolf;
	Spider;
	Cockatrice;
}