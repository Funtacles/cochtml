package data.body.enums;

/**
 * @author Funtacles
 */
enum HairType 
{
	Normal;
	Feather;
	Ghost;
	Goo;
	Anemone;
	Quill;
	BasaliskSpines;
	BasaliskPlume;
	Wool;
}
