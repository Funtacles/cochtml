package data.body.enums;

/**
 * @author Funtacles
 */
enum VaginaWetness 
{
	Dry;
	Normal;
	Wet;
	Slick;
	Drooling;
	Slavering;
}