package data.body;
import storage.tml.TMLElement;
import data.body.enums.HairType;

/**
 * ...
 * @author Funtacles
 */
class Hair
{
	public var type:HairType;
	public var color:String;
	public var length:Int;
	
	public function new() 
	{
		type = HairType.Normal;
		color = "";
		length = 0;
	}
	
	public function setFromTml(tml:TMLElement) {
		type = HairType.createByName(tml.asString());
		
		if (tml.hasChild("Color"))
			color = tml.getFirstChild("Color").asString();
			
		if (tml.hasChild("Length"))
			length = tml.getFirstChild("Length").asInt();
	}
	
	
}