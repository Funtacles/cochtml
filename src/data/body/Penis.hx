package data.body;
import data.body.enums.PenisType;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class Penis
{
	public var length:Float;
	public var thickness:Float;		
	public var type:PenisType;
	
	/**
	 * The thickness of the knot relative to normal thickness
	 */ 
	public var knotMultiplier:Float;
	
	public var isPierced:Bool;
	

	public function new(tml:TMLElement)
	{
		type = PenisType.createByName(tml.asString());
		
		length = tml.getFirstChild("Length").asFloat();
		
		thickness = tml.getFirstChild("Thickness").asFloat();
	}
	
	public function description(prop:String = null):String {
		return "";
	}
	
	
	public var area(get, never):Float;
	private function get_area():Float
	{
		return thickness * length;
	}
	
	public function hasKnot():Bool
	{
		return knotMultiplier > 1;
	}
}