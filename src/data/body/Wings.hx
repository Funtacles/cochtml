package data.body;
import data.body.enums.WingType;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class Wings
{
	public var type:WingType;
	public var color:String;

	public function new(tml:TMLElement) {
		setFromTML(tml);
	}
	
	public function description(prop:String = null):String {
		return "";
	}

	public function canDye():Bool
	{
		return [Harpy, LargeFeathered].indexOf(type) != -1;
	}
	
	public function setFromTML(tml:TMLElement):Void {
		type = WingType.createByName(tml.content);
		
		if (type == null)
			throw 'Invalid skin type: "${tml.content}"';
		
	}
}