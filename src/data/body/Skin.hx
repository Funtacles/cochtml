package data.body;
import data.body.enums.SkinType;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class Skin
{

	public var type:SkinType;
	public var tone:String;
	public var furColor:String;
	
	private var tml:TMLElement;

	public function new() {
		
	}
	
	/**
	 * Returns a word describing what your skin is covered in, if anything.
	 * @return
	 */
	public function coveringDesc():String {
		var skin:String = "";
		
		var adj = getAdjective();
		
		if(!StringUtil.isNullOrEmpty(adj))
			skin += adj + ", ";
		
		skin += furColor;
		
		return skin + " " + getCoveringTypeDesc();
	}
	
	public function getCoveringTypeDesc():String {
		switch(type)
		{
			case Fur:
				return "fur";
			case SkinType.Feathered:
				return "feathers";
			default:
				return "";
		}
	}
	
	public function skinDesc():String {
		var skin:String = "";
		
		var adj = getAdjective();
		if(!StringUtil.isNullOrEmpty(adj))
			skin += adj + ", ";
			
		skin += tone;
		
		return skin + " " + getSkinTypeDesc();
	}
	
	public function description():String
	{
		var skin:String = "";
		
		var adj = getAdjective();
		
		if(!StringUtil.isNullOrEmpty(adj))
			skin += adj + ", ";
		
		skin += (isFluffy() ? furColor : tone);
		
		return skin + " " + getTypeDesc();
	}
	
	public function getAdjective():String {
		switch(type)
		{
			case DragonScales:
				return "tough";
			case Goo:
				return "slimy";
			default:
				return "";
		}
	}
	
	public function getTypeDesc():String {
		switch(type)
		{
			case Fur:
				return "fur";
			case DragonScales:
				return "shield-shaped dragon scales";
			case LizardScales:
				return "scales";
			default:
				return "skin";
		}
	}
	
	public function getSkinTypeDesc():String {
		switch(type)
		{
			case DragonScales:
				return "shield-shaped dragon scales";
			case LizardScales:
				return "scales";
			default:
				return "skin";
		}
	}

	public function isFurry():Bool
	{
		return [Fur, Wool].indexOf(type) != -1;
	}

	public function isFluffy():Bool
	{
		return [Fur, Wool, Feathered].indexOf(type) != -1;
	}
	
	public function isScaly():Bool {
		return [DragonScales, FishScales, LizardScales].indexOf(type) != -1;
	}
	
	public function setFromTML(tml:TMLElement):Void {
		type = SkinType.createByName(tml.content);
		if (tml.hasChild("Tone"))
			tone = tml.getFirstChild("Tone").asString();
		if (tml.hasChild("FurColor"))
			furColor = tml.getFirstChild("FurColor").asString();
	}
	
	public static function fromTML(tml:TMLElement):Skin {
		var skin = new Skin();
		skin.setFromTML(tml);
		return skin;
	}
}