package data.body;
import data.body.enums.TailType;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class Tail 
{
	public var type:TailType;
	public var tailCount:Int;
	
	public var tailVenom:Int;
	public var tailRecharge:Int;
	

	public function new() 
	{
		
	}
	
	public function setFromTML(tml:TMLElement):Void{
		
	}
	
	public static function fromTML(tml:TMLElement):Tail {
		var tail = new Tail();
		tail.setFromTML(tml);
		return tail;
	}
	
}