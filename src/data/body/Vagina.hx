package data.body;
import data.body.enums.VaginaLooseness;
import data.body.enums.VaginaType;
import data.body.enums.VaginaWetness;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class Vagina 
{
	public static inline var DEFAULT_CLIT_LENGTH:Float = 0.5;
	
	public var vaginalWetness:VaginaWetness;
	public var vaginalLooseness:VaginaLooseness;
	public var type:VaginaType;
	public var virgin:Bool;
	//Used during sex to determine how full it currently is.  For multi-dick sex.
	public var fullness:Float = 0;
	public var labiaPierced:Float = 0;
	public var labiaPShort:String = "";
	public var labiaPLong:String = "";		
	public var clitPierced:Int = 0;
	public var clitPShort:String = "";
	public var clitPLong:String = "";
	public var clitLength:Float;
	public var recoveryProgress:Int;
	
	public function new()
	{
	}

	/**
	 * Wetness factor used for calculating capacity.
	 * 
	 * @return wetness factor based on wetness
	 */
	public function wetnessFactor():Float {
		return 1 + vaginalWetness.getIndex() / 10 ;
	}
	
	private function baseCapacity(bonusCapacity:Float):Float {
		return bonusCapacity + 8 * vaginalLooseness.getIndex() * vaginalLooseness.getIndex();
	}
	
	/**
	 * The capacity of the vagina, calculated using looseness and wetness.
	 * 
	 * @param bonusCapacity extra space to add
	 * @return the total capacity, with all factors considered.
	 */
	public function capacity(bonusCapacity:Float = 0):Float {
		return baseCapacity(bonusCapacity) * wetnessFactor();
	}
	
	//TODO call this in the setter? With new value > old value check?
	/**
	 * Resets the recovery counter.
	 * The counter is used for looseness recovery over time, a reset usualy occurs when the looseness increases.
	 */
	public function resetRecoveryProgress():Void {
		this.recoveryProgress = 0;
	}
	
	public function setFromTML(tml:TMLElement):Void {
		type = VaginaType.createByName(tml.asString());
		
		if (tml.hasChild("Wetness"))
			vaginalWetness = VaginaWetness.createByName(tml.getFirstChild("Wetness").asString());
			
		if (tml.hasChild("Looseness"))
			vaginalLooseness = VaginaLooseness.createByName(tml.getFirstChild("Looseness").asString());
			
		if (tml.hasChild("Clit"))
			clitLength = tml.getFirstChild("Clit").asFloat();
		
		virgin = tml.hasChild("Virgin");
	}
	
	public static function fromTML(tml:TMLElement):Vagina {
		var vagina = new Vagina();
		vagina.setFromTML(tml);
		return vagina;
	}
	
}
