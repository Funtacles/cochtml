package data.body;
import data.body.enums.PregnancyType;

/**
 * ...
 * @author Funtacles
 */
class Womb 
{
	public var pregType:Null<PregnancyType>;
	public var gestation:Int;
	
	public var buttPregType:Null<PregnancyType>;
	public var buttGestation:Int;
	
	
	public function new() 
	{
		
	}

	public static inline var INCUBATION_IMP:Int                  = 432; //Time for standard imps. Imp lords, Ceraph, Lilium and the imp horde cause slightly faster pregnancies
	public static inline var INCUBATION_MINOTAUR:Int             = 432;
	public static inline var INCUBATION_MOUSE:Int                = 350;
	public static inline var INCUBATION_OVIELIXIR_EGGS:Int       =  50;
	public static inline var INCUBATION_HELL_HOUND:Int           = 352;
	public static inline var INCUBATION_CENTAUR:Int              = 420;
	public static inline var INCUBATION_CORRWITCH:Int            = 240; //Relatively short, but hard to get pregnancy.
	public static inline var INCUBATION_MARBLE:Int               = 368;
	public static inline var INCUBATION_BUNNY_BABY:Int           = 200;
	public static inline var INCUBATION_BUNNY_EGGS:Int           = 808; //High time indicates neon egg pregnancy
	public static inline var INCUBATION_ANEMONE:Int              = 256;
	public static inline var INCUBATION_IZMA:Int                 = 300;
	public static inline var INCUBATION_SPIDER:Int               = 400;
	public static inline var INCUBATION_BASILISK:Int             = 250;
	public static inline var INCUBATION_DRIDER:Int               = 400;
	public static inline var INCUBATION_GOO_GIRL:Int             =  85;
	public static inline var INCUBATION_EMBER:Int                = 336;
	public static inline var INCUBATION_SATYR:Int                = 160;
	public static inline var INCUBATION_COTTON:Int               = 350;
	public static inline var INCUBATION_URTA:Int                 = 515;
	public static inline var INCUBATION_SAND_WITCH:Int           = 360; 
	public static inline var INCUBATION_FROG_GIRL:Int            =  30;
	public static inline var INCUBATION_FAERIE:Int               = 200;
	public static inline var INCUBATION_BEE:Int                  =  48;
	public static inline var INCUBATION_SANDTRAP:Int             =  42;
	public static inline var INCUBATION_HARPY:Int                = 168;
	public static inline var INCUBATION_SHIELA:Int               =  72;
	public static inline var INCUBATION_SALAMANDER:Int           = 336;
	public static inline var INCUBATION_MINERVA:Int           	 = 216; 
	public static inline var INCUBATION_BEHEMOTH:Int           	 =1440; //Sorry Behemoth, but Isabella wins.
	public static inline var INCUBATION_PHOENIX:Int           	 = 168; 
	public static inline var INCUBATION_KIHA:Int                 = 384;
	public static inline var INCUBATION_ISABELLA:Int             = 2160; //Longest pregnancy ever. 
	public static inline var INCUBATION_TENTACLE_BEAST_SEED:Int  =  120; //short stuff, really.
	
	private static inline var PREG_TYPE_MASK:Int                 = 0x0000FFFF; //Should be safe with 65535 different pregnancy types
	private static inline var PREG_NOTICE_MASK:Int               = 0x7FFF0000; //Use upper half to store the latest stages of pregnancy the player has noticed
	
	public function isPregnant():Bool { return pregType != null; }

	public function isButtPregnant():Bool { return buttPregType != null; }
	
	public function knockUp(newPregType:PregnancyType, newPregIncubation:Int):Void
	{
		if (!isPregnant()) knockUpForce(newPregType, newPregIncubation);
	}
	
	public function knockUpForce(newPregType:PregnancyType, newPregIncubation:Int):Void
	{
		pregType = newPregType;
		gestation = newPregIncubation;
	}

	public function buttKnockUp(newPregType:PregnancyType, newPregIncubation:Int):Void
	{
		if (!isButtPregnant()) buttKnockUpForce(newPregType, newPregIncubation);
	}
	
	public function buttKnockUpForce(newPregType:PregnancyType, newPregIncubation:Int):Void
	{
		buttPregType = newPregType;
		buttGestation = newPregIncubation;
	}

	public function pregnancyAdvance():Void
	{
		if (gestation > 0 ){
			gestation--;
		}
		
		if (buttGestation > 0) {
			buttGestation--;
		}
	}
}