package data.body;
import data.body.enums.LowerBodyType;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class LowerBody 
{
	public var type:LowerBodyType;
	public var skin:Skin;
	public var legCount:Int;
	
	public function new(tml:TMLElement) 
	{
		type = LowerBodyType.createByName(tml.asString());
		if(tml.hasChild("Legs"))
			legCount = tml.getFirstChild("Legs").asInt();
		if(tml.hasChild("Skin"))
			skin = Skin.fromTML(tml.getFirstChild("Skin"));
	}
 
	public function nagaUnderBodyColor():String {
		return NagaLowerBodyColors[skin.tone];
	}
	
	public static var NagaLowerBodyColors:Map<String, String> = [
		"red" 			=> "orange",
		"orange" 		=> "yellow",
		"yellow" 		=> "yellowgreen",
		"yellowgreen"	=> "yellow",
		"green"			=> "light green",
		"spring green"  => "cyan",
		"cyan"			=> "ocean blue",
		"ocean blue"	=> "light blue",
		"blue"			=> "light blue",
		"purple"		=> "light purple",
		"magenta"		=> "blue",
		"deep pink"		=> "pink",
		"black"			=> "dark gray",
		"white"			=> "light gray",
		"gray"			=> "light gray",
		"light gray"	=> "white",
		"dark gray"		=> "gray",
		"pink"			=> "pale pink"
	];
	
}