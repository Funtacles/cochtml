package data.body;
import data.body.enums.AnalLooseness;
import data.body.enums.AnalWetness;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class Ass 
{
	public var analWetness:AnalWetness;
	public var analLooseness:AnalLooseness;
	//Used to determine thickness of knot relative to normal thickness
	//Used during sex to determine how full it currently is.  For multi-dick sex.
	public var fullness:Float = 0;
	public var virgin:Bool = true;
	
	public function new() 
	{
	}
	
	public function setFromTML(tml:TMLElement):Void {
		if (tml.hasChild("AnalWetness"))
			analWetness = AnalWetness.createByName(tml.getFirstChild("AnalWetness").asString());
		
		if (tml.hasChild("AnalLooseness"))
			analLooseness = AnalLooseness.createByName(tml.getFirstChild("AnalLooseness").asString());
			
		virgin = tml.hasChild("Virgin");
	}
	
	public static function fromTML(tml:TMLElement):Ass {
		var ass = new Ass();
		ass.setFromTML(tml);
		return ass;
	}
	
}