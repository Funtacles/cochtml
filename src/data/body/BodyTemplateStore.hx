package data.body;
import storage.tml.*;

/**
 * ...
 * @author Funtacles
 */
class BodyTemplateStore 
{
	private static var _docMap:Map<String, String>;
	private static var _store:Map<String, TMLElement>;
	
	public static function init():Void {
		_store = new Map<String, TMLElement>();
		_docMap = new Map<String, String>();
		
		for (doc in TMLDocuments.getDocuments())
		{
			var templates = TMLDocuments.getAllTags(doc, "Template");
			
			if (templates == null)
				continue;
			
			for (template in templates)
			{
				if (_store.exists(template.asString()))
					throw 'Duplicate body template definition "${template.asString()}" in "$doc"';
				
				_store.set(template.asString(), template);
				_docMap.set(template.asString(), doc);
			}
		}
	}
	
	public static function getTemplate(id:String):TMLElement {
		if (!_store.exists(id))
			return null;
		
		var template = _store.get(id).fullCopy();
		
		// If the template doesn't inherit from other templates just return it.
		if (template.getFirstChild("Inherits") == null)
			return template.fullCopy();
		
		var inherit = template.getFirstChild("Inherits").asString();
		if (StringUtil.isNullOrEmpty(inherit))
		{
			var doc = _docMap.get(template.asString());
			throw 'Empty "Inherits" tag found in body template "${template.asString()}" in "$doc"';
		}
		
		var superTemplate = getTemplate(inherit);
		
		return applyInherits(superTemplate, template);
	}
	
	private static function applyInherits(base:TMLElement, sub:TMLElement):TMLElement {
		var complete = base;
		
		for (child in sub.getChildren())
		{
			if (child.name == "Inherits")
				continue;
			
			var baseChildren = base.getChildrenWithName(child.name);
			
			if (baseChildren.length == 0) // Tag doesn't exist in base
				base.addChild(child);
			else {
				
			}
		}
		
		return complete;
	}
	
}