package data.body;
import data.body.enums.BreastSize;
import storage.tml.TMLElement;

/**
 * ...
 * @author Funtacles
 */
class BreastRow 
{
	public var breasts:Int = 2;
	public var nipplesPerBreast:Int = 1;
	public var breastRating:BreastSize;
	public var lactationMultiplier:Float = 0;
	public var nippleLength:Float;
	
	//Fullness used for lactation....if 75 or greater warning bells start going off!
	//If it reaches 100 it reduces lactation multiplier.
	public var milkFullness:Float = 0;
	public var fullness:Float = 0;
	public var fuckable:Bool = false;
	public var nippleCocks:Bool = false;
	
	public function new(tml:TMLElement)
	{
		breastRating = BreastSize.createByName(tml.asString());
		
		if (tml.hasChild("Nipples"))
			nipplesPerBreast = tml.getFirstChild("Nipples").asInt();
			
		if(tml.hasChild("Breasts"))
			breasts = tml.getFirstChild("Breasts").asInt();
			
		if (tml.hasChild("Lactation"))
			lactationMultiplier = tml.getFirstChild("Lactation").asFloat();
			
		fuckable = tml.hasChild("Fuckable");
		
		fuckable = tml.hasChild("NippleCocks");
	}
}