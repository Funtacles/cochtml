package resources;

/**
* This file is auto-generated on build. Any changes made to it will be lost.
*/
class Resources 
{
	/**
	 * TML file contents are contained here as a string. Each file's key is its file path relative to the
	 * resources\data folder and without the file extention.
	 */
	public static var tmlFiles:Map<String, String> = [
		"general" => Embedder.embedTextFile("resources\\data\\general.tml"),
		"intro" => Embedder.embedTextFile("resources\\data\\intro.tml"),
		"chargen\\predef-characters" => Embedder.embedTextFile("resources\\data\\chargen\\predef-characters.tml"),
		"chargen\\default-templates" => Embedder.embedTextFile("resources\\data\\chargen\\default-templates.tml"),
		"events\\camp" => Embedder.embedTextFile("resources\\data\\events\\camp.tml"),
		"events\\imp" => Embedder.embedTextFile("resources\\data\\events\\imp.tml"),
		"events\\intro" => Embedder.embedTextFile("resources\\data\\events\\intro.tml"),
		"events\\masturbation" => Embedder.embedTextFile("resources\\data\\events\\masturbation.tml"),
	];
}
