package resources;

/**
 * ...
 * @author Funtacles
 */
class Embedder 
{
	macro public static function embedTextFile(path:String):ExprOf<{}> {
		var content:String;
		
		try {
			var p = haxe.macro.Context.resolvePath(path);
			content = sys.io.File.getContent(p);
		}
		catch(e:Dynamic) {
			return haxe.macro.Context.error('Failed to load file $path: $e', haxe.macro.Context.currentPos());
		}
		
		return haxe.macro.Context.makeExpr(content, haxe.macro.Context.currentPos());
	}
	
}