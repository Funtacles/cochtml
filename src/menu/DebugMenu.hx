package menu;

import data.*;
import data.body.enums.*;
import data.inventory.InventorySlot;
import storage.tml.*;
import system.ParserContext;
import ui.SelectListOption;
import js.Browser;
import js.html.SelectElement;
import system.PlayerAppearance;
import data.body.BodyTemplateStore;
import system.Parser;
import data.game.global.Achievement;
import enums.Stat;
import data.event.*;

/**
 * ...
 * @author Funtacles
 */
class DebugMenu 
{
	private static var _back:Void -> Void;
	
	public static function start(back:Void->Void): Void {
		_back = back;
		root();
    }
	
	private static function root():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("Debuging Tools");
		
		UI.setMainButton(0, "Appearance", testAppearance);
		UI.setMainButton(1, "Inventory", testInventory);
		UI.setMainButton(2, "Achievement", function(){ Achievement.awardAchievement(Achievement.STORY_NEWCOMER); });
		UI.setMainButton(3, "Stat Bar", statBarTest);
		UI.setMainButton(4, "Events", eventTest);
		
		UI.setMainButton(14, "Back", _back);
	}
	

	// **** Chargen Tests ****
	private static function testAppearance():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("Test the appearance text display.");
		
		UI.setMainButton(0, "Default Male", showDefaultMale);
		UI.setMainButton(1, "Default Fem.", showDefaultFemale);
		UI.setMainButton(2, "Default Herm", showDefaultHerm);
		UI.setMainButton(3, "Random", showRandom);
		UI.setMainButton(4, "Select Predef", choosePredef);
		
		UI.setMainButton(14, "Back", root);
	}
	
	private static function showDefaultMale():Void {
		UI.clearMainText();
		
		var male:Player = new Player();
		male.buildFromTemplate(BodyTemplateStore.getTemplate("Male"));
		male.skin.tone = "olive";
		male.hair.color = "brown";
		male.hipRating = 4;
		male.femininity = 30;
		male.thickness = 50;
		male.buttRating = 4;
		
		var parsedText = Parser.parse(PlayerAppearance.getAppearance(male), {player: male, other: new Character() });
		
		UI.write(parsedText);
	}
	
	private static function showDefaultFemale():Void {
		UI.clearMainText();
		
		var female:Player = new Player();
		female.buildFromTemplate(BodyTemplateStore.getTemplate("Female"));
		female.skin.tone = "olive";
		female.hair.color = "brown";
		female.hipRating = 4;
		female.femininity = 70;
		female.thickness = 50;
		female.buttRating = 4;
		
		var parsedText = Parser.parse(PlayerAppearance.getAppearance(female), {player: female, other: new Character() });
		
		UI.write(parsedText);
	}
	
	private static function showDefaultHerm():Void {
		UI.clearMainText();
		
		var herm:Player = new Player();
		herm.buildFromTemplate(BodyTemplateStore.getTemplate("Herm"));
		herm.skin.tone = "olive";
		herm.hair.color = "brown";
		herm.hipRating = 4;
		herm.femininity = 50;
		herm.thickness = 50;
		herm.buttRating = 4;
		
		var parsedText = Parser.parse(PlayerAppearance.getAppearance(herm), {player: herm, other: new Character() });
		
		UI.write(parsedText);
	}
	
	private static function showRandom():Void {
		UI.clearMainText();
		
	}
	
	private static function choosePredef():Void{
		UI.clearMainText();
		UI.hideMainButtons();
		
		var predefs = TMLDocuments.getAllTags("predef-characters", "Def");
		
		var list = new Array<SelectListOption>();
		for (character in predefs)
		{
			var name = character.getFirstChild("Name");
			list.push(new SelectListOption(name.asString(), name.asString()));
		}
		
		var selectList = Util.generalSelectListHtml("predef", list);
		UI.write(selectList);
		
		UI.setMainButton(0, "Select", function(){
			var nameField = cast(Browser.document.getElementById("predef"), SelectElement);
			showPredef(nameField.value);
		});
		UI.setMainButton(4, "Back", testAppearance);
	}
	
	private static function showPredef(name:String):Void {
		UI.clearMainText();
	}
	
	
	// **** Inventory Tests ****
	private static function testInventory() {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("Test the inventory system.");
		
		UI.setMainButton(14, "Back", root);
	}
	
	
	// **** Stat Bar ****

	private static function statBarTest():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		var selectedStat:Int = 0;
		var stats = Stat.createAll();
		
		UI.writeLine("Selected " + stats[selectedStat].getName());
		
		var player:Player = new Player();
		UI.initSidebar(player);
		UI.sidebarVisible(true);
		
		UI.setMainButton(0, "Change up", function () {
			selectedStat--; 
			if (selectedStat < 0) selectedStat = 0; 
			UI.clearMainText();
			UI.writeLine("Selected " + stats[selectedStat].getName());
		});
		UI.setMainButton(1, "Change down", function () {
			selectedStat++; 
			if (selectedStat > 11) selectedStat = 11;
			UI.clearMainText();
			UI.writeLine("Selected " + stats[selectedStat].getName());
		});
		UI.setMainButton(2, "Decr. Stat", function() {
			UI.changeStat(stats[selectedStat], -5);
		});
		UI.setMainButton(3, "Incr. Stat", function() {
			UI.changeStat(stats[selectedStat], 5);
		});
		
		UI.setMainButton(4, "Clear Chg.", function() {
			UI.hideStatArrows();
		});
		
		UI.setMainButton(14, "Back", function () { UI.sidebarVisible(false); root(); });
	}
	
	public static function eventTest():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		var herm:Player = new Player();
		herm.buildFromTemplate(BodyTemplateStore.getTemplate("Herm"));
		herm.skin.tone = "olive";
		herm.hair.color = "brown";
		herm.hipRating = 4;
		herm.femininity = 50;
		herm.thickness = 50;
		herm.buttRating = 4;
		
		
		UI.setMainButton(0, "Test", function(){
			Game.setPlayer(herm);
			
			UI.initSidebar(herm);
			UI.sidebarVisible(true);
			
			EventMenu.startEvent("FistYourself");
		});
		
		UI.setMainButton(14, "Back", root);
	}
}