package menu;
import data.Player;
import js.Browser;
import js.html.Event;
import js.html.InputElement;
import js.html.InputEvent;
import js.html.SelectElement;
import storage.tml.*;
import ui.SelectListOption;
import data.body.BodyTemplateStore;
import system.Appearance;
import data.body.enums.*;
import data.game.perk.PerkStore;
import enums.Stat;

/**
 * ...
 * @author Funtacles
 */
class CharCreation 
{
	private static var predefs:Array<TMLElement>;
	
	private static var charName:String;
	private static var charSex:String;
	private static var charPhysique:String;
	private static var charSkin:String;
	private static var charHair:String;
	private static var beardStyle:String;
	private static var beardLength:String;
	
	private static var reachedSummary:Bool;
	
	private static var player:Player;
	
	public static function start():Void {
		UI.hideSystemButtons();
		
		reachedSummary = false;
		chooseName();
	}
	
	/**
	 * Sets up the choose name view.
	 */
	public static function chooseName():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("<div id='desc-area' style='height: 28vh;'></div>");
		var e = TMLDocuments.getFirstTag("intro", "NamePick");
		setDesc(e.content + "<br><br>What is your name?");
		
		UI.write("<input id='charname' type='text' maxlength='16' />");
		
		predefs = TMLDocuments.getAllTags("chargen\\predef-characters", "Def");
		var selectList = Util.generalSelectListHtml("predef", getSelectListOptions(predefs) , "Predefined Characters...");
		UI.write(selectList);
		
		var input = Browser.document.getElementById("charname");
		input.oninput = onNameChange;
		
		var elem = Browser.document.getElementById("predef");
		elem.onchange = onPredefChange;
		
		UI.setMainButton(4, "Next", resolveName);
		UI.disableMainButton(4);
	}
	
	/**
	 * Event listener for changes to the name text box.
	 * @param	e
	 */
	private static function onNameChange(e:InputEvent):Void {
		var element = cast(e.target, InputElement);
		if (element.value.length > 0)
			UI.enableMainButton(4);
		else
			UI.disableMainButton(4);
	}
	
	/**
	 * Event listener for when one of the predef names is chosen from the drop down.
	 * @param	e
	 */
	private static function onPredefChange(e:InputEvent):Void {
		var element = cast(e.target, SelectElement);
		var name = cast(Browser.document.getElementById("charname"), InputElement);
		name.value = element.value;
		UI.enableMainButton(4);
		
		var desc = '<b>${name.value}:</b> ';
		var hasHistory = false;
		for (predef in predefs){
			var p = predef.getFirstChild("Name");
			if (p.asString() == name.value) {
				desc += predef.getFirstChild("Desc").asString();
				hasHistory = predef.hasChild("History");
			}
		}
		
		desc += "<br><br>" + (hasHistory ? "This character has pre-defined history." : "This character has no pre-defined history.");
		
		setDesc(desc);
	}
	
	/**
	 * Sets the contents of the 'desc-area' div on the name selection view.
	 * @param	text
	 */
	private static function setDesc(text:String):Void{
		var element = Browser.document.getElementById("desc-area");
		element.innerHTML = text;
	}
	
	/**
	 * Determines if the name chosen is a special name and has history, and directs to the appropriate place in character creation process.
	 */
	private static function resolveName():Void{
		var nameField = cast(Browser.document.getElementById("charname"), InputElement);
		
		for (char in predefs){
			var name = char.getFirstChild("Name");
			if (name.asString() == nameField.value)
				specialName();
		}
		
		charName = nameField.value;
		chooseSex();
	}
	
	/**
	 * Checks with the user if they want to use the special name predef.
	 */
	private static function specialName():Void {
		UI.clearMainText();
		
		var special = TMLDocuments.getFirstTag("intro", "SpecialName");
		UI.write(special.asString());
		
		UI.hideMainButtons();
		UI.setMainButton(0, "Special Name", goWithSpecial, "Continue with the special name.");
		UI.setMainButton(1, "Default", chooseSex,  "Use the name like normal.");
		UI.setMainButton(14, "Back", chooseName, "Go back and change your name");
	}
	
	private static function goWithSpecial():Void {
		
	}
	
	private static function chooseSex():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("What is your sex?");
		
		UI.setMainButton(0, "Male", gotoNextOrSummary(choosePhysique, function() { charSex = "Male"; }), "Gives you a penis and testicles, but no breasts or vagina.");
		UI.setMainButton(1, "Female", gotoNextOrSummary(choosePhysique, function() { charSex = "Female"; }), "Gives you breasts and a vagina, but no male bits.");
		UI.setMainButton(14, "Back", gotoNextOrSummary(chooseName));
	}
	
	private static function choosePhysique():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		switch(charSex){
			case "Male":
				UI.setMainButton(0, "Lean", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "lean"; }));
				UI.setMainButton(1, "Average", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "average";  }));
				UI.setMainButton(2, "Thick", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "thick";  }));
				UI.setMainButton(3, "Girly", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "girly";  }));
				var tag = TMLDocuments.getFirstTag("intro", "MaleDesc");
				UI.write(tag.asString());
			case "Female":
				UI.setMainButton(0, "Slender", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "lean"; }));
				UI.setMainButton(1, "Average", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "average";  }));
				UI.setMainButton(2, "Curvy", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "thick";  }));
				UI.setMainButton(3, "Tomboyish", gotoNextOrSummary( chooseComplexion, function() { charPhysique = "tomboyish"; }));
				var tag = TMLDocuments.getFirstTag("intro", "FemaleDesc");
				UI.write(tag.asString());
			case "Herm":
				UI.setMainButton(0, "Fem. Slender", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "femlean"; }));
				UI.setMainButton(1, "Fem. Average", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "femaverage";  }));
				UI.setMainButton(2, "Fem. Curvy", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "femthick";  }));
				UI.setMainButton(5, "Mas. Lean", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "maslean"; }));
				UI.setMainButton(6, "Mas. Average", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "masaverage";  }));
				UI.setMainButton(7, "Mas. Thick", gotoNextOrSummary(chooseComplexion, function() { charPhysique = "masthick";  }));
		}
		
		UI.write("<br><br>What is your build?");
		
		UI.setMainButton(14, "Back", gotoNextOrSummary(chooseSex));
	}
	
	private static function chooseComplexion():Void{
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("What is your complexion?");
		
		var func = function (skin:String): Void -> Void {
			return function(){
				if (reachedSummary)
					player.skin.tone = skin;
				else
					charSkin = skin;
			};
		};
		
		UI.setMainButton(0, "Light", gotoNextOrSummary(chooseHairColor, func("light")));
		UI.setMainButton(1, "Fair", gotoNextOrSummary(chooseHairColor, func("fair")));
		UI.setMainButton(2, "Olive", gotoNextOrSummary(chooseHairColor, func("olive")));
		UI.setMainButton(3, "Dark", gotoNextOrSummary(chooseHairColor, func("dark")));
		UI.setMainButton(4, "Ebony", gotoNextOrSummary(chooseHairColor, func("ebony")));
		UI.setMainButton(5, "Mahogany", gotoNextOrSummary(chooseHairColor, func("mahogany")));
		UI.setMainButton(6, "Russet", gotoNextOrSummary(chooseHairColor, func("russet")));
		UI.setMainButton(14, "Back", gotoNextOrSummary(choosePhysique));
	}
	
	private static function chooseHairColor():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		var compl = "";
		
		if (!reachedSummary){
			switch(charSkin) {
				case "olive", "ebony":
					compl = "an " + StringUtil.capitalize(charSkin);
				default:
					compl = "a " + StringUtil.capitalize(charSkin);
			}
			
			UI.write('You selected $compl complexion<br><br>');
		}
		
		var func = function (color:String): Void -> Void {
			return function(){
				if (reachedSummary)
					player.hair.color = color;
				else
					charHair = color;
			};
		};
		
		UI.write("What is your hair color?");
		
		UI.setMainButton(0, "Blonde", gotoNextOrSummary(buildCharacter, func("blonde")));
		UI.setMainButton(1, "Brown", gotoNextOrSummary(buildCharacter, func("brown")));
		UI.setMainButton(2, "Black", gotoNextOrSummary(buildCharacter, func("black")));
		UI.setMainButton(3, "Red", gotoNextOrSummary(buildCharacter, func("red")));
		UI.setMainButton(4, "Gray", gotoNextOrSummary(buildCharacter, func("gray")));
		UI.setMainButton(5, "White", gotoNextOrSummary(buildCharacter, func("white")));
		UI.setMainButton(6, "Auburn", gotoNextOrSummary(buildCharacter, func("auburn")));
		
		UI.setMainButton(14, "Back", gotoNextOrSummary(chooseComplexion));
	}
	
	private static function buildCharacter():Void {
		player = new Player();
		player.buildFromTemplate(BodyTemplateStore.getTemplate(charSex));
		player.shortName = charName;
		
		player.skin.tone = charSkin;
		player.hair.color = charHair;
		
		player.stats[Stat.Strength] = 15;
		player.stats[Stat.Toughness] = 15;
		player.stats[Stat.Speed] = 15;
		player.stats[Stat.Intelligence] = 15;
		player.stats[Stat.Sensitivity] = 15;
		player.stats[Stat.Libido] = 15;
		player.stats[Stat.Corruption] = 15;
		
		player.lust = 15;
		player.hunger = 80;
		
		player.hipRating = 4;
		player.femininity = 70;
		player.thickness = 50;
		player.buttRating = 4;
		
		switch(charSex)
		{
			case "Male":
				player.stats[Stat.Strength] += 3;
				player.stats[Stat.Toughness] += 2;
				
				switch(charPhysique)
				{
					case "lean":
						buildLeanMale();
					case "average":
						buildAverageMale();
					case "thick":
						buildThickMale();
					case "girly":
						buildGirlyMale();
				}
			case "Female":
				player.stats[Stat.Speed] += 3;
				player.stats[Stat.Intelligence] += 2;
				
				switch(charPhysique)
				{
					case "lean":
						buildSlenderFemale();
					case "average":
						buildAverageFemale();
					case "thick":
						buildCurvyFemale();
					case "tomboyish":
						buildTomboyishFemale();
				}
			case "Herm":
				var rand = Std.random(4); //Add random extra stat point so herms get the same number of stats as everyone else.
				
				player.stats[Stat.Strength] += 		(rand == 0 ? 2 : 1);
				player.stats[Stat.Toughness] += 	(rand == 1 ? 2 : 1);
				player.stats[Stat.Speed] += 		(rand == 2 ? 2 : 1);
				player.stats[Stat.Intelligence] += 	(rand == 3 ? 2 : 1);
				
				switch(charPhysique)
				{
					case "femlean":
						buildSlenderFemale();
					case "femaverage":
						buildAverageFemale();
					case "femthick":
						buildCurvyFemale();
					case "maslean":
						buildLeanMale();
					case "masaverage":
						buildAverageMale();
					case "masthick":
						buildThickMale();
				}
		}
		
		summary();
	}
	
	private static function buildLeanMale(): Void {
		player.stats[Stat.Strength] -= 1;
		player.stats[Stat.Speed] += 1;
		
		player.femininity = 34;
		player.thickness = 30;
		player.tone += 5;
		
		player.breastRows[0].breastRating = BreastSize.Flat;
		player.buttRating = ButtSize.Tight;
		player.hipRating = HipSize.Slender;
	}

	private static function buildSlenderFemale(): Void {
		player.stats[Stat.Strength] -= 1;
		player.stats[Stat.Speed] += 1;
		
		player.femininity = 66;
		player.thickness = 30;
		player.tone += 5;
		
		player.breastRows[0].breastRating = BreastSize.BCup;
		player.buttRating = ButtSize.Tight;
		player.hipRating = HipSize.Ample;
	}

	private static function buildAverageMale(): Void {
		player.femininity = 30;
		player.thickness = 50;
		
		player.breastRows[0].breastRating = BreastSize.Flat;
		player.buttRating = ButtSize.Average;
		player.hipRating = HipSize.Average;
	}

	private static function buildAverageFemale(): Void {
		player.femininity = 70;
		player.thickness = 50;
		
		player.breastRows[0].breastRating = BreastSize.CCup;
		player.buttRating = ButtSize.Noticeable;
		player.hipRating = HipSize.Ample;
	}

	private static function buildThickMale(): Void {
		player.stats[Stat.Speed] -= 4;
		player.stats[Stat.Strength] += 2;
		player.stats[Stat.Toughness] += 2;
		
		player.femininity = 29;
		player.thickness = 70;
		player.tone -= 5;
		
		player.breastRows[0].breastRating = BreastSize.Flat;
		player.buttRating = ButtSize.Noticeable;
		player.hipRating = HipSize.Average;
	}

	private static function buildCurvyFemale(): Void {
		player.stats[Stat.Speed] -= 2;
		player.stats[Stat.Strength] += 1;
		player.stats[Stat.Toughness] += 1;
		
		player.femininity = 71;
		player.thickness = 70;
		
		player.breastRows[0].breastRating = BreastSize.DCup;
		player.buttRating = ButtSize.Large;
		player.hipRating = HipSize.Curvy;
	}

	private static function buildGirlyMale(): Void {
		player.stats[Stat.Strength] -= 2;
		player.stats[Stat.Speed] += 2;
		
		player.femininity = 50;
		player.thickness = 50;
		player.tone = 26;
		
		player.breastRows[0].breastRating = BreastSize.ACup;
		player.buttRating = ButtSize.Noticeable;
		player.hipRating = HipSize.Slender;
	}

	private static function buildTomboyishFemale(): Void {
		player.stats[Stat.Strength] += 1;
		player.stats[Stat.Speed] -= 1;
		
		player.femininity = 56;
		player.thickness = 50;
		player.tone = 50;
		
		player.breastRows[0].breastRating = BreastSize.ACup;
		player.buttRating = ButtSize.Tight;
		player.hipRating = HipSize.Slender;
	}
	
	private static function summary():Void{
		reachedSummary = true;
		
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write(TMLDocuments.getFirstTag("intro", "Summary").asString());
		UI.write("<br><br>");
		UI.writeLine("Height: " + Math.floor(player.height / 12) + "'" + player.height % 12 + "\"");
		UI.writeLine("Skin tone: " + StringUtil.capitalize(player.skin.tone));
		UI.writeLine("Hair color: " + StringUtil.capitalize(player.hair.color));
		if (player.hasCock()) {
			UI.writeLine("Cock size: " + player.cocks[0].length + "\" long, " + player.cocks[0].thickness + "\" thick");
		}
		UI.writeLine("Breast size: " + Appearance.breastCup(player.breastRows[0].breastRating));
		
		UI.setMainButton(0, "Complexion", chooseComplexion);
		UI.setMainButton(1, "Hair Color", chooseHairColor);
		if(charSex == "Male")
			UI.setMainButton(2, "Beard Style", chooseBeard);
		UI.setMainButton(3, "Set Height", chooseHeight);
		if(player.hasCock())
			UI.setMainButton(5, "Cock Size", chooseCockSize);
		if(charSex != "Male")
			UI.setMainButton(6, "Breast Size", chooseBreastSize);
		UI.setMainButton(7, "Hair Length", chooseHairLength);
		
		UI.setMainButton(9, "Done", chooseEndowment);
	}
	
	private static function chooseBeard():Void{
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("You can choose your beard length and style.<br><br>");
		UI.write("Beard: " + Appearance.beardDescription(player));
		
		UI.setMainButton(0, "Style", chooseBeardStyle);
		UI.setMainButton(1, "Length", chooseBeardLength);
		
		UI.setMainButton(14, "Back", summary);
	}
	
	private static function chooseBeardStyle():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("What beard style would you like?");
		
		//TODO Beard styles
		UI.setMainButton(0, "Normal", function(){ player.beardStyle = BeardType.Normal; chooseBeard(); });
		UI.setMainButton(1, "Goatee", function(){ player.beardStyle = BeardType.Goatee; chooseBeard(); });
		UI.setMainButton(2, "Clean Cut", function(){ player.beardStyle = BeardType.CleanCut; chooseBeard(); });
		UI.setMainButton(3, "Mountainman", function(){ player.beardStyle = BeardType.MountainMan; chooseBeard(); });
		
		UI.setMainButton(14, "Back", chooseBeard);
	}
	
	private static function chooseBeardLength():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("How long would you like your beard be? <br><br>");
		UI.write("Note: Beard will slowly grow over time, just like in the real world. Unless you have no beard. You can change your beard style later in the game.");
		
		//TODO Beard Lengths
		UI.setMainButton(0, "No Beard", function(){ player.beardLength = 0; chooseBeard(); });
		UI.setMainButton(1, "Trim", function(){ player.beardLength = 0.1; chooseBeard(); });
		UI.setMainButton(2, "Short", function(){ player.beardLength = 0.2; chooseBeard(); });
		UI.setMainButton(3, "Medium", function(){ player.beardLength = 0.5; chooseBeard(); });
		UI.setMainButton(4, "Mod. Long", function(){ player.beardLength = 1.5; chooseBeard(); });
		UI.setMainButton(5, "Long", function(){ player.beardLength = 3; chooseBeard(); });
		UI.setMainButton(6, "Very Long", function(){ player.beardLength = 6; chooseBeard(); });
		
		UI.setMainButton(14, "Back", chooseBeard);
	}
	
	private static function chooseHeight():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("Set your height in inches.<br> You can choose any height between 3.5 feet (42 inches) and 8 feet (96 inches).");
		UI.write('<br><input id="height" type="number" min="42" max="96" step="1" value="${player.height}" />');
		
		UI.setMainButton(0, "Ok", function(){
			var input = cast(Browser.document.getElementById("height"), InputElement);
			player.height = Std.parseInt(input.value);
			
			summary();
		});
		UI.setMainButton(4, "Back", summary);
	}
	
	public static function chooseCockSize():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("You can choose a cock length between 4 and 8 inches. Your starting cock length will also affect starting cock thickness." + 
			"<br><br>Cock type and size can be altered later in the game through certain items.");
		
		var func = function(length:Float): Void -> Void {
			return function (){
				player.cocks[0].length = length;
				player.cocks[0].thickness = Math.floor(((length / 5) - 0.1) * 10) / 10;
				summary();
			};
		};
		
		UI.setMainButton(0, "3\"", func(3));
		UI.setMainButton(1, "3.5\"", func(3.5));
		UI.setMainButton(2, "4\"", func(4));
		UI.setMainButton(3, "4.5\"", func(4.5));
		UI.setMainButton(4, "5\"", func(5));
		UI.setMainButton(5, "5.5\"", func(5.5));
		UI.setMainButton(6, "6\"", func(6));
		UI.setMainButton(7, "6.5\"", func(6.5));
		UI.setMainButton(8, "7", func(7));
		UI.setMainButton(9, "7.5\"", func(7.5));
		UI.setMainButton(10, "8\"", func(8));
		
		UI.setMainButton(14, "Back", summary);
	}
	
	private static function chooseBreastSize():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("You can choose a breast size. Breast size may be altered later in the game.");
		
		UI.setMainButton(0, "Flat", function() {player.breastRows[0].breastRating = BreastSize.Flat; summary(); });
		UI.setMainButton(1, "A Cup", function() {player.breastRows[0].breastRating = BreastSize.ACup; summary(); });
		UI.setMainButton(2, "B Cup", function() {player.breastRows[0].breastRating = BreastSize.BCup; summary(); });
		UI.setMainButton(3, "C Cup", function() {player.breastRows[0].breastRating = BreastSize.CCup; summary(); });
		UI.setMainButton(4, "D Cup", function() {player.breastRows[0].breastRating = BreastSize.DCup; summary(); });
		UI.setMainButton(5, "DD Cup", function() {player.breastRows[0].breastRating = BreastSize.DDCup; summary(); });
		
		UI.setMainButton(14, "Back", summary);
	}
	
	private static function chooseHairLength():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("Set your hair length in inches. <br>You can choose any length between 0(bald) and 8 feet (96 inches, most likely floor-dragging).");
		UI.write('<br><input id="length" type="number" min="40" max="96" step="1" value="${player.hair.length}" />');
		
		UI.setMainButton(0, "Ok", function(){
			var input = cast(Browser.document.getElementById("length"), InputElement);
			player.hair.length = Std.parseInt(input.value);
			
			summary();
		});
		UI.setMainButton(4, "Back", summary);
	}
	
	public static function chooseEndowment():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write("Every person is born with a gift.  What's yours?");
		var totalStartingPerks:Int = 0;
		//Attribute Perks
		var buttonTexts = ["Strong", "Tough", "Fast", "Smarts", "Libido", "Touch", "Perversion"];
		var buttonPerks = [PerkStore.Strong, PerkStore.Tough, PerkStore.Fast, PerkStore.Smart, PerkStore.Lusty, PerkStore.Sensitive, PerkStore.Pervert];
		//Endowment Perks
		if (player.hasCock()) {
			buttonTexts = buttonTexts.concat(["Big Cock", "Lots of Jizz"]);
			buttonPerks = buttonPerks.concat([PerkStore.BigCock, PerkStore.MessyOrgasms]);
		}
		if (player.hasVagina()) {
			buttonTexts = buttonTexts.concat(["Big Breasts", "Big Clit", "Fertile", "Wet Vagina"]);
			buttonPerks = buttonPerks.concat([PerkStore.BigTits, PerkStore.BigClit, PerkStore.Fertile, PerkStore.WetPussy]);
		}
		//Add buttons
		for (i in 0...buttonTexts.length) {
			if (!player.hasPerk(buttonPerks[i])) {
				UI.setMainButton(i, buttonTexts[i], function() { confirmEndowment(buttonPerks[i]); });
			} else {
				UI.setMainButton(i, buttonTexts[i], null, "You already have this starting perk.");
				totalStartingPerks++;
			}
		}
		
		//Option to skip if you have enough starting perks
		if (totalStartingPerks >= 4) UI.setMainButton(14, "Skip", chooseHistory);
	}
	
	private static function confirmEndowment(choice:String):Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		switch(choice) {
			//Attributes
			case PerkStore.Strong:
				UI.write("Are you stronger than normal? (+5 Strength)<br><br>Strength increases your combat damage, and your ability to hold on to an enemy or pull yourself away.");
			case PerkStore.Tough:
				UI.write("Are you unusually tough? (+5  Toughness)<br><br>Toughness gives you more HP and increases the chances an attack against you will fail to wound you.");
			case PerkStore.Fast:
				UI.write("Are you very quick?  (+5  Speed)<br><br>Speed makes it easier to escape combat and grapples.  It also boosts your chances of evading an enemy attack and successfully catching up to enemies who try to run.");
			case PerkStore.Smart:
				UI.write("Are you a quick learner?  (+5 Intellect)<br><br>Intellect can help you avoid dangerous monsters or work with machinery.  It will also boost the power of any spells you may learn in your travels.");
			case PerkStore.Lusty:
				UI.write("Do you have an unusually high sex-drive?  (+5 Libido)<br><br>Libido affects how quickly your lust builds over time.  You may find a high libido to be more trouble than it's worth...");
			case PerkStore.Sensitive:
				UI.write("Is your skin unusually sensitive?  (+5 Sensitivity)<br><br>Sensitivity affects how easily touches and certain magics will raise your lust.  Very low sensitivity will make it difficult to orgasm.");
			case PerkStore.Pervert:
				UI.write("Are you unusually perverted?  (+5 Corruption)<br>Corruption affects certain scenes and having a higher corruption makes you more prone to Bad Ends.<br>");
			//Gender-specific
			case PerkStore.BigCock:
				UI.write("Do you have a big cock?  (+2\" Cock Length)<br><br>A bigger cock will make it easier to get off any sexual partners, but only if they can take your size.");
			case PerkStore.MessyOrgasms:
				UI.write("Are your orgasms particularly messy?  (+50% Cum Multiplier)<br><br>A higher cum multiplier will cause your orgasms to be messier.");
			case PerkStore.BigTits:
				UI.write("Are your breasts bigger than average? (+2 cup sizes)<br><br>Larger breasts will allow you to lactate greater amounts, tit-fuck larger cocks, and generally be a sexy bitch.");
			case PerkStore.BigClit:
				UI.write("Do you have a big clit?  (1\" Long)<br><br>A large enough clit may eventually become as large as a cock.  It also makes you gain lust much faster during oral or manual stimulation.");
			case PerkStore.Fertile:
				UI.write("Is your family particularly fertile?  (+15% Fertility)<br><br>A high fertility will cause you to become pregnant much more easily.  Pregnancy may result in: Strange children, larger bust, larger hips, a bigger ass, and other weirdness.");
			case PerkStore.WetPussy:
				UI.write("Does your pussy get particularly wet?  (+1 Vaginal Wetness)<br><br>Vaginal wetness will make it easier to take larger cocks, in turn helping you bring the well-endowed to orgasm quicker.");
		}
		
		UI.setMainButton(0, "Yes", function(){ setEndowment(choice); });
		UI.setMainButton(1, "No", chooseEndowment);
	}
	
	private static function setEndowment(choice:String):Void {
		switch(choice) {
			//Attribute-specific
			case PerkStore.Strong:
				player.stats[Stat.Strength] += 5;
				player.tone += 7;
				player.thickness += 3;
				player.addPerk(PerkStore.Strong);
			case PerkStore.Tough:
				player.stats[Stat.Toughness]+= 5;
				player.tone += 5;
				player.thickness += 5;
				player.addPerk(PerkStore.Tough);
				player.hp = player.maxHP();
			case PerkStore.Fast:
				player.stats[Stat.Speed] += 5;
				player.tone += 10;
				player.addPerk(PerkStore.Fast);
			case PerkStore.Smart:
				player.stats[Stat.Intelligence] += 5;
				player.thickness -= 5;
				player.addPerk(PerkStore.Smart);
			case PerkStore.Lusty:
				player.stats[Stat.Libido] += 5;
				player.addPerk(PerkStore.Lusty);
			case PerkStore.Sensitive:
				player.stats[Stat.Sensitivity] += 5;
				player.addPerk(PerkStore.Sensitive);
			case PerkStore.Pervert:
				player.stats[Stat.Corruption] += 5;
				player.addPerk(PerkStore.Pervert);
			//Genital-specific
			case PerkStore.BigCock:
				player.femininity -= 5;
				player.cocks[0].length += 2;
				player.cocks[0].thickness =  Math.floor(((player.cocks[0].length / 5) - 0.1) * 10) / 10;
				player.addPerk(PerkStore.BigCock);
			case PerkStore.MessyOrgasms:
				player.femininity -= 2;
				player.cumMultiplier = 1.5;
				player.addPerk(PerkStore.MessyOrgasms);
			case PerkStore.BigTits:
				player.femininity += 5;
				player.breastRows[0].breastRating = BreastSize.createByIndex(player.breastRows[0].breastRating.getIndex() + 2);
				player.addPerk(PerkStore.BigTits);
			case PerkStore.BigClit:
				player.femininity -= 5;
				player.vaginas[0].clitLength = 1;
				player.addPerk(PerkStore.BigClit);
			case PerkStore.Fertile:
				player.femininity += 5;
				player.fertility += 25;
				player.hipRating += 2;
				player.addPerk(PerkStore.Fertile);
			case PerkStore.WetPussy:
				player.femininity += 7;
				player.vaginas[0].vaginalWetness = VaginaWetness.createByIndex(player.vaginas[0].vaginalWetness.getIndex() + 1);
				player.addPerk(PerkStore.WetPussy);
		}
		
		chooseHistory();
	}
	
	//-----------------
	//-- HISTORY PERKS
	//-----------------
	private static function chooseHistory():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		var totalHistoryPerks:Int = 0;
		UI.write("Before you became a champion, you had other plans for your life.  What were you doing before?");
		//Attribute Perks
		var buttonTexts = ["Alchemy", "Fighting", "Fortune", "Healing", "Religion", "Schooling", "Slacking", "Slutting", "Smithing", "Whoring"];
		var buttonPerks = [PerkStore.HistoryAlchemist, PerkStore.HistoryFighter, PerkStore.HistoryFortune, PerkStore.HistoryHealer, PerkStore.HistoryReligious, PerkStore.HistoryScholar, PerkStore.HistorySlacker, PerkStore.HistorySlut, PerkStore.HistorySmith, PerkStore.HistoryWhore];
		for (i in 0...buttonTexts.length) {
			if (!player.hasPerk(buttonPerks[i])) {
				UI.setMainButton(i, buttonTexts[i], function () { confirmHistory(buttonPerks[i]); } );
			} else {
				UI.setMainButton(i, buttonTexts[i], null, "You already have this history perk.");
				totalHistoryPerks++;
			}
		}
		
		if (totalHistoryPerks >= 3) 
			UI.setMainButton(14, "Skip", finalize);
	}
		
	private static function confirmHistory(choice:String):Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		switch (choice) {
			case PerkStore.HistoryAlchemist:
				UI.write("You spent some time as an alchemist's assistant, and alchemical items always seem to be more reactive in your hands.  Is this your history?");
			case PerkStore.HistoryFighter:
				UI.write("You spent much of your time fighting other children, and you had plans to find work as a guard when you grew up.  You do 10% more damage with physical attacks.  You will also start out with 50 gems.  Is this your history?");
			case PerkStore.HistoryFortune:
				UI.write("You always feel lucky when it comes to fortune.  Because of that, you have always managed to save up gems until whatever's needed and how to make the most out it (+15% gems on victory).  You will also start out with 250 gems.  Is this your history?");
			case PerkStore.HistoryHealer:
				UI.write("You often spent your free time with the village healer, learning how to tend to wounds.  Healing items and effects are 20% more effective.  Is this your history?");
			case PerkStore.HistoryReligious:
				UI.write("You spent a lot of time at the village temple, and learned how to meditate.  The 'masturbation' option is replaced with 'meditate' when corruption is at or below 66.  Is this your history?");
			case PerkStore.HistoryScholar:
				UI.write("You spent much of your time in school, and even begged the richest man in town, Mr. Sellet, to let you read some of his books.  You are much better at focusing, and spellcasting uses 20% less fatigue.  Is this your history?");
			case PerkStore.HistorySlacker:
				UI.write("You spent a lot of time slacking, avoiding work, and otherwise making a nuisance of yourself.  Your efforts at slacking have made you quite adept at resting, and your fatigue comes back 20% faster.  Is this your history?");
			case PerkStore.HistorySlut:
				UI.write("You managed to spend most of your time having sex.  Quite simply, when it came to sex, you were the village bicycle - everyone got a ride.  Because of this, your body is a bit more resistant to penetrative stretching, and has a higher upper limit on what exactly can be inserted.  Is this your history?");
			case PerkStore.HistorySmith:
				UI.write("You managed to get an apprenticeship with the local blacksmith.  Because of your time spent at the blacksmith's side, you've learned how to fit armor for maximum protection.  Is this your history?");
			case PerkStore.HistoryWhore:
				UI.write("You managed to find work as a whore.  Because of your time spent trading seduction for profit, you're more effective at teasing (+15% tease damage).  Is this your history?");
			default:
				UI.write("ERROR: Invalid history selection, something broke.");
		}
		
		UI.setMainButton(0, "Yes", function () { setHistory(choice);} );
		UI.setMainButton(1, "No", chooseHistory);
	}

	private static function setHistory(choice:String):Void {
		player.addPerk(choice);
		if (choice == PerkStore.HistorySlut || choice == PerkStore.HistoryWhore ) {
			if (player.hasVagina()) {
				player.vaginas[0].virgin = false;
				player.vaginas[0].vaginalLooseness = VaginaLooseness.Loose;
			}
			player.ass.analLooseness = AnalLooseness.Tight;
		}
		if (choice == PerkStore.HistoryFighter || choice == PerkStore.HistoryWhore) {
			player.gems += 50;
		}
		if (choice == PerkStore.HistoryFortune) {
			player.gems += 250;
		}
		
		finalize();
	}
	
	private static function finalize():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		player.hp = player.maxHP();
		
		/*if (flags[kFLAGS.NEW_GAME_PLUS_LEVEL] == 0) chooseGameModes();
		else*/ startTheGame();
	}
	
	
	private static function startTheGame():Void {
		player.startingRace = player.getRace();
		
		//if (flags[kFLAGS.HARDCORE_MODE] > 0) {
			//trace("Hardcore save file " + flags[kFLAGS.HARDCORE_SLOT] + " created.");
			//getGame().saves.saveGame(flags[kFLAGS.HARDCORE_SLOT])
		//}
		//if (flags[kFLAGS.GRIMDARK_MODE] > 0) {
			//flags[kFLAGS.BACKGROUND_STYLE] = 9;
		//}
		
		chooseToPlay();
	}
	
	public static function chooseToPlay():Void {
		//if (flags[kFLAGS.NEW_GAME_PLUS_LEVEL] == 0) {
			//if (player.femininity >= 55) player.setUndergarment(undergarments.C_PANTY);
			//else player.setUndergarment(undergarments.C_LOIN);
			//if (player.biggestTitSize() >= 2) player.setUndergarment(undergarments.C_BRA);
		//}
		//if (flags[kFLAGS.GRIMDARK_MODE] > 0) {
			//arrival();
			//return;
		//}
		//clearOutput();
		//outputText("Would you like to play through the 3-day prologue in Ingnam or just skip?");
		//doYesNo(goToIngnam, arrival);
		//arrival();
		
		Game.beginGameWithNewCharacter(player);
	}
	
	/**
	 * Used to modify where the button action takes the user.
	 * @param	next	The action to take on the next view unless reachedSummary is set
	 * @param	action	The action always taken regardless of whether or not reachedSummary is set.
	 * @return 	Either next() or summary()
	 */
	private static function gotoNextOrSummary(next: Void -> Void, action:Void -> Void = null):Void -> Void {
		return function() {
			if(action != null)
				action();
			if(reachedSummary)
				summary();
			else 
				next(); 
		};
	}
	
	/**
	 * Populates the select list options for the name choosing page.
	 * @param	characters	List of character tags;
	 * @return	A list of SelectListOptions containing the character names and an indication of whether or not they have a history.
	 */
	private static function getSelectListOptions(characters: Array<TMLElement>):Array<SelectListOption> {
		var list = new Array<SelectListOption>();
		for (character in characters)
		{
			var name = character.getFirstChild("Name");
			var hasHistory = character.hasChild("History");
			var display = hasHistory ? name.content + " (H)" : name.asString();
			list.push(new SelectListOption(display, name.content));
		}
		
		return list;
	}
	
}