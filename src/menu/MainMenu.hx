package menu;

import Game;
import storage.tml.*;

/**
 * ...
 * @author Funtacles
 */
class MainMenu 
{
	public static function start(): Void {
		UI.sidebarVisible(false);
        UI.hideSystemButtons();
        UI.setSystemButton(0, "New Game", Game.startNewGame);
        UI.setSystemButton(1, "Data", showLoadScreen);
		
		showStartView();
	}
	
	public static function showStartView(): Void {
        UI.clearMainText();
        UI.write("<h2>Corruption of Champions - HTML Edition</h2>");
		UI.write('<b>(${Game.version})</b>');
		var tag:TMLElement = TMLDocuments.getFirstTag("general", "MainPage");
        UI.write(tag.asString());
		
		//if (debug)
			//outputText("\n\n<b>DEBUG MODE ENABLED: ITEMS WILL NOT BE CONSUMED BY USE.</b>");
		//if (flags[kFLAGS.SHOW_SPRITES_FLAG])
			//outputText("\n\n<b>Sprites disabled.</b>");
		//if (flags[kFLAGS.EASY_MODE_ENABLE_FLAG])
			//outputText("\n\n<b>Easy Mode On: Bad-ends can be ignored.</b>");
		//if (flags[kFLAGS.SILLY_MODE_ENABLE_FLAG])
			//outputText("\n\n<b>SILLY MODE ENGAGED: Crazy, nonsensical, and possibly hilarious things may occur.</b>");
		//if (flags[kFLAGS.ITS_EVERY_DAY])
			//outputText("\n\n<b>Eternal holiday enabled.</b>");
		//if (plains.bunnyGirl.isItEaster())
			//outputText("\n\n<b>It's Easter! Enjoy the eggs!</b>");
		//if (valentines.isItValentine())
			//outputText("\n\n<b>It's Valentine's!</b>");
		//if (helFollower.isHeliaBirthday())
			//outputText("\n\n<b>It's Helia's Birthday Month!</b>");

        UI.hideMainButtons();
        UI.setMainButton(0, "Resume", "Please load or start a game first.");
        UI.setMainButton(1, "Settings", showOptions, "Configure game settings and enable cheats.");
        UI.setMainButton(2, "Instructions", showInstructions, "How to play. Starting tips. And hotkeys for easy left handed play...");
        UI.setMainButton(3, "Achievements", showAchievements, "View all achievements you have unlocked so far.");
        UI.setMainButton(4, "Mod Thread", Util.generateLinkFunction("https://forum.fenoxo.com/threads/coc-revamp-mod-v1-4-8-for-coc-1-0-2.3/"), "Check the official mod thread on Fenoxo's forum.");
        UI.setMainButton(5, "Credits", showCredits, "See the list of the all the cool people who have contributed to this game.");
        UI.setMainButton(6, "Image Credits", showImageCredits, "Check out who contributed to the image pack");
        UI.setMainButton(7, "Debug", showDebug, "View debug tools. You can also access any scene provided you know the id.");
    }
	
	public static function showLoadScreen(): Void {
        UI.clearMainText();
		var tag:TMLElement = TMLDocuments.getFirstTag("general", "SaveLoad");
        UI.write(tag.asString());

        UI.hideMainButtons();
        UI.setMainButton(1, "Load", showLoadGameList);
        UI.setMainButton(2, "Delete", showDeleteGameList);
        UI.setMainButton(14, "Back", showStartView);
    }
	
	public static function showLoadGameList(): Void {

    }

    public static function showDeleteGameList(): Void {

    }

    public static function showOptions(): Void {

    }

    public static function showInstructions(): Void {
		UI.clearMainText();
		var tag:TMLElement = TMLDocuments.getFirstTag("general", "Instructions");
        UI.write(tag.asString());

        UI.hideMainButtons();
        UI.setMainButton(14, "Back", showStartView);
    }

    public static function showAchievements(): Void {

    }

    public static function showCredits(): Void {
        UI.clearMainText();
		var tag:TMLElement = TMLDocuments.getFirstTag("general", "Credits");
        UI.write(tag.asString());

        UI.hideMainButtons();
        UI.setMainButton(14, "Back", showStartView);
    }

    public static function showImageCredits(): Void {

    }

    public static function showDebug(): Void {
		DebugMenu.start(showStartView);
    }
}