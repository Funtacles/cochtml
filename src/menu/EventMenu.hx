package menu;
import data.Character;
import data.event.*;

/**
 * ...
 * @author Funtacles
 */
class EventMenu 
{
	public static function startEvent(eventId:String):Void {
		var evt:Event = EventStore.getEvent(eventId);
		
		runEvent(evt);
	}
	
	private static function runEvent(evt:Event):Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write(evt.getText({player: Game.player, other: new Character()}));
		
		evt.applyEffects(Game.player);
		
		buildOptions(evt);
	}
	
	private static function buildOptions(evt:Event):Void {
		var options = new Array<Dynamic>();
		
		for (elem in evt.content)
		{
			switch(elem.name)
			{
				case Event.NextLabel:
					var next:Event = EventStore.getEvent(elem.asString());
					UI.setMainButton(0, "Next", function() {runEvent(next); });
					return;
				case Event.ExitLabel:
					UI.setMainButton(0, "Next", function() {
						UI.hideStatArrows();
						CampMenu.showCampRoot();
					});
					return;
			}
		}
	}
	
}