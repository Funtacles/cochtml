package menu;
import data.nav.Camp;
import menu.InventoryMenu;
import system.PlayerAppearance;
import system.Parser;

/**
 * ...
 * @author Funtacles
 */
class CampMenu 
{
	public static function showCampRoot():Void {
		UI.hideSystemButtons();
		UI.setSystemButton(0, "Main Menu", MainMenu.start);
		UI.setSystemButton(1, "Data", null);
		UI.setSystemButton(2, "Stats", null);
		
		if(Game.player.levelUpAvailable())
			UI.setSystemButton(3, "Level Up", null);
			
		UI.setSystemButton(4, "Perks", function() {PlayerMenu.displayPerks(showCampRoot); });
		UI.setSystemButton(5, "Appearance", showAppearance);
		
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write(Camp.getCampDescription());
		
		UI.setMainButton(0, "Explore", showExploreMenu);
		UI.setMainButton(2, "Inventory", InventoryMenu.showPlayerInv);
		
		UI.setMainButton(4, "Camp Actions", showCampActions);
		
		UI.setMainButton(8, "Masturbate");
		UI.setMainButton(9, "Codex");
	}
	
	public static function showExploreMenu():Void {
		
	}
	
	public static function showCampActions():Void {
		
	}
	
	public static function showAppearance():Void {
		UI.clearMainText();
		UI.hideMainButtons();
		
		UI.write(Parser.parse(PlayerAppearance.getAppearance(Game.player), {player: Game.player}));
		
		UI.setMainButton(4, "Back", showCampRoot);
	}
}