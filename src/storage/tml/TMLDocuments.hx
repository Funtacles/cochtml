package storage.tml;
import haxe.io.Error;
import js.Browser;
import resources.Resources;
import storage.tml.TMLDocuments.TagObject;

/**
 * ...
 * @author Funtacles
 */
class TMLDocuments 
{
	public static inline var RawTextOpen:String = "[\"";
	public static inline var RawTextClose:String = "\"]";
	
	private static var documents:Map<String, TMLCollection>;
	
	/**
	 * Loads and parses the TML documents from Resources and stores the results in "documents".
	 */
	public static function loadTmlDocuments():Void {
		documents = new Map<String, TMLCollection>();
		
		var currentFile:String = "";
		try{
			for (key in Resources.tmlFiles.keys()) {
				currentFile = key;
				var list = parse(Resources.tmlFiles[key]);
				documents.set(key, TMLCollection.fromArray(list));
			}
		} catch (msg:String){
			throw 'In "$currentFile.tml" on $msg';
		}
	}
	
	/**
	 * Returns a list containing all tml document ids.
	 * @return
	 */
	public static function getDocuments():Array<String> {
		var keys = new Array<String>();
		for (key in documents.keys())
			keys.push(key);
		return keys;
	}
	
	/**
	 * Returns the first top level tag from the specified document with the specified name.
	 * Throws an exception if the document does not exist or the tag can't be found;
	 * @param	doc		The document to search through.
	 * @param	tag		The tag to search for.
	 * @return	The first tag with the matching name.
	 */
	public static function getFirstTag(doc:String, tag:String):TMLElement {
		var elements:Array<TMLElement> = getAllTags(doc, tag);
		if (elements == null || elements.length == 0)
			throw 'Document "$doc" contains no top level tags with the name "$tag".';
		
		return elements[0];
	}
	
	/**
	 * Returns all top level tags from the specified document with the specified name.
	 * Throws an exception if the document doesn't exist.
	 * @param	doc		The document to search through.
	 * @param	tag		The tag to search for.
	 * @return	A list containing all top level tags with a matching name.
	 */
	public static function getAllTags(doc:String, tag:String):Array<TMLElement> {
		var document:TMLCollection = TMLDocuments.documents[doc];
		if (document == null)
			throw 'Documents list contained no document with the name "$doc".';
			
		return document.getValue(tag);
	}
	
	/**
	 * Checks if the specified document contains the specified top level tag.
	 * @param	doc		The document to search through.
	 * @param	tag		The tag to search for.
	 * @return	True if the document contains the tag and false if not.
	 */
	public static function containsTag(doc:String, tag:String):Bool {
		var tags = getAllTags(doc, tag);
		return tags != null && tags.length > 0;
	}
	
	/**
	 * Parses a string extracted from a tml file and returns a list of all tags defined in the file.
	 * @param	tml		The string formated in TML.
	 * @return	A list containing all top level tags with their descendents.
	 */
	public static function parse(tml:String):Array<TMLElement> {
		_lineCount = 0;
		_cursorPosition = 0;
		_currentDocument = tml;
		
		var list = new Array<TMLElement>();
		try{
			var tree:Array<TagObject> = buildTagTree(_currentDocument);
			
			for (obj in tree)
				list.push(convertToTmlObject(obj));
			
		} catch (msg:String) {
			throw 'line $_lineCount - $msg';
		}
		
		return list;
	}
	
	private static var _lineCount:Int;
	private static var _cursorPosition:Int;
	private static var _currentDocument:String;
	private static function getNextLine():String {
		var startPosition:Int = _cursorPosition;
		
		var index:Int = _currentDocument.indexOf("\n", _cursorPosition);
		
		if (index == -1) {
			_cursorPosition = _currentDocument.length;
		} else {
			_cursorPosition = index + 1;
		}
		
		_lineCount++;
		
		return _currentDocument.substring(startPosition, _cursorPosition);
	}
	
	private static function isEndOfDocument():Bool{
		return _cursorPosition >= _currentDocument.length;
	}
	
	private static function buildTagTree(document:String):Array<TagObject> {
		var list = new Array<TagObject>();
		var lastNode:TagObject = null;
		var lastDepth:Int = 0;
		var currentDepth:Int = 0;
		var tagStartLine:Int = 0;
		
		while (!isEndOfDocument()) {
			var line = getNextLine();
			
			validateLine(line);
			
			var lineType = determineLineType(line);
			
			if (lineType == Empty)
				continue;
			
			currentDepth = getLineDepth(line);
			
			if (lastNode == null && currentDepth > 0)
				throw "Floating tag - The first tag in the structure must be at depth 0 (no indentation).";
			
			if (currentDepth > lastDepth + 1)
				throw "Floating tag - The tag depth is 2 or more deeper than its intended parent (tag indented too far).";
			
			var sep = line.indexOf(":");
			var name:String = StringTools.trim(sep != -1 ? line.substring(0, sep) : line);
			var content:String = sep != -1 ? StringTools.trim(line.substring(sep + 1)) : null;
			
			validateTagName(name);
			
			tagStartLine = _lineCount;
			
			if (lineType == RawTextStart){
				content = content.substring(2);	// Trim [" off front
				
				if (isRawTextEnd(content)) { // Special case for closer on same line as opener
					var closeIndex:Int = content.indexOf(RawTextClose);
					content = content.substring(0, closeIndex);
				} else {
					while (!isRawTextEnd(line) && !isEndOfDocument()){
						line = getNextLine();
						content += " " + line;
					}
					
					if (line.indexOf(RawTextClose) == -1)
						throw "End of file reached without multiline string being closed.";
					
					var closeIndex:Int = content.indexOf(RawTextClose);
					content = content.substring(0, closeIndex);
				}
			}
			
			var tag:TagObject = {name: name, content: content, lineNumber: tagStartLine, children: new Array<TagObject>() };
			
			if (currentDepth == 0)
				list.push(tag);
			else {
				var parent:TagObject = lastNode;
				for (i in 0...(lastDepth - currentDepth + 1)) {
					parent = parent.parent;
				}
				tag.parent = parent;
				parent.children.push(tag);
			}
			
			lastNode = tag;
			lastDepth = currentDepth;
		}
		
		return list;
	}
	
	private static function convertToTmlObject(tag:TagObject):TMLElement {
		var tml:TMLElement;
		
		if (tag.content != null && tag.content.length > 0) {
			tml = new TMLElement(tag.name, tag.content);
		} else {
			tml = new TMLElement(tag.name);
		}
		
		tml.lineNumber = tag.lineNumber;
		
		for (child in tag.children){
			tml.addChild(convertToTmlObject(child));
		}
		
		return tml;
	}
	
	private static function determineLineType(line:String):LineType {
		var delimIndex = line.indexOf(":");
		
		if (delimIndex != -1){
			var rhs:String = line.substring(delimIndex + 1);
			rhs = StringTools.trim(rhs);
			if (StringTools.startsWith(rhs ,RawTextOpen))
				return LineType.RawTextStart;
		} 
		
		if (StringTools.trim(line).length > 0){
			return LineType.Tag;
		}
		
		return LineType.Empty;
	}
	
	private static function isRawTextEnd(line:String):Bool{
		line = StringTools.trim(line);
		return line.indexOf(RawTextClose) != -1;
	}
	
	private static function validateTagName(name:String):Void {
		name = StringTools.trim(name);
		if (name.indexOf(" ") != -1 || name.indexOf("\t") != -1)
			throw "Tag names cannot contain whitespace.";
	}
	
	private static function validateLine(line:String):Void {
		for (i in 0...line.length){
			
		}
	}
	
	private static function getLineDepth(line:String ):Int {
		var depth:Int = 0;
		for (i in 0...line.length){
			
			depth = i;
			var char = line.charAt(i);
			if (char != "\t")
				break;
		}
		
		return depth;
	}
}

enum LineType {
	Empty;
	Tag;
	StringTag;
	NumberTag;
	RawTextStart;
}

typedef TagObject = {
	var name:String;
	var content:String;
	var children:Array<TagObject>;
	var lineNumber:Int;
	@:optional var parent:TagObject;
}
