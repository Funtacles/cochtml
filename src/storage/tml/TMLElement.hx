package storage.tml;
import data.DiceRoll;
import enums.TMLType;

/**
 * ...
 * @author Funtacles
 */
class TMLElement 
{
	private var _childNodes:Array<TMLElement>;
	public var content:String;
	
	public var name:String;
	public var lineNumber:Int;
	
	public function new(name:String, content:String = ""){
		this.name = name;
		
		this.content = Util.isNullOrUndefined(content) ? "" : content;
		
		_childNodes = new Array<TMLElement>();
	}
	
	public function addChild(child:TMLElement):Void {
		_childNodes.push(child);
	}
	
	public function removeChild(child:TMLElement):Void {
		_childNodes.remove(child);
	}
	
	public function getChildrenWithName(name:String):Array<TMLElement> {
		var list = new Array<TMLElement>();
		for (child in _childNodes){
			if (child.name == name)
				list.push(child);
		}
		return list;
	}
	
	public function getFirstChild(name:String):TMLElement {
		for (child in _childNodes){
			if (child.name == name)
				return child;
		}
		
		return null;
	}
	
	public function hasChild(name:String):Bool {
		return getFirstChild(name) != null;
	}
	
	public function asString():String {
		if (content.length == 0)
			return "";
		
		return content;
	}
	
	public function asInt():Null<Int>{
		if (!StringUtil.isInt(content))
			return null;
		
		return Std.parseInt(content);
	}
	
	public function asFloat():Null<Float> {
		if (!StringUtil.isFloat(content))
			return null;
		
		return Std.parseFloat(content);
	}
	
	public function asDiceRoll():DiceRoll {
		return DiceRoll.parse(content);
	}
	
	public function getType():TMLType {
		if (content == null || content.length == 0)
			return None;
		
		if (StringUtil.isDiceRoll(content))
			return Dice;
		
		if (StringUtil.isInt(content))
			return Integer;
		
		if (StringUtil.isFloat(content))
			return Number;
		
		return Text;
	}
	
	/**
	 * Produces a copy of the element with only the name and content.
	 * @return
	 */
	public function simpleCopy():TMLElement {
		return new TMLElement(this.name, this.content);
	}
	
	/**
	 * Produces a full copy of the element, including copies of all descendents.
	 * @return
	 */
	public function fullCopy():TMLElement {
		var copy = this.simpleCopy();
		
		for (child in _childNodes)
		{
			copy.addChild(child.fullCopy());
		}
		
		return copy;
	}
	
	public function getChildren():Array<TMLElement> {
		return _childNodes;
	}
}