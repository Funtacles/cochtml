package storage.tml;

/**
 * ...
 * @author Funtacles
 */
class TMLCollection 
{
	private var map:Map<String, Array<TMLElement>>;
	
	public function new() 
	{
		map = new Map<String, Array<TMLElement>>();
	}
	
	public function getValue(key:String):Array<TMLElement> {
		return map.get(key);
	}
	
	public function setValue(key:String, v:TMLElement):TMLElement {
		if (!map.exists(key))
			map.set(key, new Array<TMLElement>());
		
		var array = map.get(key);
		if(array.indexOf(v) == -1)
			array.push(v);
		
		return v;
	}
	
	public static function fromArray(list:Array<TMLElement>):TMLCollection{
		var collection:TMLCollection = new TMLCollection();
		for (element in list){
			collection.setValue(element.name, element);
		}
		return collection;
	}
}