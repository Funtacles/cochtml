package storage;

import js.Lib;

/**
 * ...
 * @author Functacles
 */
@:native("localforage")
extern class LocalForage 
{
	public static var LOCALSTORAGE:String;
	public static var INDEXEDDB:String;
	public static var WEBSQL:String;
	
	public static function config(options:Any):Bool;
	
	public static function supports(driverName: String): Bool;
	
	public static function getItem<T>(key: String, callback: Any -> T -> Void): Void;
	
	public static function setItem<T>(key: String, value: T, callback:Any -> T -> Void): Void;
	
	public static function length(callback:Any -> Int -> Void): Void;
	
}