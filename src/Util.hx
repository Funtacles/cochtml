package;
import js.Browser;
import ui.SelectListOption;

/**
 * ...
 * @author Funtacles
 */
class Util 
{
    public static function numberWithCommas(x: Float): String {
        var parts:Array<String> = Std.string(x).split(".");
		var r = ~/\B(?=(\d{3})+(?!\d))/g;
        parts[0] = r.replace(parts[0], ",");
        return parts.join(".");
    }

    public static function pad(num: Int):String {
        return num < 10 ? '0${num}' : '${num}';
    }

    /**
     * Creates and returns a function that opens the passed in url in a new window or tab. Used for buttons.
     * @param url   The url to go to in the new window/tab;
     */
    public static function generateLinkFunction(url: String): Void -> Void {
        return function () { Browser.window.open(url, "_blank"); };
    }
	
	public static function generalSelectListHtml(id:String, list:Array<SelectListOption>, placeholder:String = null):String {
		var select:String = '<select id="$id">\n';
		
		if (placeholder != null)
			select += '<option disabled selected value>$placeholder</option>';
		
		for (item in list)
		{
			select += '\t<option value="${item.value}">${item.text}</option>\n';
		}
		
		select += "</select>";
		return select;
	}
	
	
	public static function isNullOrUndefined(obj:Any):Bool {
		var undefined:Bool = untyped __js__('"undefined" === typeof obj');
		return undefined || obj == null;
	}
}
