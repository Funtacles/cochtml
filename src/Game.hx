package;
import data.GameTime;
import enums.*;
import haxe.Resource;
import menu.*;
import js.Browser;
import storage.tml.TMLDocuments;
import data.Player;
import data.body.BodyTemplateStore;
import data.game.flag.Flags;
import data.game.global.Achievement;
import data.event.EventStore;
import data.nav.Camp;

/**
 * The central class that defines the function of the game.
 * @author Funtacles
 */
class Game 
{
	public static inline var HtmlVersion:String = "1.0.0";
	public static inline var BaseVersion:String = "1.0.2";
	public static inline var ModVersion:String = "1.4.8";
	public static inline var HggVersion:String = "1.1.10";
	public static var version: String = 'Version $HtmlVersion - Base $BaseVersion, CoC Mod $ModVersion, /hgg/ Mod $HggVersion';

    public static var gameState:GameState;
	
	public static var player(default, null):Player;
	public static var gameTime(default, null):GameTime;
	
	public static inline var StartEventId:String = "Arrival";

    /**
     * Performs game setup and sets the game state to MainMenu.
     */
    public static function start(): Void {
        //SaveSystem.init();
		
		try {
			TMLDocuments.loadTmlDocuments();
			BodyTemplateStore.init();
			Flags.init();
			EventStore.init();
			Achievement.init();
			Camp.init();
			
			gameState = GameState.StartMenu;
			
			MainMenu.start();
		} catch (e:String) {
			UI.clearMainText();
			UI.write("<b>Error encontered during setup:</b><br>" + e);
		}
    }
	
	static var buttonOn:Bool;
    public static function startNewGame(): Void {
		CharCreation.start();
    }

	public static function beginGameWithNewCharacter(char:Player):Void {
		player = char;
		
		gameTime = new GameTime();
		gameTime.addUpdateListener(updateTime);
		
		UI.initSidebar(player);
		UI.sidebarVisible(true);
		
		EventMenu.startEvent(StartEventId);
	}
	
	
	public static function setPlayer(char:Player):Void {
		player = char;
	}
	
	private static function updateTime(gt:GameTime):Void {
		UI.setTime(gt.hours, gt.minutes);
		UI.setDay(gt.day);
	}
	
	public static function changeStat(stat:Stat, amount:Int):Void {
		var newValue = player.changeStat(stat, amount);
		
		UI.changeStat(stat, newValue);
	}
	
	public static function setStat(stat:Stat, value:Int):Void {
		var diff = value - player.stats[stat];
		changeStat(stat, diff);
	}
	
	//public function gameOver(clear:Boolean = false):void { //Leaves text on screen unless clear is set to true
		//var textChoices:Number = rand(5);
		//if (silly && rand(5) == 0 && flags[kFLAGS.HARDCORE_MODE] == 0) textChoices = 5 + rand(4); //20% chance of humourous bad end texts.
		//if (clear) clearOutput();
		//outputText("\n\n<font color=\"#800000\">");
		////Standard
		//if (textChoices == 0) outputText("<b>GAME OVER</b>");
		//if (textChoices == 1) outputText("<b>Game over, man! Game over!</b>");
		//if (textChoices == 2) outputText("<b>You just got Bad-Ended!</b>");
		//if (textChoices == 3) outputText("<b>Your adventures have come to an end...</b>");
		//if (textChoices == 4) outputText("<b>Oh dear, you are bad-ended!</b>");	//Runescape
		////Silly Mode
		//if (textChoices == 5) outputText("<b>Don't lose hope... " + player.short + "! Stay determined!</b>"); //Undertale
		//if (textChoices == 6) outputText("<b>Wasted</b>"); //Grand Theft Auto V
		//if (textChoices == 7) outputText("<b>Ya dun goofed</b>"); //One of the memes
		//if (textChoices == 8) outputText("<b>Git gud</b>");	//One of the memes
		//outputText("</font>");
		////Delete save on hardcore.
		//if (flags[kFLAGS.HARDCORE_MODE] > 0) {
			//outputText("\n\n<b>Error deleting save file.</b>");
			///*outputText("\n\n<b>Your save file has been deleted, as you are on Hardcore Mode!</b>");
			//flags[kFLAGS.TEMP_STORAGE_SAVE_DELETION] = flags[kFLAGS.HARDCORE_SLOT];
			//var test:* = SharedObject.getLocal(flags[kFLAGS.TEMP_STORAGE_SAVE_DELETION], "/");
			//if (test.data.exists)
			//{
				//trace("DELETING SLOT: " + flags[kFLAGS.TEMP_STORAGE_SAVE_DELETION]);
				//test.clear();
			//}*/
		//}
		//flags[kFLAGS.TIMES_BAD_ENDED]++;
		//awardAchievement("Game Over!", kACHIEVEMENTS.GENERAL_GAME_OVER, true, true);
		//menu();
		//addButton(0, "Game Over", gameOverMenuOverride, null, null, null, "Your game has ended. Please load a saved file or start a new game.");
		//if (flags[kFLAGS.HARDCORE_MODE] <= 0) addButton(1, "Nightmare", camp.wakeFromBadEnd, null, null, null, "It's all just a dream. Wake up.");
		////addButton(3, "NewGamePlus", charCreation.newGamePlus, null, null, null, "Start a new game with your equipment, experience, and gems carried over.");
		////if (flags[kFLAGS.EASY_MODE_ENABLE_FLAG] == 1 || debug) addButton(4, "Debug Cheat", playerMenu);
		//gameOverMenuOverride();
		//inCombat = false;
		//kGAMECLASS.monsterArray.length = 0;
		//dungeonLoc = 0; //Replaces inDungeon = false;
	//}
}